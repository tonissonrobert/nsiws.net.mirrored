﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataSoapFormat.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Model
{
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The sample data soap format.
    /// IMPORTANT This is needed only when overriding existing SDMX XML formats to use under SOAP endpoints. If this is not the case remove it.
    /// </summary>
    public class SampleDataSoapFormat : AbstractSoapFormat, IDataFormat
    {
        /// <summary>
        /// The SDMX data format
        /// </summary>
        private readonly DataType sdmxDataFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleDataSoapFormat"/> class.
        /// </summary>
        /// <param name="sdmxDataFormat">The SDMX data format.OPTIONAL. The target format may not be present in the enum.</param>
        /// <param name="writer">The XML writer. This is needed for XML formats under SOAP..</param>
        public SampleDataSoapFormat(DataType sdmxDataFormat, XmlWriter writer) : base(writer)
        {
            this.sdmxDataFormat = sdmxDataFormat;
        }

        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        /// <p />
        /// This is expected to return a not null response.
        /// </summary>
        public string FormatAsString
        {
            get
            {
                return this.ToString();
            }
        }

        /// <summary>
        /// Gets the SDMX data format that this interface is describing.
        /// If this is not describing an SDMX message then this will return null and the implementation class will be expected
        /// to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat
        {
            get
            {
                return this.sdmxDataFormat;
            }
        }
    }
}