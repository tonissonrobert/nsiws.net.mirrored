﻿// -----------------------------------------------------------------------
// <copyright file="StructureWritingContract.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------


namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Factory
{

    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Samples.Ws.SampleFormatPlugin.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;

    /// <summary>
    /// The Sample structure writing contract.
    /// </summary>
    public class StructureWritingContract : AbstractStructureWriterPluginContract, IStructureWriterPluginContract
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StructureWritingContract"/> class.
        /// </summary>
        public StructureWritingContract()
            : base(
                    new[]
                    {
                        "application/vnd+myformat",
                        /* add your supported media types e.g. "mytype/subtype+a;version=2.3", "othertype/someothersubtype" */
                    })
        {
        }

        /// <summary>
        /// Gets the structure format.
        /// </summary>
        /// <param name="soapRequest">Type of the structure.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Format.IStructureFormat" />.
        /// </returns>
        public override IStructureFormat GetStructureFormat(ISoapRequest<StructureOutputFormat> soapRequest, XmlWriter xmlWriter)
        {
            ///// Used in SOAP structure requests. If you don't need SOAP support, just return null ////
            ///// Used in SOAP data requests. If you don't need SOAP support, just return null ////
            /*
            if (soapRequest == null)
            {
                throw new ArgumentNullException(nameof(soapRequest));
            }

            if (xmlWriter == null)
            {
                throw new ArgumentNullException(nameof(xmlWriter));
            }

            ///// Used in SOAP data requests. If you don't need SOAP support, just return null ////

            // TODO CHANGE THIS sample list of supported formats
            var supportedFormatDataTypes = new[] { StructureOutputFormatEnumType.SdmxV1StructureDocument };

            // get the web operation if needed
            var operation = soapRequest.Operation;

            // the format type, e.g. CompactData v2.0, GenericData v2.0, GenericData v2.1, StructureSpecificData (CompactData) v2.1
            var formatType = soapRequest.FormatType;

            // the SDMX Extensions, mostly needed for SDMX v2.0 whether to used the fixed XSD schema
            var sdmxExtension = soapRequest.Extensions;

            if (!formatType.OutputVersion.IsXmlFormat())
            {
                return null;
            }

            // Check if we support this requested format
            if (!supportedFormatDataTypes.Contains(formatType.EnumType))
            {
                return null;
            }

            return new SampleStructureSoapFormat(xmlWriter, formatType);
            */
            return null;
        }

        /// <summary>
        /// Builds the format. This is used only for REST. This needs to be overridden. The <paramref name="mediaType" /> and the <paramref name="encoding" /> are the most important.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="header">The header.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="sortedAcceptHeaders">The sorted accept headers.</param>
        /// <param name="query"></param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Format.IStructureFormat" />.
        /// </returns>
        protected override IStructureFormat BuildFormat(
            string mediaType,
            WebHeaderCollection header,
            Encoding encoding,
            ISortedAcceptHeaders sortedAcceptHeaders, IRestStructureQuery query)
        {

            // get requested languages if needed sorted by quality
            var languages = sortedAcceptHeaders.Languages;
            //// -or-
            // override and use the this.GetLanguage method
            var language = this.GetLanguage(header, sortedAcceptHeaders);

            // In case we need to, we can check the requested media types sorted by quality 
            var acceptHeaderValue = sortedAcceptHeaders.ContentTypes;

            // build the data format. This could be different for each supported media type
            var format = new SampleStructureRestFormat(encoding);

            return format;
        }
    }
}