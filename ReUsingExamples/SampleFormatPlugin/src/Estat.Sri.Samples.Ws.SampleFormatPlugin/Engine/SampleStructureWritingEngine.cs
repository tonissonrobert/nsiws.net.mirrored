﻿// -----------------------------------------------------------------------
// <copyright file="SampleStructureWritingEngine.cs" company="EUROSTAT">
//   Date Created : 2016-08-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Samples.Ws.SampleFormatPlugin.Engine
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;

    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// The sample structure writing engine.
    /// </summary>
    public class SampleStructureWritingEngine : IStructureWriterEngine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SampleStructureWritingEngine"/> class.
        /// </summary>
        /// <param name="outputStream">The output stream.</param>
        /// <param name="encoding">The encoding.</param>
        /// <remarks>This is used for REST.</remarks>
        public SampleStructureWritingEngine(Stream outputStream, Encoding encoding)
        {
            if (outputStream == null)
            {
                throw new ArgumentNullException(nameof(outputStream));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleStructureWritingEngine"/> class.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <remarks>This is used for SOAP. SOAP is limited to XML</remarks>
        public SampleStructureWritingEngine(XmlWriter writer)
        {
            if (writer == null)
            {
                throw new ArgumentNullException(nameof(writer));
            }

            //// TODO This is needed only when overriding existing SDMX XML formats to use under SOAP endpoints. If this is not the case remove it.
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Writes the @maintainableObject out to the output location in the format specified by the implementation
        /// </summary>
        /// <param name="maintainableObject">The maintainableObject.</param>
        public void WriteStructure(IMaintainableObject maintainableObject)
        {
            if (maintainableObject == null)
            {
                throw new ArgumentNullException(nameof(maintainableObject));
            }

            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            sdmxObjects.AddIdentifiable(maintainableObject);
            this.WriteStructures(sdmxObjects);
        }

        /// <summary>
        /// Writes the sdmxObjects to the output location in the format specified by the implementation
        /// </summary>
        /// <param name="sdmxObjects">SDMX objects</param>
        public void WriteStructures(ISdmxObjects sdmxObjects)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException(nameof(sdmxObjects));
            }

            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
            if (managed)
            {
                // TODO dispose anything that needs to. 
            }
        }
    }
}