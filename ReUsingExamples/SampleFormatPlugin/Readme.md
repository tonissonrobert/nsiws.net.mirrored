﻿# Sample format plug-in for NSI Web Service

This a template project for building data and structure formats for SDMX RI NSI Web Service.

## How to use
1. Clone it from : 
2. Open SampleFormatPlugin.sln with Visual Studio
3. In case the format will used only under REST then feel free to remove [SOAP classes and methods](#disable_soap). 
4. In case there will be a data format only then feel free to remove [Structure classes and methods](#disable_str). 
5. In case there will be a structure format then feel free to remove [data classes and methods](#disable_data). 
6. Change namespace in the classes to avoid collisions with other plugins. [Instructions how to quickly change the namespace without any Visual Studio extension](http://stackoverflow.com/questions/2871314/change-project-namespace-in-visual-studio)
7. Add the supported format types to [Structure plugin][StructureWritingContract] and [Data plug-in][SampleDataWritingContract] at constructor
```c#
 : base(new []{ /* add your supported media types e.g. "mytype/subtype+a;version=2.3", "othertype/someothersubtype" */ })
```
8. Add format logic to the [Data Writer][SampleDataWriterEngine] and the [Structure Writer][SampleStructureWritingEngine]
Examples : [Structure Writer example][structureWriterSample]

9. Build and copy to the bin\\Plugins folder of NSI WS. Any dependencies should be copied in the NSI WS bin folder.
10. Use a REST and/or SOAP client to test

## <a id="disable_soap"/> Remove SOAP support
a. Modify the SOAP method to ``return null`` _or_ remove it in [Structure plugin][StructureWritingContract] and [Data plug-in][SampleDataWritingContract]
Example for [Structure plugin][StructureWritingContract] 
```C#
public IStructureFormat GetStructureFormat(ISoapRequest<StructureOutputFormat> structureType, XmlWriter xmlWriter)
{
	return null;
}
```

Example for [Data plug-in][SampleDataWritingContract]
```C#
public IDataFormat GetDataFormat(ISoapRequest<DataType> dataType, XmlWriter xmlWriter
{
	return null;
}
```
b. Remove the SOAP classes
[DataWriterSoapFactory][]
[SampleStructureWriterSoapFactory][]
[SampleDataSoapFormat][]
[SampleStructureSoapFormat][]

c. Remove the SOAP constructors from the writer engines [Data Writer Engine][SampleDataWriterEngine] and [Structure Writer Engine][SampleStructureWritingEngine]. The SOAP constructors are the ones accepting a XmlWriter as a parameter.

## <a id="disable_str"/> Remove Structure format support
Remove the following files
[StructureWritingContract][]
[SampleStructureWritingEngine][]
[SampleStructureSoapFormat][]
[SampleStructureRestFormat][]
[SampleStructureWriterSoapFactory][]
[SampleStructureWriterRestFactory][]

## <a id="disable_data"/> Remove Data format support

Remove the following files
[SampleDataWritingContract][]
[SampleDataWriterEngine][]
[DataWriterSoapFactory][]
[DataWriterRestFactory][]
[SampleDataSoapFormat][]
[SampleDataRestFormat][]


[StructureWritingContract]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Factory/StructureWritingContract.cs
[SampleDataWritingContract]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Factory/SampleDataWritingContract.cs
[SampleDataWriterEngine]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Engine/SampleDataWriterEngine.cs
[SampleStructureWritingEngine]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Engine/SampleStructureWritingEngine.cs
[structureWriterSample]: https://webgate.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/sdmxsource.net/browse/GuideTests/Chapter8/HtmlStructureWriterEngine.cs
[DataWriterSoapFactory]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Factory/DataWriterSoapFactory.cs
[DataWriterRestFactory]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Factory/DataWriterRestFactory.cs
[SampleStructureWriterSoapFactory]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Factory/SampleStructureWriterSoapFactory.cs
[SampleStructureWriterRestFactory]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Factory/SampleStructureWriterRestFactory.cs
[SampleDataSoapFormat]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Model/SampleDataSoapFormat.cs
[SampleDataRestFormat]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Model/SampleDataRestFormat.cs
[SampleStructureSoapFormat]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Model/SampleStructureSoapFormat.cs
[SampleStructureRestFormat]: src/Estat.Sri.Samples.Ws.SampleFormatPlugin/Model/SampleStructureRestFormat.cs
