﻿// -----------------------------------------------------------------------
// <copyright file="SampleAdvancedSdmxDataRetriever.cs" company="EUROSTAT">
//   Date Created : 2016-09-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace SampleRetrieverPlugin.Manager
{
    using System;
    using System.Linq;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;
    using Org.Sdmxsource.Util;

    using SampleRetrieverPlugin.Constant;

    /// <summary>
    /// A Sample data retriever. 
    /// </summary>
    /// <remarks>Note this sample doesn't support AllDimensions (Flat) and actual processing the data query criteria</remarks>
    /// <seealso cref="Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data.ISdmxDataRetrievalWithWriter" />
    public class SampleAdvancedSdmxDataRetriever : IAdvancedSdmxDataRetrievalWithWriter
    {
        /// <summary>
        /// The header retrieval manager
        /// </summary>
        private readonly IHeaderRetrievalManager _headerRetrievalManager = new SampleDataHeaderRetrievalManager();

        /// <summary>
        /// Queries for data conforming to the parameters defined by the DataQuery,
        /// the response is written to the DataWriterEngine
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        public void GetData(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException(nameof(dataQuery));
            }

            if (dataWriter == null)
            {
                throw new ArgumentNullException(nameof(dataWriter));
            }

            // the most important part, the dataflow
            var dataFlow = dataQuery.Dataflow;
            
            // write the data header
            var messageHeader = this._headerRetrievalManager.Header;
            dataWriter.WriteHeader(messageHeader);

            // build the dataset header we need this for SDMX v2.1 dimension at observation
            IDatasetStructureReference dsr = new DatasetStructureReferenceCore(string.Empty, dataQuery.DataStructure.AsReference, null, null, dataQuery.DimensionAtObservation);
            IDatasetHeader datasetHeader = new DatasetHeaderCore(messageHeader.DatasetId, messageHeader.Action, dsr);

            dataWriter.StartDataset(null, dataQuery.DataStructure, datasetHeader);

            // TODO replace the following with actual code for retrieving and writing data.
            // It is just an example to show how to use IDataWriteEngine and IDataQuery
            WriteData(dataQuery, dataWriter);
        }

        /// <summary>
        /// Writes the data.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        private static void WriteData(IComplexDataQuery dataQuery, IDataWriterEngine dataWriter)
        {
            // get all dimensions except observation dimensions
            var seriesDimensions =
                dataQuery.DataStructure.GetDimensions()
                    .Where(dimension => dimension.Id != dataQuery.DimensionAtObservation)
                    .ToArray();

            // The following just shows how to use IDataWriterEngine. 
            // It make a lot of assumptions in order to remain simple
            string lastSeriesKey = string.Empty;
            for (int index0 = 0; index0 < SampleData.Dataset.GetLength(0); index0++)
            {
                // We check against criteria although in production this could be done more efficiently in a SQL Query
                if (!MatchQuery(dataQuery, index0))
                {
                    continue;
                }

                // retrieve and store the current series key. This is needed in order to avoid writing one series per observation
                string[] currentSeriesKey = new string[seriesDimensions.Length];
                for (var i = 0; i < seriesDimensions.Length; i++)
                {
                    var seriesDimension = seriesDimensions[i];
                    currentSeriesKey[i] = SampleData.GetValue(index0, seriesDimension.Id);
                }

                var key = string.Join(",", currentSeriesKey);
                if (!lastSeriesKey.Equals(key))
                {
                    // if we have a new series then
                    lastSeriesKey = key;

                    // start series
                    dataWriter.StartSeries();

                    // write the series dimensions
                    foreach (var seriesDimension in seriesDimensions)
                    {
                        dataWriter.WriteSeriesKeyValue(seriesDimension.Id, SampleData.GetValue(index0, seriesDimension.Id));
                    }

                    // write the series attributes (none in this example but added for completeness 
                    foreach (var attribute in dataQuery.DataStructure.GetSeriesAttributes(dataQuery.DimensionAtObservation))
                    {
                        var value = SampleData.GetValue(index0, attribute.Id);
                        dataWriter.WriteAttributeValue(attribute.Id, value);
                    }
                }

                // write the observation value and the dimension at observation
                var dimensionAtObservationValue = SampleData.GetValue(index0, dataQuery.DimensionAtObservation);
                var obsValue = SampleData.GetValue(index0, dataQuery.DataStructure.PrimaryMeasure.Id);
                dataWriter.WriteObservation(dataQuery.DimensionAtObservation, dimensionAtObservationValue, obsValue);

                // write the observation attribute 
                foreach (
                    var observationAttribute in
                        dataQuery.DataStructure.GetObservationAttributes(dataQuery.DimensionAtObservation))
                {
                    var value = SampleData.GetValue(index0, observationAttribute.Id);
                    dataWriter.WriteAttributeValue(observationAttribute.Id, value);
                }
            }
        }

        /// <summary>
        /// Matches the query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="rowNumber">The row number.</param>
        /// <returns>
        /// True if this row is queried false otherwise
        /// </returns>
        private static bool MatchQuery(IComplexDataQuery dataQuery, int rowNumber)
        {
            // We could check against criteria although this could be done more efficiently in a SQL Query
            var dataQuerySelectionGroup = dataQuery.SelectionGroups.FirstOrDefault();
            if (dataQuerySelectionGroup == null)
            {
                return true;
            }

            foreach (var dimension in dataQuery.DataStructure.GetDimensions(SdmxStructureEnumType.Dimension, SdmxStructureEnumType.MeasureDimension))
            {
                var criteria = dataQuerySelectionGroup.GetSelectionsForConcept(dimension.Id);

                if (criteria == null || !ObjectUtil.ValidCollection(criteria.Values))
                {
                    continue;
                }

                var dimensionValue = SampleData.GetValue(rowNumber, dimension.Id);

                if (!criteria.Values.Any(value => value.OrderedOperator.EnumType == OrderedOperatorEnumType.Equal && value.Value.Equals(dimensionValue)) ||
                    criteria.Values.Any(value => value.OrderedOperator.EnumType == OrderedOperatorEnumType.NotEqual && value.Value.Equals(dimensionValue)))
                {
                     return false;
                }
            }

            return true;
        }
    }
}