﻿// -----------------------------------------------------------------------
// <copyright file="SampleMutableStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2016-09-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace SampleRetrieverPlugin.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// The sample  mutable structure search manager.
    /// </summary>
    public class SampleMutableStructureSearchManager : IMutableStructureSearchManager
    {
        /// <summary>
        /// The cross reference mutable retrieval manager
        /// </summary>
        private readonly ICrossReferenceMutableRetrievalManager _crossReferenceMutableRetrievalManager;

        /// <summary>
        /// The _mutable object retrieval manager.
        /// </summary>
        private readonly ISdmxMutableObjectRetrievalManager _mutableObjectRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleMutableStructureSearchManager"/> class.
        /// </summary>
        /// <param name="mutableObjectRetrievalManager">
        /// The mutable object retrieval manager.
        /// </param>
        /// <param name="crossReferenceMutableRetrievalManager">
        /// The cross reference mutable retrieval manager.
        /// </param>
        public SampleMutableStructureSearchManager(
            ISdmxMutableObjectRetrievalManager mutableObjectRetrievalManager,
            ICrossReferenceMutableRetrievalManager crossReferenceMutableRetrievalManager)
        {
            this._mutableObjectRetrievalManager = mutableObjectRetrievalManager;
            this._crossReferenceMutableRetrievalManager = crossReferenceMutableRetrievalManager;
        }

        /// <summary>
        /// Returns the latest version of the maintainable for the given maintainable input
        /// </summary>
        /// <param name="maintainableObject">
        /// The maintainable Object.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base.IMaintainableMutableObject"/>.
        /// </returns>
        public IMaintainableMutableObject GetLatest(IMaintainableMutableObject maintainableObject)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns a set of maintainable that match the given query parameters
        /// </summary>
        /// <param name="structureQuery">
        /// The structure Query.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Mutable.IMutableObjects"/>.
        /// </returns>
        public IMutableObjects GetMaintainables(IRestStructureQuery structureQuery)
        {
            var structureQueryDetail = structureQuery.StructureQueryMetadata.StructureQueryDetail;
            var requestedStructuresStub = structureQueryDetail.EnumType == StructureQueryDetailEnumType.AllStubs;
            var referencedStructuresStub = requestedStructuresStub
                                           || structureQueryDetail.EnumType
                                           == StructureQueryDetailEnumType.ReferencedStubs;

            var referenceDetails = structureQuery.StructureQueryMetadata.StructureReferenceDetail;
            var returnLatest = structureQuery.StructureQueryMetadata.IsReturnLatest;
            var structureReference = structureQuery.StructureReference;

            var requestedObjects = this._mutableObjectRetrievalManager.GetMutableMaintainables(
                structureReference,
                returnLatest,
                requestedStructuresStub);

            var objects = new MutableObjectsImpl(requestedObjects);
            switch (referenceDetails.EnumType)
            {
                case StructureReferenceDetailEnumType.Null:
                case StructureReferenceDetailEnumType.None:
                    // Don't add anything else
                    break;
                case StructureReferenceDetailEnumType.Parents:
                    {
                        var parents = this.RetrieveParents(requestedObjects, referencedStructuresStub);
                        objects.AddIdentifiables(parents);
                    }

                    break;
                case StructureReferenceDetailEnumType.ParentsSiblings:
                    {
                        // first get the parents
                        var parents = this.RetrieveParents(requestedObjects, referencedStructuresStub);

                        objects.AddIdentifiables(parents);

                        // and for each parent get the children
                        var siblings = this.RetrieveChildren(parents, referencedStructuresStub);
                        objects.AddIdentifiables(siblings);
                    }

                    break;
                case StructureReferenceDetailEnumType.Children:
                    {
                        var children = this.RetrieveChildren(requestedObjects, referencedStructuresStub);
                        objects.AddIdentifiables(children);
                    }

                    break;
                case StructureReferenceDetailEnumType.Descendants:
                    {
                        // first get the children
                        var children = this.RetrieveChildren(requestedObjects, referencedStructuresStub);
                        objects.AddIdentifiables(children);

                        // now get the children's children until we have no more children
                        IList<IMaintainableMutableObject> currentChildren = children;
                        while (currentChildren.Count > 0)
                        {
                            currentChildren = this.RetrieveChildren(currentChildren, referencedStructuresStub);
                            objects.AddIdentifiables(currentChildren);
                        }
                    }

                    break;
                case StructureReferenceDetailEnumType.All:
                    {
                        // first get the parents
                        var parents = this.RetrieveParents(requestedObjects, referencedStructuresStub);
                        objects.AddIdentifiables(parents);

                        // and for each parent get the children
                        var siblings = this.RetrieveChildren(parents, referencedStructuresStub);
                        objects.AddIdentifiables(siblings);

                        // then get the children
                        var children = this.RetrieveChildren(requestedObjects, referencedStructuresStub);
                        objects.AddIdentifiables(children);

                        // now get the children's children until we have no more children
                        IList<IMaintainableMutableObject> currentChildren = children;
                        while (currentChildren.Count > 0)
                        {
                            currentChildren = this.RetrieveChildren(currentChildren, referencedStructuresStub);
                            objects.AddIdentifiables(currentChildren);
                        }
                    }

                    break;
                case StructureReferenceDetailEnumType.Specific:
                    {
                        var foundSpecific = this.GetSpecificStructures(
                            structureQuery,
                            requestedObjects,
                            referencedStructuresStub);
                        objects.AddIdentifiables(foundSpecific);
                    }

                    break;
            }

            return objects;
        }

        /// <summary>
        /// Retrieves all structures that match the given query parameters in the list of query objects.  The list
        /// must contain at least one StructureQueryObject.
        /// </summary>
        /// <param name="queries">
        /// The queries.
        /// </param>
        /// <param name="resolveReferences">
        /// - if set to true then any cross referenced structures will also be available in the SdmxObjects container
        /// </param>
        /// <param name="returnStub">
        /// - if set to true then only stubs of the returned objects will be returned.
        /// </param>
        /// <returns>
        /// The <see cref="IMutableObjects"/>.
        /// </returns>
        public IMutableObjects RetrieveStructures(
            IList<IStructureReference> queries,
            bool resolveReferences,
            bool returnStub)
        {
            IMutableObjects objects = new MutableObjectsImpl();
            foreach (var structureReference in queries)
            {
                var structureQueryDetail =
                    StructureQueryDetail.GetFromEnum(
                        returnStub ? StructureQueryDetailEnumType.AllStubs : StructureQueryDetailEnumType.Full);

                var structureReferenceDetail =
                    StructureReferenceDetail.GetFromEnum(
                        resolveReferences
                            ? StructureReferenceDetailEnumType.Children
                            : StructureReferenceDetailEnumType.None);

                var returnLatest = structureReference.MaintainableReference.HasVersion();

                var restQuery = new RESTStructureQueryCore(
                    structureQueryDetail,
                    structureReferenceDetail,
                    null,
                    structureReference,
                    returnLatest);
                objects.AddIdentifiables(this.GetMaintainables(restQuery).AllMaintainables);
            }

            return objects;
        }

        /// <summary>
        /// Gets the specific structures.
        /// </summary>
        /// <param name="structureQuery">
        /// The structure query.
        /// </param>
        /// <param name="requestedObjects">
        /// The requested objects.
        /// </param>
        /// <param name="referencedStructuresStub">
        /// if set to <c>true</c> [referenced structures stub].
        /// </param>
        /// <returns>
        /// The specific structures
        /// </returns>
        private List<IMaintainableMutableObject> GetSpecificStructures(
            IRestStructureQuery structureQuery,
            ISet<IMaintainableMutableObject> requestedObjects,
            bool referencedStructuresStub)
        {
            var specificReference = structureQuery.StructureQueryMetadata.SpecificStructureReference;
            var foundSpecific = new List<IMaintainableMutableObject>();
            foreach (var requestedObject in requestedObjects)
            {
                var specificFromChildren =
                    this._crossReferenceMutableRetrievalManager.GetCrossReferencedStructures(
                        requestedObject,
                        referencedStructuresStub,
                        specificReference);
                foundSpecific.AddRange(specificFromChildren);
                var specificFromParentsOrSiblings =
                    this._crossReferenceMutableRetrievalManager.GetCrossReferencingStructures(
                        requestedObject,
                        referencedStructuresStub,
                        specificReference);

                foundSpecific.AddRange(specificFromParentsOrSiblings);
            }

            return foundSpecific;
        }

        /// <summary>
        /// Retrieves the parents.
        /// </summary>
        /// <param name="requestedObjects">
        /// The requested objects.
        /// </param>
        /// <param name="referencedStructuresStub">
        /// if set to <c>true</c> [referenced structures stub].
        /// </param>
        /// <returns>
        /// The parents
        /// </returns>
        private List<IMaintainableMutableObject> RetrieveParents(
            IEnumerable<IMaintainableMutableObject> requestedObjects,
            bool referencedStructuresStub)
        {
            List<IMaintainableMutableObject> allParents = new List<IMaintainableMutableObject>();
            foreach (var requestedObject in requestedObjects)
            {
                var parents = this._crossReferenceMutableRetrievalManager.GetCrossReferencingStructures(
                    requestedObject,
                    referencedStructuresStub);
                allParents.AddRange(parents);
            }

            return allParents;
        }

        /// <summary>
        /// Retrieves the children.
        /// </summary>
        /// <param name="requestedObjects">
        /// The requested objects.
        /// </param>
        /// <param name="referencedStructuresStub">
        /// if set to <c>true</c> [referenced structures stub].
        /// </param>
        /// <returns>
        /// The children
        /// </returns>
        private List<IMaintainableMutableObject> RetrieveChildren(
            IEnumerable<IMaintainableMutableObject> requestedObjects,
            bool referencedStructuresStub)
        {
            List<IMaintainableMutableObject> allChildren = new List<IMaintainableMutableObject>();
            foreach (var requestedObject in requestedObjects)
            {
                var children = this._crossReferenceMutableRetrievalManager.GetCrossReferencedStructures(
                    requestedObject,
                    referencedStructuresStub);
                allChildren.AddRange(children);
            }

            return allChildren;
        }
    }
}