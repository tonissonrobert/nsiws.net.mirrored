﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataHeaderRetrievalManager.cs" company="EUROSTAT">
//   Date Created : 2016-09-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace SampleRetrieverPlugin.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Header;

    /// <summary>
    /// The sample data header retrieval manager.
    /// </summary>
    public class SampleDataHeaderRetrievalManager : IHeaderRetrievalManager
    {
        /// <summary>
        /// Gets a header object
        /// </summary>
        public IHeader Header
        {
            get
            {
                // The Header ID, this could be a unique identifier
                var headerId = Guid.NewGuid().ToString();

                // The prepared time. Change this if it is not now
                var prepared = DateTime.Now;

                // Reporting period. If this is not available, for example due to streaming then either another HeaderImpl constructor should be used or 
                // set the properties ReportingBegin/End to null after creating the HeaderImpl object.
                var reportingBegin = new DateTime(2013, 1, 1);
                var reportingEnd = new DateTime(2014, 1, 1);

                // The list of receivers. This can also be empty or null
                IList<IParty> receivers = new List<IParty>();

                // The Sender, we need to provide at least the ID of the sender. 
                var sender = new PartyCore(null, "TEST", null, null);

                // Whether this message is a test.
                var isTest = true;

                return new HeaderImpl(headerId, prepared, reportingBegin,  reportingEnd, receivers, sender, isTest);
            }
        }
    }
}