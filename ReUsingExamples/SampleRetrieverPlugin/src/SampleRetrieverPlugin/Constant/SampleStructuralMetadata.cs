﻿// -----------------------------------------------------------------------
// <copyright file="SampleStructuralMetadata.cs" company="EUROSTAT">
//   Date Created : 2016-09-23
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace SampleRetrieverPlugin.Constant
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Codelist;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.ConceptScheme;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    /// <summary>
    /// Sample artefacts arranged in a way to simulate tables in a database
    /// </summary>
    public static class SampleStructuralMetadata
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SampleStructuralMetadata));

        /// <summary>
        /// The artefacts
        /// </summary>
        private static readonly string[,] _artefacts =
            {
                { "Dsd", "TEST_DSD", "TEST_AGENCY", "1.0", "TEST NAME" },
                { "ConceptScheme", "TEST_CS", "TEST_AGENCY", "1.0", "TEST NAME" },
                { "CodeList", "CL_FREQ", "TEST_AGENCY", "1.0", "TEST NAME" },
                { "CodeList", "CL_REF_AREA", "TEST_AGENCY", "1.0", "TEST NAME" },
                { "CodeList", "CL_INDICATOR", "TEST_AGENCY", "1.0", "TEST NAME" },
                {
                    "CodeList", "CL_OBS_STATUS", "TEST_AGENCY", "1.0", "TEST NAME"
                },
            };

        /// <summary>
        /// The sample component and concept
        /// </summary>
        /// <remarks>For simplicity we assume that everything has Agency : TEST_AGENCY and Version : 1.0</remarks>
        private static readonly string[,] _componentConcept =
            {
                /* 0 DSD ID */ /* 1 Component Type*/
                /* 2 Component/Concept ID*/ /* 3  Codelist id */
                /* 4 Text Format */ /* 5Attachment level */
                /*6 Assignment status*/
                {
                    "TEST_DSD", "Dimension", "FREQ", "CL_FREQ",
                    string.Empty, string.Empty, string.Empty
                },
                {
                    "TEST_DSD", "Dimension", "REF_AREA", "CL_REF_AREA",
                    string.Empty, string.Empty, string.Empty
                },
                {
                    "TEST_DSD", "Dimension", "INDICATOR", "CL_INDICATOR",
                    string.Empty, string.Empty, string.Empty
                },
                {
                    "TEST_DSD", "TimeDimension", "TIME_PERIOD",
                    string.Empty,
                    TextEnumType.ObservationalTimePeriod.ToString(),
                    string.Empty, string.Empty
                },
                {
                    "TEST_DSD", "PrimaryMeasure", "OBS_VALUE",
                    string.Empty, "Decimal", string.Empty, string.Empty
                },
                {
                    "TEST_DSD", "DataAttribute", "OBS_STATUS",
                    "CL_OBS_STATUS", string.Empty, "Observation",
                    "Mandatory"
                }
            };

        /// <summary>
        /// The codes
        /// </summary>
        private static readonly string[,] _codes =
            {
                { "CL_FREQ", "A", string.Empty }, { "CL_FREQ", "M", string.Empty },
                { "CL_REF_AREA", "EU", string.Empty }, { "CL_REF_AREA", "LU", "EU" },
                { "CL_INDICATOR", "NS0010", string.Empty },
                { "CL_INDICATOR", "NS0020", string.Empty },
            };

        /// <summary>
        /// Gets the component count.
        /// </summary>
        /// <returns>The number of component in the database</returns>
        public static int GetComponentCount()
        {
            return _componentConcept.GetLength(0);
        }

        /// <summary>
        /// Gets the DSD identifier from component.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns>The DSD id</returns>
        public static string GetDsdIdFromComponent(int row)
        {
            return _componentConcept[row, 0];
        }

        /// <summary>
        /// Gets the codelist identifier from component.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns>The codelist id</returns>
        public static string GetCodelistIdFromComponent(int row)
        {
            return _componentConcept[row, 3];
        }

        /// <summary>
        /// Gets the maintainable artefacts
        /// </summary>
        /// <typeparam name="TMaintainable">
        /// The type of the maintainable.
        /// </typeparam>
        /// <param name="structureType">
        /// Type of the structure.
        /// </param>
        /// <param name="maintainableRef">
        /// The maintainable reference.
        /// </param>
        /// <returns>
        /// The list of maintainable artefacts without any children
        /// </returns>
        public static IList<TMaintainable> GetArtefact<TMaintainable>(
            SdmxStructureType structureType,
            IMaintainableRefObject maintainableRef) where TMaintainable : IMaintainableMutableObject, new()
        {
            List<TMaintainable> maintainables = new List<TMaintainable>();
            for (int i = 0; i < _artefacts.GetLength(0); i++)
            {
                if (!_artefacts[i, 0].Equals(structureType.ToEnumType().ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if (maintainableRef.HasMaintainableId() && !maintainableRef.MaintainableId.Equals(_artefacts[i, 1]))
                {
                    continue;
                }

                if (maintainableRef.HasAgencyId() && !maintainableRef.AgencyId.Equals(_artefacts[i, 2]))
                {
                    continue;
                }

                if (maintainableRef.HasVersion() && !maintainableRef.Version.Equals("*")
                    && !maintainableRef.Version.Equals(_artefacts[i, 3]))
                {
                    continue;
                }

                var maintainable = new TMaintainable
                                       {
                                           Id = _artefacts[i, 1],
                                           AgencyId = _artefacts[i, 2],
                                           Version = _artefacts[i, 3]
                                       };
                maintainable.AddName("en", _artefacts[i, 4]);
                maintainables.Add(maintainable);
            }

            return maintainables;
        }

        /// <summary>
        /// Builds the DSD reference.
        /// </summary>
        /// <param name="dataflow">
        /// The dataflow.
        /// </param>
        public static void BuildDsdReference(IDataflowMutableObject dataflow)
        {
            dataflow.DataStructureRef = new StructureReferenceImpl(
                dataflow.AgencyId,
                dataflow.Id,
                dataflow.Version,
                SdmxStructureEnumType.Dsd);
        }

        /// <summary>
        /// Adds the children.
        /// </summary>
        /// <param name="dataStructure">
        /// The data structure.
        /// </param>
        public static void AddChildren(IDataStructureMutableObject dataStructure)
        {
            for (int i = 0; i < _componentConcept.GetLength(0); i++)
            {
                if (_componentConcept[i, 0].Equals(dataStructure.Id))
                {
                    switch (_componentConcept[i, 1])
                    {
                        case "Dimension":
                            dataStructure.AddDimension(
                                BuildConceptRef(GetComponentId(i)),
                                BuildCodelistRef(_componentConcept[i, 3]));
                            break;
                        case "DataAttribute":
                            var attribute = dataStructure.AddAttribute(
                                BuildConceptRef(GetComponentId(i)),
                                BuildCodelistRef(_componentConcept[i, 3]));
                            AttributeAttachmentLevel attachmentLevel;
                            if (Enum.TryParse(_componentConcept[i, 5], true, out attachmentLevel))
                            {
                                attribute.AttachmentLevel = attachmentLevel;
                            }

                            attribute.AssignmentStatus = _componentConcept[i, 6];

                            break;
                        case "TimeDimension":
                            var timeDimension = new DimensionMutableCore
                                                    {
                                                        StructureType =
                                                            SdmxStructureType.GetFromEnum(
                                                                SdmxStructureEnumType.TimeDimension),
                                                        TimeDimension = true,
                                                        Id = DimensionObject.TimeDimensionFixedId,
                                                        ConceptRef =
                                                            BuildConceptRef(GetComponentId(i))
                                                    };
                            AddTextFormat(i, timeDimension);
                            dataStructure.AddDimension(timeDimension);
                            break;
                        case "PrimaryMeasure":
                            var primaryMeasure = dataStructure.AddPrimaryMeasure(BuildConceptRef(GetComponentId(i)));
                            AddTextFormat(i, primaryMeasure);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Adds the children.
        /// </summary>
        /// <param name="codelist">
        /// The code list.
        /// </param>
        public static void AddChildren(ICodelistMutableObject codelist)
        {
            for (int i = 0; i < _codes.GetLength(0); i++)
            {
                if (!codelist.Id.Equals(_codes[i, 0]))
                {
                    continue;
                }

                ICodeMutableObject code = new CodeMutableCore();
                code.Id = _codes[i, 1];
                code.AddName("en", "A name for " + code.Id);
                if (!string.IsNullOrEmpty(_codes[i, 2]))
                {
                    code.ParentCode = _codes[i, 2];
                }

                codelist.AddItem(code);
            }
        }

        /// <summary>
        /// Adds the children.
        /// </summary>
        /// <param name="conceptScheme">
        /// The concept scheme.
        /// </param>
        public static void AddChildren(IConceptSchemeMutableObject conceptScheme)
        {
            for (int i = 0; i < _componentConcept.GetLength(0); i++)
            {
                IConceptMutableObject concept = new ConceptMutableCore();
                concept.Id = _codes[i, 1];
                concept.AddName("en", "A name for " + concept.Id);

                conceptScheme.AddItem(concept);
            }
        }

        /// <summary>
        /// Gets the component identifier.
        /// </summary>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetComponentId(int i)
        {
            return _componentConcept[i, 2];
        }

        /// <summary>
        /// Adds the text format.
        /// </summary>
        /// <param name="i">
        /// The i.
        /// </param>
        /// <param name="timeDimension">
        /// The time dimension.
        /// </param>
        private static void AddTextFormat(int i, IComponentMutableObject timeDimension)
        {
            TextEnumType textEnumType;
            if (Enum.TryParse(_componentConcept[i, 4], true, out textEnumType))
            {
                var textFormatMutableCore = new TextFormatMutableCore { TextType = TextType.GetFromEnum(textEnumType) };
                timeDimension.Representation = new RepresentationMutableCore { TextFormat = textFormatMutableCore };
            }
        }

        /// <summary>
        /// Builds the concept reference.
        /// </summary>
        /// <param name="id">
        /// The identifier.
        /// </param>
        /// <returns>
        /// The concept reference
        /// </returns>
        private static IStructureReference BuildConceptRef(string id)
        {
            _log.DebugFormat("Building Concept Reference for concept {0}", id);
            return new StructureReferenceImpl(
                _artefacts[1, 2],
                _artefacts[1, 1],
                _artefacts[1, 3],
                SdmxStructureEnumType.Concept,
                id);
        }

        /// <summary>
        /// Builds the code list reference.
        /// </summary>
        /// <param name="id">
        /// The identifier.
        /// </param>
        /// <returns>
        /// The code list reference
        /// </returns>
        private static IStructureReference BuildCodelistRef(string id)
        {
            _log.DebugFormat("Building Codelist reference for concept {0}", id);
            var codelist =
                GetArtefact<CodelistMutableCore>(
                    SdmxStructureType.GetFromEnum(SdmxStructureEnumType.CodeList),
                    new MaintainableRefObjectImpl(null, id, null)).First();
            return new StructureReferenceImpl(
                codelist.AgencyId,
                codelist.Id,
                codelist.Version,
                SdmxStructureEnumType.CodeList);
        }
    }
}