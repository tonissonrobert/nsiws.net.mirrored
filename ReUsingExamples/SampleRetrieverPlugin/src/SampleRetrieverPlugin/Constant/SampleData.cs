﻿// -----------------------------------------------------------------------
// <copyright file="SampleData.cs" company="EUROSTAT">
//   Date Created : 2016-09-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace SampleRetrieverPlugin.Constant
{
    using System.Collections.Generic;

    /// <summary>
    /// This class contains an array with sample data. This normally wont be needed in a production plug-in.
    /// </summary>
    public static class SampleData
    {
        /// <summary>
        /// The sample dataset array, some values to simulate a table in a database
        /// </summary>
        public static readonly string[,] Dataset =
            {
                { "M", "LU", "NS0010", "2013-03", "2.1", "A" },
                { "M", "LU", "NS0010", "2014-03", "2.2", "A" },
                { "M", "LU", "NS0020", "2013-03", "2.3", "A" },
                { "M", "LU", "NS0020", "2014-03", "2.4", "A" }
            };

        /// <summary>
        /// The column names.
        /// </summary>
        public static readonly IList<string> ColumnNames = new[] { "FREQ", "REF_AREA", "INDICATOR", "TIME_PERIOD", "OBS_VALUE", "OBS_STATUS" };

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="componentId">The component identifier.</param>
        /// <returns>The value of the</returns>
        public static string GetValue(int row, string componentId)
        {
            var indexOf = ColumnNames.IndexOf(componentId);
            return Dataset[row, indexOf];
        }
    }
}