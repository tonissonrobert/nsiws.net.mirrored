﻿// -----------------------------------------------------------------------
// <copyright file="SampleRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2016-09-22
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace SampleRetrieverPlugin.Factory
{
    using System.Collections.Generic;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Manager.Data;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;

    using SampleRetrieverPlugin.Manager;

    /// <summary>
    /// This is the entry point for NSI Web Service. 
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Factory.IRetrieverFactory" />
    public class SampleRetrieverFactory : IRetrieverFactory
    {
        /// <summary>
        /// The factory identifier. TODO Change this value to a unique identifier.
        /// </summary>
        private const string FactoryId = "SampleRetrieverFactory";

        /// <summary>
        /// The cross reference mutable retrieval manager
        /// </summary>
        private readonly SampleCrossReferenceManager _crossReferenceMutableRetrievalManager;

        /// <summary>
        /// The mutable object retrieval manager
        /// </summary>
        private readonly SampleMutableObjectRetriever _mutableObjectRetrievalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleRetrieverFactory"/> class.
        /// </summary>
        public SampleRetrieverFactory()
        {
            this._mutableObjectRetrievalManager = new SampleMutableObjectRetriever();
            this._crossReferenceMutableRetrievalManager =
                new SampleCrossReferenceManager(this._mutableObjectRetrievalManager);
        }

        /// <summary>
        /// Gets an instance of <see cref="T:Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data.ISdmxDataRetrievalWithWriter"/> using the provided SDMX version.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The SDMX schema version.
        /// </param>
        /// <param name="principal">
        /// The principal. Can be <c>null</c>.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data.ISdmxDataRetrievalWithWriter"/>.
        /// </returns>
        public ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            var sdmxv20 = sdmxSchema.EnumType != SdmxSchemaEnumType.VersionTwo;

            // This is responsible for retrieving data for REST (any version) and SOAP v2.0
            return new DataRetrieverWithWriterWrapper(new SampleAdvancedSdmxDataRetriever(), new DataQuery2ComplexQueryBuilder(sdmxv20));
        }

        /// <summary>
        /// Gets an instance of <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Manager.ISdmxDataRetrievalWithCrossWriter"/> using the provided SDMX version.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The SDMX schema version.
        /// </param>
        /// <param name="principal">
        /// The principal. Can be <c>null</c>.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Manager.ISdmxDataRetrievalWithCrossWriter"/>.
        /// </returns>
        public ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            // This is responsible for retrieving SDMX v2.0 Cross Sectional data for REST (any version) and SOAP v2.0
            return new SampleXsDataRetriever();
        }

        /// <summary>
        /// Gets an instance of <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Manager.ISdmxDataRetrievalWithCrossWriter"/> (SDMX v2.1 only).
        /// </summary>
        /// <param name="principal">
        /// The principal. Can be <c>null</c>.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data.IAdvancedSdmxDataRetrievalWithWriter"/>.
        /// </returns>
        public IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            // This is responsible for retrieving data using the advanced SDMX v2.1 SOAP queries.
            return new SampleAdvancedSdmxDataRetriever();
        }

        /// <summary>
        /// Gets the advanced mutable structure search manager.
        /// </summary>
        /// <param name="principal">
        /// The principal. Can be <c>null</c>.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable.IAdvancedMutableStructureSearchManager"/>.
        /// </returns>
        public IAdvancedMutableStructureSearchManager GetAdvancedMutableStructureSearchManager(IPrincipal principal)
        {
            // This is responsible for retrieving structures using the advanced SDMX v2.1 SOAP queries. 
            // Optionally the principal could be used for authorization. This can be null.
            // This method will be used only if there is *no* DataflowAuthorizationPlugin that supports the current authentication method.
            return new SampleIAdvancedMutableStructureSearchManager(new SampleMutableStructureSearchManager(this._mutableObjectRetrievalManager, this._crossReferenceMutableRetrievalManager));
        }

        /// <summary>
        /// Gets the authentication advanced mutable structure search manager.
        /// Those
        /// </summary>
        /// <param name="authorizedDataflows">
        /// The authorized dataflows.
        /// </param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Manager.IAuthAdvancedMutableStructureSearchManager"/>.
        /// </returns>
        public IAdvancedMutableStructureSearchManager GetAuthAdvancedMutableStructureSearch(
            IList<IMaintainableRefObject> authorizedDataflows)
        {
            // This is responsible for retrieving structures using the advanced SDMX v2.1 SOAP queries. 
            // Optionally the list of authorized dataflows could be used for dataflows. This can be null.
            // This method will be used only if there is a DataflowAuthorizationPlugin that supports the current authentication method.
            return new SampleIAdvancedMutableStructureSearchManager(new SampleMutableStructureSearchManager(this._mutableObjectRetrievalManager, this._crossReferenceMutableRetrievalManager));
        }

        /// <summary>
        /// Gets the authentication mutable structure search manager.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The SDMX schema.
        /// </param>
        /// <param name="authorizedDataflows">
        /// The authorized dataflows.
        /// </param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Manager.IAuthMutableStructureSearchManager"/>.
        /// </returns>
        public IMutableStructureSearchManager GetAuthMutableStructureSearch(
            SdmxSchema sdmxSchema,
            IList<IMaintainableRefObject> authorizedDataflows)
        {
            // This is responsible for retrieving structures using the SDMX v2.0 SOAP queries or SDMX v2.1 REST Queries. 
            // Optionally the list of authorized dataflows could be used for dataflows. This can be null.
            // This method will be used only if there is a DataflowAuthorizationPlugin that supports the current authentication method.
            return new SampleMutableStructureSearchManager(
                this._mutableObjectRetrievalManager,
                this._crossReferenceMutableRetrievalManager);
        }

        /// <summary>
        /// Gets the mutable structure search.
        /// </summary>
        /// <param name="sdmxSchema">
        /// The SDMX schema.
        /// </param>
        /// <param name="principal">
        /// The principal. Can be <c>null</c>.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable.IMutableStructureSearchManager"/>.
        /// </returns>
        public IMutableStructureSearchManager GetMutableStructureSearch(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            // This is responsible for retrieving structures using the SDMX v2.0 SOAP queries and SDMX v2.1 REST queries. 
            // Optionally the principal could be used for authorization. This can be null.
            // This method will be used only if there is *no* DataflowAuthorizationPlugin that supports the current authentication method.
            return new SampleMutableStructureSearchManager(
                this._mutableObjectRetrievalManager,
                this._crossReferenceMutableRetrievalManager);
        }

        /// <summary>
        /// Gets the SDMX object retrieval.
        /// </summary>
        /// <param name="principal">
        /// The principal. Can be <c>null</c>.
        /// </param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.ISdmxObjectRetrievalManager"/>.
        /// </returns>
        public ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal)
        {
            // This is responsible for retrieving structural metadata for data requests. In most cases it is just DSD and Dataflow. 
            // But some formats like SDMX Json also require Codelists and Concept Schemes.
            // Optionally the principal could be used for authorization. This can be null.
            return new MutableWrapperSdmxObjectRetrievalManager(
                this._mutableObjectRetrievalManager,
                new SampleMutableStructureSearchManager(
                    this._mutableObjectRetrievalManager,
                    this._crossReferenceMutableRetrievalManager));
        }

        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            throw new System.NotImplementedException();
        }

        public IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Gets the unique identifier of the implementation.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id
        {
            get
            {
                // This must be unique!
                return FactoryId;
            }
        }
    }
}