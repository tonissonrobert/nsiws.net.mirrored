# Command line tool collection

## Unblocking on Windows

Because of a Windows security measure, files downloaded from the internet might be marked as unsafe, which will cause tools to fail to load. To work around this issue, before unzipping the .zip file please right click at the zip file and click at the Unblock button or check button. For more information please consult the following link from Microsoft:

https://blogs.msdn.microsoft.com/delay/p/unblockingdownloadedfile/

Important. If any 3rd party plugins are used then please unblock those as well.

## Data monitor job

### Data Monitor Jon Overview

The `DataMonitorJob.exe` tool checks for data updates in the Mapping Store DB.
Every time it runs it will go through all `SDMX dataflow` that have a Provision Agreement and have new updates and for each Provision agreement it will send a data registration request to the configured SDMX Registry.

### Configuration

The configuration file is `DataMonitorJob.exe.config` under `appSettings`.
e.g.

```xml
 <appSettings>
    <!-- REST default base for SDMX RI. For SOAP the endpoint URL should be provided-->
    <add key="BaseResourceUrl" value="https://webgate.ec.europa.eu/nsi-jax-ws/rest/"/>
    <!-- Uncomment the following line and provide the -->
<!--    <add key="WSDLUrl" value="https://webgate.ec.europa.eu/nsi-jax-ws/NSIStdV21Service?wsdl" /> -->
    <add key="WADLUrl" value="https://webgate.ec.europa.eu/nsi-jax-ws/rest/application.wadl"/>
    <add key="data.registrator.registry.http.username" value="abc"/>
    <add key="data.registrator.registry.http.password" value="abc"/>
    <add key="data.registrator.registry.http.useHttpAuthentication" value="false"/>
    <add key="data.registrator.registry.endpoint.v21.wsdl" value="https://registry.sdmx.org/FusionRegistry/ws/soap/sdmxSoap.wsdl"/>
    <add key="data.registrator.registry.endpoint.v21" value="https://registry.sdmx.org/FusionRegistry/ws/rest"/>
    <!-- Use Global Registry REST API instead of SDMX v2.1 SOAP API -->
    <add key="data.registrator.registry.endpoint.isRest" value="true" />

```

It has the following options:

| Option             | Required | Type  | Description                                                                                                |
|--------------------|----------|-------|------------------------------------------------------------------------------------------------------------|
|  `BaseResourceUrl` | Yes      | `URL` | The base data dissemination WS URL. For NSI WS and REST is `/rest/` while for SOAP the SDMX v2.1 endpoint  |
|  `WSDLUrl`         | No       | `URL` | The SOAP Service WSDL URL. This is required only if `BaseResourceUrl` is a SOAP endpoint                   |
|  `WADLUrl`         | No       | `URL` | The REST Service WADL URL. This is required only if `BaseResourceUrl` is a REST base URL                   |
|  `data.registrator.registry.http.username` | No | `string` | The Registry User Name                                                                |
|  `data.registrator.registry.http.password` | No | `string` | The Registry password                                                                 |
|  `data.registrator.registry.http.useHttpAuthentication` | Yes | `boolean` | A value indicating whether to enable HTTP authentication               |
|  `data.registrator.registry.endpoint.v21.wsdl` | No | `URL` | The Registry SOAP v2.1 WSDL URL                                                     |
|  `data.registrator.registry.endpoint.v21` | Yes | `URL` | The Registry SOAP v2.1 endpoint URL                                                      |
|  `data.registrator.registry.endpoint.isRest` | Yes | `boolean` | The endpoint uses Global Registry REST API                                                      |

Also it needs a connection string setting, default name `MappingStoreServer` but the name change be set in command line.
e.g.

```xml
<connectionStrings>
    <clear/>
    <add name="MappingStoreServer" connectionString="Data Source=localhost;Initial Catalog=msdb_scratch;User Id=mauser;Password=123"
      providerName="System.Data.SqlClient"/>
```

### Examples

To use the default connection string:

```bat
DataMonitorJob.exe
```

To use an alternative connection string setting:

```bat
DataMonitorJob.exe my_connection_string_name
```

To setup with Windows Scheduler it can be configured via GUI or command line, see for more details the [Microsoft Documentation](https://technet.microsoft.com/en-us/library/cc748993(v=ws.11).aspx)

Example to run every 5 minutes:

```bat
schtasks /create /sc minute /mo 5 /tn "MyDataMonitorJob" /tr DataMonitorJob.exe
```

## Mapping Store Database initialization/upgrade

### Overview

Please use the command line tool under the Tool folder to list, initialize or upgrade a Mapping Store DB
`Estat.Sri.Mapping.Tool.exe`

Note this tool has separate configuration. Its configuration is in
`Estat.Sri.Mapping.Tool.exe.config`

### Tool examples

Get tool help:

```bat
Estat.Sri.Mapping.Tool.exe help
```

List databases:

```bat
Estat.Sri.Mapping.Tool.exe list
```

Upgrade a Mapping Assistant database *PLEASE BACKUP FIRST the database*

```bat
Estat.Sri.Mapping.Tool.exe upgrade -m connection_string_name -t latest
```
If the database is not already a mapping store database and -i parameter is present an initialize action will be performed instead of upgrade

```bat
Estat.Sri.Mapping.Tool.exe upgrade -m connection_string_name -i
```

Initialize a new Mapping Assistant database
*WARNING* it will delete all Mapping Assistant DB related tables and their data

```bat
Estat.Sri.Mapping.Tool.exe init -m connection_string_name
```

Alternative it is possible to pass the connection string and provider in the command line for `list`, `init` and `upgrade` commands:

```bat
 Estat.Sri.Mapping.Tool.exe init -c "Data Source=HOST;Initial Catalog=MASTORE_DB;User Id=USER;Password=PASS" -p "System.Data.SqlClient"
```

```bat
 Estat.Sri.Mapping.Tool.exe upgrade -c "Data Source=HOST;Initial Catalog=MASTORE_DB;User Id=USER;Password=PASS" -p "System.Data.SqlClient"
```
