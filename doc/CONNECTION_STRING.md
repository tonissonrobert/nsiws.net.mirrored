# Connection String

## Overview

The connection strings are searched by default in the following configuration files:

1. `c:\ProgramData\Eurostat\nsiws.config`
   1. The path and filename is configurable. See [CONFIGURATION](CONFIGURATION.md)
   1. An example file called `nsiws.config` is included in the root of the package.
   1. This file can remain the same across upgrades or re-deployments of the Web Service
   1. Optional, if it doesn't exist it is not used
1. `config/app.config`.
    1. Requires restart of the application pool to read the changes
    1. This file is copied to `NSIWebServiceCore.dll.config` on every restart/refresh of the application pool.
    1. Optional, if it doesn't exist it is not used
1. `NSIWebServiceCore.dll.config`
    1. Requires restart of the IIS or application pool to read the changes.

## Configuration

The configuration element is used to define the connection string to the Mapping Store Database Server is defined in the connectionString section of the configuration file, see [Overview](#Overview). An example of the connection string is:

```xml
<!-- Replace HOST with hostname or IP address of the server/PC running mapping store database -->
    <!-- Replace MSDB with Mapping store database -->
    <!-- Replace USER with Mapping store database user name -->
    <!-- Replace PASS with Mapping store database user password -->
    <!-- Uncomment only one <add.../> at a time.-->
    <!-- Sql Server without any instance-->
    <add name="MappingStoreServer" connectionString="Data Source=HOST\SQLEXPRESS;Initial Catalog=MSDB;Integrated Security=False;User ID=USER;Password=PASS"
     providerName="System.Data.SqlClient" />
```

The element contains 3 attributes:

* name: defines the name of the connection string and for NSI WS it must always be “MappingStoreServer”. The application searches for this name in order to connect to the Mapping Store Database Server. 
  * *WARNING* There is an exception when authentication is enabled. Then the name must match name the users configured with Mapping Assistant are allowed to access.
* connectionString: defines the connection string that the application is using in order to connect to the database server. This string varies depending on the database server (e.g. Microsoft SQL Server, Oracle, MySql Server)
* providerName: defines the client that will be used by the application in order to connect to the database server. The accepted values for this attribute are:
  * for Microsoft SQL Server:  `System.Data.SqlClient`
  * for Oracle Server: `Oracle.ManagedDataAccess.Client`
  * for MySQL Server: `MySql.Data.MySqlClient`

For each of the database servers the connection string element may have the values presented bellow.

### Note for authentication/authorization

When authentication/authorization is enabled, see [AUTH](AUTH.md), then the name used for the connection string must in the allowed Mapping Store list for each authenticatied user. See Mapping Assistant Web application documentation.
It is possible to have multiple connection strings for Mapping store databases
The authenticated users by default will access their default Mapping Store database configured in Mapping Assistant.
To choose another mapping store they can use the non-standard query parameter `?sid=<name of the mapping store connection string>`.

## The connectionString element for Microsoft SQL Server

```xml
<connectionStrings>
    <add name="MappingStoreServer"
connectionString="Data Source=.\SQLEXPRESS;Initial Catalog=MADB; Integrated Security=True"    providerName="System.Data.SqlClient"/>
</connectionStrings>
```

## The connectionString element for Oracle Database Server

```xml
<connectionStrings>
    <add name="MappingStoreServer"
   connectionString="Data Source=localhost/XE;User ID=sis;Password=sis"
         providerName="Oracle.ManagedDataAccess.Client" />
</connectionStrings>
```

## The connectionString element for MySql Server

```xml
<connectionStrings>
<add name="MappingStoreServer" connectionString="server=127.0.0.1;user id=root;password=123456;database=MADB"
providerName="MySql.Data.MySqlClient"/>
</connectionStrings>
```
