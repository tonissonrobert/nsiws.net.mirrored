# Extended API

This section covers the API extensions that are not part of SDMX.

## POST key in REST data queries and available constraints

In order to workaround URL size limitations it is possible to include the `key` in the body of the request in REST data queries and available constraints.

The request must:

1. use `POST` HTTP method
1. use the keyword `body` in place of the `key` e.g. `/rest/data/A_DATAFLOW/body/ALL?startPeriod=2003` or `/rest/availableconstraint/ESTAT,SSTSCONS_PROD_M/body/All/FREQ+ADJUSTMENT`
1. use and set the Content-Type HTTP header to `application/x-www-form-urlencoded`
1. include the `key` in the body in the following format, `key=<key>`, e.g. `key=A.IT.00020.1.PROD`

Full example:

```http
POST /ws/rest/data/SSTSCONS_PROD_QT2/body/ HTTP/1.1
Host: localhost
Accept: application/vnd.sdmx.structurespecificdata+xml
Content-Length: 24
Content-Type: application/x-www-form-urlencoded

key=Q.IT.W.PROD.NS0030..
```

## Local codes/values as annotations

_Note this feature needs to be enabled in `Web.Config`, see [CONFIGURATION](CONFIGURATION.md)._

The Web Service supports retrieving the actual local data (from Dissemeniation database) together with the SDMX corresponding data.
To instruct the Web Service to retrieve local data as annotations, include the `annotations=localdata` query parameter in a SDMX REST data query.

```http
GET protocol://wsentrypoint/rest/data/FLOW_REF/KEY?annotations=localdata HTTP/1.1

Accept: desired format
```

At `dataset` level the mapping annotation will be included with annotation type `SRI_MAPPING`, title the component id and text the dataset column name(s). In case the component is mapped to a constant value then no `Text` will be included in the output.

Example:

```xml
<message:DataSet action="Information" ss:dataScope="DataStructure" xsi:type="ns1:DataSetType" ss:structureRef="ESTAT_STS_2_0">
  <common:Annotations>
    <common:Annotation>
    <!-- Component ID -->
    <common:AnnotationTitle>REF_AREA</common:AnnotationTitle>
    <!-- Type to recognize those annotations -->
    <common:AnnotationType>SRI_MAPPING</common:AnnotationType>
    <!-- Dataset columns separated with ';' -->
    <common:AnnotationText xml:lang="en">STS_SDMX_DATA1_REF_AREA</common:AnnotationText>
    </common:Annotation>
    <common:Annotation>
    <common:AnnotationTitle>ADJUSTMENT</common:AnnotationTitle>
    <common:AnnotationType>SRI_MAPPING</common:AnnotationType>
    <common:AnnotationText xml:lang="en">STS_SDMX_DATA1_ADJUSTMENT</common:AnnotationText>
    </common:Annotation>
    <common:Annotation>
    <common:AnnotationTitle>STS_INDICATOR</common:AnnotationTitle>
    <common:AnnotationType>SRI_MAPPING</common:AnnotationType>
    <common:AnnotationText xml:lang="en">STS_SDMX_DATA1_STS_INDICATOR</common:AnnotationText>
    </common:Annotation>
    <common:Annotation>
    <common:AnnotationTitle>STS_ACTIVITY</common:AnnotationTitle>
    <common:AnnotationType>SRI_MAPPING</common:AnnotationType>
    <common:AnnotationText xml:lang="en">STS_SDMX_DATA1_STS_ACTIVITY</common:AnnotationText>
    </common:Annotation>
    <common:Annotation>
    <common:AnnotationTitle>STS_INSTITUTION</common:AnnotationTitle>
    <common:AnnotationType>SRI_MAPPING</common:AnnotationType>
    <common:AnnotationText xml:lang="en">STS_SDMX_DATA1_STS_INSTITUTION</common:AnnotationText>
    </common:Annotation>
    <common:Annotation>
    <common:AnnotationTitle>STS_BASE_YEAR</common:AnnotationTitle>
    <common:AnnotationType>SRI_MAPPING</common:AnnotationType>
    <common:AnnotationText xml:lang="en">STS_SDMX_DATA1_STS_BASE_YEAR</common:AnnotationText>
    </common:Annotation>
```

At `series`, `group` and `obsevation` levels will include annotations of type `SRI_LOCAL_DATA`, title the dataset column name and text the local code value. The dataset columns included in each level are mapped to the level specific components with one exception; the dataset attributes will appear once under the first occurance of a child level, `series`, `group` or `obsevation`.

Example:

```xml
<common:Annotations>
        <common:Annotation>
          <!-- Dataset column name -->
          <common:AnnotationTitle>STS_SDMX_DATA1_DECIMALS</common:AnnotationTitle>
          <!-- Type to recognize those annotations -->
          <common:AnnotationType>SRI_LOCAL_DATA</common:AnnotationType>
          <!-- Dataset column value -->
          <common:AnnotationText xml:lang="en">2</common:AnnotationText>
        </common:Annotation>
        <common:Annotation>
          <common:AnnotationTitle>STS_SDMX_DATA1_TITLE</common:AnnotationTitle>
          <common:AnnotationType>SRI_LOCAL_DATA</common:AnnotationType>
        </common:Annotation>
        <common:Annotation>
          <common:AnnotationTitle>STS_SDMX_DATA1_TITLE_COMPL</common:AnnotationTitle>
          <common:AnnotationType>SRI_LOCAL_DATA</common:AnnotationType>
          <common:AnnotationText xml:lang="en">Elements of the full national etc.</common:AnnotationText>
        </common:Annotation>
        <common:Annotation>
          <common:AnnotationTitle>STS_SDMX_DATA1_UNIT</common:AnnotationTitle>
          <common:AnnotationType>SRI_LOCAL_DATA</common:AnnotationType>
          <common:AnnotationText xml:lang="en">PC</common:AnnotationText>
        </common:Annotation>
        <common:Annotation>
          <common:AnnotationTitle>STS_SDMX_DATA1_UNIT_MULT</common:AnnotationTitle>
          <common:AnnotationType>SRI_LOCAL_DATA</common:AnnotationType>
          <common:AnnotationText xml:lang="en">0</common:AnnotationText>
        </common:Annotation>
```

## Mapped data validation & errors

This feature introduces the possibility of validating transcodings and checking for duplicate observations. In order to use this feature the user must make a data request with `Content-Type : application/mappingValidation`.
The new `MappingValidationWriter` will output the errors in JSON format

```json
{  
   "CodelistErrors":{  
      "STS_BASE_YEAR":[  
         "1"
      ],
      "FREQ":[  
         "X"
      ]
   },
   "DuplicateObservations":[  
      "M:C:NS0040:1:1:2005-01",
      "M:C:NS0040:1:1:2007-03"
   ],
   "TimePeriodErrors":[  
      "2013-15"
   ]
}
```