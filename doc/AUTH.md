# Authentication/Authorization support

*Note* In NSI WS this is disabled by default.
*Note* In NSI WS this is required only for submission of structural metadata.
*Note* In MA WS this is enabled by default and is required for normal usage.

## Configuration

In order to add authentication and authorization support, the following changes must be applied to the [Main configuration file](CONFIGURATION.md#main-configuration-file) file:

### Step 1. Enable Authentication/Authorization middlware

First the Authentication/Authorization middleware needs to be enabled.

#### Step 1a SDMX RI Authentication/Authorization middleware Implementation

For the default SDMX RI module

[Main configuration file](CONFIGURATION.md#main-configuration-file) under `<appSettings>`

```xml
    <!--Set to true to enable authentication middleware -->
    <add key="estat.nsi.ws.config.auth" value="true" />
```

#### Step 1b Other implementation

Please refer to Middleware section of [Plugins][PLUGINS.md].

### Step 2a Configure  SDMX RI Authentication/Authorization middleware implementation

Then the `NsiAuthModule` configuration needs to be modified in the [Main configuration file](CONFIGURATION.md#main-configuration-file) file. The `NsiAuthModule` configuration section is available under `estat.nsi.ws.config` element, in the `“auth”` element.
[Main configuration file](CONFIGURATION.md#main-configuration-file)

```xml
<estat.nsi.ws.config>
    <!-- authentication configuration-->
    <auth anonymousUser="" realm="nsiws">
      <userCredentialsImplementation type="UserCredentialsHttpBasic" />
      <authenticationImplementation type="EstatSriSecurityAuthenticationProvider" />
      <!-- <authenticationImplementation type="EstatSriSecurityAuthenticationProviderXml" />-->
    </estat.nsi.ws.config>
```

The list of possible configuration options for the `NsiAuthModule` can be found in the following table:

| XML Element/Attribute              | Description                                              |
|------------------------------------|----------------------------------------------------------|
| `auth/anonymousUser`                 | Include this XML attribute with a valid username or `*` value to allow access of anonymous user where anonymous access is allowed. See Anonymous user section below |
| `auth/realm`                         | The realm or the domain. This is required by some implementations which retrieve credentials from HTTP headers.  |
| `userCredentialsImplementation/type` | The implementation of IUserCredentials interface. This implementation will be used to retrieve user credentials, in most cases UserName and Password. |
| `authenticationImplementation/type`  | The implementation of the IAuthenticationProvider interface. This implementation will be used for authentication. |

Together with the `NsiAuthModule` `(Estat.Nsi.AuthModule)`, at least one implementation for each interface is included. The table below contains the list of implementations currently included in the `NsiAuthModule` and can be used as values to the corresponding “type” XML attribute discussed in the above table.

| Interface               | Type                               | Description                                                                      | Config tool |
|-------------------------|------------------------------------|----------------------------------------------------------------------------------|-------------|
| `IUserCredentials`      | `UserCredentialsHttpBasic`         | This implementation retrieves the user credentials from HTTP Headers using Basic Authentication. | N/A  |
| `IAuthenticationProvider` | `MappingStoreAuthenticationProvider` | *Depreciated* This implementation will retrieve the authentication information from Mapping Store Database. | Desktop Mapping Assistant |
| `IAuthenticationProvider` | `EstatSriSecurityAuthenticationProvider` | This implementation will retrieve the authentication information from AUTHDB Database. | MAWS/MAWEB |
| `IAuthenticationProvider` | `EstatSriSecurityAuthenticationProviderXml` | This implementation will retrieve the authentication information from an XML file. | A text editor |

### Step 3a SDMX RI Authentication/Authorization middleware store configuration

#### AUTH DB Store configuration

To select:

```xml
<authenticationImplementation type="EstatSriSecurityAuthenticationProvider" />
```

It requires a connection string with name `AUTHDB` in [Main configuration file](CONFIGURATION.md#main-configuration-file)

#### XML Auth store

To select:

```xml
<authenticationImplementation type="EstatSriSecurityAuthenticationProviderXml" />
```

It requires an XML file, see `App_Data/auth.xml` for example.
The location of the XML file is configured in the following app setting:

```xml
    <add key="authXmlLocation" value="App_data/auth.xml" />
```

## Anonymous user

In order to allow the anonymous user to access certain data and certain dataflows then this XML attribute value must be the username of a valid user or the special value `*`. When the value is the user name of a valid user then this user will get the roles and other properties of that user.
The default authentication provider `EstatSriSecurityAuthenticationProvider` requires the user to have an empty password.

When the `*` value is used, see below, then the NSI Auth module will allow the request without asking for further authentication.

```xml
    <auth anonymousUser="*" realm="nsiws">
```

But the policy module configuration, `App_Data/nsiws.xml`, will block access to resources with `allowAnonymous="true"`.

Please note that in some cases, the client will send user credentials only when requested from the server side. When the anonymousUser is set the clients will not receive the request to authenticate from the server.  In such cases the client must support and be configured to send the user credentials pre-emptively.

## Policy module

Since: _v6.10.0_

This HTTP module, enabled by default, uses an XML file to configure access to REST resources and SOAP endpoints.

REST Example:

```xml
 <r:rest path="/rest/data/[^/]+(/.*)?" method="GET" allowAnonymous="true">
    <r:and>
        <r:permission id="CanReadData"/>
    </r:and>
  </r:rest>
```

SOAP example:

```xml
<r:soap path="/SdmxService" allowAnonymous="true">
    <r:and>
        <r:permission id="CanReadData"/>
    </r:and>
    <r:xpath>
        <!-- Declare namespaces used in the XPath script below -->
        <r:declare prefix="web" namespace="http://www.sdmx.org/resources/sdmxml/schemas/v2_1/webservices"/>
        <r:declare prefix="soap" namespace="http://schemas.xmlsoap.org/soap/envelope/"/>
        <!-- The XPath script, must return a boolean value -->
        <r:script><![CDATA[
            boolean(
                /soap:Envelope/soap:Body/web:GetStructureSpecificData
                | /soap:Envelope/soap:Body/web:GetGenericData
                | /soap:Envelope/soap:Body/web:GetStructureSpecificTimeSeriesData
                | /soap:Envelope/soap:Body/web:GetGenericTimeSeriesData
                )
            ]]>
        </r:script>
    </r:xpath>
  </r:soap>
```

For both SOAP and REST the following common options are used:

| option | value type | description |
|--------|------------|-------------|
| `path` | regular expression | A regular expression that is used to match the endpoint/resource. |
| `allowAnonymous` | boolean | A value indicating whether to allow an anonymous user to access this resource. |
| `and` | list of roles | The list of roles that the user accessing this resource must be in. The user must be in all listed roles. |
| `or` | list of roles | The list of roles that the user accessing this resource must at least be in one of them. The user must be in at least one of the roles from the listed roles. |

Note for both `and` and `or`, exists a special value `Any`.

The REST resource configuration in addition contains the option `method`. This is used to match the HTTP Method used to access the resource. The criteria used to match a REST resource are `path` and `method`.

The SOAP endpoint configuration in addition contains the option `script`. This is an XPath expression that is used to match request message send by the client. It must return a boolean value.
The criteria used to match a SOAP endpoint are `path` and `script`.

## Open ID Configuration

See [openid-middleware.md](openid-middleware.md)