# Supported Databases

## Overview

While the application tries to be compatible with any recent version of the supported databases, there is no guarantee that versions that have not been tested will work.
Sometimes newer versions of database servers require new versions of the database driver, while newer versions of database drivers could drop support for older versions of databases.

## Oracle

The application is known to work with Oracle versions 11g, 12c, 18c Express and 19c.

## Microsoft SQL Server

The application is known to work with Microsoft SQL Server 2014 (Windows) and 2017 (Linux).

## MariaDB/MySQL

The application is known to work with MariaDB 10.3 and 10.8. Note on Linux installations the `lower_case_table_names` must be set to `1`.
See [MariaDB documentation](https://mariadb.com/docs/reference/mdb/system-variables/lower_case_table_names/).
