# CONFIGURATION

## Main Configuration file

The Web Service application obtains the configuration parameter from the configuration files:

1. `config/app.config` if it exists.
1. `NSIWebServiceCore.dll.config` if `config/app.config` doesn't exists

Note, if both files above files exist than `config/app.config` takes precedence. Although the configuration file looks rather big, the most important configuration elements are the connection string and the default header values.

## Other configuration files

Other configuration files can be placed in the `config` folder.

## Mapping Store Connection

See [CONNECTION_STRING](CONNECTION_STRING.md)

## CORS Configuration

See [CORSConfiguration](CORSConfiguration.md)

## Enable support for Mapping Assistant DB/Store v5.3

It is possible to use the current version of NSI WS with Mapping Store v5.3
This is disabled by default. To enable add the following configuration setting in the [Main configuration file](#main-configuration-file) under `<appSettings>`

```xml
  <add key="MappingStoreVersion53" value="5.3"/>
```

## Miscellaneous database configuration options

The settings for each database vendor can be specified in the [Main configuration file](#main-configuration-file). There are two types of settings:

* The general `DatabaseSettings` which apply to both Mapping Assistant DB (MSDB) and Dissemination (DDB) databases.
* The `ddbSettings` are for mapping the DDB provider names to .NET database providers/drivers

```xml
<configSections>
     ….
    <sectionGroup name="estat.sri">
        <section name="mapping.store" type="Estat.Sri.MappingStoreRetrieval.Config.MappingStoreConfigSection, MappingStoreRetrieval" />
    </sectionGroup>
</configSections>
…
<estat.sri>
  <mapping.store InsertNewItems="false">
    <!-- General Database settings apply to both Mapping Store (MSDB) and dissemination databases (DDB) -->
    <!-- Options are: -->
    <!-- provider : The .NET DB Provider name. Mandatory and unique -->
    <!-- subStringCmd : The substring command. Defaults to : SUBSTR -->
    <!-- subStringNeedsLength : Whether the substring command requires to specify the length. Defaults to : false -->
    <!-- parameterMarkerFormat : The named parameter format. Mandatory. -->
    <!-- castToStringFormat : The format text that should be used to cast to string. Defaults to: {0} -->
    <!-- dateCast : The date cast command. Defaults to "" -->
    <!-- subQueryOrderByAllowed : Whether a sub query can have an order by. Defaults to true. -->
    <DatabaseSettings>
      <clear/>
      <add provider="System.Data.SqlClient" subStringCmd="SUBSTRING" subStringNeedsLength="true"  parameterMarkerFormat="@{0}" castToStringFormat="CAST({0} AS VARCHAR)" subQueryOrderByAllowed="false"/>
      <add provider="Oracle.ManagedDataAccess.Client" parameterMarkerFormat=":{0}" dateCast="DATE"/>
      <!-- Please note that System.Data.OracleClient is obsolete and no longer maintained by Microsoft. -->
      <add provider="System.Data.OracleClient" parameterMarkerFormat=":{0}" dateCast="DATE"/>
      <add provider="MySql.Data.MySqlClient" parameterMarkerFormat="@{0}" dateCast="DATE"/>
      <!-- DDB only providers -->
      <add provider="org.estat.PcAxis.PcAxisProvider" parameterMarkerFormat="@{0}"/>
      <!-- ODBC options depend on the ODBC driver used. -->
      <add provider="System.Data.Odbc" parameterMarkerFormat="?"  subStringCmd="SUBSTRING" subStringNeedsLength="true"
castToStringFormat="CAST({0} AS VARCHAR)" subQueryOrderByAllowed="false"/>
    </DatabaseSettings>
    <!-- Dissemination Database (DDB) settings only -->
    <!-- Options are: -->
    <!-- name : The DDB provider name. Mandatory and unique.-->
    <!-- provider : The .NET DB Provider name. -->
    <ddbSettings>
      <clear/>
      <add name="SqlServer" provider="System.Data.SqlClient" factoryClass="" dllName=""/>
        <add name="Oracle" provider="Oracle.ManagedDataAccess.Client" factoryClass="Oracle.ManagedDataAccess.Client.OracleClientFactory" dllName="Oracle.ManagedDataAccess"/>
        <add name="PCAxis" provider="org.estat.PcAxis.PcAxisProvider" factoryClass="Org.Estat.PcAxis.PcAxisProvider" dllName="PcAxis"/>
        <add name="MySQL" provider="MySql.Data.MySqlClient" factoryClass="MySql.Data.MySqlClient.MySqlClientFactory" dllName="MySql.Data"/>
        <add name="Odbc" provider="System.Data.Odbc" factoryClass="System.Data.Odbc.OdbcFactory" dllName="System.Data.Odbc"/>
    </ddbSettings>
  </mapping.store>
</estat.sri>
```

The `InsertNewItems` is a value to specify if the application should insert new items when a item scheme is submited.

The `DatabaseSettings` options are

| Option                         | Default Value     | Description                                                                                                                                                                                                                                                             |
|--------------------------------|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `provider`                       | None.             | The ADO.NET database provider name. It is mandatory and must be unique.                                                                                                                                                                                                 |
| `subStringCmd`                   | SUBSTR            | The substring command for this provider.                                                                                                                                                                                                                                |
| `subStringNeedsLength`           | false             | A value indicating whether the substring for this provider requires a length parameter.                                                                                                                                                                                 |
| `parameterMarkerFormat`          | None              | The named parameter format for this provider. This is used to create the parameter names inside an SQL statement and for naming the DbParameter objects. It needs to be specified. This format will be used with string.Format (or similar methods) with one parameter. |
| `castToStringFormat`             | {0}               | The format string that should be used to cast to string the specified field. The field is specified in the format parameter. . This format will be used with string.Format (or similar methods) with one parameter.                                                     |
| `subQueryOrderByAllowed`         | true              | A value indicating whether an SQL sub-query can have an ORDER BY.                                                                                                                                                                                                       |
| `dateCast`                       | “” (empty string) | The required by the provider prefix for casting to date type.                                                                                                                                                                                                           |
|                                |                   |                                                                                                                                                                                                                                                                         |

The `ddbSettings` options are:

| Option                         | Default Value     | Description                                             |
|--------------------------------|-------------------|---------------------------------------------------------|
| `provider`                     | None.             | The ADO.NET database provider name.  It is mandatory.   |
| `name`                         | None              | It is mandatory and must be unique.                     |

## Configuration of the default header values

The configuration element is used to define various values of a default header that the application will use in case no other header is defined in the mapping assistant. The settings names correspond to the Header element as defined by SDMX standard.
_Only `senderid` and `test` are mandatory._

The configuration is stored in [Main configuration file](#main-configuration-file), under `<applicationSettings>/<Estat.Sri.Ws.Controllers.Constants.HeaderSettings>`

Example:

```xml
<applicationSettings>
    <!-- Default Header configuration -->
    <!--  Specify 'SettingsHeaderRetriever' to enable (default) -->
    <Estat.Sri.Ws.Controllers.Constants.HeaderSettings>
      <!-- Mandatory values (test and senderid) -->
      <setting name="test" serializeAs="String">
        <value>true</value>
      </setting>
      <setting name="senderid" serializeAs="String">
        <value>SOME_NSI</value>
      </setting>
```

## Maximum size of the submited SDMX messages

.Net Core builtin web server, called Kestrel, has a default default maximum request body size 30,000,000 bytes, which is approximately 28.6 MB.
You can adjust the [default maximum request body size](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-2.2#maximum-request-body-size-1) by creating **config/kestrelSettings.json** file with contents similar to:

```json
{
  "Kestrel": {
    "Limits": {
        "MaxRequestBodySize": 52428800
    }
  }
}
```

In example above max request size will equal to 50MB. You can read more about Kestrel web server configuration [here](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-2.2#kestrel-options-1)

When hosted under IIS the MaxRequestBodySize is set to 2GB.

## Configuration of the dataflow logging

The Web Service can log the data requests per dataflow. This feature is enabled by default. The main configuration can be found in `config/log4net.config`. Specifically the following sections:

```xml
<appender name="DataflowLogger" type="log4net.Appender.RollingFileAppender">
  <file value="App_Data/dataflow_logging.csv"/>
  <rollingStyle value="Size"/>
  <maximumFileSize value="100MB" />
  <maxSizeRollBackups value="10" />
  <datePattern value="yyyyMMdd"/>
  <appendToFile value="true"/>
  <layout type="log4net.Layout.PatternLayout">
    <conversionPattern value="%d{yyyy-MM-dd HH:mm:ss,fff};%m%n"/>
  </layout>
</appender>

<!-- Comment out the following to disable logging dataflow requests -->
<logger name="org.estat.nsiws.dataflowlogger" additivity="false">
  <level value="INFO"/>
  <appender-ref ref="DataflowLogger" />
</logger>
```

To change the output file, modify the value at the `<file>` element. To change the output format please change the value at the `<conversionPattern>`. In case the separator in the `<conversionPattern>` value is changed from semi-colon `;` to something else please also change the corresponding value at the `log.df.file.separator` under `<appSettings>` in the [Main configuration file](#main-configuration-file) file:

```xml
<add key="log.df.file.separator" value=";"/>
```

To disable dataflow logging comment out or remove the following lines from the `config/log4net.config`:

```xml
<logger name="org.estat.nsiws.dataflowlogger" additivity="false">
 <level value="INFO"/>
 <appender-ref ref="DataflowLogger" />
</logger>
```

The logging is performed using the .NET log4net library and more detailed information. More information can be found at the Apache log4net website at: [http://logging.apache.org/log4net/release/config-examples.html](http://logging.apache.org/log4net/release/config-examples.html).

### Optional Logging to the Google cloud using log4net

Optionally it is possible to add google cloud logging. Remove .sample extension from `googleLogging.private.json.sample` file in the config folder and fill it with appropriate values for your environment.

```json
{
  "googleLogging": {
    "enabled": true,
    "projectId": "YOUR_GOOGLE_PROJECT_ID",
    "logId": "YOUR_LOG_ID",
    "logLevel": "Debug"
  }
}
```

This configuration dynamically adds `Google.Cloud.Logging.Log4Net.GoogleStackdriverAppender` to the log4net appender's list. This configuration enables to use .net Core standard configration so config
values may come from .json file in a config folder or environment variables.

Alternatively it's possible to achieve the same result by adding an appender in log4net.config the classic way see more in [Google cloud logging appender manual](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest)

## Other settings under `appSettings`

The following table contains the rest of the settings that can be configured in the [Main configuration file](#main-configuration-file) configuration file, under the `<appSettings>` element.

| Setting | Type of value | Default  | Description |
|---------|-------------|---|-------------|
| `defaultHeaderRetriever` | A class name that implements `IHeaderRetrievalManager` | SettingsHeaderRetriever | Must be in a plugin DLL. The default reads configuration from `Web.Config` |
| `log.df.file.separator` | A character | ; | The dataflow request log file separator |
| `pluginFolder` | relative or absolute path | Plugins | The plugins DLL folder. If it is not absolute then the path is relative to the bin path. See also [PLUGINS](PLUGINS.md) |
| `enableSubmitStructure` | Boolean | false | Enable submit structure functionality see [SUBMIT](SUBMIT_STRUCTURE.md) |
| `AllowLocalDataAsAnnotations` | Boolean | false | Allow the `?annotations=localdata` parameter in SDMX REST data queries. When this is enabled and the query parameter `annotations=localdata` is used then together with the data, annotations containing the mapping to the local data and columns will be included. |
| `configLocation` | file path | `c:\ProgramData\Eurostat\nsiws.config` | Load `ConnectionStringSettings` configuration from the specified file |

## Format configuration

NSIWS REST supports the `format` and `formatVersion` query parameters to allow selecting a different format than the default without using the `Accept` header, e.g. in a browser.
Please note that browser will still send an `Accept` header but the value in the `format` query parameter will be appended with the highest priority `q=1`.

The values that are accepted in the `format` query parameter are configurable, and can be configured in the [Main configuration file](#main-configuration-file) configuration file under `<FormatMapping>`. Below are the default values:

```xml
<FormatMapping>
    <Mappings>
      <Mapping Format="genericdata" AcceptHeader="application/vnd.sdmx.genericdata+xml"/>
      <Mapping Format="jsondata" AcceptHeader="application/vnd.sdmx.data+json"/>
      <Mapping Format="structure" AcceptHeader="application/vnd.sdmx.structure+xml"/>
      <Mapping Format="structurespecificdata" AcceptHeader="application/vnd.sdmx.structurespecificdata+xml"/>
      <Mapping Format="csv" AcceptHeader="application/vnd.sdmx.data+csv"/>
      <Mapping Format="csvfile" AcceptHeader="application/vnd.sdmx.data+csv;file=true"/>
      <Mapping Format="csvfilewithlabels" AcceptHeader="application/vnd.sdmx.data+csv;file=true;labels=both"/>
    </Mappings>
  </FormatMapping>
```

| XML attribute  | Type of value |  Description                                                                                                  |
|----------------|---------------|---------------------------------------------------------------------------------------------------------------|
| `Format`       | key           | The key which will be used in `format` query parameter                                                        |
| `AcceptHeader` | content type  | The corresponding `Accept` header value to use when `format` query parameter value matches the `Format` value |

Please note it is possible to include Content Type parameters in the `Acceptheader`

Example for format mapping SDMX v2.0 Compact:

```xml
  <Mapping Format="compactdata" AcceptHeader="application/vnd.sdmx.compactdata+xml;version=2.0"/>
```

## Other configuration in json files

The Web Service supports reading certain configuration from `.json` files from the `config` folder.
Note the names of the file is not important. Only the extension `.json` and the contents of the file.
Nevertheless the default file names will be mentioned.

### Cross Sectional SDMX 2.0 support in SDMX 2.1 formats

Controls the behavior of the Web Service conserning Cross Sectional SDMX v2.0 support in SDMX v2.1 Structure and data formats.
By default it is allowed. To disable it the `datastructure.allowSdmx21CrossSectional` must be set to false or the setting file must be removed.

Default file: `Properties.json`

Default configuration:

```json
{
  "datastructure": {
    "allowSdmx21CrossSectional": "true"
    }
}
```

### Create automatic Category from Categorisation 

There is a feature to create a stub Category for any missing categories when it imports a SDMX v2.1 Categorisation or SDMX v2.0 Dataflow/Metadataflow with CategoryReference
By default this feature is working and to disable it the value `categorisation.createStubCategory` must be set to false or the setting file must be removed.

Default file: `Properties.json`

Default configuration:

```json
"categorisation": {
    "createStubCategory": "true"
  }
```

### Dissemination db connection

It is possible to set the dissemination db connection in configuration file, which will overwrite the value stored in the Mapping Store Database.
There can only be one connection string, which will be used for all of the mappings.
The possible values of "dbType": "SqlServer", "MySQL", "Oracle"

Default file: `Properties.json`

Default configuration:

```json
"disseminationDbConnection": {
  "dbType": "SqlServer",
  "connectionString": ""
}
```

### Default culture info

It is possible to set the default culture of the service using a two-letter culture code. By default the value is set to empty, which means that the system-default language will be used. 

Default file: `Properties.json`

Default configuration:

```json
"defaultCultureInfo": ""
```

### Structure usage and dataset action for response header 

When requesting data for SDMX v2.1 there is a configurabile option to use either the datastructure or the dataflow in the header of the response.

Default file: `sdmx-header.json`

Default configuration:

```json
{
  "structureUsage": {
    "structureType": "dataflow"
  },
  "datasetAction": "Information"
}
```


## Logging configuration

There are two main types of log files
* System Activity - not related to user interaction, for example the service startup and initialization.
```xml
<appender name="SystemActivityAppender" type="log4net.Appender.RollingFileAppender">
    <file type="log4net.Util.PatternString" value="c:/ProgramData/Eurostat/logs/nsi-ws-%processid.log"/>
    <rollingStyle value="Date"/>
    <datePattern value="yyyyMMdd"/>
    <maxSizeRollBackups value="10" />
    <maximumFileSize value="100MB" />
    <staticLogFileName value="true" />
    <appendToFile value="true"/>
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date|[%thread]|%-5level|%property{log4net:HostName}|%property{User}|%logger|%message|%exception%newline" />
    </layout>
    <!--Filter system activity-->
    <filter type="Estat.Sri.Mapping.Ws.ServiceCore.SystemFilter,Estat.Sri.Mapping.Ws.ServiceCore">
    </filter>
  </appender>
```  
* User activity - related to user interaction, for example a new data import, data request.

```xml
  <appender name="UserActivityAppender" type="log4net.Appender.RollingFileAppender">
   <!--Output file -->
    <file type="log4net.Util.PatternString" value="c:/ProgramData/Eurostat/logs/nsi-ws-user-%processid.log"/>
    <rollingStyle value="Date"/>
    <datePattern value="yyyyMMdd"/>
    <maxSizeRollBackups value="10" />
    <maximumFileSize value="100MB" />
    <staticLogFileName value="true" />
    <appendToFile value="true"/>
    <layout type="log4net.Layout.PatternLayout">
      <conversionPattern value="%date|[%thread]|%-5level|%property{log4net:HostName}|%property{Application}|%logger|%property{User}|%message|%exception|%property{UrlReferrer}|%property{HttpMethod}|%property{RequestPath}|%property{QueryString}%newline" />
    </layout>
	<!--Log level WARNNING -->
	<!--<Threshold value="WARN" />-->
    <!--Filter user activity-->
    <filter type="log4net.Filter.PropertyFilter">
      <key value="User" />
      <StringToMatch value="null" />
      <acceptOnMatch value="false" />
    </filter>
   <!--<filter type="log4net.Filter.LevelMatchFilter">
     <LevelToMatch value="DEBUG" />
     <acceptOnMatch value="false" />
   </filter>-->
  </appender>
```

The above settings is configured in log4net.config file.

The most important attribute to be configured for the log files is the file name; in the above example the configured values are “nsi-ws-%processid.log” respectively “nsi-ws-user-%processid.log”. The user could modify this with any valid filename and should in production installations. If the file does not exist, the logging module will create a new file and will log into it. If the file already exists, then the logging module will continue to log at the end of it.
Please note that in order for the log file to be created and/or append log events to an existing log file, the file system permissions must allow this.
The level node controls whether logging is enabled and what level of messages should it record. The list of possible values can be found in the following table:

| Setting | Description                                                |
|---------|------------------------------------------------------------|
| Off     | Turn off logging                                           |
| FATAL   | Log only critical level messages                           |
| ERROR   | Log only critical and error messages                       |
| WARN    | Log only critical,  error and warning messages             |
| INFO    | Log information, warning, error and fatal messages         |
| DEBUG   | Log debug messages and above (Info, Warn, Error and Fatal) |
| ALL     | Log all messages                                           |

The other settings are related with the format of the log entry, which by default is configured to contain the following:

* Timestamp of the log entry;
* Rolling style: when a new log file is created and the old one is archived
* Pattern layout: what and how the log information is written in the log file
* Message containing the description of the log entry generated event;

More information can be found at the Apache log4net website at:
[http://logging.apache.org/log4net/release/config-examples.html](http://logging.apache.org/log4net/release/config-examples.html)
The Apache log4net is developed by the Apache Software Foundation and more detailed information including description, features, how-to and documentation can be found at the Apache log4net web site at:
[http://logging.apache.org/log4net/](http://logging.apache.org/log4net/)

NSIWS also supports sets custom properties that can be found in the following table:

| `log4net` pattern parameter | description                                                     |
|-----------------------------|-----------------------------------------------------------------|
| `%P{User}`                  | The request current user. It is shown only after authentication |

### SQL statement logging

It is possible to log the time needed for each SQL statement.
This is disabled by default. It can be enabled in the `config/log4net.config` file by changing the level of the `org.estat.sri.sqlquerylogger` to `INFO`, see also the example below.

```xml
 <logger name="org.estat.sri.sqlquerylogger" additivity="false">
    <!-- Change calue to "INFO" from "OFF" to enable -->
    <level value="INFO"/>
    <appender-ref ref="SQLQueryLoger" />
  </logger>
```

The output can be customized using standard `log4net` configuration and plugins. Below are the SQL statement logging specific parameters.

```xml
 <conversionPattern value="%d{ISO8601};%P{elapsed};&quot;%m&quot;;%P{dbparams};%P{sourceMember};%P{sourceFile};%P{sourceLine};%n"/>
```

Sample output:

```csv
2018-08-13 18:18:55,792;00:00:00.1038800;SELECT T.DSD_ID as SYSID, A.ID,  A.AGENCY, dbo.versionToString(A.VERSION1, A.VERSION2, A.VERSION3) as VERSION, A.VALID_FROM, A.VALID_TO, A.URI, A.IS_FINAL, LN.TEXT, LN.LANGUAGE, LN.TYPE  FROM DSD T INNER JOIN ARTEFACT A ON T.DSD_ID = A.ART_ID LEFT OUTER JOIN LOCALISED_STRING LN ON LN.ART_ID = A.ART_ID WHERE  A.ID = @Id  AND dbo.isEqualVersion(A.VERSION1, A.VERSION2, A.VERSION3, @Version1,@Version2,@Version3)=1 AND  A.AGENCY = @Agency  ORDER BY A.ART_ID ;{'@Id':'DEMOGRAPHY','@Version1':'2','@Version2':'3','@Version3':'','@Agency':'ESTAT'};RetrieveArtefacts;src\MappingStore\Engine\ArtefactRetrieverEngine.cs;350;
2018-08-13 18:18:55,836;00:00:00.0070710;select T.DSD_ID as SYSID, AN.ANN_ID, AN.ID, AN.TITLE, AN.TYPE, AN.URL, AT.LANGUAGE, AT.TEXT from ANNOTATION AN LEFT OUTER JOIN ANNOTATION_TEXT AT ON AN.ANN_ID = AT.ANN_ID INNER JOIN ARTEFACT_ANNOTATION AA ON AA.ANN_ID = AN.ANN_ID INNER JOIN DSD T ON T.DSD_ID = AA.ART_ID WHERE T.DSD_ID = @p_id ;{'@p_id':'20312'};RetrieveAnnotationsWithKey;src\MappingStore\Engine\MaintainableAnnotationRetrieverEngine.cs;113;
2018-08-13 18:18:55,854;00:00:00.0076802;SELECT G.GR_ID AS GR_ID, G.ID AS GROUP_ID, C.ID as DIMENSION_REF FROM DSD D, DSD_GROUP G, DIM_GROUP DG, COMPONENT C WHERE D.DSD_ID = G.DSD_ID AND G.GR_ID = DG.GR_ID AND DG.COMP_ID = C.COMP_ID AND D.DSD_ID = @Id ORDER BY G.GR_ID ;{'@Id':'20312'};PopulateGroups;src\maapi.net\src\MappingStore\Engine\DsdRetrievalEngine.cs;686;
```

| `log4net` pattern parameter | CSV column    | description                                                    |
|-----------------------------|---------------|----------------------------------------------------------------|
| `%d{ISO8601}`               | Timestamp     | The date and time of the logging event                         |
| `%P{elapsed}`               | elapsed       | The time needed to execute the SQL query and retrieve all data |
| `%m`                        | message       | The sql query/statement                                        |
| `%P{dbparams}`              | DB parameters | the parameters given to the SQL query/statement                |
| `%P{sourceMember}`          | source method | The method that executed the SQL query                         |
| `%P{sourceFile}`            | source file   | The full path of the file that executed the SQL Query          |
| `%P{sourceLine}`            | source line   | The line number of the source file executed the SQL Query      |

