# CORS configuration for NSIWS

## Important note

Enabling CORS reduces security on the browser side.
It doesn't affect the actual web service security!

## Enable CORS middleware

To enable CORS, the CORS middleware has to be enabled from [Main configuration file](CONFIGURATION.md#main-configuration-file).

Specifically to enable CORS middleware the following settings needs to be set to true:

```xml
    <!--Set to true to enable cors middleware -->
    <add key="corsSettings" value="true"/>
```

and the configuration needs to be uncomment:

```xml
  <!-- Uncomment to enable CORS-->
  <corsSettings>
    <corsCollection>
      <!-- Allow CORS to any caller, to restrict change * to allowed domain, if multiple allowed domains can access NSI webservice then use as many add nodes as needed -->
      <add domain="*" allowed-methods="GET,POST,PUT" allowed-headers="Range, Origin, Authorization" allow-credentials="true" exposed-headers="Location" />
      <!-- More entries, domain must ne unique -->
    </corsCollection>
  </corsSettings>
```

## Disable CORA Middleware

To disable CORS the following settings needs to be set to false

```xml
    <!--Set to true to enable cors middleware -->
    <add key="corsSettings" value="false"/>
```

## Configuration settings

Only `domain` is mandatory.

| Setting | Type | HTTP header | Description |
|---------|------|-------------|--|
| domain  | `*` or a host  | [Access-Control-Allow-Origin](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin)| This matches the host or domain where the Javascript is hosted |
| allowed-methods | Comma separated list of methods | [Access-Control-Allow-Methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Methods) | The list of allowed HTTP methods |
| allowed-headers | Comma separated list of request HTTP Headers | [Access-Control-Allow-Headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers) | The list of request HTTP headers |
| allowed-credentials | True/False | [Access-Control-Allow-Credentials](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Credentials)| Controls whether to expose the response to frontend JavaScript code when credentials are used. |
| exposed-headers | Comma separated list of server HTTP headers | [Access-Control-Expose-Headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#Access-Control-Expose-Headers)| The server whitelist header that browsers are allowed to access |

CORS is documented at Mozilla's MDN [https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

### CORS configuration example

```xml
<!-- Uncomment to enable CORS-->
  <corsSettings>
    <corsCollection>
      <!-- Allow CORS to any caller, to restrict change * to allowed domain, if multiple allowed domains can access NSI webservice then use as many add nodes as needed -->
      <add domain="*" allowed-methods="GET,POST,PUT" allowed-headers="Range, Origin, Content-type, Accept, Authorization" allow-credentials="true" exposed-headers="Location" />
      <!-- More entries, domain must ne unique -->
    </corsCollection>
  </corsSettings>
```

In this example any domain will be matched, so then two responses will follow.

The first one will be the OPTIONS response of the OPTIONS 
request that will include on its headers

- Access-Control-Allow-Origin : *
- Access-Control-Allow-Headers : Range, Origin, Content-type, Accept, Authorization
- Access-Control-Allow-Methods : *
- Access-Control-Allow-Credentials : true
- Access-Control-Expose-Headers : Location

The second will be the response to the original request that will include on its headers

- Access-Control-Allow-Origin : *
