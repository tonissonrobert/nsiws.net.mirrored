# General

# Tickets
[SDMXRI-1416](https://citnet.tech.ec.europa.eu/CITnet/jira/browse/SDMXRI-1416) - Change how the id is generated when default header doesn't have an id [header id](https://citnet.tech.ec.europa.eu/CITnet/stash/projects/SDMXRI/repos/nsiws.net/commits/2ba8d241f7f09679c092fbde8c5e46fc258327ca) . 
