﻿using Estat.Nsi.AuthModule;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sri.Ws.Controllers.Manager;
using Estat.Sri.Ws.CorsModule;
using Estat.Sri.Ws.PolicyModule.Manager;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MiddlewareBuilder
{
    /// <summary></summary>
    public class AuthMiddlewareBuilder : IMiddlewareBuilder
    {
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {}

        /// <summary>Configures the specified application.</summary>
        /// <param name="app">The application.</param>
        public void Configure(IApplicationBuilder app)
        {
            // TODO find a way to enable adding/removing 3rd party middle ware

            if (SettingsManager.AuthModuleEnabled)
            {
                app.UseMiddleware<NsiAuthModule>();
            }
        }
    }
}
