﻿// -----------------------------------------------------------------------
// <copyright file="ResponseWithSdmxObjects.cs" company="EUROSTAT">
//   Date Created : 2017-11-22
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.SubmitStructure
{
    using Estat.Sdmxsource.Extension.Model.Error;
    using Org.Sdmxsource.Sdmx.Api.Model.Header;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using System;
    using System.Collections.Generic;

    public class ResponseWithSdmxObjects
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResponseWithSdmxObjects" /> class.
        /// </summary>
        /// <param name="responses">The responses.</param>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <exception cref="ArgumentNullException">
        /// sdmxObjects
        /// or
        /// responses
        /// </exception>
        public ResponseWithSdmxObjects(IList<IResponseWithStatusObject> responses, ISdmxObjects sdmxObjects, IHeader responseHeader)
        {
            if (sdmxObjects == null)
            {
                throw new ArgumentNullException(nameof(sdmxObjects));
            }

            if (responses == null)
            {
                throw new ArgumentNullException(nameof(responses));
            }

            if (responseHeader == null)
            {
                throw new ArgumentNullException(nameof(responseHeader));
            }

            Responses = responses;
            SdmxObjects = sdmxObjects;
            ResponseHeader = responseHeader;
        }

        /// <summary>
        /// Gets the responses.
        /// </summary>
        /// <value>
        /// The responses.
        /// </value>
        public IList<IResponseWithStatusObject> Responses { get; }

        /// <summary>
        /// Gets the SDMX objects.
        /// </summary>
        /// <value>
        /// The SDMX objects.
        /// </value>
        public ISdmxObjects SdmxObjects { get; }
        
        /// <summary>
        /// Gets the response header.
        /// </summary>
        /// <value>
        /// The response header.
        /// </value>
        public IHeader ResponseHeader { get; }
    }
}
