﻿// -----------------------------------------------------------------------
// <copyright file="SubmitStructureConstant.cs" company="EUROSTAT">
//   Date Created : 2016-08-08
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.SubmitStructure
{
    public class SubmitStructureConstant
    {
        public enum ActionType
        {
            Append,
            Replace,
            Delete
        }

        public static string xmlTemplate = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
            "<mes:Structure " +
            "xmlns:mes=\"http://www.sdmx.org/resources/sdmxml/schemas/v2_1/message\">" +
            "  <mes:Header>" +
            "    <mes:ID>SUBMITSTRUCTURE</mes:ID> " +
            "    <mes:Test>false</mes:Test>" +
            "    <mes:Prepared>2014-05-06T21:53:11.874Z</mes:Prepared>" +
            "    <mes:Sender id=\"MG\"/>" +
            "    <mes:Receiver id=\"unknown\"/>" +
            "  </mes:Header> " +
            "</mes:Structure>";

    }
}
