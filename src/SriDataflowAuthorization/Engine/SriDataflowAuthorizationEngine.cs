﻿// -----------------------------------------------------------------------
// <copyright file="SriDataflowAuthorizationEngine.cs" company="EUROSTAT">
//   Date Created : 2016-02-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.DataflowAuthorization.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Nsi.AuthModule.Engine;
    using Estat.Nsi.AuthModule.Model;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Engine;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// The SDMX-RI implementation of <see cref="IDataflowAuthorizationEngine"/>
    /// </summary>
    public class SriDataflowAuthorizationEngine : IDataflowAuthorizationEngine
    {
        /// <summary>
        /// The _dataflow principal
        /// </summary>
        private readonly DataflowPrincipal _dataflowPrincipal;

        /// <summary>
        /// The authorization provider
        /// </summary>
        private readonly IAuthorizationProvider _authorizationProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriDataflowAuthorizationEngine" /> class.
        /// </summary>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <param name="authorizationProvider">The authorization provider.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataflowPrincipal" /> -or- <paramref name="authorizationProvider"/> is <see langword="null" />.</exception>
        public SriDataflowAuthorizationEngine(DataflowPrincipal dataflowPrincipal, IAuthorizationProvider authorizationProvider)
        {
            if (dataflowPrincipal == null)
            {
                throw new ArgumentNullException(nameof(dataflowPrincipal));
            }

            if (authorizationProvider == null)
            {
                throw new ArgumentNullException(nameof(authorizationProvider));
            }

            this._dataflowPrincipal = dataflowPrincipal;
            this._authorizationProvider = authorizationProvider;
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="structureReferences">The structure references.</param><param name="permission">The permission.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IEnumerable<IStructureReference> structureReferences, PermissionType permission)
        {
            if (!this._dataflowPrincipal.IsInRole(permission.ToString()))
            {
                return AuthorizationStatus.Denied;
            }

            if (structureReferences.Any(structureReference => this.GetAuthorization(structureReference, permission) == AuthorizationStatus.Authorized))
            {
                return AuthorizationStatus.Authorized;
            }

            return AuthorizationStatus.Denied;
        }

        /// <summary>
        /// Gets the authorization.
        /// </summary>
        /// <param name="structureReference">The structure reference.</param><param name="permission">The permission.</param>
        /// <returns>
        /// The <see cref="T:Estat.Sdmxsource.Extension.Constant.AuthorizationStatus"/>.
        /// </returns>
        public AuthorizationStatus GetAuthorization(IStructureReference structureReference, PermissionType permission)
        {
            if (!this._dataflowPrincipal.IsInRole(permission.ToString()))
            {
                return AuthorizationStatus.Denied;
            }

            if (structureReference != null && structureReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow)
            {
                return this._authorizationProvider.AccessControl(this._dataflowPrincipal.Identity.Name, structureReference) ? AuthorizationStatus.Authorized : AuthorizationStatus.Denied;
            }

            return AuthorizationStatus.Denied;
        }

        /// <summary>
        /// Gets the authorized dataflows.
        /// </summary>
        /// <returns>The <see cref="IList{IMaintainableRefObject}"/>.</returns>
        public IList<IMaintainableRefObject> RetrieveAuthorizedDataflows()
        {
            return this._authorizationProvider.GetDataflows(this._dataflowPrincipal.Identity.Name).ToArray();
        }
    }
}