﻿// -----------------------------------------------------------------------
// <copyright file="WsdlRegistry.cs" company="EUROSTAT">
//   Date Created : 2013-10-21
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;

namespace Estat.Sri.Ws.Wsdl
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     The WSDL registry.
    /// </summary>
    public sealed class WsdlRegistry
    {
        /// <summary>
        ///     The _WSDL map
        /// </summary>
        private readonly IDictionary<string, ServiceInfo> _serviceMap = new Dictionary<string, ServiceInfo>(StringComparer.Ordinal);

        /// <summary>
        ///     Gets or sets the WSDL REST URL.
        /// </summary>
        /// <value>
        ///     The WSDL rest URL.
        /// </value>
        public string WsdlRestAddress { get; set; }

        /// <summary>
        /// Adds the specified name.
        /// </summary>
        /// <param name="serviceInfo">
        /// The WSDL information.
        /// </param>
        /// <exception cref="ArgumentNullException"><paramref name="serviceInfo"/> is <see langword="null" />.</exception>
        public void Add(ServiceInfo serviceInfo)
        {
            if (serviceInfo == null)
            {
                throw new ArgumentNullException("serviceInfo");
            }

            this._serviceMap[serviceInfo.Name] = serviceInfo;
        }

        /// <summary>
        /// Adds the specified WSDL info.
        /// </summary>
        /// <param name="serviceInfos">The WSDL info.</param>
        /// <exception cref="ArgumentNullException"><paramref name="serviceInfos"/> is <see langword="null" />.</exception>
        public void Add(params ServiceInfo[] serviceInfos)
        {
            if (serviceInfos == null)
            {
                throw new ArgumentNullException("serviceInfos");
            }

            foreach (var wsdlInfo in serviceInfos)
            {
                this._serviceMap[wsdlInfo.Name] = wsdlInfo;
            }
        }

        public List<ServiceInfo> GetAll()
        {
            return this._serviceMap.Values.ToList();
        }
        /// <summary>
        /// Gets the WSDL path.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The WSDL path
        /// </returns>
        public ServiceInfo GetWsdlInfo(string name)
        {
            ServiceInfo servicePath;
            if (this._serviceMap.TryGetValue(name, out servicePath))
            {
                return servicePath;
            }

            return null;
        }
    }
}