// -----------------------------------------------------------------------
// <copyright file="SettingsFromConfigurationManager.cs" company="EUROSTAT">
//   Date Created : 2020-09-23
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Configuration
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Extensions.Configuration;

    public class SettingsFromConfigurationManager
    {
        private readonly IConfiguration _configuration;

        public SettingsFromConfigurationManager(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        private bool GetBool(string key)
        {
            var configurationSection = _configuration.GetSection(key);
            
            return configurationSection.Exists() 
                   && bool.TryParse(configurationSection.Value, out bool value) 
                   && value;
        }

        public AuthorizationConfiguration Authorization
        {
            get
            {
                IConfigurationSection configurationSection = _configuration.GetSection("authorization");
                if (configurationSection.Exists())
                {
                    return configurationSection.Get<AuthorizationConfiguration>();
                }

                return null;
            }
        }

        public bool AllowSdmx21Cross => GetBool("datastructure:allowSdmx21CrossSectional");

        public IList<string> VaryByHeaderList
        {
            get
            {
                IConfigurationSection configurationSection = _configuration.GetSection("httpHeaders:varyHeader:varyBy");
                if (configurationSection.Exists())
                {
                    return configurationSection.GetChildren().Select(x => x.Value).ToArray();
                }

                return null;
            }
        }

        public bool CreateStubCategories => GetBool("categorisation:createStubCategory");

        public MappingStoreConfig MappingStoreId
        {
            get
            {
                IConfigurationSection configurationSection = this._configuration.GetSection("mappingStore:Id");
                MappingStoreConfig config;
                if (configurationSection.Exists())
                {
                    config = configurationSection.Get<MappingStoreConfig>();
                }
                else
                {
                    config = new MappingStoreConfig()
                    {
                        Default = "MappingStoreServer",
                        FromQueryParameter = QueryParameterOption.AuthorizedOnly
                    };
                }

                return config;
            }
        }

        public string StructureUsage
        {
            get
            {
                IConfigurationSection configurationSection = this._configuration.GetSection("structureUsage:structureType");
                if (configurationSection.Exists())
                {
                    return configurationSection.Value;
                }

                return "dataflow";
            }
        }

        public string DatasetAction
        {
            get
            {
                IConfigurationSection configurationSection = this._configuration.GetSection("datasetAction");
                if (configurationSection.Exists())
                {
                    return configurationSection.Value;
                }

                return "Information";
            }
        }

        public bool EnableReleaseManagement => GetBool("enableReleaseManagement");

        public bool EnableResponseCompression => GetBool("enableResponseCompression");

        public bool ApplyContentConstraintsOnDataQueries => GetBool("applyContentConstraintsOnDataQueries");

        public bool AutoDeleteMappingSets => GetBool("autoDeleteMappingSets");

        public DisseminationDbConfig DisseminationDbConfig
        {
            get
            {
                IConfigurationSection dbTypeSection = this._configuration.GetSection("disseminationDbConnection:dbType");
                IConfigurationSection connectionStringSection = this._configuration.GetSection("disseminationDbConnection:connectionString");
                DisseminationDbConfig config = null;

                if (dbTypeSection.Exists() && !string.IsNullOrEmpty(dbTypeSection.Value) &&
                    connectionStringSection.Exists() && !string.IsNullOrEmpty(connectionStringSection.Value))
                {
                    config = new DisseminationDbConfig(dbTypeSection.Value, connectionStringSection.Value);
                }

                return config;
            }
        }

        public CultureInfo DefaultCultureInfo
        {
            get
            {
                IConfigurationSection connectionStringSection = this._configuration.GetSection("defaultCultureInfo");
                CultureInfo config = null;

                if (connectionStringSection.Exists() && !string.IsNullOrEmpty(connectionStringSection.Value))
                {
                    try
                    {
                        config = CultureInfo.GetCultureInfo(connectionStringSection.Value);
                    }
                    catch (CultureNotFoundException)
                    {
                        return null;
                    }
                }

                return config;
            }
        }

        public bool UseCultureSpecificColumnAndDecimalSeparators => GetBool("useCultureSpecificColumnAndDecimalSeparators");

        public bool UseHierarchicalCodelists => GetBool("useHierarchicalCodelists");

    }
}