namespace Estat.Sri.Ws.Configuration
{
    public class AuthorizationConfiguration
    {
        /// <summary>
        /// Gets or sets the authorization method
        /// </summary>
        /// <example>
        /// ```json
        /// {
        ///     "auth": { "method" : "estat.sri.ws.auth.dotstat" }
        /// }
        /// ```
        /// </example>
        public string Method { get; set; }

        // TODO enum {Thread, Context}
        public string PrincipalFrom { get; set; }
        public bool Enabled { get; set; }
    }
}
