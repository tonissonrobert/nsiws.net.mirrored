using Microsoft.Extensions.Configuration;

namespace Estat.Sri.Ws.Configuration
{
    public static class AuthorizationConfigurationUtils
    {
        public static AuthorizationConfiguration GetConfiguration(IConfiguration configuration)
        {
            return new SettingsFromConfigurationManager(configuration).Authorization;
        }
    }
}
