// -----------------------------------------------------------------------
// <copyright file="NSIEstatV20ServiceErrorHandler.cs" company="EUROSTAT">
//   Date Created : 2017-06-09
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sri.Ws.Controllers.Constants;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace Estat.Sri.Ws.Soap
{
    using System;
    using System.Threading.Tasks;
    using Estat.Sri.Ws.Controllers.Builder;
    using Estat.Sri.Ws.Controllers.Factory;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The SRI SDMX v 20 service error handler.
    /// </summary>
    public class NSIEstatV20ServiceErrorHandler : INSIEstatV20Service
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NSIEstatV20ServiceErrorHandler));

        /// <summary>
        /// The decorated service
        /// </summary>
        private readonly INSIEstatV20Service _decoratedService;

        /// <summary>
        ///     The _fault builder
        /// </summary>
        private readonly IMessageFaultSoapBuilder _messageFaultBuilder;

        /// <summary>
        /// Initializes a new instance of the <see cref="NSIEstatV20ServiceErrorHandler" /> class.
        /// </summary>
        /// <param name="decoratedService">The decorated service.</param>
        /// <param name="messageFaultFactory">The message fault factory.</param>
        public NSIEstatV20ServiceErrorHandler(INSIEstatV20Service decoratedService, IMessageFaultSoapBuilderFactory messageFaultFactory)
        {
            this._decoratedService = decoratedService;
            this._messageFaultBuilder = messageFaultFactory.GetMessageFaultSoapBuilder(SdmxSchemaEnumType.VersionTwo, SoapNamespaces.SdmxV20Estat);
        }

        public async Task HandleRequest(IReadableDataLocation request, SoapOperation soapOperation, HttpContext context)
        {
            try
            {
                await this._decoratedService.HandleRequest(request, soapOperation, context);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw this._messageFaultBuilder.BuildException(e, soapOperation.ToString());
            }
        }
    }
}