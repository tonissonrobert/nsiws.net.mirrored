﻿// -----------------------------------------------------------------------
// <copyright file="NsiV20ServiceBase.cs" company="EUROSTAT">
//   Date Created : 2013-10-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Controllers.Utils;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Ws.Soap
{
    using System.Net;
    using System.Threading.Tasks;
    using System.Xml;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Extension;
    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///     The SDMX  v2.0 service base class.
    /// </summary>
    public abstract class NsiV20ServiceBase
    {
        /// <summary>
        /// The data request controller
        /// </summary>
        private readonly IDataRequestController _dataRequestController;

        /// <summary>
        /// The structure request controller
        /// </summary>
        private readonly IStructureRequestController _requestController;

        /// <summary>
        /// Initializes a new instance of the <see cref="NsiV20ServiceBase"/> class.
        /// </summary>
        /// <param name="dataRequestController">The data request controller.</param>
        /// <param name="messageBuilderManager">The message builder manager.</param>
        /// <param name="requestController">The request controller.</param>I
        protected NsiV20ServiceBase(IDataRequestController dataRequestController, IStructureRequestController requestController)
        {
            this._dataRequestController = dataRequestController;
            this._requestController = requestController;
        }

        /// <summary>
        ///     Gets the type of the endpoint.
        /// </summary>
        /// <value>
        ///     The type of the endpoint.
        /// </value>
        protected abstract WebServiceEndpoint EndpointType { get; }

        /// <summary>
        ///     Gets the namespace
        /// </summary>
        /// <value>
        ///     The namespace
        /// </value>
        protected abstract string Ns { get; }

        public async Task HandleRequest(IReadableDataLocation request, SoapOperation soapOperation, HttpContext context)
        {
            IStreamController<XmlWriter> streamController;
            if (soapOperation.IsOneOf(SoapOperation.GetCompactData, SoapOperation.GetCrossSectionalData, SoapOperation.GetGenericData))
            {
                var dataType = soapOperation.GetDataType(SdmxSchemaEnumType.VersionTwo);
                var soapRequest = new SoapDataRequest(soapOperation.ToString(), dataType, this.EndpointType.GetSdmxExtension());
                streamController = this._dataRequestController.ParseRequest(request, soapRequest, Ns, null);
                
            }
            else if (soapOperation == SoapOperation.QueryStructure)
            {
                streamController = this._requestController.ParseRequest(request, this.EndpointType, this.Ns, null);
            }
            else
            {
                throw new ServiceException(HttpStatusCode.NotImplemented); 
            }

            await XmlHelper.WriteToOutput(context, streamController);
        }

    }
}