// -----------------------------------------------------------------------
// <copyright file="NsiV21ServiceBase.cs" company="EUROSTAT">
//   Date Created : 2016-08-16
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Util.Extensions;

namespace Estat.Sri.Ws.Soap
{
    using System.Threading.Tasks;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Controllers.Builder;
    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Extension;
    using Estat.Sri.Ws.Controllers.Manager;
    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// The base class for NSI v21 services
    /// </summary>
    internal class NsiV21ServiceBase
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(NsiV21ServiceBase));

        /// <summary>
        ///     The _fault builder
        /// </summary>
        private readonly MessageFaultSoapv21Builder _messageFaultBuilder = new MessageFaultSoapv21Builder();

        /// <summary>
        /// The data request controller
        /// </summary>
        private readonly IDataRequestController _dataRequestController;

        /// <summary>
        ///     The Name-space of SDMX v2.1 services.
        /// </summary>
        private readonly string _ns;

        /// <summary>
        /// The structure request controller
        /// </summary>
        private readonly IStructureRequestController _requestController;

        /// <summary>
        /// Initializes a new instance of the <see cref="NsiV21ServiceBase"/> class.
        /// </summary>
        /// <param name="dataRequestController">
        /// The data request controller.
        /// </param>
        /// <param name="requestController">
        /// The request controller.
        /// </param>
        /// <param name="soapNamespace">
        /// The SOAP namespace.
        /// </param>
        public NsiV21ServiceBase(IDataRequestController dataRequestController, IStructureRequestController requestController, string soapNamespace)
        {
            this._dataRequestController = dataRequestController;
            this._requestController = requestController;
            this._ns = soapNamespace;
        }

        public async Task HandleRequest(IReadableDataLocation request, SoapOperation soapOperation, HttpContext context)
        {
            IStreamController<XmlWriter> streamController;
            if (soapOperation.IsOneOf(SoapOperation.GetGenericData, SoapOperation.GetStructureSpecificData))
            {
                var dataType = soapOperation.GetDataType(SdmxSchemaEnumType.VersionTwoPointOne);
                var soapRequest = new SoapDataRequest(soapOperation.ToString(), dataType, SdmxExtension.None);
                streamController = this._dataRequestController.ParseRequest(request, soapRequest, SoapNamespaces.SdmxV21, null);
            }
            else if (soapOperation.GetMessageType() == MessageEnumType.Structure)
            {
                streamController = this._requestController.ParseRequest(request, soapOperation);
            }
            else
            {
                throw new SdmxNotImplementedException(soapOperation.ToString()); 
            }

            await XmlHelper.WriteToOutput(context, streamController);
        }
    }
}