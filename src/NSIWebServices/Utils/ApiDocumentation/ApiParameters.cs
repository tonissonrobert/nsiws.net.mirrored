// -----------------------------------------------------------------------
// <copyright file="ApiParameterValues.cs" company="EUROSTAT">
//   Date Created : 2021-09-17
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.ApiDocumentation
{
    using System.Collections.Generic;
    using Microsoft.OpenApi.Models;

    public enum ApiParameter
    {
        CONTEXT, ITEM_SCHEME_TYPE, STRUCTURE_TYPE, FLOW, KEY, PROVIDER,
        AC_REFERENCES_V1, AC_REFERENCES_V2, REFERENCES_V1, REFERENCES_V2, MODE_V1, MODE_V2, C, DETAIL, STRUCT_DETAIL_V1, STRUCT_DETAIL_V2,
        ACCEPT_ENCODING, UPDATE_AFTER, START_PERIOD, END_PERIOD, DIMENSION_AT_OBSERVATION, ATTRIBUTES, MEASURES, X_LEVEL
    }

    public static class ApiParameterInfo
    {
        private class ApiParameterObject
        {
            public string[] AllowedValues { get; set; } = {};
            public string Description { get; set; }
            public string DataType { get; set; } = "string";
            public string Pattern { get; set; }
            public string DefaultValue { get; set; }
            public ParameterStyle? Style { get; set; }
            public string Example { get; set; }
        }
        
        /// <summary>
        /// The attributes associated to <see cref="ApiParameter"/>s.
        /// </summary>
        private static readonly Dictionary<ApiParameter, ApiParameterObject> _instances =
            new Dictionary<ApiParameter, ApiParameterObject>
            { 
                {
                    ApiParameter.CONTEXT, new ApiParameterObject()
                    {
                        AllowedValues = new []{ "dataflow", "datastructure", "provisionagreement", "*" },
                        Description = "The context for data retrieval. All possible contexts can be selected using <code>*</code>.",
                        DefaultValue = "*"
                    }
                },
                {
                    ApiParameter.ITEM_SCHEME_TYPE, new ApiParameterObject()
                    {
                        AllowedValues = new []{ "conceptscheme", "codelist", "categoryscheme", "agencyscheme", "dataproviderscheme",
                            "dataconsumerscheme", "organisationunitscheme", "transformationscheme", "rulesetscheme", "userdefinedoperatorscheme",
                            "customtypescheme", "namepersonalisationscheme", "vtlmappingscheme", "valuelist", "structure" },
                        DefaultValue = "structure",
                        Description = "The type of item scheme (e.g. codelist, agencyscheme, etc.)"
                    }
                },
                {
                  ApiParameter.STRUCTURE_TYPE, new ApiParameterObject()
                  {
                      AllowedValues = new [] { "datastructure", "metadatastructure", "dataflow", "metadataflow", "provisionagreement",
                          "structureset", "process", "categorisation", "dataconstraint", "metadataconstraint", "conceptscheme", "codelist",
                          "categoryscheme", "hierarchy", "hierarchyassociation", "agencyscheme", "dataproviderscheme", "dataconsumerscheme",
                          "organisationunitscheme", "transformationscheme", "rulesetscheme", "userdefinedoperatorscheme", "customtypescheme",
                          "namepersonalisationscheme", "vtlmappingscheme", "valuelist", "structuremap", "representationmap", "conceptschememap",
                          "categoryschememap", "organisationschememap", "reportingtaxonomymap", "structure" },
                      DefaultValue = "structure",
                      Description = "The type of structural metadata (e.g. codelist, dataflow, etc.)"
                  }  
                },
                {
                    ApiParameter.FLOW, new ApiParameterObject()
                    {
                        Description = "<p>The <strong>statistical domain</strong> (aka dataflow) of the data to be returned.</p><p>Examples:</p><ul>" +
                            "<li><code>EXR</code>: The ID of the domain</li><li><code>ECB,EXR</code>: The EXR domain, maintained by the ECB</li>" +
                            "<li><code>ECB,EXR,1.0</code>: Version 1.0 of the EXR domain, maintained by the ECB</li></ul>",
                        Pattern = @"^([a-zA-Z][a-zA-Z\d_-]*(\.[a-zA-Z][a-zA-Z\d_-]*)*,)?[a-zA-Z\d_@$-]+(,(latest|(\d+(\.\d+)*)))?$"
                    }  
                },
                {
                    ApiParameter.KEY, new ApiParameterObject()
                    {
                        Description = "<p>The (possibly partial) <strong>key identifying the data to be returned</strong>.</p><p>The keyword <code>all</code> can" +
                            " be used to indicate that all data belonging to the specified dataflow and provided by the specified provider must be returned.</p>" +
                            "<p>The examples below are based on the following key: Frequency, Country, Component of inflation, Unit of measure.</p><ul><li><code>" +
                            "M.DE.000000.ANR</code>: Full key, matching exactly one series, i.e. the monthly (<code>M</code>) rates of change (<code>ANR</code>) of " +
                            "overall inflation (<code>000000</code>) in Germany (<code>DE</code>).</li><li><code>A+M.DE.000000.ANR</code>: Retrieves both annual and " +
                            "monthly data (<code>A+M</code>), matching exactly two series</li><li><code>A+M..000000.ANR</code>: The second dimension is wildcarded, and " +
                            "it wil therefore match the annual and monthly rates of change of overall inflation in any country.</li></ul>",
                        Pattern = @"^([\.A-Za-z\d_@$-]+(\+[A-Za-z\d_@$-]+)*)*$"
                    }  
                },
                {
                    ApiParameter.PROVIDER, new ApiParameterObject()
                    {
                        Description = "<p>The <strong>provider of the data</strong> to be retrieved.</p><p>The keyword <code>all</code> can be used to indicate that " +
                            "all data matching the supplied key and belonging to the specified dataflow and provided by any data provider must be returned.</p>" +
                            "<p>Examples:</p><ul><li><code>ECB</code>: Data provided by ECB</li><li><code>CH2+NO2</code>: Data provided by CH2 or NO2</li></ul>",
                        Pattern = @"^(([A-Za-z][A-Za-z\d_-]*)(\.[A-Za-z][A-Za-z\d_-]*)*,)?[A-Za-z\d_@$-]+(\+([A-Za-z][A-Za-z\d_-]*(\.[A-Za-z][A-Za-z\d_-]*)*,)?[A-Za-z\d_@$-]+)*$"
                    }
                },
                {
                    ApiParameter.AC_REFERENCES_V1, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "none", "all", "datastructure", "conceptscheme", "codelist", "dataproviderscheme", "dataflow" },
                        Description = "Instructs the web service to return (or not) the artefacts referenced by the ContentConstraint to be returned.",
                        DefaultValue = "none"
                    }
                },
                {
                    ApiParameter.AC_REFERENCES_V2, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "none", "all", "datastructure", "conceptscheme", "codelist", "dataproviderscheme", "dataflow" },
                        Description = "Instructs the web service to return (or not) the artefacts referenced by the ContentConstraint to be returned." +
                                      "<p>Multiple values are supported.</p>",
                        DataType = "array"
                    }
                },
                {
                    ApiParameter.REFERENCES_V1, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "none", "parents", "parentsandsiblings", "children", "descendants", "all", "datastructure", "metadatastructure", 
                            "categoryscheme", "conceptscheme", "codelist", "hierarchicalcodelist", "organisationscheme", "agencyscheme", "dataproviderscheme", 
                            "dataconsumerscheme", "organisationunitscheme", "dataflow", "metadataflow", "reportingtaxonomy", "provisionagreement", "structureset", 
                            "process", "categorisation", "contentconstraint", "actualconstraint", "allowedconstraint", "attachmentconstraint", "transformationscheme", 
                            "rulesetscheme", "userdefinedoperatorscheme", "customtypescheme", "namepersonalisationscheme", "namealiasscheme" },
                        DefaultValue = "none",
                        Description = "<p>Instructs the web service to return (or not) the artefacts referenced by the artefact to be returned.</p>" +
                            "<p>Possible values are:</p><ul><li><code>none</code>: No references will be returned</li><li><code>parents</code>: " +
                            "Returns the artefacts that use the artefact matching the query</li><li><code>parentsandsiblings</code>: Returns the artefacts that " +
                            "use the artefact matching the query, as well as the artefacts referenced by these artefacts</li><li><code>children</code>: " +
                            "Returns the artefacts referenced by the artefact to be returned</li><li><code>descendants</code>: References of references, " +
                            "up to any level, will be returned</li><li><code>all</code>: The combination of parentsandsiblings and descendants</li>" +
                            "<li>In addition, a concrete type of resource may also be used (for example, references=codelist).</li></ul>",
                    }
                },
                {
                    ApiParameter.REFERENCES_V2, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "none", "parents", "parentsandsiblings", "ancestors", "children", "descendants", "all", "datastructure", 
                            "metadatastructure", "categoryscheme", "conceptscheme", "codelist", "hierarchy", "hierarchyassociation", "agencyscheme",
                            "dataproviderscheme", "dataconsumerscheme", "organisationunitscheme", "dataflow", "metadataflow", "reportingtaxonomy",
                            "provisionagreement", "structureset", "process", "categorisation", "dataconstraint", "metadataconstraint", "transformationscheme",
                            "rulesetscheme", "userdefinedoperatorscheme", "customtypescheme", "namepersonalisationscheme", "namealiasscheme", "valuelist",
                            "structuremap", "representationmap", "conceptschememap", "categoryschememap", "organisationschememap", "reportingtaxonomymap" },
                        DefaultValue = "none",
                        Description = "Instructs the web service to return (or not) the artefacts referenced by the artefact to be returned " +
                                      "(for example, the code lists and concepts used by the data structure definition matching the query), " +
                                      "as well as the artefacts that use the matching artefact (for example, the dataflows that use the data " +
                                      "structure definition matching the query).",
                        DataType = "array"
                    }
                },
                {
                    ApiParameter.MODE_V1, new ApiParameterObject()
                    {
                        AllowedValues = new []{ "exact", "available" },
                        Description = "Instructs the web service to return a ContentConstraint which defines a Cube Region containing values which will be returned " +
                                      "by executing the query (mode='exact') vs a Cube Region showing what values remain valid selections that could be added to the " +
                                      "data query (mode='available'). A valid selection is one which results in one or more series existing for the selected value, " +
                                      "based on the current data query selection state defined by the current path parameters.",
                        DefaultValue = "exact"
                    }
                },
                {
                    ApiParameter.MODE_V2, new ApiParameterObject()
                    {
                        AllowedValues = new []{ "exact", "available" },
                        Description = "Instructs the web service to return a ContentConstraint which defines a Cube Region containing " +
                                      "values which will be returned by executing the query (<code>exact</code>) vs a Cube Region showing " +
                                      "what values remain valid selections that could be added to the data query (<code>available</code>).",
                        DefaultValue = "exact"
                    }
                },
                {
                    ApiParameter.C, new ApiParameterObject()
                    {
                        Description = "<div class=\"renderedMarkdown\"><p>Filter data by component value (e.g. <code>c[FREQ]=A</code>)." +
                                      "</p><p>Multiple values are supported.</p><p>In addition, operators may be used:</p><table><thead>" +
                                      "<tr><th>Operator</th><th>Meaning</th><th>Note</th></tr></thead><tbody><tr><td>eq</td><td>Equals</td>" +
                                      "<td>Default if no operator is specified and there is only one value (e.g. <code>c[FREQ]=M</code> is " +
                                      "equivalent to <code>c[FREQ]=eq:M</code>)</td></tr><tr><td>ne</td><td>Not equal to</td></tr>" +
                                      "<tr><td>lt</td><td>Less than</td></tr><tr><td>le</td><td>Less than or equal to</td></tr><tr><td>gt</td>" +
                                      "<td>Greater than</td></tr><tr><td>ge</td><td>Greater than or equal to</td></tr><tr><td>co</td>" +
                                      "<td>Contains</td></tr><tr><td>nc</td><td>Does not contain</td></tr><tr><td>sw</td><td>Starts with</td>" +
                                      "</tr><tr><td>ew</td><td>Ends with</td></tr></tbody></table><p>Operators appear as prefix to the component " +
                                      "value(s) and are separated from it by a <code>:</code> (e.g. <code>c[TIME_PERIOD]=ge:2020-01+le:2020-12</code>).</p></div>",
                        DataType = "object",
                        Style = ParameterStyle.DeepObject,
                        Example = "{ \"TIME_PERIOD\": \"ge:2020-01+le:2020-12\", \"FREQ\": \"M\" }"
                    }
                },
                {
                    ApiParameter.DETAIL, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "full", "dataonly", "serieskeysonly", "nodata" },
                        Description = "<p>The <strong>amount of information</strong> to be returned.</p><p>Possible options are:</p><ul><li><code>full</code>: " +
                            "All data and documentation</li><li><code>dataonly</code>: Everything except attributes</li><li><code>serieskeysonly</code>: " +
                            "The series keys. This is useful to return the series that match a certain query, without returning the actual data (e.g. overview page)" +
                            "</li><li><code>nodata</code>: The series, including attributes and annotations, without observations.</li></ul>",
                       DefaultValue = "full"
                    }  
                },
                {
                    ApiParameter.STRUCT_DETAIL_V1, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "allstubs", "referencestubs", "referencepartial", "allcompletestubs", "referencecompletestubs", "full" },
                        Description = "<p>The amount of information to be returned.</p><p>Possible values are:</p><ul><li><code>allstubs</code>: " +
                            "All artefacts should be returned as stubs, containing only identification information, as well as the artefacts' name</li>" +
                            "<li><code>referencestubs</code>: Referenced artefacts should be returned as stubs, containing only identification information, " +
                            "as well as the artefacts' name</li><li><code>referencepartial</code>: Referenced item schemes should only include items used by " +
                            "the artefact to be returned. For example, a concept scheme would only contain the concepts used in a DSD, and its isPartial flag would " +
                            "be set to <code>true</code></li><li><code>allcompletestubs</code>: All artefacts should be returned as complete stubs, containing " +
                            "identification information, the artefacts' name, description, annotations and isFinal information</li><li><code>referencecompletestubs" +
                            "</code>: Referenced artefacts should be returned as complete stubs, containing identification information, the artefacts' name, description, " +
                            "annotations and isFinal information</li><li><code>full</code>: All available information for all artefacts should be returned</li></ul>",
                        DefaultValue = "full"
                    }  
                },
                {
                    ApiParameter.STRUCT_DETAIL_V2, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "full", "allstubs", "referencestubs", "allcompletestubs", "referencecompletestubs", "referencepartial", "raw" },
                        Description = "The desired amount of information to be returned.",
                        DefaultValue = "full"
                    }  
                },
                {
                    ApiParameter.ACCEPT_ENCODING, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "br", "compress", "deflate", "exi", "gzip", "identity", "pack200-gzip", "zstd" },
                        DefaultValue = "identity"
                    }
                },
                {
                    ApiParameter.UPDATE_AFTER, new ApiParameterObject()
                    {
                        Description = "<p>The last time the query was performed by the client.</p>" + 
                                      "<p>If this parameter is used, the returned message should only include the dimension values " +
                                      "for the data that have changed since that point in time (updates and revisions).</p>"
                    }
                },
                {
                    ApiParameter.START_PERIOD, new ApiParameterObject()
                    {
                        Description = "<p>The start of the period for which results should be supplied (inclusive).</p><p>Can be expressed using 8601 " +
                            "dates or SDMX reporting periods.</p><p>Examples:</p><ul><li><code>2000</code>: Year (ISO 8601)</li><li><code>2000-01</code>: " +
                            "Month (ISO 8601)</li><li><code>2000-01-01</code>: Date (ISO 8601)</li><li><code>2000-Q1</code>: Quarter (SDMX)</li>" +
                            "<li><code>2000-W01</code>: Week (SDMX)</li></ul>",
                        Pattern = @"^\d{4}-?((\d{2}(-\d{2})?)|A1|S[1|2]|Q[1-4]|T[1-3]|M(0[1-9]|1[0-2])|W(0[1-9]|[1-4][0-9]|5[0-3])|D(0[0-9][1-9]|[1-2][0-9][0-9]|3[0-5][0-9]|36[0-6]))?$"
                    }
                },
                {
                    ApiParameter.END_PERIOD, new ApiParameterObject()
                    {
                        Description = "<p>The end of the period for which results should be supplied (inclusive).</p><p>Can be expressed using 8601 " +
                            "dates or SDMX reporting periods.</p><p>Examples:</p><ul><li><code>2000</code>: Year (ISO 8601)</li><li><code>2000-01</code>: " +
                            "Month (ISO 8601)</li><li><code>2000-01-01</code>: Date (ISO 8601)</li><li><code>2000-S1</code>: Semester (SDMX)</li>" +
                            "<li><code>2000-D001</code>: Day (SDMX)</li></ul>",
                        Pattern = @"^\d{4}-?((\d{2}(-\d{2})?)|A1|S[1|2]|Q[1-4]|T[1-3]|M(0[1-9]|1[0-2])|W(0[1-9]|[1-4][0-9]|5[0-3])|D(0[0-9][1-9]|[1-2][0-9][0-9]|3[0-5][0-9]|36[0-6]))?$"
                    }
                },
                {
                    ApiParameter.DIMENSION_AT_OBSERVATION, new ApiParameterObject()
                    {
                        Description = "<p>Indicates <strong>how the data should be packaged</strong>.</p><p>The options are:</p><ul><li><code>TIME_PERIOD</code>: " +
                            "A timeseries view</li><li>The ID of any other dimension: A cross-sectional view of the data</li><li><code>AllDimensions</code>: " +
                            "A flat view of the data.</li></ul>",
                        Pattern = @"^[A-Za-z][A-Za-z\d_-]*$",
                        DefaultValue = "TIME_PERIOD"
                    }
                },
                {
                    ApiParameter.MEASURES, new ApiParameterObject()
                    {
                        AllowedValues = new []{ "all", "none" },
                        Description = "<p>This parameter specifies the measures to be returned. Possible options are: <code>all</code> (all measures), " +
                                      "<code>none</code> (no measure), <code>{measure_id}</code>: The ID of one or more measures the caller is interested in.</p>",
                        DefaultValue = "all"
                    }
                },
                {
                    ApiParameter.ATTRIBUTES, new ApiParameterObject()
                    {
                        AllowedValues = new []{ "all", "none", "dsd", "msd" },
                        Description = "<p>This parameter specifies the attributes to be returned. Possible options are: <code>dsd</code> " +
                                      "(all the attributes defined in the data structure definition), <code>msd</code> (all the reference metadata attributes), " +
                                      "<code>dataset</code> (all the attributes attached to the dataset-level), <code>series</code> (all the attributes attached to the series-level), " +
                                      "<code>obs</code> (all the attributes attached to the observation-level), <code>all</code> (all attributes), <code>none</code>" +
                                      " (no attributes), <code>{attribute_id}</code>: The ID of one or more attributes the caller is interested in.</p>",
                        DefaultValue = "dsd"
                    }
                },
                {
                    ApiParameter.X_LEVEL, new ApiParameterObject()
                    {
                        AllowedValues = new [] { "all", "upperOnly", "currentOnly" },
                        DefaultValue = "all",
                        Description = "<p>This parameter specifies the level of metadata to be returned. Possible options are: " +
                                      "<ul><li>all - all user filtered dims: value or '~', all non filtered dims: no filter applied</li>" +
                                      "<li>upperOnly - all user filtered dims: value or '~', all non filtered dims: is '~'</li>" +
                                      "<li>currentOnly - all user filtered dims: value, all non filtered dims: is '~'</li></ul></p>"
                    }
                }
            };

        #region GetPropertyFor

        public static string[] GetValuesFor(ApiParameter parameter)
        {
            return _instances[parameter].AllowedValues;
        }

        public static string GetDescriptionFor(ApiParameter parameter)
        {
            return _instances[parameter].Description;
        }
        
        public static string GetDataTypeFor(ApiParameter parameter)
        {
            return _instances[parameter].DataType;
        }
        
        public static string GetDefaultValueFor(ApiParameter parameter)
        {
            return _instances[parameter].DefaultValue;
        }

        public static string GetPatternFor(ApiParameter parameter)
        {
            return _instances[parameter].Pattern;
        }

        public static ParameterStyle? GetStyleFor(ApiParameter parameter)
        {
            return _instances[parameter].Style;
        }

        public static string GetExampleFor(ApiParameter parameter)
        {
            return _instances[parameter].Example;
        }

        #endregion
    }
}