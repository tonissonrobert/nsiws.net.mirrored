﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerResponseAttribute.cs" company="EUROSTAT">
//   Date Created : 2021-09-15
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.ApiDocumentation
{
    using System;
    
    // ReSharper disable ClassNeverInstantiated.Global

    /// <summary>
    /// Controller method attribute, meant to provide the success response type of the endpoint.
    /// According to the <see cref="Name"/> and <see cref="ApiVersion"/>, the specifications can return the available content types.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class ProducesApiResponseTypeAttribute : Attribute
    {
        public ProducesApiResponseTypeAttribute(ApiConstants.SuccessResponseType name, ApiConstants.SupportedApiVersion apiVersion)
        {
            this.Name = name;
            this.ApiVersion = apiVersion;
        }

        public ApiConstants.SuccessResponseType Name { get; }

        public ApiConstants.SupportedApiVersion ApiVersion { get; }
    }
}