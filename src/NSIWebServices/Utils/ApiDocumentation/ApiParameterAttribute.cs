﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerQueryParameterAttribute.cs" company="EUROSTAT">
//   Date Created : 2021-09-14
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.ApiDocumentation
{
    using System;
    using Microsoft.OpenApi.Models;

    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class ApiParameterAttribute : Attribute
    {
        public ApiParameterAttribute(string name, ParameterLocation location)
        {
            this.Name = name;
            this.Location = location;
        }

        /// <summary>
        /// Initializes a new <see cref="ApiParameterAttribute"/>,
        /// that takes information from a <see cref="ApiParameter"/>.
        /// </summary>
        /// <param name="name">The name of the parameter to apply to.</param>
        /// <param name="location">The location of the parameter.</param>
        /// <param name="parameterType">The type of parameter to get information from.</param>
        public ApiParameterAttribute(string name, ParameterLocation location, ApiParameter parameterType)
            :this(name, location)
        {
            this.AvailableValues = ApiParameterInfo.GetValuesFor(parameterType);
            this.Description = ApiParameterInfo.GetDescriptionFor(parameterType);
            this.DataType = ApiParameterInfo.GetDataTypeFor(parameterType);
            this.DefaultValue = ApiParameterInfo.GetDefaultValueFor(parameterType);
            this.Pattern = ApiParameterInfo.GetPatternFor(parameterType);
            this.Style = ApiParameterInfo.GetStyleFor(parameterType);
            this.Example = ApiParameterInfo.GetExampleFor(parameterType);
        }

        public string Name { get; }
        
        public ParameterLocation Location { get; }
        
        public string Description { get; set; }
        
        public bool Required { get; set; }
        
        /// <summary>
        /// The data type; defaults to 'string'
        /// </summary>
        public string DataType { get; set; } = "string";

        /// <summary>
        /// the data format
        /// </summary>
        public string Format { get; set; }

        /// <summary>
        /// The matching pattern for the data
        /// </summary>
        public string Pattern { get; set; }

        /// <summary>
        /// The list of available values
        /// </summary>
        public string[] AvailableValues { get; set; } = {};

        public string DefaultValue { get; set; }

        public ParameterStyle? Style { get; set; }

        public string Example { get; set; }
    }
}