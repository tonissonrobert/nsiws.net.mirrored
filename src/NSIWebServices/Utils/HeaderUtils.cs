// -----------------------------------------------------------------------
// <copyright file="HeaderUtils.cs" company="EUROSTAT">
//   Date Created : 2013-10-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Controllers.Utils;
using Estat.Sri.Ws.Configuration;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Rest.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;

    using Estat.Sdmxsource.Extension.Constant;

    using log4net;
    using Microsoft.Extensions.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Factory;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Manager;
    using Org.Sdmxsource.Sdmx.SdmxQueryBuilder.Model;

    /// <summary>
    /// The header utils.
    /// </summary>
    internal static class HeaderUtils
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(HeaderUtils));

        private static readonly string[] _nonSdmxQueryParameters = { "sid", "annotations", "format" , "formatVersion", "numberOfErrors" };

        private static readonly IStructureQueryFormat<string> structureQueryFormat = new RestQueryFormat();
        private static readonly IStructureQueryBuilderManager structureQueryBuilderManager = new StructureQueryBuilderManager(new RestStructureQueryFactory());
        /// <summary>
        /// The structure match
        /// </summary>
        private static readonly Regex _structureMatch = new Regex(@"^(datastructure|metadatastructure|categoryscheme|conceptscheme|codelist|hierarchicalcodelist|organisationscheme|agencyscheme|dataproviderscheme|dataconsumerscheme|organisationunitscheme|dataflow|metadataflow|reportingtaxonomy|provisionagreement|structureset|process|categorisation|contentconstraint|attachmentconstraint|structure|actualconstraint|allowedconstraint)$", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        private static readonly char[] _invalidFileNameChars = { '*', '\"', '<', '>', '|', ':', '/', '\\', '?' };
        #endregion:

        /// <summary>
        /// Determines whether the specified structure is valid
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <returns>
        ///   <c>true</c> if structure is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsStructureValid(string structure)
        {
            if (structure == null)
            {
                throw new ArgumentNullException(nameof(structure));
            }

            Match match = _structureMatch.Match(structure);
            return match.Success;
        }

        /// <summary>
        /// Builds the rest query bean.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency id.</param>
        /// <param name="resourceId">The resource id.</param>
        /// <param name="version">The version.</param>
        /// <param name="queryParameters">The query parameters.</param>
        /// <returns>
        /// The <see cref="IRestStructureQuery" />.
        /// </returns>
        /// <exception cref="WebFaultException{String}">An exception is thrown</exception>
        public static IRestStructureQuery BuildRestQueryBean(string structure, string agencyId, string resourceId, string version, string filter, HttpContext context)
        {
            var queryString = new string[5];
            queryString[0] = structure;
            queryString[1] = agencyId;
            queryString[2] = resourceId;
            queryString[3] = version;
            queryString[4] = filter;
            var paramsDict = HeaderUtils.GetQueryStringAsDict(context);

            IRestStructureQuery query;

            try
            {
                query = new RESTStructureQueryCore(queryString, paramsDict);
            }
            catch (SdmxException e)
            {
                _logger.Error(e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                throw new ServiceException(HttpStatusCode.BadRequest, e);
            }

            return query;
        }

        /// <summary>
        /// Builds a rest query object with multiple beans.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyIds">The agency id.</param>
        /// <param name="resourceIds">The resource id.</param>
        /// <param name="versions">The version.</param>
        /// <param name="filter"></param>
        /// <param name="context">The http request context.</param>
        /// <returns>
        /// The <see cref="IRestStructureQuery" />.
        /// </returns>
        /// <exception cref="WebFaultException{String}">An exception is thrown</exception>
        public static IRestStructureQuery BuildRestQueryMultipleBeans(string structure, string agencyIds, string resourceIds, string versions, string filter, HttpContext context)
        {
            var queryString = new string[5];
            queryString[0] = structure;
            queryString[1] = agencyIds;
            queryString[2] = resourceIds;
            queryString[3] = versions;
            queryString[4] = filter;
            var paramsDict = HeaderUtils.GetQueryStringAsDict(context);

            IRestStructureQuery query;

            try
            {
                query = new RESTStructureQueryCoreV2(queryString, paramsDict);
            }
            catch (SdmxException e)
            {
                _logger.Error(e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                throw new ServiceException(HttpStatusCode.BadRequest, e);
            }

            return query;
        }

        /// <summary>
        /// Gets the type of the on the fly annotation.
        /// </summary>
        /// <param name="queryParameters">The query parameters.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">queryParameters</exception>
        public static OnTheFlyAnnotationType GetOnTheFlyAnnotationType(NameValueCollection queryParameters)
        {
            if (queryParameters == null)
            {
                throw new ArgumentNullException("queryParameters");
            }

            var annotationsValues = queryParameters["annotations"];
            if (string.IsNullOrWhiteSpace(annotationsValues))
            {
                return OnTheFlyAnnotationType.None;
            }

            var values = annotationsValues.Split(',');
            OnTheFlyAnnotationType returnValue = OnTheFlyAnnotationType.None;
            foreach (var value in values)
            {
                OnTheFlyAnnotationType annotationType;
                if (Enum.TryParse(value, true, out annotationType))
                {
                    returnValue |= annotationType;
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Gets the query string as dictionary.
        /// </summary>
        /// <param name="queryParameters">The query parameters.</param>
        /// <returns>The <see cref="IDictionary{String, String}" />.</returns>
        /// <exception cref="WebFaultException">Bad request</exception>
        /// <exception cref="ArgumentNullException"><paramref name="queryParameters"/> is <see langword="null" />.</exception>
        public static IDictionary<string, string> GetQueryStringAsDict(HttpContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            IDictionary<string, string> paramsDict = new Dictionary<string, string>();
            var enumQ = context.Request.Query.GetEnumerator();
            while (enumQ.MoveNext())
            {
                var queryName = enumQ.Current.Key;
                var queryValue = enumQ.Current.Value.FirstOrDefault();
                if (paramsDict.ContainsKey(queryName))
                {
                    _logger.Error("Duplicate parameters values is semantically error");
                    throw new ServiceException(HttpStatusCode.BadRequest);
                }

                if (!_nonSdmxQueryParameters.Contains(queryName))
                {
                    paramsDict.Add(queryName, queryValue);
                }
            }

            return paramsDict;
        }

        /// <summary>
        /// Build the SDMX REST part of a structure query from the specified <paramref name="query"/>.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The  SDMX REST part of a structure query 
        /// </returns>
        public static string BuildRestStructureQuery(IRestStructureQuery query)
        {
            string restStructureRequest = structureQueryBuilderManager.BuildStructureQuery(query, structureQueryFormat);
            
            // remove query parameters
            var queryParamStart = restStructureRequest.IndexOf('?');
            if (queryParamStart > 0)
            {
                return restStructureRequest.Substring(0, queryParamStart);
            }

            return restStructureRequest;
        }

        /// <summary>
        /// Build the SDMX REST part of a structure query from the specified <paramref name="query"/>.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <returns>
        /// The  SDMX REST part of a structure query 
        /// </returns>
        public static string BuildRestStructureQuery(IStructureReference query)
        {
            return BuildRestStructureQuery(new RESTStructureQueryCore(query));
        }

        /// <summary>
        /// Determines if the <c>HTTP</c> header Location is needed by the specified <paramref name="statusCode"/>.
        /// </summary>
        /// <param name="statusCode">
        /// The <c>HTTP</c> status code.
        /// </param>
        /// <returns>
        /// <c>true</c> if <c>HTTP</c> header Location can be used; otherwise <c>false</c>
        /// </returns>
        public static bool IsLocationNeeded(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.Created: // This could instead have the list of stub artefacts
                case HttpStatusCode.Accepted:
                case HttpStatusCode.MovedPermanently:
                case HttpStatusCode.TemporaryRedirect:
                case HttpStatusCode.Found:
                case HttpStatusCode.SeeOther:
                case (HttpStatusCode)308:
                    return true;
                default:
                    return false;
            }
        }

        public static IList<string> VaryByHeaders(IConfiguration configuration)
        {
            return new SettingsFromConfigurationManager(configuration).VaryByHeaderList;
        }

        /// <summary>
        /// Set http headers for compressed or uncompressed file attachments
        /// </summary>
        /// <param name="ctx">Current HttpContext</param>
        /// <param name="mediaType">response contentType</param>
        /// <param name="fullFilename">outputs full filename with file extension</param>
        /// <param name="filename">filename without file extension</param>
        public static void SetResponseContentDispositionHeader(this HttpContext ctx, string mediaType, out string fullFilename, string filename)
        {
            var fileExtension = mediaType.Split('/', '+').Last();
            fullFilename = $"{string.Join("_", filename.Split(_invalidFileNameChars))}.{fileExtension}";

            var rawAcceptMatch = ctx.Request.Headers["Accept"].FirstOrDefault(x => x.Contains(mediaType, StringComparison.OrdinalIgnoreCase));

            if (string.IsNullOrEmpty(rawAcceptMatch))
            {
                return;
            }

            if (rawAcceptMatch.Contains("zip=true", StringComparison.OrdinalIgnoreCase)) //if true then attachment should be a zip file
            {
                ctx.Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{fullFilename}.zip\"");
            }
            else if (rawAcceptMatch.Contains("file=true", StringComparison.OrdinalIgnoreCase)) //if querying for uncompressed attached file
            {
                ctx.Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{fullFilename}\"");
            }
        }

        /// <summary>
        /// Check if zip file attachment 
        /// </summary>
        /// <param name="ctx">Current Http Context</param>
        /// <returns>true if zip file attachment</returns>
        public static bool IsZipContentDispositionHeader(this HttpContext ctx)
        {
            var rawAcceptMatch = ctx.Request.Headers["Accept"].FirstOrDefault();

            return rawAcceptMatch != null && rawAcceptMatch.Contains("zip=true", StringComparison.OrdinalIgnoreCase);
        }
    }
}