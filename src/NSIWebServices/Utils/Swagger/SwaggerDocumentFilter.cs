﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerDocumentFilter.cs" company="EUROSTAT">
//   Date Created : 2021-09-20
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.Swagger
{
    using System;
    using System.Collections.Generic;
    using ApiDocumentation;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.SwaggerGen;
    
    // ReSharper disable ClassNeverInstantiated.Global

    /// <summary>
    /// Add servers to the documentation
    /// </summary>
    public class SwaggerDocumentFilter : IDocumentFilter
    {
        private readonly string _serverUrl;

        public SwaggerDocumentFilter() { }
        
        /// <summary>
        /// Initializes a new instance of <see cref="SwaggerDocumentFilter"/>
        /// </summary>
        /// <param name="serverUrl">The base url of the application.</param>
        public SwaggerDocumentFilter(string serverUrl)
        {
            this._serverUrl = serverUrl;
        }
        
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            if (!string.IsNullOrEmpty(this._serverUrl))
            {
                AddServer(swaggerDoc.Servers, this._serverUrl, "this server");   
            }
            // add servers based on specs
            switch (swaggerDoc.Info.Version)
            {
                case { } str when str.Equals(ApiConstants.SupportedApiVersion.V1.ToString(), StringComparison.OrdinalIgnoreCase):
                    AddServer(swaggerDoc.Servers, "https://sdw-wsrest.ecb.europa.eu/service", "ECB");
                    AddServer(swaggerDoc.Servers, "https://sdw-wsrest.ecb.europa.eu/service", "Eurostat");
                    AddServer(swaggerDoc.Servers, "http://stats.oecd.org/sdmx-json", "OECD (data only)");
                    AddServer(swaggerDoc.Servers, "http://data.un.org/ws/rest", "UNdata");
                    AddServer(swaggerDoc.Servers, "https://www.ilo.org/sdmx/rest", "ILO");
                    AddServer(swaggerDoc.Servers, "https://virtserver.swaggerhub.com/sdmx-rest/sdmx-rest/1.3.0", "SwaggerHub API Auto Mocking");
                    break;
                case { } str when str.Equals(ApiConstants.SupportedApiVersion.V2.ToString(), StringComparison.OrdinalIgnoreCase):
                    AddServer(swaggerDoc.Servers, "https://localhost/", "Mock implementation (just for demo purposes!)");
                    break;
            }
        }

        private static void AddServer(IList<OpenApiServer> existingServers, string url, string description)
        {
            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentNullException(nameof(url));
            }
            
            var newServer = new OpenApiServer()
            {
                Url = url,
                Description = description
            };
            
            if (existingServers.Contains(newServer))
            {
                return;
            }
            
            existingServers.Add(newServer);
        }
    }
}