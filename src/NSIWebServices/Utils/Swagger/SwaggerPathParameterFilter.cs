﻿// -----------------------------------------------------------------------
// <copyright file="SwaggerPAthParameterFilter.cs" company="EUROSTAT">
//   Date Created : 2021-09-14
//   Copyright (c) 2009, 2021 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Rest.Utils.Swagger
{
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.SwaggerGen;
    using ApiDocumentation;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Microsoft.OpenApi.Any;
    using Microsoft.AspNetCore.Mvc.Controllers;
    
    // ReSharper disable ClassNeverInstantiated.Global
    
    /// <summary>
    /// Parses the route to set default value arguments for path parameters.
    /// </summary>
    public class SwaggerPathParameterFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var controllerName = ((ControllerActionDescriptor)context.ApiDescription.ActionDescriptor).ControllerName;
            if (string.IsNullOrEmpty(controllerName) || !ApiConstants.CONTROLLERS.Contains(controllerName))
                return;
            
            var httpMethodAttributes = context.MethodInfo
                .GetCustomAttributes(true)
                .OfType<Microsoft.AspNetCore.Mvc.Routing.HttpMethodAttribute>();

            var httpMethodWithOptional = httpMethodAttributes.FirstOrDefault(m => m.Template?.Contains("=") ?? false);
            if (httpMethodWithOptional == null)
                return;

            var nameValueRegex = new Regex(@"(?<={)\w+=.+(?=})");
            foreach (var pathParam in httpMethodWithOptional.Template.Split("/"))
            {
                var nameValueMatch = nameValueRegex.Match(pathParam);
                if (!nameValueMatch.Success)
                    continue;
                
                var nameValuePair = nameValueMatch.Value.Split("=");
                var paramName = nameValuePair[0];
                var paramDefaultValue = nameValuePair[1];
                var parameter = operation.Parameters.FirstOrDefault(p => p.In == ParameterLocation.Path && p.Name == paramName);
                if (parameter != null)
                {
                    parameter.Schema.Default = new OpenApiString(paramDefaultValue);
                }
            }
            
            // remove misplaced apiVersion parameter
            var apiVersionParameter = operation.Parameters.FirstOrDefault(p => p.Name.Equals("apiVersion"));
            if (apiVersionParameter != null)
            {
                operation.Parameters.Remove(apiVersionParameter);
            }
        }
    }
}