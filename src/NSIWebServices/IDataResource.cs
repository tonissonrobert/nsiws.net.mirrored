// -----------------------------------------------------------------------
// <copyright file="IDataResource.cs" company="EUROSTAT">
//   Date Created : 2013-10-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Rest
{

    /// <summary>
    /// The DataResource interface.
    /// </summary>
    public interface IDataResource
    {
        /// <summary>
        /// Gets the generic data.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        void GetGenericData(string flowRef, string key, string providerRef);

        /// <summary>
        /// Gets the generic data.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        Task GetGenericDataAsync(string flowRef, string key, string providerRef);

        /// <summary>
        /// Gets the generic data asynchronous for REST 2.0.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="key">The key(s).</param>
        Task GetGenericDataV2Async(string context, string agencyID, string resourceID, string version, string key);
    }
}