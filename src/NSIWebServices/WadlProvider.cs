﻿// -----------------------------------------------------------------------
// <copyright file="WadlProvider.cs" company="EUROSTAT">
//   Date Created : 2015-10-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;

namespace Estat.Sri.Ws.Rest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Exception;

    /// <summary>
    /// Class WADL Provider.
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Rest.IWadlProvider" />
    public class WadlProvider : IWadlProvider
    {
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// The predefined application WADL
        /// </summary>
        private const string ApplicationWadl = "application.wadl";

        /// <summary>
        /// The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(WadlProvider));

        public WadlProvider(IHttpContextAccessor contextAccessor)
        {
            this._contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Gets the WADL.
        /// </summary>
        /// <returns>The <see cref="Stream"/> containing the WADL.</returns>
        
        public void GetWadl()
        {
            try
            {
                var context = this._contextAccessor.HttpContext;
                if (context != null)
                {
                    var originalString = $"{context.Request.Scheme}://{context.Request.Host}{context.Request.PathBase}{context.Request.Path}";
                    var index = originalString.IndexOf(ApplicationWadl, StringComparison.OrdinalIgnoreCase);
                    var restUri = originalString.Substring(0, index);

                    ApplicationWadlTemplate applicationWadl = new ApplicationWadlTemplate();
                    applicationWadl.Session = new Dictionary<string, object>(StringComparer.Ordinal) { { "applicationUrl", restUri } };
                    applicationWadl.Initialize();

                    var buffer = Encoding.UTF8.GetBytes(applicationWadl.TransformText());
                    context.Response.ContentType = "application/vnd.sun.wadl+xml";
                    context.Response.Body.WriteAsync(buffer);
                }
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
                throw;
            }

            //_log.Error("WebOperationContextCurrent is null");
            //throw new SdmxInternalServerException();
        }
    }
}