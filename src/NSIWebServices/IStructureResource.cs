// -----------------------------------------------------------------------
// <copyright file="IStructureResource.cs" company="EUROSTAT">
//   Date Created : 2013-10-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Rest
{
    using System.IO;

    /// <summary>
    ///     TODO: Update summary.
    /// </summary>
    public interface IStructureResource
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the structure.
        /// </summary>
        /// <param name="structure">The structure. It can be one of the maintainable SDMX resources.</param>
        /// <param name="agencyId">The agency identifier; or ALL.</param>
        /// <param name="resourceId">The resource identifier; or ALL.</param>
        /// <param name="version">The version; or LATEST or ALL.</param>
        /// <param name="filter"></param>
        Task GetStructureAsync(string structure, string agencyId, string resourceId, string version,string filter);

        /// <summary>
        /// Gets multiple structures as required in SDMX REST 2.0 and later.
        /// </summary>
        /// <param name="structure">The structure. It can be one of the maintainable SDMX resources.</param>
        /// <param name="agencyIds">The comma-separated agency identifiers; or ALL.</param>
        /// <param name="resourceIds">The comma-separated resource identifiers; or ALL.</param>
        /// <param name="versions">The comma-separated versions; or LATEST or ALL.</param>
        /// <param name="filter">The comma-separated item identifiers.</param>
        Task GetStructuresAsync(string structure, string agencyIds, string resourceIds, string versions, string filter);

        /// <summary>
        /// Append the specified structure stream.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        void Store(Stream structureStream);

        /// <summary>
        /// Replaces the specified structure stream.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        /// <param name="structureStream">The structure stream.</param>
        void Replace(string structure, string agencyId, string resourceId, string version, Stream structureStream);

        /// <summary>
        /// Patches the specified structure stream.
        /// </summary>
        /// <param name="structureStream">The structure stream.</param>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        void Patch(Stream structureStream, string structure, string agencyId, string resourceId, string version);

        /// <summary>
        /// Deletes the specified structure.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        void Delete(string structure, string agencyId, string resourceId, string version);
        #endregion
    }
}