// -----------------------------------------------------------------------
// <copyright file="DataResource.cs" company="EUROSTAT">
//   Date Created : 2013-10-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using System.Threading.Tasks;
using Controllers.Utils;
using Estat.Sri.Ws.Controllers.Builder;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Ws.Rest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;

    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Model;
    using Estat.Sri.Ws.Rest.Utils;

    using log4net;
    using Microsoft.Extensions.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;

    /// <summary>
    /// The data resource.
    /// </summary>
    /// <seealso cref="Estat.Sri.Ws.Rest.IDataResource" />
    public class DataResource : IDataResource
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(DataResource));

        /// <summary>
        /// The _data request controller
        /// </summary>
        private readonly IDataRequestController _dataRequestController;

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataResource" /> class.
        /// </summary>
        /// <param name="dataRequestController">The data request controller.</param>
        /// <param name="contextAccessor"></param>
        /// <exception cref="System.ArgumentNullException">dataRequestController
        /// or
        /// messageBuilderManager
        /// or
        /// formatResolverManager</exception>
        public DataResource(IDataRequestController dataRequestController, IHttpContextAccessor contextAccessor, IConfiguration configuration)
        {
            if (dataRequestController == null)
            {
                throw new ArgumentNullException("dataRequestController");
            }

            //if (messageBuilderManager == null)
            //{
            //    throw new ArgumentNullException("messageBuilderManager");
            //}

            this._dataRequestController = dataRequestController;
            this._contextAccessor = contextAccessor;
            this._configuration = configuration;
        }

        /// <summary>
        /// The get generic data.
        /// </summary>
        /// <param name="flowRef">The flow ref.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider ref.</param>
        /// <returns>The <see cref="void" />.</returns>
        /// <exception cref="WebFaultException{T}">Cannot serve content type:  + requestAccept</exception>
        public void GetGenericData(string flowRef, string key, string providerRef)
        {
            HttpContext ctx = this._contextAccessor.HttpContext;

            if (string.IsNullOrEmpty(flowRef))
            {
                throw new ServiceException(HttpStatusCode.NotImplemented); // $$$ Strange 501 ?
            }

            this.ProcessRequest(flowRef, key, providerRef, ctx);
        }

        /// <summary>
        /// The get generic data asynchronous.
        /// </summary>
        /// <param name="flowRef">The flow ref.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider ref.</param>
        /// <returns>The <see cref="void" />.</returns>
        /// <exception cref="WebFaultException{T}">Cannot serve content type:  + requestAccept</exception>
        public async Task GetGenericDataAsync(string flowRef, string key, string providerRef)
        {
            HttpContext ctx = this._contextAccessor.HttpContext;

            if (string.IsNullOrEmpty(flowRef))
            {
                throw new ServiceException(HttpStatusCode.NotImplemented); // $$$ Strange 501 ?
            }

            await this.ProcessRequestAsync(flowRef, key, providerRef, ctx);
        }

        /// <summary>
        /// The get generic data asynchronous for REST 2.0
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="key">The key(s).</param>
        /// <returns></returns>
        public async Task GetGenericDataV2Async(string context, string agencyID, string resourceID, string version, string key)
        {
            HttpContext ctx = this._contextAccessor.HttpContext;

            await this.ProcessRequestV2Async(context, agencyID, resourceID, version, key, ctx);
        }

        /// <summary>
        /// Builds the query bean.
        /// </summary>
        /// <param name="flowRef">The flow ref.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider ref.</param>
        /// <param name="queryParameters">The query parameters.</param>
        /// <returns>The <see cref="IRestDataQuery" />.</returns>
        /// <exception cref="WebFaultException{String}">An error occurred</exception>
        /// <exception cref="WebFaultException{T}">An error occurred</exception>
        private IRestDataQuery BuildQueryBean(string flowRef, string key, string providerRef)
        {
            var queryString = new string[4];
            queryString[0] = "data";
            queryString[1] = flowRef;
            queryString[2] = key;
            queryString[3] = providerRef;

            IDictionary<string, string> paramsDict = HeaderUtils.GetQueryStringAsDict(this._contextAccessor.HttpContext);
            IRestDataQuery restQuery;

            try
            {
                restQuery = new RESTDataQueryCore(queryString, paramsDict);
            }
            catch (SdmxException e)
            {
                _logger.Error(e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                throw new ServiceException(HttpStatusCode.BadRequest,e);
            }

            return restQuery;
        }

        /// <summary>
        /// Builds the query bean for REST 2.0.
        /// </summary>
        /// <param name="context">The data context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="key">The key(s).</param>
        /// <returns>The <see cref="IRestDataQuery" />.</returns>
        private IRestDataQuery BuildQueryBeanV2(string context, string agencyID, string resourceID, string version, string key)
        {
            var queryString = new string[6];
            queryString[0] = "data";
            queryString[1] = context;
            queryString[2] = agencyID;
            queryString[3] = resourceID;
            queryString[4] = version;
            queryString[5] = key;

            IDictionary<string, string> paramsDict = HeaderUtils.GetQueryStringAsDict(this._contextAccessor.HttpContext);
            IRestDataQuery restQuery;

            try
            {
                restQuery = new RESTDataQueryCoreV2(queryString, paramsDict);
            }
            catch (SdmxException e)
            {
                _logger.Error(e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message, e);
                throw new ServiceException(HttpStatusCode.BadRequest, e);
            }

            return restQuery;
        }

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="flowRef">The flow ref.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider ref.</param>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="void" />.</returns>
        /// <exception cref="WebFaultException{String}">Cannot serve content type:  + requestAccept</exception>
        private void ProcessRequest(string flowRef, string key, string providerRef, HttpContext ctx)
        {
            var acceptHeader = ctx.Request.Headers["Accept"];
            var requestAccept = acceptHeader.FirstOrDefault() ?? "";
            _logger.Info("Got request Content type= " + requestAccept);

            AcceptHeaderFromQueryParameters.Update(ctx.Request.Query, ctx.Request.Headers);
            IRestDataQuery query = BuildQueryBean(flowRef, key, providerRef);
            
            // TODO stop using WebHeaderCollection, it doesn't support HTTP/2
            var webHeaderCollection = new WebHeaderCollection();
            foreach (var header in ctx.Request.Headers)
            {
                if (!header.Key.StartsWith(':'))
                {
                    webHeaderCollection.Add(header.Key, header.Value);
                }
            }

            ctx.RequestAborted.ThrowIfCancellationRequested();

            using (new HeaderScope(webHeaderCollection, false, ctx.RequestAborted))
            {
                var response = this._dataRequestController.ParseRequest(query, ctx.Request.Headers);
                _logger.Info("Selected representation info for the controller: format =" + response.ResponseContentHeader.MediaType);
                IMessageFormat messageFormat = new RestFormat(this._contextAccessor, response.ResponseContentHeader) { Name = "Data" };
                RestMessageBuilder builder = new RestMessageBuilder(this._contextAccessor, response.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));

                //Set Content-Disposition header if payload should be in Zip or uncompressed file as attachment
                ctx.SetResponseContentDispositionHeader(response.ResponseContentHeader.MediaType.MediaType, out string fileName, $"{flowRef}+{key}");

                var controller = ctx.IsZipContentDispositionHeader() ? new ZipStreamController(response.Function, fileName) : new StreamController<Stream>(response.Function);

                builder.Build(controller, messageFormat);
            }
        }

        /// <summary>
        /// Processes the request asynchronous.
        /// </summary>
        /// <param name="flowRef">The flow ref.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider ref.</param>
        /// <param name="ctx">The context.</param>
        /// <returns>The <see cref="void" />.</returns>
        /// <exception cref="WebFaultException{String}">Cannot serve content type:  + requestAccept</exception>
        private async Task ProcessRequestAsync(string flowRef, string key, string providerRef, HttpContext ctx)
        {
            var acceptHeader = ctx.Request.Headers["Accept"];
            var requestAccept = acceptHeader.FirstOrDefault() ?? "";
            _logger.Info("Got request Content type= " + requestAccept);

            AcceptHeaderFromQueryParameters.Update(ctx.Request.Query, ctx.Request.Headers);
            IRestDataQuery query = BuildQueryBean(flowRef, key, providerRef);

            // TODO stop using WebHeaderCollection, it doesn't support HTTP/2
            var webHeaderCollection = new WebHeaderCollection();
            foreach (var header in ctx.Request.Headers)
            {
                if (!header.Key.StartsWith(':'))
                {
                    webHeaderCollection.Add(header.Key, header.Value);
                }
            }

            ctx.RequestAborted.ThrowIfCancellationRequested();

            using (new HeaderScope(webHeaderCollection, false, ctx.RequestAborted))
            {
                var response = this._dataRequestController.ParseRequest(query, ctx.Request.Headers);
                _logger.Info("Selected representation info for the controller: format =" + response.ResponseContentHeader.MediaType);
                IMessageFormat messageFormat = new RestFormat(this._contextAccessor, response.ResponseContentHeader) { Name = "Data" };
                RestMessageBuilder builder = new RestMessageBuilder(this._contextAccessor, response.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));

                //Set Content-Disposition header if payload should be in Zip or uncompressed file as attachment
                ctx.SetResponseContentDispositionHeader(response.ResponseContentHeader.MediaType.MediaType, out string fileName, $"{flowRef}+{key}");

                var controller = ctx.IsZipContentDispositionHeader() 
                    ? new ZipStreamController(response.Function, fileName) 
                    : new StreamController<Stream>(response.Function);

                await builder.BuildAsync(controller, messageFormat);
            }
        }

        /// <summary>
        /// Processes the request asynchronous for REST 2.0
        /// </summary>
        /// <param name="context">The data context.</param>
        /// <param name="agencyID">The agency(ies).</param>
        /// <param name="resourceID">The resource id(s).</param>
        /// <param name="version">The version(s).</param>
        /// <param name="key">The key(s).</param>
        /// <param name="ctx">The HTTP context.</param>
        /// <returns></returns>
        private async Task ProcessRequestV2Async(string context, string agencyID, string resourceID, string version, string key, HttpContext ctx)
        {
            var acceptHeader = ctx.Request.Headers["Accept"];
            var requestAccept = acceptHeader.FirstOrDefault() ?? "";
            _logger.Info("Got request Content type= " + requestAccept);

            AcceptHeaderFromQueryParameters.Update(ctx.Request.Query, ctx.Request.Headers);
            IRestDataQuery query = BuildQueryBeanV2(context, agencyID, resourceID, version, key);

            // TODO stop using WebHeaderCollection, it doesn't support HTTP/2
            var webHeaderCollection = new WebHeaderCollection();
            foreach (var header in ctx.Request.Headers)
            {
                if (!header.Key.StartsWith(':'))
                {
                    webHeaderCollection.Add(header.Key, header.Value);
                }
            }

            ctx.RequestAborted.ThrowIfCancellationRequested();

            using (new HeaderScope(webHeaderCollection, false, ctx.RequestAborted))
            {
                var response = this._dataRequestController.ParseRequest(query, ctx.Request.Headers);
                _logger.Info("Selected representation info for the controller: format =" + response.ResponseContentHeader.MediaType);
                IMessageFormat messageFormat = new RestFormat(this._contextAccessor, response.ResponseContentHeader) { Name = "Data" };
                RestMessageBuilder builder = new RestMessageBuilder(this._contextAccessor, response.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));

                //Set Content-Disposition header if payload should be in Zip or uncompressed file as attachment
                ctx.SetResponseContentDispositionHeader(response.ResponseContentHeader.MediaType.MediaType, out string fileName, $"{context},{agencyID},{resourceID},{version}+{key}");

                var controller = ctx.IsZipContentDispositionHeader() ? new ZipStreamController(response.Function, fileName) : new StreamController<Stream>(response.Function);

                await builder.BuildAsync(controller, messageFormat);
            }
        }
    }
}