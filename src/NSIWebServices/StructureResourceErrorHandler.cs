﻿// -----------------------------------------------------------------------
// <copyright file="StructureResourceErrorHandler.cs" company="EUROSTAT">
//   Date Created : 2017-06-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Rest
{
    using System;
    using System.IO;
    using Estat.Sri.Ws.Controllers.Builder;

    using log4net;

    /// <summary>
    /// The structure resource error handler.
    /// </summary>
    public class StructureResourceErrorHandler : IStructureResource
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(StructureResourceErrorHandler));

        /// <summary>
        /// The builder
        /// </summary>
        private readonly ServiceExceptionRestBuilder _builder;

        /// <summary>
        /// The decorated resource
        /// </summary>
        private readonly IStructureResource _decoratedResource;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureResourceErrorHandler"/> class.
        /// </summary>
        /// <param name="decoratedResource">
        /// The decorated resource.
        /// </param>
        public StructureResourceErrorHandler(IStructureResource decoratedResource)
        {
            this._decoratedResource = decoratedResource;
            this._builder = new ServiceExceptionRestBuilder();
        }

        /// <inheritdoc />
        public void Delete(string structure, string agencyId, string resourceId, string version)
        {
            try
            {
                this._decoratedResource.Delete(structure, agencyId, resourceId, version);
            }
            catch (Exception e)
            {
                 BuildErrorResponse(e);
            }
        }

        /// <inheritdoc />
        public async Task GetStructureAsync(string structure, string agencyId, string resourceId, string version,string filter)
        {
            try
            {
                 await this._decoratedResource.GetStructureAsync(structure, agencyId, resourceId, version,filter);
            }
            catch (Exception e)
            {
                 BuildErrorResponse(e);
            }
        }

        /// <inheritdoc />
        public async Task GetStructuresAsync(string structure, string agencyIds, string resourceIds, string versions, string filter)
        {
            try
            {
                await this._decoratedResource.GetStructuresAsync(structure, agencyIds, resourceIds, versions, filter);
            }
            catch (Exception e)
            {
                BuildErrorResponse(e);
            }
        }

        /// <inheritdoc />
        public void Patch(Stream structureStream, string structure, string agencyId, string resourceId, string version)
        {
            try
            {
                 this._decoratedResource.Patch(structureStream, structure, agencyId, resourceId, version);
            }
            catch (Exception e)
            {
                 BuildErrorResponse(e);
            }
        }

        /// <inheritdoc />
        public void Store(Stream structureStream)
        {
            try
            {
                this._decoratedResource.Store(structureStream);
            }
            catch (Exception e)
            {
                BuildErrorResponse(e);
            }
        }

        /// <inheritdoc />
        public void Replace(string structure, string agencyId, string resourceId, string version, Stream structureStream)
        {
            try
            {
                this._decoratedResource.Replace(structure, agencyId, resourceId, version, structureStream);
            }
            catch (Exception e)
            {
                BuildErrorResponse(e);
            }
        }

        /// <summary>
        /// Builds the error response.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns>The <see cref="Message"/></returns>
        private void BuildErrorResponse(Exception e)
        {
            _logger.Error(e);

            // We cannot throw WebFaultException because it will always return HTTP Status Code Accepted (202) 
            var exception = this._builder.Build(e);
            throw exception;
        }
    }
}