﻿// -----------------------------------------------------------------------
// <copyright file="SdmxRestServiceHostFactory.cs" company="EUROSTAT">
//   Date Created : 2013-10-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------



namespace Estat.Sri.Ws.Rest
{
    //using System.Web.Configuration;
    //using System;
    //using System.ServiceModel;
    //using System.ServiceModel.Activation;
    //using System.ServiceModel.Description;
    //using System.ServiceModel.Web;

    //using log4net;
    //using Estat.Sri.Ws.Controllers.Manager;
    //using Estat.Sri.Ws.Controllers.Builder;

    ///// <summary>
    ///// The sdmx rest service host factory.
    ///// </summary>
    //public class SdmxRestServiceHostFactory : WebServiceHostFactory
    //{
    //    /// <summary>
    //    /// The _log.
    //    /// </summary>
    //    private static readonly ILog _log = LogManager.GetLogger(typeof(SdmxRestServiceHostFactory));

    //    /// <summary>
    //    /// The _service
    //    /// </summary>
    //    private readonly object _service;

    //    /// <summary>
    //    /// The _type.
    //    /// </summary>
    //    private readonly Type _type;

    //    /// <summary>
    //    /// Initializes a new instance of the <see cref="SdmxRestServiceHostFactory" /> class.
    //    /// </summary>
    //    /// <param name="type">The type.</param>
    //    /// <param name="service">The service.</param>
    //    public SdmxRestServiceHostFactory(Type type, object service)
    //    {
    //        _log.DebugFormat("Init SdmxRestServiceHostFactory({0})", type);
    //        this._type = type;
    //        this._service = service;
    //    }

    //    /// <summary>
    //    /// The create service host.
    //    /// </summary>
    //    /// <param name="serviceType">
    //    /// The service type.
    //    /// </param>
    //    /// <param name="baseAddresses">
    //    /// The base addresses.
    //    /// </param>
    //    /// <returns>
    //    /// The <see cref="ServiceHost"/>.
    //    /// </returns>
    //    /// <exception cref="ArgumentNullException"><paramref name="baseAddresses"/> is <see langword="null" />.</exception>
    //    protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
    //    {
    //        if (baseAddresses == null)
    //        {
    //            throw new ArgumentNullException("baseAddresses");
    //        }

    //        try
    //        {
    //            _log.DebugFormat("Creating REST service host for {0}", serviceType);

    //            ServiceHost serviceHost = new ServiceHost(this._service, baseAddresses);
    //            var webBehavior = new WebHttpBehavior { AutomaticFormatSelectionEnabled = false, HelpEnabled = true, FaultExceptionEnabled = false, DefaultBodyStyle = WebMessageBodyStyle.Bare };

    //            foreach (Uri u in baseAddresses)
    //            {
    //                if (u.Scheme.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
    //                {
    //                    _log.DebugFormat("Create binding for {0}", u);

    //                    var useCache = false;
    //                    var section = WebConfigurationManager.OpenWebConfiguration("/").GetSection("system.web/caching/outputCacheSettings") as OutputCacheSettingsSection;
    //                    if (section != null && section.OutputCacheProfiles["restOutputCache"] != null)
    //                    {
    //                        useCache = section.OutputCacheProfiles["restOutputCache"].Enabled;
    //                    }

    //                    // TODO do we need ContentTypeMapper set ?
    //                    var binding = new WebHttpBinding
    //                    {
    //                        TransferMode = useCache ? TransferMode.Buffered : TransferMode.Streamed,
    //                        ContentTypeMapper = new SdmxContentMapper(),
    //                        MaxReceivedMessageSize = SettingsManager.MaxReceivedMessageSize
    //                    };

    //                    if (u.Scheme == "http")
    //                    {
    //                        binding.Security.Mode = WebHttpSecurityMode.TransportCredentialOnly;
    //                    }
    //                    else if (u.Scheme == "https")
    //                    {
    //                        binding.Security.Mode = WebHttpSecurityMode.Transport;
    //                        binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
    //                    }

    //                    var endpoint = serviceHost.AddServiceEndpoint(this._type, binding, u);
    //                    endpoint.Behaviors.Add(webBehavior);
    //                }
    //            }

    //            return serviceHost;
    //        }
    //        catch (Exception e)
    //        {
    //            _log.Error("While creating service host", e);
    //            throw;
    //        }
    //    }
    //}
}