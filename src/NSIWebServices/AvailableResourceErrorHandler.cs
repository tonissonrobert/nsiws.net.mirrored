﻿// -----------------------------------------------------------------------
// <copyright file="AvailableResourceErrorHandler.cs" company="EUROSTAT">
//   Date Created : 2017-06-08
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Rest
{
    using log4net;
    using System;

    public class AvailableResourceErrorHandler : IAvailableResource
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(AvailableResourceErrorHandler));

        /// <summary>
        /// The builder
        /// </summary>
        //private readonly WebFaultExceptionRestBuilder _builder = new WebFaultExceptionRestBuilder();

        /// <summary>
        /// The decorated resource
        /// </summary>
        private readonly IAvailableResource _decoratedResource;

        public AvailableResourceErrorHandler(IAvailableResource decoratedResource)
        {
            if (decoratedResource == null)
            {
                throw new ArgumentNullException(nameof(decoratedResource));
            }

            _decoratedResource = decoratedResource;
        }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <param name="componentIds">The component ids.</param>
        /// <param name="httpContext"></param>
        /// <returns>void.</returns>
        public void GetData(string flowRef, string key, string providerRef, string componentIds)
        {
            try
            {
                _decoratedResource.GetData(flowRef, key, providerRef, componentIds);
            }
            catch (Exception e)
            {
                BuildErrorResponse(e);
            }
        }

        /// <summary>
        /// Gets the data asynchronous.
        /// </summary>
        /// <param name="flowRef">The flow reference.</param>
        /// <param name="key">The key.</param>
        /// <param name="providerRef">The provider reference.</param>
        /// <param name="componentIds">The component ids.</param>
        /// <param name="httpContext"></param>
        /// <returns>void.</returns>
        public async Task GetDataAsync(string flowRef, string key, string providerRef, string componentIds)
        {
            try
            {
                await _decoratedResource.GetDataAsync(flowRef, key, providerRef, componentIds);
            }
            catch (Exception e)
            {
                BuildErrorResponse(e);
            }
        }

        /// <summary>
        /// Gets the data asynchronous based on the rest v2 protocol.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="agencyIds">The agency(ies).</param>
        /// <param name="resourceIds">The resource id(s).</param>
        /// <param name="versions">The version(s).</param>
        /// <param name="keys">The key(s).</param>
        /// <param name="componentIds">The component id(s).</param>
        /// <returns>void.</returns>
        public async Task GetDataV2Async(string context, string agencyIds, string resourceIds, string versions, string keys, string componentIds)
        {
            try
            {
                await _decoratedResource.GetDataV2Async(context, agencyIds, resourceIds, versions, keys, componentIds);
            }
            catch (Exception e)
            {
                BuildErrorResponse(e);
            }
        }

        /// <summary>
        /// Builds the error response.
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns>The <see cref="void"/></returns>
        private void BuildErrorResponse(Exception e)
        {
            _logger.Error(e);

            // We cannot throw WebFaultException because it will always return HTTP Status Code Accepted (202) 
            throw e;
        }
    }
}
