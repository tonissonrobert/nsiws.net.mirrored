// -----------------------------------------------------------------------
// <copyright file="StructureResource.cs" company="EUROSTAT">
//   Date Created : 2013-10-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Globalization;
using System.Threading.Tasks;
using Controllers.Utils;
using Estat.Sri.Ws.Controllers.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Estat.Sri.Ws.Rest
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mime;
    using System.Text;
    using System.Text.RegularExpressions;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Extension;
    using Estat.Sdmxsource.Extension.Model.Error;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.Controllers.Model;
    using Estat.Sri.Ws.Rest.Utils;

    using log4net;
    using Microsoft.Extensions.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    ///  The SDMX-ML Structural meta-data resource implementation 
    /// </summary>
    public class StructureResource : IStructureResource
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(StructureResource));

        #endregion

        #region Fields

        /// <summary>
        /// The _structure request controller
        /// </summary>
        private readonly IStructureRequestController _structureRequestController;

        /// <summary>
        /// The rest structure submit controller
        /// </summary>
        private readonly IRestStructureSubmitController _restStructureSubmitController;

        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// The sorted accept headers builder
        /// </summary>
        private readonly SortedAcceptHeadersBuilder _acceptHeaderBuilder = new SortedAcceptHeadersBuilder();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureResource" /> class.
        /// </summary>
        /// <param name="structureRequestController">The structure request controller.</param>
        /// <param name="restStructureSubmitController">The rest structure submit controller.</param>
        /// <exception cref="ArgumentNullException">
        /// structureRequestController
        /// or
        /// messageBuilderManager
        /// </exception>
        public StructureResource(IStructureRequestController structureRequestController, IRestStructureSubmitController restStructureSubmitController,IHttpContextAccessor contextAccessor, IConfiguration configuration)
        {
            if (structureRequestController == null)
            {
                throw new ArgumentNullException(nameof(structureRequestController));
            }

            //if (messageBuilderManager == null)
            //{
            //    throw new ArgumentNullException(nameof(messageBuilderManager));
            //}

            if (restStructureSubmitController == null)
            {
                throw new ArgumentNullException(nameof(restStructureSubmitController));
            }

            this._structureRequestController = structureRequestController;
            this._restStructureSubmitController = restStructureSubmitController;
            this._contextAccessor = contextAccessor;
            this._configuration = configuration;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get structure.
        /// </summary>
        /// <param name="structure">
        /// The structure.
        /// </param>
        /// <param name="agencyId">
        /// The agency id.
        /// </param>
        /// <param name="resourceId">
        /// The resource id.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="Message"/>.
        /// </returns>
        public async Task GetStructureAsync(string structure, string agencyId, string resourceId, string version,string filter)
        {

            if (!IsStructureValid(structure))
            {
                throw new ServiceException(HttpStatusCode.BadRequest,new Exception("Invalid structure: " + structure));
            }

            await this.ProcessRequestAsync(structure, agencyId, resourceId, version,filter, this._contextAccessor.HttpContext);
        }

        /// <summary>
        /// Gets multiple structures as required in SDMX REST 2.0 and later.
        /// </summary>
        /// <param name="structure">
        /// The structure.
        /// </param>
        /// <param name="agencyIds">
        /// The comma-separated agency ids.
        /// </param>
        /// <param name="resourceIds">
        /// The comma-separated resource id.
        /// </param>
        /// <param name="versions">
        /// The comma-separated version.
        /// </param>
        /// <param name="filter">
        /// The items ids.
        /// </param>
        /// <returns>
        /// The <see cref="Message"/>.
        /// </returns>
        public async Task GetStructuresAsync(string structure, string agencyIds, string resourceIds, string versions, string filter)
        {
            if (!IsStructureValid(structure))
            {
                throw new ServiceException(HttpStatusCode.BadRequest, new Exception("Invalid structure: " + structure));
            }

            await this.ProcessRequestAsync(structure, agencyIds, resourceIds, versions, filter, this._contextAccessor.HttpContext, true);
        }

        /// <inheritdoc />
        public void Store(Stream structureStream)
        {
            using (structureStream)
            {
                this.CheckIfSubmitIsEnabled();


                var httpContext = this._contextAccessor.HttpContext;
                var baseUri = new Uri($"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.PathBase}");
                ContentType contentType = GetContentType(httpContext);
                Encoding encoding = GetEncoding();

                var response = this._restStructureSubmitController.Post(structureStream, GetStoreId(httpContext), contentType);
                GetSubmitResponse(this, httpContext, encoding, response, baseUri);
            }
        }

        /// <inheritdoc />
        public void Replace(string structure, string agencyId, string resourceId, string version, Stream structureStream)
        {
            using (structureStream)
            {
                this.CheckIfSubmitIsEnabled();


                var ctx = this._contextAccessor.HttpContext;
                var baseUri = new Uri($"{ctx.Request.Scheme}://{ctx.Request.Host}{ctx.Request.PathBase}");
                ContentType contentType = GetContentType(ctx);
                Encoding encoding = GetEncoding();
                IRestStructureQuery query = HeaderUtils.BuildRestQueryBean(structure, agencyId, resourceId, version,string.Empty, this._contextAccessor.HttpContext);
                var response = this._restStructureSubmitController.Put(structureStream, query, GetStoreId(ctx), contentType);
                GetSubmitResponse(this, ctx, encoding, response, baseUri);
            }
        }

        /// <inheritdoc />
        public void Patch(Stream structureStream, string structure, string agencyId, string resourceId, string version)
        {
            using (structureStream)
            {
                this.CheckIfSubmitIsEnabled();
                var ctx = this._contextAccessor.HttpContext;
                var baseUri = new Uri($"{ctx.Request.Scheme}://{ctx.Request.Host}{ctx.Request.PathBase}");

                Encoding encoding = GetEncoding();
                IRestStructureQuery query = HeaderUtils.BuildRestQueryBean(structure, agencyId, resourceId, version,string.Empty, this._contextAccessor.HttpContext);
                var response = this._restStructureSubmitController.Patch(structureStream, query, GetStoreId(ctx));
                GetSubmitResponse(this, ctx, encoding, response, baseUri);
            }
        }

        /// <inheritdoc />
        public void Delete(string structure, string agencyId, string resourceId, string version)
        {
            if (string.IsNullOrWhiteSpace(version) || version == "latest")
            {
                throw new SdmxSemmanticException("Can only delete a structure with a specific version");
            }

            var httpContext = this._contextAccessor.HttpContext;
            this.CheckIfSubmitIsEnabled();

            if (!HeaderUtils.IsStructureValid(structure))
            {
                throw new ServiceException(HttpStatusCode.BadRequest, new Exception("Invalid structure: " + structure));
            }

            Encoding encoding = GetEncoding();
            IRestStructureQuery query = HeaderUtils.BuildRestQueryBean(structure, agencyId, resourceId, version,string.Empty,httpContext);
            var response = this._restStructureSubmitController.Delete(query, GetStoreId(httpContext));
            var baseUri = new Uri($"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.PathBase}");
             GetSubmitResponse(this, httpContext, encoding, response, baseUri);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the store identifier.
        /// </summary>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The store identifier from the URL</returns>
        private static string GetStoreId(HttpContext ctx)
        {
            return null; // wait until this is implemented in java
            // return ctx.IncomingRequest.UriTemplateMatch.QueryParameters["sid"];
        }

        /// <summary>
        /// Gets the encoding.
        /// </summary>
        /// <returns>The <see cref="Encoding"/></returns>
        private static Encoding GetEncoding()
        {
            return new UTF8Encoding(false);
        }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <param name="ctx">The CTX.</param>
        /// <returns>The content type</returns>
        private static ContentType GetContentType(HttpContext ctx)
        {
            ContentType contentType = null;
            if (!string.IsNullOrWhiteSpace(ctx.Request.ContentType))
            {
                contentType = new ContentType(ctx.Request.ContentType);
            }

            return contentType;
        }

        /// <summary>
        /// Builds the rest query bean.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency id.</param>
        /// <param name="resourceId">The resource id.</param>
        /// <param name="version">The version.</param>
        /// <param name="filter"></param>
        /// <param name="multipleStructures">Indicates if query refers to multiple structures.</param>
        /// <returns>
        /// The <see cref="IRestStructureQuery" />.
        /// </returns>
        /// <exception cref="ServiceException">An exception is thrown</exception>
        private IRestStructureQuery BuildRestQueryBean(string structure, string agencyId, string resourceId, string version,string filter, bool multipleStructures = false)
        {
            if (multipleStructures)
            {
                return HeaderUtils.BuildRestQueryMultipleBeans(structure, agencyId, resourceId, version, filter, this._contextAccessor.HttpContext);
            }
            else
            {
                return HeaderUtils.BuildRestQueryBean(structure, agencyId, resourceId, version, filter, this._contextAccessor.HttpContext);
            }
        }

        /// <summary>
        /// The is structure valid.
        /// </summary>
        /// <param name="structure">
        /// The structure.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private static bool IsStructureValid(string structure)
        {
            return HeaderUtils.IsStructureValid(structure);
        }

        /// <summary>
        /// Gets the submit response.
        /// </summary>
        /// <param name="instance">
        ///     The instance.
        /// </param>
        /// <param name="context"></param>
        /// <param name="encoding">
        ///     The encoding.
        /// </param>
        /// <param name="response">
        ///     The response.
        /// </param>
        /// <param name="baseUri">
        ///     The base Uri.
        /// </param>
        /// <returns>
        /// The <see cref="Message"/>.
        /// </returns>
        private static void GetSubmitResponse(StructureResource instance, HttpContext context, Encoding encoding, IList<IResponseWithStatusObject> response, Uri baseUri)
        {
            var statusCode = (HttpStatusCode)response.GetHttpCode();

            // SDMX REST 1.2.0 workaround
            if (statusCode == HttpStatusCode.BadRequest && response.SelectMany(r => r.Messages).All(m => SdmxErrorCode.GetFromEnum(SdmxErrorCodeEnumType.SemanticError).Equals(m.ErrorCode)))
            {
                statusCode = HttpStatusCode.Forbidden;
            }

            context.Response.StatusCode = (int)statusCode;
            SetLocation(context, statusCode, response, baseUri);

            switch (statusCode)
            {

                case HttpStatusCode.Accepted:
                case HttpStatusCode.NoContent:

                // TODO possibly include html in the response for redirects with the location
                case HttpStatusCode.MovedPermanently:
                case HttpStatusCode.TemporaryRedirect:
                case HttpStatusCode.Found:
                case HttpStatusCode.SeeOther:
                case (HttpStatusCode)308:
                    context.Response.WriteAsync(string.Empty, GetEncoding());
                    break;
                case HttpStatusCode.OK:
                case HttpStatusCode.Created:
                    instance._restStructureSubmitController.Write(response, context.Response.Body, encoding);
                    break;
                default:
                    instance._restStructureSubmitController.Write(response, context.Response.Body, encoding);
                    break;
            }
        }

        /// <summary>
        /// Sets the HTTP header Location.
        /// </summary>
        /// <param name="ctx">
        /// The CTX.
        /// </param>
        /// <param name="statusCode">
        /// The status code.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <param name="baseUri">
        /// The base Uri.
        /// </param>
        private static void SetLocation(HttpContext ctx, HttpStatusCode statusCode, IList<IResponseWithStatusObject> response, Uri baseUri)
        {
            if (!HeaderUtils.IsLocationNeeded(statusCode))
            {
                return;
            }

            var structureReference = response.LastOrDefault()?.StructureReference;
            if (structureReference == null)
            {
                // unable to evaluate the location
                return;
            }

            var restUri = HeaderUtils.BuildRestStructureQuery(structureReference);

            ctx.Request.HttpContext.Response.Headers.Add(nameof(HttpResponseHeader.Location), string.Format(CultureInfo.InvariantCulture, "{0}/{1}", baseUri, restUri));

            //var getOperation = "{structure}/{agencyId=ALL}/{resourceId=ALL}/{version=LATEST}/";
            //TODO fix this
            //var restUri = HeaderUtils.BuildRestStructureQuery(structureReference).Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            //var url = getOperation.BindByPosition(baseUri, restUri).ToString();
            //if (url.Last() != '/')
            //{
            //    url += '/';
            //}

            //ctx.Response.Headers.Add("Location", url);
        }

        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency id.</param>
        /// <param name="resourceId">The resource id.</param>
        /// <param name="version">The version.</param>
        /// <param name="ctx">The current <see cref="HttpContext"/>.</param>
        /// <param name="multipleStructures">Indicates whether the request allows multiple structures.</param>
        /// <returns>
        /// The <see cref="Message" />.
        /// </returns>
        /// <exception cref="ServiceException">
        /// Cannot serve content type
        /// </exception>
        /// <exception cref="ServiceException">Cannot serve content type</exception>
        private async Task ProcessRequestAsync(string structure, string agencyId, string resourceId, string version, string filter, HttpContext ctx, bool multipleStructures = false)
        {
            Match match = Regex.Match(resourceId, @"[A-Za-z0-9\-\*]+$", RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                throw new ServiceException(HttpStatusCode.BadRequest);
            }

            var acceptHeader = ctx.Request.Headers["Accept"];
            var requestAccept = acceptHeader.FirstOrDefault() ?? "";
            _logger.Info("Got request Content type= " + requestAccept);

            AcceptHeaderFromQueryParameters.Update(ctx.Request.Query, ctx.Request.Headers);
            IRestStructureQuery query = this.BuildRestQueryBean(structure, agencyId, resourceId, version, filter, multipleStructures);
            var response = this._structureRequestController.ParseRequest(query, ctx.Request.Headers, ctx);
            IMessageFormat messageFormat = new RestFormat(this._contextAccessor, response.ResponseContentHeader) { Name = "Structure" };
            RestMessageBuilder builder = new RestMessageBuilder(this._contextAccessor, response.ResponseContentHeader, HeaderUtils.VaryByHeaders(this._configuration));

            //Set Content-Disposition header if payload should be in Zip or file uncompressed as attachment
            ctx.SetResponseContentDispositionHeader(response.ResponseContentHeader.MediaType.MediaType, out string fileName, $"{structure}_{resourceId}_{agencyId}_{version}");
            
            var controller = ctx.IsZipContentDispositionHeader() 
                ? new ZipStreamController(response.Function, fileName) 
                : new StreamController<Stream>(response.Function);

            await builder.BuildAsync(controller, messageFormat);
        }


        /// <summary>
        /// Checks if submit is enabled.
        /// </summary>
        /// <exception cref="ServiceException">Submit is not enabled</exception>
        private void CheckIfSubmitIsEnabled()
        {
            if (!SettingsManager.EnableSubmitStructure || this._restStructureSubmitController == null)
            {
                throw new ServiceException(HttpStatusCode.Forbidden);
            }
        }

        #endregion
    }
}