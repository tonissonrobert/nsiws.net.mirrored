﻿// -----------------------------------------------------------------------
// <copyright file="DataValidationFormat.cs" company="EUROSTAT">
//   Date Created : 2018-11-22
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Text;

namespace Estat.Sri.Ws.Format.Sdmx.Model
{
    /// <summary>
    /// The data validation format. It will return instead of data, the validation results of the data
    /// </summary>
    public class DataValidationFormat : AbstractRestFormat, IDataFormat
    {
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrievalManager;
        private readonly SdmxSchema _sdmxSchema;

        public DataValidationFormat(Encoding encoding, SdmxSchema sdmxSchema, ISdmxObjectRetrievalManager sdmxObjectRetrievalManager) : base(encoding)
        {
            if (sdmxSchema == null)
            {
                throw new ArgumentNullException(nameof(sdmxSchema));
            }

            if (sdmxObjectRetrievalManager == null)
            {
                throw new ArgumentNullException(nameof(sdmxObjectRetrievalManager));
            }

            _sdmxSchema = sdmxSchema;
            _sdmxObjectRetrievalManager = sdmxObjectRetrievalManager;
        }

        public string FormatAsString
        {
            get
            {
                return "Data Validation format";
            }
        }

        public DataType SdmxDataFormat { get { return DataType.GetFromEnum(DataEnumType.Other); } }

        public ISdmxObjectRetrievalManager SdmxObjectRetrievalManager
        {
            get
            {
                return _sdmxObjectRetrievalManager;
            }
        }

        public SdmxSchema SdmxSchema
        {
            get
            {
                return _sdmxSchema;
            }
        }
    }
}