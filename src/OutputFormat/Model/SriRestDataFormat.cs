﻿// -----------------------------------------------------------------------
// <copyright file="SriRestDataFormat.cs" company="EUROSTAT">
//   Date Created : 2016-07-01
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Model
{
    using System;
    using System.Globalization;
    using System.Text;

    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The SDMX RI data format.
    /// </summary>
    public class SriRestDataFormat : AbstractRestFormat, IDataFormat
    {
        /// <summary>
        /// The _format as string
        /// </summary>
        private readonly string _formatAsString;

        /// <summary>
        /// The _SDMX data format
        /// </summary>
        private readonly DataType _sdmxDataFormat;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriRestDataFormat"/> class.
        /// </summary>
        /// <param name="sdmxDataFormat">The SDMX data format.</param>
        /// <param name="encoding">The encoding.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="sdmxDataFormat"/> is null</exception>
        /// <remarks>Use this with REST</remarks>
        public SriRestDataFormat(DataType sdmxDataFormat, Encoding encoding) : base(encoding ?? new UTF8Encoding(false))
        {
            if (sdmxDataFormat == null)
            {
                throw new ArgumentNullException("sdmxDataFormat");
            }

            this._sdmxDataFormat = sdmxDataFormat;
            this._formatAsString = string.Format(CultureInfo.InvariantCulture, "SDMX RI Data Format {0};charSet={1}", sdmxDataFormat, this.Encoding);
        }

        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        ///                 <p/>
        ///                 This is expected to return a not null response.
        /// </summary>
        public string FormatAsString
        {
            get
            {
                return this._formatAsString;
            }
        }

        /// <summary>
        /// Gets the sdmx data format that this interface is describing.
        ///                 If this is not describing an SDMX message then this will return null and the implementation class will be expected
        ///                 to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat
        {
            get
            {
                return this._sdmxDataFormat;
            }
        }
    }
}