﻿// -----------------------------------------------------------------------
// <copyright file="RestUtils.cs" company="EUROSTAT">
//   Date Created : 2015-10-26
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Utils
{
    using System;
    using System.Text;

    using log4net;

    /// <summary>
    /// REST related utils
    /// </summary>
    public static class RestUtils
    {
        /// <summary>
        /// The logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetLogger(typeof(RestUtils));

        /// <summary>
        /// Gets the char set encoding.
        /// </summary>
        /// <param name="charSet">The character set.</param>
        /// <returns>
        /// The response encoding
        /// </returns>
        public static Encoding GetCharSetEncoding(string charSet)
        {
            if (!string.IsNullOrWhiteSpace(charSet))
            {
                try
                {
                    var encoding = Encoding.GetEncoding(charSet);
                    if (encoding.Equals(Encoding.UTF8))
                    {
                        return new UTF8Encoding(false);
                    }

                    return encoding;
                }
                catch (ArgumentException e)
                {
                    _logger.Error(charSet, e);
                }
            }

            return new UTF8Encoding(false);
        }
    }
}