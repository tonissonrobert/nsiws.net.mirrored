﻿// -----------------------------------------------------------------------
// <copyright file="DataValidationEngineFactory.cs" company="EUROSTAT">
//   Date Created : 2018-11-23
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
//
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Engine;
using Estat.Sri.Ws.Format.Sdmx.Model;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Factory;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.IO;

namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    public class DataMappingValidationWriterFactory : IDataWriterFactory
    {
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            if (outStream == null)
            {
                return null;
            }

            DataValidationFormat format = dataFormat as DataValidationFormat;
            if (format == null)
            {
                return null;
            }

            return new MappingValidationWriterEngine(outStream, format.SdmxObjectRetrievalManager, format.SdmxSchema);
        }
    }
}