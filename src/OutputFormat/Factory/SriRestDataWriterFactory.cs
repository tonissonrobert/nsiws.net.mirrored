// -----------------------------------------------------------------------
// <copyright file="SriRestDataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-17
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using System;
    using System.IO;

    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Format.Sdmx.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.EdiParser.Engine;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// Factory for generating the SDMX RI default <see cref="IDataWriterEngine"/>
    /// </summary>
    public class SriRestDataWriterFactory : IDataWriterFactory
    {
        private readonly SettingsFromConfigurationManager _configuration;

        /// <summary>
        /// The tool indicator
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriRestDataWriterFactory"/> class.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="toolIndicator">The tool indicator.</param>
        public SriRestDataWriterFactory(SettingsFromConfigurationManager configuration = null, IToolIndicator toolIndicator = null)
        {
            this._configuration = configuration;
            this._toolIndicator = toolIndicator;
        }

        /// <summary>
        /// Gets the data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="outStream">The out stream.</param>
        /// <returns>The <see cref="IDataWriterEngine"/>.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="outStream"/> is null.</exception>
        public IDataWriterEngine GetDataWriterEngine(IDataFormat dataFormat, Stream outStream)
        {
            SriRestDataFormat sriDataFormat = dataFormat as SriRestDataFormat;
            if (sriDataFormat != null)
            {
                if (outStream == null)
                {
                    throw new ArgumentNullException("outStream");
                }

                DataWriterEngineBase engine = null;
                switch (sriDataFormat.SdmxDataFormat.BaseDataFormat.EnumType)
                {
                    case BaseDataFormatEnumType.Generic:

                        engine = new GenericDataWriterEngine(outStream, sriDataFormat.SdmxDataFormat.SchemaVersion, sriDataFormat.Encoding);
                        break;
                    case BaseDataFormatEnumType.Compact:
                        engine = new CompactDataWriterEngine(outStream, sriDataFormat.SdmxDataFormat.SchemaVersion, sriDataFormat.Encoding);
                        break;
                    case BaseDataFormatEnumType.Edi:
                        return new GesmesTimeSeriesWriter(outStream, true);
                }

                if (engine != null && this._toolIndicator != null)
                {
                    engine.GeneratedFileComment = this._toolIndicator.GetComment();
                }

                return engine;
            }

            return null;
        }
    }
}