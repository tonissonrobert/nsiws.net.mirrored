﻿// -----------------------------------------------------------------------
// <copyright file="MappingValidationFactory.cs" company="EUROSTAT">
//   Date Created : 2018-1-31
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sri.Ws.Format.Sdmx.Model;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;

namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    public class MappingValidationFactory : AbstractDataWriterPluginContract
    {
        /// <summary>
        /// The _content list
        /// When no version is specified in the request, the application will use the first one in this list.
        /// </summary>
        private static readonly IList<string> _contentList = new[]
        {
            "application/mappingValidation;version=2.1",
            "application/mappingValidation;version=2.0",
            "application/mappingValidation"
        };

        public MappingValidationFactory()
            : base(_contentList)
        {
        }

        /// <inheritdoc />
        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="encoding">The encoding.</param>
        /// <param name="dataRequest">The data request.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        protected override IDataFormat BuildFormat(string mediaType, Encoding encoding, IRestDataRequest dataRequest)
        {
            if (dataRequest == null)
            {
                throw new ArgumentNullException(nameof(dataRequest));
            }

            ContentType contentType = new ContentType(mediaType);
            SdmxSchemaEnumType sdmxSchema = SdmxSchemaEnumType.VersionTwoPointOne;
            if (contentType.Parameters.ContainsKey("version"))
            {
                string requestedVersion = contentType.Parameters["version"];
                if ("2.0".Equals(requestedVersion))
                {
                    sdmxSchema = SdmxSchemaEnumType.VersionTwo;
                }
            }

            // get the Structure retriever manager in case we need other structural metadata
            Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.ISdmxObjectRetrievalManager retrieverManager = dataRequest.RetrievalManager;
            return new DataValidationFormat(encoding, SdmxSchema.GetFromEnum(sdmxSchema), retrieverManager);
        }
    }
}