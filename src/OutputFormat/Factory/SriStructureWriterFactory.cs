﻿// -----------------------------------------------------------------------
// <copyright file="SriStructureWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2016-07-12
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using System.IO;

    using Estat.Sdmxsource.Extension.Engine;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;
    using Org.Sdmxsource.Sdmx.Structureparser.Factory;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The SDMX-RI Rest structure writer factory. It just decorates the <see cref="SdmxStructureWriterFactory" />.
    /// </summary>
    /// <remarks>Used a decorator to make the <see cref="SdmxStructureWriterFactory"/> more discoverable to developers.</remarks>
    public class SriStructureWriterFactory : IStructureWriterFactory
    {
        /// <summary>
        /// The tool indicator
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// The decorated factory
        /// </summary>
        private readonly IStructureWriterFactory _decoratedFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriStructureWriterFactory" /> class.
        /// </summary>
        /// <param name="toolIndicator">The tool indicator.</param>
        public SriStructureWriterFactory(IToolIndicator toolIndicator = null)
        {
            this._toolIndicator = toolIndicator;
            this._decoratedFactory = new SdmxStructureWriterFactory();
        }

        /// <summary>
        /// Obtains a StructureWritingEngine engine for the given output format
        /// </summary>
        /// <param name="structureFormat">An implementation of the StructureFormat to describe the output format for the structures
        ///                 (required)
        ///             </param><param name="streamWriter">The output stream to write to (can be null if it is not required)</param>
        /// <returns>
        /// Null if this factory is not capable of creating a data writer engine in the requested format
        /// </returns>
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            if (structureFormat == null)
            {
                return null;
            }

            IStructureWriterEngine structureWriterEngine = this._decoratedFactory.GetStructureWriterEngine(structureFormat, streamWriter);
            if (this._toolIndicator == null || structureFormat.SdmxOutputFormat == null)
            {
                return structureWriterEngine;
            }

            var outputFormat = structureFormat.SdmxOutputFormat;
            SdmxSchema schemaVersion = outputFormat.OutputVersion;

            // TODO Split AbstractWritingEngine to a writing part and to a building part and decorate the building part. Possibly after the sync with 1.5.x
            AbstractWritingEngine decorated = structureWriterEngine as AbstractWritingEngine;
            if (decorated != null)
            {
                SdmxXmlStructureFormat sdmxXmlStructureFormat = structureFormat as SdmxXmlStructureFormat;
                if (sdmxXmlStructureFormat != null)
                {
                    return new WritingEngineDecorator(this._toolIndicator, decorated, sdmxXmlStructureFormat.Writer);
                }

                SdmxStructureFormat sdmxStructureFormat = structureFormat as SdmxStructureFormat;
                if (sdmxStructureFormat != null)
                {
                    return new WritingEngineDecorator(this._toolIndicator, decorated, schemaVersion, streamWriter, true) { Encoding = sdmxStructureFormat.OutputEncoding };
                }
            }

            return structureWriterEngine;
        }
    }
}
