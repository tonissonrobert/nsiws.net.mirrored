﻿// -----------------------------------------------------------------------
// <copyright file="SriRestCrossDataWriterFactory.cs" company="EUROSTAT">
//   Date Created : 2016-07-04
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Factory
{
    using System;
    using System.IO;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sri.Ws.Format.Sdmx.Model;

    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The default SDMX RI implementation of <see cref="ICrossDataWriterFactory"/> for REST.
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Factory.ICrossDataWriterFactory" />
    public class SriRestCrossDataWriterFactory : ICrossDataWriterFactory
    {
        /// <summary>
        /// The tool indicator
        /// </summary>
        private readonly IToolIndicator _toolIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriRestCrossDataWriterFactory"/> class.
        /// </summary>
        /// <param name="toolIndicator">The tool indicator.</param>
        public SriRestCrossDataWriterFactory(IToolIndicator toolIndicator = null)
        {
            this._toolIndicator = toolIndicator;
        }

        /// <summary>
        /// Gets the SDMX v2.0 Cross Sectional data writer engine.
        /// </summary>
        /// <param name="dataFormat">The data format. The implementation may have extra properties. </param><param name="outputStream">The output stream. It can be null. But in this case the output should be provided in the <paramref name="dataFormat"/> implementation.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.DataParser.Engine.ICrossSectionalWriterEngine"/>.
        /// </returns>
        public ICrossSectionalWriterEngine GetWriterEngine(IDataFormat dataFormat, Stream outputStream)
        {
            SriRestDataFormat sriDataFormat = dataFormat as SriRestDataFormat;
            if (sriDataFormat != null)
            {
                if (outputStream == null)
                {
                    throw new ArgumentNullException("outputStream");
                }

                var engine = new CrossSectionalWriterEngine(outputStream, dataFormat.SdmxDataFormat.SchemaVersion, sriDataFormat.Encoding);
                if (this._toolIndicator != null)
                {
                    engine.GeneratedFileComment = this._toolIndicator.GetComment();
                }

                return engine;
            }

            return null;
        }
    }
}