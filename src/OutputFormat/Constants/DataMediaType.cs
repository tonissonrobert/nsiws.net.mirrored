// -----------------------------------------------------------------------
// <copyright file="DataMediaType.cs" company="EUROSTAT">
//   Date Created : 2013-10-07
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mime;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    ///   Enumerator for various SDMX content types.
    /// </summary>
    public enum DataMediaEnumType
    {
        /// <summary>
        /// The generic data.
        /// </summary>
        GenericData,
        
        /// <summary>
        /// The generic data.
        /// </summary>
        GenericDataV20,

        /// <summary>
        /// The structure specific data.
        /// </summary>
        StructureSpecificData, 

        /// <summary>
        /// The application xml.
        /// </summary>
        ApplicationXml, 

        /// <summary>
        /// The text xml mime type
        /// </summary>
        TextXml, 

        /// <summary>
        /// The compact data.
        /// </summary>
        CompactData, 

        /// <summary>
        /// The cross sectional data.
        /// </summary>
        CrossSectionalData, 

        /// <summary>
        /// The EDI data.
        /// </summary>
        EdiData, 

        /// <summary>
        /// The CSV data.
        /// </summary>
        CsvData,
        /// <summary>
        /// The generic data for 3.0.
        /// </summary>
        GenericDataV30,
        /// <summary>
        /// The structure specific data for 3.0
        /// </summary>
        StructureSpecificDataV30
    }

    /// <summary>
    /// The data media type.
    /// </summary>
    public sealed class DataMediaType : BaseConstantType<DataMediaEnumType>
    {
        /// <summary>
        /// The instances.
        /// </summary>
        private static readonly IDictionary<DataMediaEnumType, DataMediaType> _instances;

        /// <summary>
        /// The _ordered i list
        /// </summary>
        private static readonly IList<DataMediaType> _orderedIList;

        /// <summary>
        /// The _format.
        /// </summary>
        private readonly DataType _format;

        /// <summary>
        /// The _media type name.
        /// </summary>
        private readonly string _mediaTypeName;

        /// <summary>
        /// Initializes static members of the <see cref="DataMediaType"/> class.
        /// </summary>
        static DataMediaType()
        {
            _orderedIList = new[]
            {
                                 new DataMediaType(DataMediaEnumType.GenericData, SdmxMedia.GenericData, DataEnumType.Generic21),
                                 new DataMediaType(DataMediaEnumType.GenericDataV30, SdmxMedia.GenericData, DataEnumType.Generic30),
                                 new DataMediaType(DataMediaEnumType.StructureSpecificDataV30, SdmxMedia.StructureSpecificData, DataEnumType.Compact30),
                                 new DataMediaType(DataMediaEnumType.StructureSpecificData, SdmxMedia.StructureSpecificData, DataEnumType.Compact21),
                                 new DataMediaType(DataMediaEnumType.ApplicationXml, SdmxMedia.ApplicationXml, DataEnumType.Generic21),
                                 new DataMediaType(DataMediaEnumType.TextXml, SdmxMedia.TextXml, DataEnumType.Generic21),
                                 new DataMediaType(DataMediaEnumType.GenericDataV20, SdmxMedia.GenericData, DataEnumType.Generic20),
                                 new DataMediaType(DataMediaEnumType.CompactData, SdmxMedia.CompactData, DataEnumType.Compact20),
                                 new DataMediaType(DataMediaEnumType.CrossSectionalData, SdmxMedia.CrossSectionalData, DataEnumType.CrossSectional20),
                                 new DataMediaType(DataMediaEnumType.EdiData, SdmxMedia.EdiData, DataEnumType.EdiTs),
            };

            _instances = _orderedIList.ToDictionary(type => type.EnumType);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataMediaType"/> class.
        /// </summary>
        /// <param name="enumType">
        /// The enum type.
        /// </param>
        /// <param name="mediaTypeName">
        /// The media type name.
        /// </param>
        /// <param name="format">
        /// The format.
        /// </param>
        private DataMediaType(DataMediaEnumType enumType, string mediaTypeName, DataEnumType format)
            : base(enumType)
        {
            this._mediaTypeName = mediaTypeName;
            this._format = DataType.GetFromEnum(format);
        }

        /// <summary>
        /// Gets the values.
        /// </summary>
        public static IEnumerable<DataMediaType> Values
        {
            get
            {
                return _orderedIList;
            }
        }

        /// <summary>
        /// Gets the format.
        /// </summary>
        public DataType Format
        {
            get
            {
                return this._format;
            }
        }

        /// <summary>
        /// Gets the media type.
        /// </summary>
        public ContentType MediaType
        {
            get
            {
                var contentType = new ContentType(string.Format(CultureInfo.InvariantCulture, "{0};version={1}", this._mediaTypeName, this._format.SchemaVersion));
                return contentType;
            }
        }

        /// <summary>
        /// The get from enum.
        /// </summary>
        /// <param name="enumType">
        /// The enum type.
        /// </param>
        /// <returns>
        /// The <see cref="DataMediaType"/>.
        /// </returns>
        public static DataMediaType GetFromEnum(DataMediaEnumType enumType)
        {
            DataMediaType output;
            if (_instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        /// Gets the format.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <returns>
        /// The <see cref="StructureOutputFormat"/>.
        /// </returns>
        public static DataType GetFormat(ContentType contentType)
        {
            foreach (var structureMediaType in _orderedIList)
            {
                if (structureMediaType.MediaType.Equals(contentType))
                {
                    return structureMediaType._format;
                }
            }

            return DataType.GetFromEnum(DataEnumType.Generic21);
        }

        /// <summary>
        /// The get type from name.
        /// </summary>
        /// <param name="mediaTypeName">
        /// The media type name.
        /// </param>
        /// <returns>
        /// The <see cref="DataMediaType"/>.
        /// </returns>
        public static DataMediaType GetTypeFromName(string mediaTypeName)
        {
            if (string.IsNullOrEmpty(mediaTypeName) || new ContentType(mediaTypeName).MediaType.EndsWith("/*", StringComparison.Ordinal))
            {
                mediaTypeName = GetFromEnum(DataMediaEnumType.GenericData)._mediaTypeName;
            }

            return Values.FirstOrDefault(m => m._mediaTypeName.Equals(mediaTypeName, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this._mediaTypeName;
        }
    }
}