﻿// -----------------------------------------------------------------------
// <copyright file="StructureMediaType.cs" company="EUROSTAT">
//   Date Created : 2013-10-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Sdmx.Constants
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Net.Mime;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// The structure media type.
    /// </summary>
    public enum StructureMediaEnumType
    {
        /// <summary>
        /// The application XML.
        /// </summary>
        ApplicationXml, 

        /// <summary>
        /// Mime type <c>text/xml</c>.
        /// </summary>
        TextXml,

        /// <summary>
        /// The structure.
        /// </summary>
        Structure,

        /// <summary>
        /// The structure for SDMX V2.0
        /// </summary>
        StructureV20,

        /// <summary>
        /// The query structure for SDMX V2.0
        /// </summary>
        QueryStructureResponse,

        /// <summary>
        /// The edi structure.
        /// </summary>
        EdiStructure
    }

    /// <summary>
    /// The structure media type.
    /// </summary>
    public class StructureMediaType : BaseConstantType<StructureMediaEnumType>
    {
        #region Static Fields

        /// <summary>
        /// The instances.
        /// </summary>
        private static readonly IDictionary<StructureMediaEnumType, StructureMediaType> _instances;

        /// <summary>
        /// The _ordered instances
        /// </summary>
        private static readonly IList<StructureMediaType> _orderedInstances;

        #endregion

        #region Fields

        /// <summary>
        /// The _SDMX media
        /// </summary>
        private readonly string _sdmxMedia;

        /// <summary>
        /// The _structure output format
        /// </summary>
        private readonly StructureOutputFormat _structureOutputFormat;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="StructureMediaType"/> class.
        /// </summary>
        static StructureMediaType()
        {
            _orderedInstances = new[]
                                    {
                                        new StructureMediaType(StructureMediaEnumType.Structure, SdmxMedia.Structure, StructureOutputFormatEnumType.SdmxV21StructureDocument),
                                        new StructureMediaType(StructureMediaEnumType.StructureV20, SdmxMedia.Structure, StructureOutputFormatEnumType.SdmxV2StructureDocument),
                                        new StructureMediaType(StructureMediaEnumType.QueryStructureResponse, null, StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument),
                                        new StructureMediaType(StructureMediaEnumType.ApplicationXml, SdmxMedia.ApplicationXml, StructureOutputFormatEnumType.SdmxV21StructureDocument),
                                        new StructureMediaType(StructureMediaEnumType.TextXml, MediaTypeNames.Text.Xml, StructureOutputFormatEnumType.SdmxV21StructureDocument)
                                    };
            _instances = _orderedInstances.ToDictionary(type => type.EnumType);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureMediaType" /> class.
        /// </summary>
        /// <param name="mediaType">The Media type.</param>
        /// <param name="sdmxMedia">The SDMX media.</param>
        /// <param name="structureOutputFormat">The structure output format.</param>
        public StructureMediaType(StructureMediaEnumType mediaType, string sdmxMedia, StructureOutputFormatEnumType structureOutputFormat)
            : base(mediaType)
        {
            this._sdmxMedia = sdmxMedia;
            this._structureOutputFormat = StructureOutputFormat.GetFromEnum(structureOutputFormat);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the REST compatible values
        /// </summary>
        public static IEnumerable<StructureMediaType> RestValues
        {
            get
            {
                return _orderedInstances.Where(type => type.HasMediaType);
            }
        }

        /// <summary>
        /// Gets the SOAP compatible values
        /// </summary>
        public static IEnumerable<StructureMediaType> SoapValues
        {
            get
            {
                return _orderedInstances.Where(type => type._structureOutputFormat.OutputVersion.IsXmlFormat());
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has media type.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has media type; otherwise, <c>false</c>.
        /// </value>
        public bool HasMediaType
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this._sdmxMedia);
            }
        }

        /// <summary>
        /// Gets the media type name.
        /// </summary>
        public ContentType MediaType
        {
            get
            {
                if (!this.HasMediaType)
                {
                    throw new ArgumentException("Tried to get media type for SOAP only value");
                }

                // this should not be cached as ContentType is mutable
                var format = string.Format(CultureInfo.InvariantCulture, "{0};version={1}", this._sdmxMedia, this._structureOutputFormat.OutputVersion);
                var contentType = new ContentType(format);
                return contentType;
            }
        }

        /// <summary>
        /// Gets the format.
        /// </summary>
        /// <value>
        /// The format.
        /// </value>
        public StructureOutputFormat Format
        {
            get
            {
                return this._structureOutputFormat;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Gets the value from <paramref name="enumType"/>
        /// </summary>
        /// <param name="enumType">
        /// Type of the enumeration.
        /// </param>
        /// <returns>
        /// The <see cref="StructureMediaType"/>.
        /// </returns>
        public static StructureMediaType GetFromEnum(StructureMediaEnumType enumType)
        {
            StructureMediaType output;
            if (_instances.TryGetValue(enumType, out output))
            {
                return output;
            }

            return null;
        }

        /// <summary>
        /// Gets the format.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <returns>
        /// The <see cref="StructureOutputFormat"/>.
        /// </returns>
        public static StructureOutputFormat GetFormat(ContentType contentType)
        {
            foreach (var structureMediaType in RestValues)
            {
                if (structureMediaType.MediaType.Equals(contentType))
                {
                    return structureMediaType._structureOutputFormat;
                }
            }

            return StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);
        }

        #endregion
    }
}