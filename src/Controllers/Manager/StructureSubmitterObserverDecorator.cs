﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sdmxsource.Extension.Model.Error;
using log4net;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;

namespace Estat.Sri.Ws.Controllers.Manager
{
    public class StructureSubmitterObserverDecorator : IStructureSubmitter
    {
        private readonly IStructureSubmitter _decoratedManager;
        private readonly StructureEventQueue _queue;
        private readonly IStructureChangesObserver _structureObserver;
        private readonly IHttpContextAccessor _contextAccessor;

        public StructureSubmitterObserverDecorator(IStructureSubmitter decoratedManager, StructureEventQueue queue, IHttpContextAccessor contextAccessor, IStructureChangesObserver structureObserver = null)
        {
            _decoratedManager = decoratedManager;
            _queue = queue;
            _structureObserver = structureObserver;
            _contextAccessor = contextAccessor;
        }

        public IList<IResponseWithStatusObject> SubmitStructures(string storeId, ISdmxObjects sdmxObjects)
        {
            var response = _decoratedManager.SubmitStructures(storeId, sdmxObjects);
            this.Process(response);
            return response;
        }

        // non-blocking call
        private void Process(IList<IResponseWithStatusObject> response)
        {
            if (this._structureObserver == null)
                return;

            var userPrincipal = _contextAccessor.HttpContext.User.Clone();

            _queue.Enqueue(async cancellationToken => await this._structureObserver.Notify(response, userPrincipal));
        }

        public sealed class StructureEventQueue
        {
            private static readonly ILog _log = LogManager.GetLogger(typeof(StructureEventQueue));
            private readonly ConcurrentQueue<Func<CancellationToken, Task>> Queue = new ConcurrentQueue<Func<CancellationToken, Task>>();
            private int _runningTasks;

            public int MillisecondsToWaitBeforePickingUpTask { get; }
            public int TaskInQueue => Queue.Count;
            public int RunningTasks => _runningTasks;

            public StructureEventQueue(int millisecondsToWaitBeforePickingUpTask)
            {
                if (millisecondsToWaitBeforePickingUpTask < 250)
                    throw new ArgumentException("< 250 Milliseconds will eat the CPU", nameof(millisecondsToWaitBeforePickingUpTask));

                MillisecondsToWaitBeforePickingUpTask = millisecondsToWaitBeforePickingUpTask;
            }

            public void Enqueue(Func<CancellationToken, Task> task)
            {
                Queue.Enqueue(task);
            }

            public async Task Dequeue(CancellationToken cancellationToken)
            {
                if (Queue.TryDequeue(out var nextTaskAction))
                {
                    Interlocked.Increment(ref _runningTasks);
                    try
                    {
                        await nextTaskAction(cancellationToken);
                    }
                    catch (System.Exception e)
                    {
                        _log.Error(e);
                    }
                    finally
                    {
                        Interlocked.Decrement(ref _runningTasks);
                    }
                }

                await Task.CompletedTask;
            }
        }
    }
}