﻿// -----------------------------------------------------------------------
// <copyright file="SettingsManager.cs" company="EUROSTAT">
//   Date Created : 2009-10-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using static System.Reflection.Assembly;

namespace Estat.Sri.Ws.Controllers.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Security;
    using System.Web;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.Ws.Controllers.Constants;

    using log4net;

    /// <summary>
    ///     This singleton class is used to read the configuration settings from the web.config file
    ///     and load them in corresponding objects
    /// </summary>
    public class SettingsManager
    {
        /// <summary>
        ///     The singleton instance
        /// </summary>
        private static readonly SettingsManager _instance = new SettingsManager();

        /// <summary>
        ///     Private field that stores the Log object
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        ///     Holds the set of assemblies
        /// </summary>
        private readonly Dictionary<string, Assembly> _platformAssemblies = new Dictionary<string, Assembly>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The on the fly annotation types
        /// </summary>
        private OnTheFlyAnnotationType _onTheFlyAnnotationTypes;

        /// <summary>
        ///     The _default prefix.
        /// </summary>
        private string _defaultPrefix;

        /// <summary>
        ///     Holds the path to 32bit (win32 platform) assemblies
        /// </summary>
        private string _path32 = SettingsConstants.DefaultBin32;

        /// <summary>
        ///     Holds the path to 64bit (x64 platform) assemblies
        /// </summary>
        private string _path64 = SettingsConstants.DefaultBin64;

        /// <summary>
        ///     Prevents a default instance of the <see cref="SettingsManager" /> class from being created.
        /// </summary>
        private SettingsManager()
        {
            // initialize the log
            this._log = LogManager.GetLogger(typeof(SettingsManager));

            // Initialize any DLL settings
            this.InitializeDll();

            this.InitializePrefix();

            this.InitializeOnFlyAnnotations();
        }

        #region 6.2 ISTAT ENHANCEMENT

        /// <summary>
        ///  Gets a value indicating whether the Submit Structure functionality should be enabled.
        /// </summary>
        public static bool EnableSubmitStructure
        {
            get
            {
                return bool.Parse(ConfigurationManager.AppSettings["enableSubmitStructure"]);
            }
        }

        #endregion

        /// <summary>
        /// Gets the on the fly annotation.
        /// </summary>
        /// <value>
        /// The on the fly annotation.
        /// </value>
        public static OnTheFlyAnnotationType OnTheFlyAnnotation => _instance._onTheFlyAnnotationTypes;

        /// <summary>
        ///     Gets the default prefix.
        /// </summary>
        /// <value>
        ///     The default prefix.
        /// </value>
        public static string DefaultPrefix
        {
            get
            {
                return _instance._defaultPrefix;
            }
        }

        /// <summary>
        /// Gets the default header retriever.
        /// </summary>
        /// <value>
        /// The default header retriever.
        /// </value>
        public static string DefaultHeaderRetriever
        {
            get
            {
                return ConfigurationManager.AppSettings["defaultHeaderRetriever"];
            }
        }

        /// <summary>
        /// Gets the plugin folder.
        /// </summary>
        /// <value>The plugin folder.</value>
        public static string PluginFolder
        {
            get
            {
                return ConfigurationManager.AppSettings["pluginFolder"];
            }
        }

        /// <summary>
        /// Gets a value indicating whether <c>NSIAuthModule</c> middleware is enabled
        /// </summary>
        public static bool AuthModuleEnabled
        {
            get
            {
                var value = ConfigurationManager.AppSettings["estat.nsi.ws.config.auth"];
                if (!string.IsNullOrWhiteSpace(value) && string.Equals(bool.TrueString, value, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether <c>NSIAuthModule</c> middleware is enabled
        /// </summary>
        public static bool CorsModuleEnabled
        {
            get
            {
                var value = ConfigurationManager.AppSettings["corsSettings"];
                if (!string.IsNullOrWhiteSpace(value) && string.Equals(bool.TrueString, value, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether <c>NSIAuthModule</c> middleware is enabled
        /// </summary>
        public static bool PolicyModuleEnabled
        {
            get
            {
                var value = ConfigurationManager.AppSettings["estat.sri.ws.policymodule"];
                if (!string.IsNullOrWhiteSpace(value) && string.Equals(bool.TrueString, value, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the MaxReceivedMessageSize from the config
        /// If the key is not present returns the default value
        /// </summary>
        /// <value>The MaxReceivedMessageSize.</value>
        public static long MaxReceivedMessageSize
        {
            get
            {
                long maxReceivedMessageSize, defaultMaxReceivedMessageSize = 65536;

                if (ConfigurationManager.AppSettings["MaxReceivedMessageSize"] != null)
                {
                    return long.TryParse(ConfigurationManager.AppSettings["MaxReceivedMessageSize"], out maxReceivedMessageSize) ? 
                        maxReceivedMessageSize : defaultMaxReceivedMessageSize;
                }
 
                return defaultMaxReceivedMessageSize;
            }
        }

        public static string MiddlewareImplementation => ConfigurationManager.AppSettings["middlewareImplementation"];

        /// <summary>
        /// Gets the number of errors.
        /// </summary>
        /// <value>
        /// The number of errors.
        /// </value>
        public static string NumberOfErrors => ConfigurationManager.AppSettings["numberOfErrors"];

        /// <summary>
        /// Gets the struval URL.
        /// </summary>
        /// <value>
        /// The struval URL.
        /// </value>
        public static string StruvalUrl => ConfigurationManager.AppSettings["struvalUrl"];
        /// <summary>
        /// Gets the CSV configuration.
        /// </summary>
        /// <value>
        /// The CSV configuration.
        /// </value>
        public static string CsvConfig => ConfigurationManager.AppSettings["csvConfig"];

        /// <summary>
        /// Initializes the on fly annotations.
        /// </summary>
        private void InitializeOnFlyAnnotations()
        {
            var alllowLocalDataAsAnnoations = ConfigurationManager.AppSettings["AllowLocalDataAsAnnotations"];
            bool value;
            if (!string.IsNullOrWhiteSpace(alllowLocalDataAsAnnoations) && bool.TryParse(alllowLocalDataAsAnnoations, out value) && value)
            {
                this._onTheFlyAnnotationTypes |= OnTheFlyAnnotationType.LocalData;
            }
        }

        /// <summary>
        /// Handle resolving of the <c>System.Data.SQLite.dll</c> depending on the current platform
        /// </summary>
        /// <param name="sender">
        /// The source
        /// </param>
        /// <param name="args">
        /// The <see cref="ResolveEventArgs"/>
        /// </param>
        /// <returns>
        /// An <see cref="Assembly"/>; otherwise null
        /// </returns>
        private Assembly CurrentDomainAssemblyResolve(object sender, ResolveEventArgs args)
        {
            string name = new AssemblyName(args.Name).Name;
            Assembly dll;
            if (this._platformAssemblies.TryGetValue(name, out dll))
            {
                if (dll == null)
                {
                    dll = this.LoadAssembly(name);
                    this._platformAssemblies[name] = dll;
                }
            }
            else
            {
                this._log.Warn(string.Format(CultureInfo.InvariantCulture, "Assembly '{0}' was requested but is not configured.", args.Name));
            }

            return dll;
        }

        /// <summary>
        ///     Get the current platform specific binary path
        /// </summary>
        /// <returns>
        ///     The platform specific path
        /// </returns>
        private string GetPlatformPath()
        {
            return IntPtr.Size == 8 ? this._path64 : this._path32;
        }

        /// <summary>
        ///     Append any path and platform specific settings
        /// </summary>
        private void InitializeDll()
        {
            try
            {
                string p32 = ConfigurationManager.AppSettings[SettingsConstants.Bin32];
                if (!string.IsNullOrEmpty(p32))
                {
                    this._path32 = p32;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            string p64 = ConfigurationManager.AppSettings[SettingsConstants.Bin64];
            

            if (!string.IsNullOrEmpty(p64))
            {
                this._path64 = p64;
            }
            var location = GetEntryAssembly().Location;
            var directory = Path.GetDirectoryName(location);
            string path = Path.Combine(directory, this.GetPlatformPath());
            string configuredPath = ConfigurationManager.AppSettings[SettingsConstants.PathSetting];

            if (!string.IsNullOrEmpty(configuredPath))
            {
                this._log.Info("Configured path:" + configuredPath);
                path = string.Format(CultureInfo.InvariantCulture, "{0};{1}", configuredPath, path);
            }

            string env = Environment.GetEnvironmentVariable(SettingsConstants.PathEnvironment);
            Environment.SetEnvironmentVariable(SettingsConstants.PathEnvironment, string.Format(CultureInfo.InvariantCulture, "{0}{1}{2}", path, Path.PathSeparator, env));
            this._platformAssemblies.Add(SettingsConstants.SqlLiteDataProvider, null);
            string assemlies = ConfigurationManager.AppSettings[SettingsConstants.PlatformSpecificAssemblies];
            if (!string.IsNullOrEmpty(assemlies))
            {
                string[] list = assemlies.Split(',');
                foreach (string assembly in list)
                {
                    this._platformAssemblies[assembly.Trim()] = null;
                }
            }

            AppDomain.CurrentDomain.AssemblyResolve += this.CurrentDomainAssemblyResolve;
        }

        /// <summary>
        ///     Initializes the prefix.
        /// </summary>
        private void InitializePrefix()
        {
            this._defaultPrefix = ConfigurationManager.AppSettings[SettingsConstants.DefaultPrefix];
        }

        /// <summary>
        /// Try load the assembly with the specified simple name from the <see cref="GetPlatformPath()"/> path
        /// </summary>
        /// <param name="name">
        /// The simple name of the assembly
        /// </param>
        /// <returns>
        /// The assembly or null
        /// </returns>
        private Assembly LoadAssembly(string name)
        {
            string platformPath = this.GetPlatformPath();

            string dllPath = Path.Combine(platformPath, name + SettingsConstants.DllExtension);
            Assembly ret = null;

            string info = string.Format(CultureInfo.InvariantCulture, Messages.LoadingAssemblyFormat1, name);
            this._log.Info(info);

            var location = GetEntryAssembly().Location;
            string root = Path.GetDirectoryName(location);
            dllPath = Path.Combine(root, dllPath);
            dllPath = Path.GetFullPath(dllPath);
            if (!File.Exists(dllPath))
            {
                string message = string.Format(CultureInfo.InvariantCulture, Messages.ErrorAssemblyDoesntExistFormat2, name, dllPath);
                this._log.Error(message);
                return null;
            }

            try
            {
                AssemblyName aname = AssemblyName.GetAssemblyName(dllPath);
                if (aname != null && aname.Name.Contains(name))
                {
                    ret = Load(aname);
                    this._log.Info(string.Format(CultureInfo.InvariantCulture, Messages.SettingsManager_LoadAssembly_Assembly__0__was_loaded_successfully, name));
                }
            }
            catch (FileLoadException e)
            {
                this._log.Error(string.Format(CultureInfo.InvariantCulture, Messages.SettingsManager_LoadAssembly_An_error_occured_while_trying_to_pre_load_DLL_____0__, e));
            }
            catch (BadImageFormatException e)
            {
                this._log.Error(string.Format(CultureInfo.InvariantCulture, Messages.SettingsManager_LoadAssembly_The_DLL___0___seems_to_be_for_a_different_architecture, dllPath));
                this._log.Error(e.ToString());
            }
            catch (SecurityException e)
            {
                this._log.Error(string.Format(CultureInfo.InvariantCulture, Messages.SettingsManager_LoadAssembly_Could_not_access_the__0___Please_check_the_permissions, dllPath));
                this._log.Error(e.ToString());
            }
            catch (ArgumentException e)
            {
                this._log.Error(string.Format(CultureInfo.InvariantCulture, Messages.SettingsManager_LoadAssembly_An_error_occured_while_trying_to_pre_load_DLL_____0__, e));
            }
            catch (FileNotFoundException e)
            {
                string message = string.Format(CultureInfo.InvariantCulture, Messages.ErrorAssemblyDoesntExistFormat2, dllPath, dllPath);
                this._log.Error(message);
                this._log.Error(e.ToString());
            }

            return ret;
        }
    }
}