﻿// -----------------------------------------------------------------------
// <copyright file="AuthorizationStructureSubmitterDecorator.cs" company="EUROSTAT">
//   Date Created : 2020-9-10
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Estat.Sdmxsource.Extension.Manager;
using Estat.Sdmxsource.Extension.Model.Error;
using Estat.Sri.Ws.Controllers.Factory;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using System.Collections.Generic;

namespace Estat.Sri.Ws.Controllers.Manager
{
    public class AuthorizationStructureSubmitterDecorator : IStructureSubmitter
    {
        private readonly IStructureSubmitter _decoratedManager;
        private readonly ISdmxAuthorizationScopeFactory authorizationScopeFactory;

        public AuthorizationStructureSubmitterDecorator(IStructureSubmitter decoratedManager, ISdmxAuthorizationScopeFactory authorizationScopeFactory)
        {
            _decoratedManager = decoratedManager;
            this.authorizationScopeFactory = authorizationScopeFactory;
        }

        public IList<IResponseWithStatusObject> SubmitStructures(string storeId, ISdmxObjects sdmxObjects)
        {
            using (Mapping.Api.Utils.SdmxAuthorizationScope scope = authorizationScopeFactory.Create())
            {
                return _decoratedManager.SubmitStructures(storeId, sdmxObjects);
            }
        }
    }
}
