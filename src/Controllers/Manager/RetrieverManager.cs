// -----------------------------------------------------------------------
// <copyright file="RetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2015-11-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Ws.Controllers.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Config;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;

    using Estat.Sri.CustomRequests.Builder;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Factory;

    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.Query;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Workspace;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    ///     The default implementation of <see cref="IRetrieverManager" />
    /// </summary>
    public class RetrieverManager : IRetrieverManager
    {
        /// <summary>
        ///     The _log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(RetrieverManager));

        /// <summary>
        ///     The _dataflow authorization manager
        /// </summary>
        private readonly IDataflowAuthorizationManager _dataflowAuthorizationManager;

        private readonly IRangeHeaderHandler _headerHandler;

        /// <summary>
        ///     The _dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        ///     The _data query parse manager
        /// </summary>
        private readonly IDataQueryParseManager _dataQueryParseManager = new DataQueryParseManager(SdmxSchemaEnumType.Null);

        /// <summary>
        ///     The Query Parsing manager.
        /// </summary>
        private readonly IQueryParsingManager _queryParsingManager = new QueryParsingManager(SdmxSchemaEnumType.Null);

        /// <summary>
        ///     The Query Parsing Manager for special requests V20
        /// </summary>
        private readonly IQueryParsingManager _queryParsingManagerCustomRequests;

        /// <summary>
        ///     The _retriever factories
        /// </summary>
        private readonly IList<IRetrieverFactory> _retrieverFactories;

        /// <summary>
        ///     The _structure request behavior
        /// </summary>
        private readonly RequestBehaviorType _structureRequestBehavior;

        /// <summary>
        /// Authorization scope factory
        /// </summary>
        private readonly ISdmxAuthorizationScopeFactory authorizationScopeFactory;

        /// <summary>
        /// The configuration.
        /// </summary>
        private readonly SettingsFromConfigurationManager _configuration;

        /// <summary>
        ///     Initializes a new instance of the <see cref="RetrieverManager" /> class.
        /// </summary>
        /// <param name="retrieverFactories">The retriever factories.</param>
        /// <param name="dataflowPrincipalManager">The dataflow principal manager.</param>
        /// <param name="dataflowAuthorizationManager">The dataflow authorization manager.</param>
        /// <param name="headerHandler"></param>
        /// <exception cref="ArgumentNullException"><paramref name="retrieverFactories" /> is <see langword="null" />.</exception>
        /// <exception cref="ArgumentException"><paramref name="retrieverFactories" /> is empty.</exception>
        public RetrieverManager(IEnumerable<IRetrieverFactory> retrieverFactories, IDataflowPrincipalManager dataflowPrincipalManager, IDataflowAuthorizationManager dataflowAuthorizationManager, IRangeHeaderHandler headerHandler, ISdmxAuthorizationScopeFactory authorizationScopeFactory, SettingsFromConfigurationManager configuration)
        {
            this._dataflowPrincipalManager = dataflowPrincipalManager;
            this._dataflowAuthorizationManager = dataflowAuthorizationManager;
            this._headerHandler = headerHandler;
            this._queryParsingManagerCustomRequests = new QueryParsingManager(SdmxSchemaEnumType.VersionTwo, new QueryBuilder(null, new ConstrainQueryBuilderV2(), null));
            if (retrieverFactories == null)
            {
                throw new ArgumentNullException("retrieverFactories");
            }

            IBuilder<IList<IRetrieverFactory>, IEnumerable<IRetrieverFactory>> retrieverOrderBuilder = new RetrieverOrderBuilder();
            this._retrieverFactories = retrieverOrderBuilder.Build(retrieverFactories);

            if (this._retrieverFactories.Count == 0)
            {
                throw new ArgumentException(Messages.NoRetrieverFactoriesSpecified, "retrieverFactories");
            }

            this._structureRequestBehavior = ModuleConfigSection.GetSection().StructureRequestBehavior;
            this.authorizationScopeFactory = authorizationScopeFactory;

            this._configuration = configuration;
        }

        /// <summary>
        ///     Gets the data response generator complex.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{TWriter}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<IDataWriterEngine> GetDataResponseComplex(IReadableDataLocation input)
        {
            return this.GetDataResponse((manager, factory) => this.BuildDataResponseGeneratorComplex(input, manager, factory));
        }

        /// <summary>
        ///     Gets the data response for SDMX v2.0 Cross Sectional.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(IReadableDataLocation input)
        {
            return this.GetDataResponse((manager, factory) => this.BuildDataResponseGeneratorCross(input, manager, factory, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo)));
        }

        /// <summary>
        ///     Gets the SDMX v2.0 CrossSectional data response from a REST request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse&lt;ICrossSectionalWriterEngine&gt;" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<ICrossSectionalWriterEngine> GetDataResponseCross(DataRequest request)
        {
            return this.BuildDataResponseGeneratorCross(request, SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne));
        }

        /// <summary>
        ///     Gets the data response simple.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<IDataWriterEngine> GetDataResponseSimple(IReadableDataLocation input)
        {
            return this.GetDataResponse((manager, factory) => this.BuildDataResponseGenerator(input, manager, factory));
        }

        /// <summary>
        ///     Gets the data response simple.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        /// <exception cref="SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        public IDataResponse<IDataWriterEngine> GetDataResponseSimple(DataRequest request)
        {
            return this.BuildDataResponse(request);
        }

        /// <summary>
        ///     Parse structure request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Operation not accepted</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">Could not find requested structures</exception>
        public ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestStructureQuery query)
        {
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            using (var scope = authorizationScopeFactory.Create())
            {
                IPrincipal dataflowPrincipal = this._dataflowPrincipalManager.GetCurrentPrincipal();

                var tryRetrieverFactory = this.BuildRetrieverMethod(
                    dataflowPrincipal, 
                    factory => this.GetMutableObjects(factory, sdmxSchema, query), 
                    (factory, list) => GetMutableObjects(factory, sdmxSchema, query, list)
                );

                return this.TryFactories(tryRetrieverFactory);
            }
        }

        /// <summary>
        /// Parses the request.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Objects.ISdmxObjects" />.</returns>
        public ISdmxObjects ParseRequest(SdmxSchema sdmxSchema, IRestAvailableConstraintQuery query)
        {
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            IRestAvailabilityQuery queryV2 = query as IRestAvailabilityQuery;
            if (queryV2 != null && queryV2.Context != SdmxStructureEnumType.Dataflow)
            {
                throw new NotImplementedException($"Availability queries for '{query.FlowRef.MaintainableStructureEnumType.StructureType}' is not implemented." +
                    " Only 'Dataflow' is accepted.");
            }

            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            using (var scope = this.authorizationScopeFactory.Create())
            {
                if (scope.IsEnabled)
                {
                    if (queryV2 != null)
                    {
                        bool AuthFunc(IStructureReference flowRef) => 
                            scope.CurrentSdmxAuthorisation.CanReadData(flowRef, false);
                        this.HandleQueryV2Authorisation(queryV2, AuthFunc);
                    }
                    else
                    {
                        // TODO get isPit
                        if (!scope.CurrentSdmxAuthorisation.CanReadData(query.FlowRef, false))
                        {
                            string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, query.FlowRef.MaintainableId);
                            throw new SdmxUnauthorisedException(errorMessage);
                        }
                    }
                }
                else
                {
                    // TODO old authorization mechanism
                    var engine = this._dataflowAuthorizationManager.GetAuthorizationEngine(principal);
                    if (engine != null)
                    {
                        if (queryV2 != null)
                        {
                            bool AuthFunc(IStructureReference flowRef) => 
                                engine.GetAuthorization(flowRef, PermissionType.CanReadData) != AuthorizationStatus.Denied;
                            this.HandleQueryV2Authorisation(queryV2, AuthFunc);
                        }
                        else
                        {
                            var status = engine.GetAuthorization(query.FlowRef, PermissionType.CanReadData);
                            if (status == AuthorizationStatus.Denied)
                            {
                                string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, query.FlowRef.MaintainableId);
                                throw new SdmxUnauthorisedException(errorMessage);
                            }
                        } 
                    }
                }
            }

            Func<IRetrieverFactory, ISdmxObjects> tryRetrieverFactory = (factory) => this.GetMutableObjects(factory, sdmxSchema, query, principal);
            return this.TryFactories(tryRetrieverFactory);
        }

        /// <summary>
        /// Handles the authorisation check for the availability query for SDMX REST 2.0
        /// </summary>
        /// <param name="query">The query to authorise.</param>
        /// <param name="authFunc">The authorisation method to use.</param>
        /// <exception cref="SdmxUnauthorisedException">If authorisation fails.</exception>
        private void HandleQueryV2Authorisation(IRestAvailabilityQuery query, Func<IStructureReference, bool> authFunc)
        {
            bool isValid = false;
            var flowRefIterator = query.StartFlowRefIterator();
            while (flowRefIterator.MoveNext())
            {
                HeaderScope.RequestAborted.ThrowIfCancellationRequested();

                // TODO get isPit
                if (authFunc(query.FlowRef))
                {
                    isValid = true;
                }
                else
                {
                    // wait for other structure references to end
                    string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, query.FlowRef.MaintainableId);
                    _log.Info(errorMessage);
                }
            }
            if (!isValid)
            {
                string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowQueryFormat1, query.ToString());
                throw new SdmxUnauthorisedException(errorMessage);
            }
        }

        /// <summary>
        ///     Gets the structure response for SDMX SOAP.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="input">The input.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="input" /> is <see langword="null" />.</exception>
        /// <exception cref="SdmxSemmanticException">Operation not accepted.</exception>
        /// <exception cref="SdmxNoResultsException">Could not find requested structures</exception>
        public ISdmxObjects ParseRequest(SdmxSchemaEnumType sdmxSchema, IReadableDataLocation input)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }

            using (var scope = authorizationScopeFactory.Create())
            {
                IPrincipal dataflowPrincipal = this._dataflowPrincipalManager.GetCurrentPrincipal();

                return sdmxSchema == SdmxSchemaEnumType.VersionTwoPointOne 
                    ? this.GetStructureResponseV21(input, dataflowPrincipal) 
                    : this.GetStructureResponseV20(input, dataflowPrincipal);
            }
        }

        /// <summary>
        /// Parses the data request.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns>
        /// The <see cref="DataRequest" />.
        /// </returns>
        public DataRequest ParseDataRequest(IRestDataQuery dataQuery)
        {
            return this.GetDataRequest(dataQuery);
        }

        /// <summary>
        ///     Gets the <see cref="IMutableObjects" /> from SOAP V21 SOAP structure.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="query">The query.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private static IMutableObjects GetFromSoapV21Structure(IRetrieverFactory retrieverFactory, IComplexStructureQuery query, IList<IMaintainableRefObject> allowedDataflows)
        {
            var retrieverManager = retrieverFactory.GetAuthAdvancedMutableStructureSearch(allowedDataflows);
            if (retrieverManager != null)
            {
                return retrieverManager.GetMaintainables(query);
            }

            return null;
        }

        /// <summary>
        ///     Gets the mutable objects.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="queryWorkspace">The query workspace.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private static IMutableObjects GetMutableObjects(IRetrieverFactory retrieverFactory, SdmxSchema sdmxSchema, IQueryWorkspace queryWorkspace, IList<IMaintainableRefObject> allowedDataflows)
        {
            IMutableObjects mutable = null;
            var mutableStructureSearch = retrieverFactory.GetAuthMutableStructureSearch(sdmxSchema, allowedDataflows);
            if (mutableStructureSearch != null)
            {
                mutable = mutableStructureSearch.RetrieveStructures(queryWorkspace.SimpleStructureQueries, queryWorkspace.ResolveReferences, false);
            }

            return mutable;
        }

        /// <summary>
        /// Gets the mutable objects.
        /// </summary>
        /// <param name="factory">The factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private ISdmxObjects GetMutableObjects(IRetrieverFactory factory, SdmxSchema sdmxSchema, IRestAvailableConstraintQuery query, IPrincipal principal)
        {
            var mutableStructureSearch = factory.GetAvailableDataManager(sdmxSchema, principal);
            var retrievalManager = factory.GetSdmxObjectRetrieval(principal);
            IMutableObjects mutableObjects;
            
            if (query is IRestAvailabilityQuery queryV2)
            {
                mutableObjects = new MutableObjectsImpl();

                var flowRefIterator = queryV2.StartFlowRefIterator();
                bool hasMultipleReferences = flowRefIterator.Count > 1;
                
                while (flowRefIterator.MoveNext() && !HeaderScope.RequestAborted.IsCancellationRequested)
                {
                    try
                    {
                        var dynamiccQuery = new AvailableConstraintQuery(queryV2, retrievalManager);
                        mutableObjects = mutableStructureSearch.Retrieve(dynamiccQuery, mutableObjects, hasMultipleReferences);
                    }
                    catch (SdmxNoResultsException)
                    {
                        // wait for other structure references to end
                        _log.Debug($"{factory.Id} returned no results for {queryV2.FlowRef}.");
                    }
                }

                if (mutableObjects.AllMaintainables.Count == 0)
                {
                    throw new SdmxNoResultsException();
                }    
            }
            else
            {
                var dynamiccQuery = new AvailableConstraintQuery(query, retrievalManager);
                mutableObjects = mutableStructureSearch.Retrieve(dynamiccQuery);
            }

            return mutableObjects.ImmutableObjects;
        }

        /// <summary>
        ///     Gets the mutable objects.
        /// </summary>
        /// <param name="factory">The factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private IMutableObjects GetMutableObjects(IRetrieverFactory factory, SdmxSchema sdmxSchema, IRestStructureQuery query, IList<IMaintainableRefObject> allowedDataflows)
        {
            var mutableStructureSearch = factory.GetAuthMutableStructureSearch(sdmxSchema, allowedDataflows);
            var mutableObjects = mutableStructureSearch?.GetMaintainables(query);
            return this._headerHandler.HandleStructures(mutableObjects, query.StructureType.EnumType);
        }

        /// <summary>
        ///     Tries the retriever factory.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="structureRetriever">The structure retriever.</param>
        /// <returns>The <see cref="ISdmxObjects" /> if any results are returned; otherwise null.</returns>
        private ISdmxObjects TryRetrieverFactory(IRetrieverFactory retrieverFactory, Func<IRetrieverFactory, IMutableObjects> structureRetriever)
        {
            try
            {
                IMutableObjects mutable = structureRetriever(retrieverFactory);
                if (mutable != null && mutable.AllMaintainables.Count > 0)
                {
                    return mutable.ImmutableObjects;
                }
            }
            catch (SdmxNoResultsException e)
            {
                _log.Debug(retrieverFactory.Id + " retriever factory returned no results with exception.", e);
            }

            _log.Debug(retrieverFactory.Id + " retriever factory returned no results.");
            return null;
        }

        /// <summary>
        ///     Builds the data response.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        private IDataResponse<IDataWriterEngine> BuildDataResponse(DataRequest request)
        {
            IDataQuery dataQuery = request.DataQuery;
            if (dataQuery.Dataflow != null)
            {
                var retrieverFactory = request.RetrieverFactory;

                // we want SDMX v2.1 behavior with REST even for 2.0 data
                var dataRetrievalManager = retrieverFactory.GetDataRetrieval(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne), this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<IDataWriterEngine, IDataQuery>(new[] { dataQuery }, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        private IDataResponse<IDataWriterEngine> BuildDataResponseGenerator(IReadableDataLocation input, ISdmxObjectRetrievalManager retrievalManager, IRetrieverFactory retrieverFactory)
        {
            var dataQueries = this._dataQueryParseManager.BuildDataQuery(input, retrievalManager);
            if (dataQueries != null && dataQueries.Count > 0)
            {
                // SOAP v2.0 so we always have SDMX v2.0
                var dataRetrievalManager = retrieverFactory.GetDataRetrieval(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo), this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<IDataWriterEngine, IDataQuery>(dataQueries, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator complex.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <returns>The <see cref="IDataResponse{IDataWriterEngine}" />.</returns>
        private IDataResponse<IDataWriterEngine> BuildDataResponseGeneratorComplex(IReadableDataLocation input, ISdmxObjectRetrievalManager retrievalManager, IRetrieverFactory retrieverFactory)
        {
            var complexDataQuery = this._dataQueryParseManager.BuildComplexDataQuery(input, retrievalManager);
            if (complexDataQuery != null && complexDataQuery.Count > 0)
            {
                var dataRetrievalManager = retrieverFactory.GetAdvancedDataRetrieval(this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<IDataWriterEngine, IComplexDataQuery>(complexDataQuery, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator SDMX v2.0 Cross Sectional.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}" />.</returns>
        private IDataResponse<ICrossSectionalWriterEngine> BuildDataResponseGeneratorCross(DataRequest input, SdmxSchema sdmxSchema)
        {
            IDataQuery dataQuery = input.DataQuery;
            if (dataQuery.Dataflow != null)
            {
                IRetrieverFactory retrieverFactory = input.RetrieverFactory;

                // SOAP v2.0 so we always have SDMX v2.0
                var dataRetrievalManager = retrieverFactory.GetDataRetrievalWithCross(sdmxSchema, this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<ICrossSectionalWriterEngine, IDataQuery>(new[] { dataQuery }, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the data response generator SDMX v2.0 Cross Sectional.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="retrievalManager">The retrieval manager.</param>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IDataResponse{ICrossSectionalWriterEngine}" />.</returns>
        private IDataResponse<ICrossSectionalWriterEngine> BuildDataResponseGeneratorCross(IReadableDataLocation input, ISdmxObjectRetrievalManager retrievalManager, IRetrieverFactory retrieverFactory, SdmxSchema sdmxSchema)
        {
            var dataQueries = this._dataQueryParseManager.BuildDataQuery(input, retrievalManager);
            if (dataQueries != null && dataQueries.Count > 0)
            {
                // SOAP v2.0 so we always have SDMX v2.0
                var dataRetrievalManager = retrieverFactory.GetDataRetrievalWithCross(sdmxSchema, this._dataflowPrincipalManager.GetCurrentPrincipal());
                return new DataResponse<ICrossSectionalWriterEngine, IDataQuery>(dataQueries, dataRetrievalManager.GetDataAsync);
            }

            return null;
        }

        /// <summary>
        ///     Builds the structure retriever method.
        /// </summary>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <param name="structureRetriever">The structure retriever.</param>
        /// <param name="structureRetrieverWithAuthorization">The structure retriever with authorization.</param>
        /// <returns>The <see cref="Func{IRetrieverFactory, ISdmxObjects}" />.</returns>
        private Func<IRetrieverFactory, ISdmxObjects> BuildRetrieverMethod(IPrincipal dataflowPrincipal, Func<IRetrieverFactory, IMutableObjects> structureRetriever, Func<IRetrieverFactory, IList<IMaintainableRefObject>, IMutableObjects> structureRetrieverWithAuthorization)
        {
            // TODO old authorization mechanism
            var allowedDataflows = this.GetAllowedDataflows(dataflowPrincipal);
            if (allowedDataflows == null)
            {
                return factory => TryRetrieverFactory(factory, structureRetriever);
            }
            
            return factory => TryRetrieverFactory(factory, retrieverFactory => structureRetrieverWithAuthorization(retrieverFactory, allowedDataflows));
        }

        /// <summary>
        ///     Gets the allowed dataflows.
        /// </summary>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <returns>The list of allowed dataflows represented as references with <see cref="IMaintainableRefObject" />.</returns>
        private IMaintainableRefObject[] GetAllowedDataflows(IPrincipal dataflowPrincipal)
        {
            // TODO old authorization mechanism
            if (this._dataflowAuthorizationManager != null)
            {
                var engine = this._dataflowAuthorizationManager.GetAuthorizationEngine(dataflowPrincipal);
                if (engine != null)
                {
                    return engine.RetrieveAuthorizedDataflows().ToArray();
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the data request.
        /// </summary>
        /// <param name="dataQuery">
        /// The data query.
        /// </param>
        /// <returns>
        /// The <see cref="DataRequest"/>.
        /// </returns>
        private DataRequest GetDataRequest(IRestDataQuery dataQuery)
        {
            Func<ISdmxObjectRetrievalManager, IRetrieverFactory, DataRequest> func = (manager, factory) =>
            {
                IDataQuery query;

                if (dataQuery is IRestDataQueryV2 dataQueryV2)
                {
                    query = new DataQueryV2Impl(dataQueryV2, manager, this._configuration.ApplyContentConstraintsOnDataQueries);
                }
                else
                {
                    query = new DataQueryImpl(dataQuery, manager, this._configuration.ApplyContentConstraintsOnDataQueries);
                }

                if (query.Dataflow != null)
                {
                    return new DataRequest(query, factory, manager);
                }

                return null;
            };

            return GetDataResponseBase(func);
        }

        /// <summary>
        ///     Gets the data response.
        /// </summary>
        /// <typeparam name="TDataWriter">The type of the data writer.</typeparam>
        /// <param name="dataQueryParser">The data query parser.</param>
        /// <returns>The <see cref="IDataResponse{TDataWriter}" />.</returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">
        ///     Could not find Dataflow and/or DSD related
        ///     with this data request
        /// </exception>
        private IDataResponse<TDataWriter> GetDataResponse<TDataWriter>(Func<ISdmxObjectRetrievalManager, IRetrieverFactory, IDataResponse<TDataWriter>> dataQueryParser) where TDataWriter : IDisposable
        {
            return GetDataResponseBase(dataQueryParser);
        }

        /// <summary>
        /// Gets the data response.
        /// </summary>
        /// <typeparam name="TReturnValue">The type of the return value.</typeparam>
        /// <param name="dataQueryParser">The data query parser.</param>
        /// <returns>
        /// The <see cref="IDataResponse{TDataWriter}" />.
        /// </returns>
        /// <exception cref="SdmxNoResultsException">Could not find Dataflow and/or DSD related with this data request</exception>
        private TReturnValue GetDataResponseBase<TReturnValue>(Func<ISdmxObjectRetrievalManager, IRetrieverFactory, TReturnValue> dataQueryParser) where TReturnValue : class
        {
            foreach (var retrieverFactory in this._retrieverFactories)
            {
                MappingStoreIoc.ServiceKey = retrieverFactory.Id;
                var sdmxObjectRetrieval = retrieverFactory.GetSdmxObjectRetrieval(this._dataflowPrincipalManager.GetCurrentPrincipal());

                if (sdmxObjectRetrieval != null)
                {
                    try
                    {
                        var dataResponse = dataQueryParser(sdmxObjectRetrieval, retrieverFactory);
                        if (dataResponse != null)
                        {
                            return dataResponse;
                        }
                    }
                    catch (SdmxNoResultsException e)
                    {
                        _log.Debug(retrieverFactory.Id + " retriever factory returned no results with exception.", e);
                    }

                    _log.Info(retrieverFactory.Id + " retriever factory returned no results");
                }
                else
                {
                    _log.Info(retrieverFactory.Id + " retriever factory didn't return a result");
                }
            }

            throw new SdmxNoResultsException("Could not find Dataflow and/or DSD related with this data request");
        }

        /// <summary>
        ///     Gets the <see cref="IMutableObjects" /> from SOAP V21 SOAP structure.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private IMutableObjects GetFromSoapV21Structure(IRetrieverFactory retrieverFactory, IComplexStructureQuery query)
        {
            var retrieverManager = retrieverFactory.GetAdvancedMutableStructureSearchManager(this._dataflowPrincipalManager.GetCurrentPrincipal());
            if (retrieverManager != null)
            {
                return retrieverManager.GetMaintainables(query);
            }

            return null;
        }

        /// <summary>
        ///     Gets the mutable objects.
        /// </summary>
        /// <param name="retrieverFactory">The retriever factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="queryWorkspace">The query workspace.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private IMutableObjects GetMutableObjects(IRetrieverFactory retrieverFactory, SdmxSchema sdmxSchema, IQueryWorkspace queryWorkspace)
        {
            IMutableObjects mutable = null;
            var mutableStructureSearch = retrieverFactory.GetMutableStructureSearch(sdmxSchema, this._dataflowPrincipalManager.GetCurrentPrincipal());
            if (mutableStructureSearch != null)
            {
                mutable = mutableStructureSearch.RetrieveStructures(queryWorkspace.SimpleStructureQueries, queryWorkspace.ResolveReferences, false);
            }

            return mutable;
        }

        /// <summary>
        ///     Gets the mutable objects.
        /// </summary>
        /// <param name="factory">The factory.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="query">The query.</param>
        /// <returns>The <see cref="IMutableObjects" />.</returns>
        private IMutableObjects GetMutableObjects(IRetrieverFactory factory, SdmxSchema sdmxSchema, IRestStructureQuery query)
        {
            var mutableStructureSearch = factory.GetMutableStructureSearch(sdmxSchema, this._dataflowPrincipalManager.GetCurrentPrincipal());
            if (mutableStructureSearch != null)
            {
                var mutableObjects = mutableStructureSearch.GetMaintainables(query);
                return this._headerHandler.HandleStructures(mutableObjects, query.StructureType.EnumType);
            }

            return null;
        }

        /// <summary>
        ///     Gets the structure response for SDMX V20 SOAP.
        /// </summary>
        /// <param name="input">The readable data location.</param>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        private ISdmxObjects GetStructureResponseV20(IReadableDataLocation input, IPrincipal dataflowPrincipal)
        {
            var queryWorkspace = this._queryParsingManagerCustomRequests.ParseQueries(input);
            if (queryWorkspace == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            var query = queryWorkspace.SimpleStructureQueries;
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            var sdmxSchema = SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwo);
            var tryRetrieverFactory = this.BuildRetrieverMethod(dataflowPrincipal, 
                factory => this.GetMutableObjects(factory, sdmxSchema, queryWorkspace), 
                (factory, list) => GetMutableObjects(factory, sdmxSchema, queryWorkspace, list)
            );
            return this.TryFactories(tryRetrieverFactory);
        }

        /// <summary>
        ///     Gets the structure response for SDMX SOAP V21.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="dataflowPrincipal">The dataflow principal.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Operation not accepted.</exception>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">Could not find requested structures</exception>
        private ISdmxObjects GetStructureResponseV21(IReadableDataLocation input, IPrincipal dataflowPrincipal)
        {
            var queryWorkspace = this._queryParsingManager.ParseQueries(input);
            if (queryWorkspace == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            IComplexStructureQuery query = queryWorkspace.ComplexStructureQuery;
            if (query == null)
            {
                throw new SdmxSemmanticException(Messages.ErrorOperationNotAccepted);
            }

            var tryRetrieverFactory = this.BuildRetrieverMethod(dataflowPrincipal, 
                factory => this.GetFromSoapV21Structure(factory, query), 
                (factory, list) => GetFromSoapV21Structure(factory, query, list)
            );

            return this.TryFactories(tryRetrieverFactory);
        }

        /// <summary>
        ///     Tries the factories.
        /// </summary>
        /// <param name="tryRetrieverFactory">The method that will try all retriever factory.</param>
        /// <returns>The <see cref="ISdmxObjects" />.</returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxNoResultsException">Could not find requested structures</exception>
        private ISdmxObjects TryFactories(Func<IRetrieverFactory, ISdmxObjects> tryRetrieverFactory)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            
            foreach (var retrieverFactory in this._retrieverFactories)
            {
                HeaderScope.RequestAborted.ThrowIfCancellationRequested();

                MappingStoreIoc.ServiceKey = retrieverFactory.Id;
                ISdmxObjects immutableObjects;
                try
                {
                    immutableObjects = tryRetrieverFactory(retrieverFactory);
                }
                catch (SdmxNoResultsException)
                {
                    continue;
                }
                if (immutableObjects != null)
                {
                    if (this._structureRequestBehavior == RequestBehaviorType.First)
                    {
                        return immutableObjects;
                    }

                    if (this._structureRequestBehavior == RequestBehaviorType.Merge)
                    {
                        sdmxObjects.Merge(immutableObjects);
                    }
                }
            }

            if (sdmxObjects.GetAllMaintainables().Count == 0)
            {
                throw new SdmxNoResultsException("Could not find requested structures");
            }

            return sdmxObjects;
        }
    }
}