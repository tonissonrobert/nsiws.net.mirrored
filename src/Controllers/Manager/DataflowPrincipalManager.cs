﻿// -----------------------------------------------------------------------
// <copyright file="DataflowPrincipalManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-14
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Controllers.Manager
{
    using System.Security.Principal;
    using System.Web;

    using Estat.Sdmxsource.Extension.Manager;

    /// <summary>
    ///     Implementation that retrieves the <see cref="IPrincipal" /> from <c>HttpContext.Current.User</c>
    /// </summary>
    public class DataflowPrincipalManager : IDataflowPrincipalManager
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public DataflowPrincipalManager(IHttpContextAccessor contextAccessor)
        {
            this._contextAccessor = contextAccessor;
        }

        /// <summary>
        ///     Gets the current <see cref="IPrincipal" />.
        /// </summary>
        /// <returns>The <see cref="IPrincipal" />; otherwise null.</returns>
        public IPrincipal GetCurrentPrincipal()
        {
            if (Thread.CurrentPrincipal != null)
            {
                return Thread.CurrentPrincipal;
            }
            else
            {
                return null;
            }
        } 
    }
}