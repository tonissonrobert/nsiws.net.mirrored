﻿// -----------------------------------------------------------------------
// <copyright file="IMessageFaultSoapBuilderFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-11
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Factory
{
    using Estat.Sri.Ws.Controllers.Builder;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// A factory interface for getting <see cref="IMessageFaultSoapBuilder"/> from SDMX Scheme version
    /// </summary>
    /// <remarks>For WCF only.</remarks>
    public interface IMessageFaultSoapBuilderFactory
    {
        /// <summary>
        /// Gets the message fault SOAP builder.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IMessageFaultSoapBuilder"/>.</returns>
        IMessageFaultSoapBuilder GetMessageFaultSoapBuilder(SdmxSchemaEnumType sdmxSchema);

        /// <summary>
        /// Gets the message fault SOAP builder.
        /// </summary>
        /// <param name="soap20namespace">The SDMX Web Service namespace.</param>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <returns>The <see cref="IMessageFaultSoapBuilder"/>.</returns>
        IMessageFaultSoapBuilder GetMessageFaultSoapBuilder(SdmxSchemaEnumType sdmxSchema, string soap20namespace);
    }
}