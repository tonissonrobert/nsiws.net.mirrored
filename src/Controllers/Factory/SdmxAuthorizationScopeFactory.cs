﻿using estat.sri.ws.auth.api;
using estat.sri.ws.auth.generic;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Ws.Controllers.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Estat.Sri.Ws.Controllers.Factory
{
    public class SdmxAuthorizationScopeFactory : ISdmxAuthorizationScopeFactory
    {
        /// <summary>
        ///     The _dataflow principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        private readonly IGenericAuthorization _genericAuthorization;
        private readonly IStoreIdBuilderManager _storeIdBuilderManager;

        public SdmxAuthorizationScopeFactory(IDataflowPrincipalManager dataflowPrincipalManager, IStoreIdBuilderManager storeIdBuilderManager, IGenericAuthorization genericAuthorization = null)
        {
            _dataflowPrincipalManager = dataflowPrincipalManager ?? throw new ArgumentNullException(nameof(dataflowPrincipalManager));
            this._genericAuthorization = genericAuthorization;
            this._storeIdBuilderManager = storeIdBuilderManager;
        }

        public SdmxAuthorizationScope Create()
        {
            SdmxAuthorizationImpl sdmxAuthorization = null;

            var principal = this._dataflowPrincipalManager.GetCurrentPrincipal();
            if (this._genericAuthorization != null && principal is SriPrincipalWithRules principalWithRules)
            {
                string storeId = this._storeIdBuilderManager.Build(principalWithRules);

                sdmxAuthorization = new SdmxAuthorizationImpl(principalWithRules, storeId, this._genericAuthorization);
            }

            return new SdmxAuthorizationScope(sdmxAuthorization);

        }
    }
}
