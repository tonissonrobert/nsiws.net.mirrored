﻿// -----------------------------------------------------------------------
// <copyright file="IDataQueryVisitorFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Factory
{
    using Estat.Sdmxsource.Extension.Engine;

    using Org.Sdmxsource.Sdmx.Api.Constants;

    /// <summary>
    /// A factory interface for retrieving <see cref="IDataQueryVisitor"/> instances based on the <see cref="BaseDataFormat"/> and <see cref="SdmxSchema"/>
    /// </summary>
    public interface IDataQueryVisitorFactory
    {
        /// <summary>
        /// Gets the data query visitor.
        /// </summary>
        /// <param name="dataFormat">The data format.</param>
        /// <returns>The <see cref="IDataQueryVisitor"/>.</returns>
        IDataQueryVisitor GetDataQueryVisitor(DataType dataFormat);
    }
}