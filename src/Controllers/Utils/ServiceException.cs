﻿using System;
using System.Net;

namespace Controllers.Utils
{
    public class ServiceException : Exception
    {
        private readonly HttpStatusCode _httpStatusCode;
        private readonly Exception _ex;

        public ServiceException(HttpStatusCode httpStatusCode)
        {
            this._httpStatusCode = httpStatusCode;
        }
        public ServiceException(HttpStatusCode httpStatusCode, string message) : base(message)
        {
            this._httpStatusCode = httpStatusCode;
        }
        public ServiceException(HttpStatusCode httpStatusCode, Exception e) : base(e.Message, e)
        {
            this._httpStatusCode = httpStatusCode;
        }

        public HttpStatusCode HttpStatusCode => this._httpStatusCode;

    }
}
