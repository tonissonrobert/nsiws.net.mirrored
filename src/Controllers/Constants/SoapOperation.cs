﻿// -----------------------------------------------------------------------
// <copyright file="SoapOperation.cs" company="EUROSTAT">
//   Date Created : 2013-10-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Constants
{
    /// <summary>
    ///     The soap operation.
    /// </summary>
    public enum SoapOperation
    {
        /// <summary>
        ///     The default value.
        /// </summary>
        Null = 0, 

        /// <summary>
        ///     The get compact data.
        /// </summary>
        GetCompactData, 

        /// <summary>
        ///     The get compact data.
        /// </summary>
        GetUtilityData, 

        /// <summary>
        ///     The get cross sectional data.
        /// </summary>
        GetCrossSectionalData, 

        /// <summary>
        ///     The get generic data.
        /// </summary>
        GetGenericData, 

        /// <summary>
        ///     The get generic time series data.
        /// </summary>
        GetGenericTimeSeriesData, 

        /// <summary>
        ///     The get structure specific data.
        /// </summary>
        GetStructureSpecificData, 

        /// <summary>
        ///     The get structure specific time series data.
        /// </summary>
        GetStructureSpecificTimeSeriesData, 

        /// <summary>
        ///     The get generic metadata.
        /// </summary>
        GetGenericMetadata, 

        /// <summary>
        ///     The get structure specific metadata.
        /// </summary>
        GetStructureSpecificMetadata, 

        /// <summary>
        ///     The get structures.
        /// </summary>
        GetStructures, 

        /// <summary>
        ///     The get dataflow.
        /// </summary>
        GetDataflow, 

        /// <summary>
        ///     The get metadataflow.
        /// </summary>
        GetMetadataflow, 

        /// <summary>
        ///     The get data structure.
        /// </summary>
        GetDataStructure, 

        /// <summary>
        ///     The get metadata structure.
        /// </summary>
        GetMetadataStructure, 

        /// <summary>
        ///     The get category scheme.
        /// </summary>
        GetCategoryScheme, 

        /// <summary>
        ///     The get concept scheme.
        /// </summary>
        GetConceptScheme, 

        /// <summary>
        ///     The get codelist.
        /// </summary>
        GetCodelist, 

        /// <summary>
        ///     The get hierarchical codelist.
        /// </summary>
        GetHierarchicalCodelist, 

        /// <summary>
        ///     The get organisation scheme.
        /// </summary>
        GetOrganisationScheme, 

        /// <summary>
        ///     The get reporting taxonomy.
        /// </summary>
        GetReportingTaxonomy, 

        /// <summary>
        ///     The get structure set.
        /// </summary>
        GetStructureSet, 

        /// <summary>
        ///     The get process.
        /// </summary>
        GetProcess, 

        /// <summary>
        ///     The get categorisation.
        /// </summary>
        GetCategorisation, 

        /// <summary>
        ///     The get provision agreement.
        /// </summary>
        GetProvisionAgreement, 

        /// <summary>
        ///     The get constraint.
        /// </summary>
        GetConstraint, 

        /// <summary>
        ///     The get data schema.
        /// </summary>
        GetDataSchema, 

        /// <summary>
        ///     The get metadata schema.
        /// </summary>
        GetMetadataSchema, 

        /// <summary>
        ///     The query structure
        /// </summary>
        QueryStructure,

        /// <summary>
        /// The submit registrations
        /// </summary>
        SubmitRegistrations,

        /// <summary>
        /// The query registration
        /// </summary>
        QueryRegistration,

        /// <summary>
        /// The submit structure
        /// </summary>
        SubmitStructure,

        /// <summary>
        /// The submit subscriptions
        /// </summary>
        SubmitSubscriptions,

        /// <summary>
        /// The query subscription
        /// </summary>
        QuerySubscription,

        /// <summary>
        /// The registry interface request
        /// </summary>
        RegistryInterfaceRequest
    }
}