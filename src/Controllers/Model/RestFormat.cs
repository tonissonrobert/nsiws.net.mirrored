﻿// -----------------------------------------------------------------------
// <copyright file="RestFormat.cs" company="EUROSTAT">
//   Date Created : 2015-12-14
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.Controllers.Model
{

    using Estat.Sdmxsource.Extension.Model;

    /// <summary>
    ///     Format for REST WCF output
    /// </summary>
    public class RestFormat : IMessageFormat
    {
        /// <summary>
        /// The content type
        /// </summary>
        private readonly IResponseContentHeader _contentType;

        /// <summary>
        /// The _context
        /// </summary>
        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestFormat" /> class.
        /// </summary>
        /// <param name="contextAccessor">The context.</param>
        /// <param name="contentType">Type of the content.</param>
        public RestFormat(IHttpContextAccessor contextAccessor, IResponseContentHeader contentType)
        {
            this._contextAccessor = contextAccessor;
            this._contentType = contentType;
        }

        /// <summary>
        /// Gets the type of the content.
        /// </summary>
        /// <value>The type of the content.</value>
        public IResponseContentHeader ContentType
        {
            get
            {
                return this._contentType;
            }
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        /// <value>The context.</value>
        public IHttpContextAccessor Context
        {
            get
            {
                return this._contextAccessor;
            }
        }

        /// <summary>
        ///     Gets the a non null name for this format
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        
    }
}