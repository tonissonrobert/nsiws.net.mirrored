// -----------------------------------------------------------------------
// <copyright file="AuthDataQueryVisitor.cs" company="EUROSTAT">
//   Date Created : 2015-12-09
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Controllers.Engine
{
    using System;
    using System.Globalization;
    using System.Security.Principal;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Mapping.Api.Utils;
    using Estat.Sri.Ws.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Util;

    /// <summary>
    ///     The authorization for data queries
    /// </summary>
    public class AuthDataQueryVisitor : IDataQueryVisitor
    {
        /// <summary>
        /// The _dataflow authorization manager
        /// </summary>
        private readonly IDataflowAuthorizationManager _dataflowAuthorizationManager;

        /// <summary>
        ///     The _principal manager
        /// </summary>
        private readonly IDataflowPrincipalManager _principalManager;

        /// <summary>
        /// The enableReleaseManagement configuration
        /// </summary>
        private readonly bool _enableReleaseManagement;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthDataQueryVisitor" /> class.
        /// </summary>
        /// <param name="principalManager">The principal manager.</param>
        /// <param name="dataflowAuthorizationManager">The dataflow authorization manager.</param>
        /// <param name="enableReleaseManagement">The current enableReleaseManagement configuration.</param>
        public AuthDataQueryVisitor(IDataflowPrincipalManager principalManager, IDataflowAuthorizationManager dataflowAuthorizationManager, bool enableReleaseManagement) 
        {
            this._principalManager = principalManager;
            this._dataflowAuthorizationManager = dataflowAuthorizationManager;
            this._enableReleaseManagement = enableReleaseManagement;
        }

        /// <summary>
        ///     Visits the specified data query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <exception cref="ArgumentNullException"><paramref name="dataQuery"/> is <see langword="null" />.</exception>
        /// <exception cref="SdmxUnauthorisedException">
        ///     Not authorized
        /// </exception>
        public void Visit(IBaseDataQuery dataQuery)
        {
            if (dataQuery == null)
            {
                throw new ArgumentNullException("dataQuery");
            }

            this.Authorize(dataQuery.Dataflow);
        }

        /// <summary>
        ///     Try to authorize using the dataflow from <see cref="IDataflowObject" />.
        ///     It uses the <see cref="_principalManager" /> to retrieve the <see cref="IPrincipal" /> and uses that to
        ///     check the use is authorized.
        /// </summary>
        /// <param name="dataflow">
        ///     The dataflow.
        /// </param>
        /// <exception cref="SdmxUnauthorisedException">
        ///     Not authorized
        /// </exception>
        /// <exception cref="ArgumentNullException"><paramref name="dataflow"/> is <see langword="null" />.</exception>
        protected void Authorize(IDataflowObject dataflow)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException("dataflow");
            }

            using(var scope = new SdmxAuthorizationScope(Mapping.Api.Constant.Authorisation.Optional))
            {
                if (scope.IsEnabled)
                {
                    if (!scope.CurrentSdmxAuthorisation.CanReadData(dataflow.AsReference, this._enableReleaseManagement && HeaderScope.IsPit))
                    {
                        Unauthorized(dataflow);
                        return;
                    }
                }
                else
                {
                    // TODO old authorization
                    var engine = this._dataflowAuthorizationManager.GetAuthorizationEngine(_principalManager.GetCurrentPrincipal());
                    if (engine != null)
                    {
                        var status = engine.GetAuthorization(dataflow.AsReference, PermissionType.CanReadData);

                        if (status != AuthorizationStatus.Authorized)
                        {
                            Unauthorized(dataflow);
                            return;
                        }
                    }
                }
            }
        }

        private static void Unauthorized(IDataflowObject dataflow)
        {
            string errorMessage = string.Format(CultureInfo.CurrentCulture, Messages.NoDataForDataflowFormat1, dataflow.Id);
            throw new SdmxUnauthorisedException(errorMessage);
        }
    }
}