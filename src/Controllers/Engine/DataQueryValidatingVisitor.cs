﻿// -----------------------------------------------------------------------
// <copyright file="DataQueryValidatingVisitor.cs" company="EUROSTAT">
//   Date Created : 2015-12-09
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Engine
{
    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sri.Ws.Controllers.Controller;

    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

    /// <summary>
    /// Visits a <see cref="IBaseDataQuery"/> and checks if it is valid
    /// </summary>
    public class DataQueryValidatingVisitor : IDataQueryVisitor
    {
        /// <summary>
        ///     The _validator
        /// </summary>
        private readonly IDataRequestValidator _validator;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DataQueryValidatingVisitor" /> class.
        /// </summary>
        /// <param name="validator">The validator.</param>
        public DataQueryValidatingVisitor(IDataRequestValidator validator)
        {
            this._validator = validator;
        }

        /// <summary>
        ///     Visits the specified data query.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        public void Visit(IBaseDataQuery dataQuery)
        {
            this._validator.Validate(dataQuery);
        }
    }
}