﻿// -----------------------------------------------------------------------
// <copyright file="SoapWriter.cs" company="EUROSTAT">
//   Date Created : 2019-02-04
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Xml;
using Estat.Sri.Ws.Controllers.Constants;
using Estat.Sri.Ws.Controllers.Model;

namespace Estat.Sri.Ws.Controllers.Engine
{
    public class SoapWriter
    {
        private const string SoapNs = "http://schemas.xmlsoap.org/soap/envelope/";
        private readonly XmlWriter _writer;

        private SoapWriteState _state;

        public SoapWriter(XmlWriter writer)
        {
            this._writer = writer;
        }

        public void WriteStartEnvelope()
        {
            if (_writer.WriteState != WriteState.Start)
            {
                throw new InvalidOperationException("Expected xml writer that didn't start yet");
            }

            _writer.WriteStartDocument();
            _writer.WriteStartElement("s", "Envelope", SoapNs);
            this._state = SoapWriteState.EnvelopeStarted;
            _writer.WriteStartElement("s", "Body", SoapNs);
            this._state = SoapWriteState.BodyStarted;
        }
        
        public void WriteBodyContents(XmlQualifiedName operation, string queryParameter = null) 
        {
            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            _writer.WriteStartElement("web", operation.Name, operation.Namespace);
            this._state = SoapWriteState.OperationStarted;
            if (!string.IsNullOrWhiteSpace(queryParameter))
            {
                _writer.WriteStartElement("web", queryParameter, operation.Namespace);
                this._state = SoapWriteState.QueryParameterStarted;
            }
        }

        public void WriteFault(XmlQualifiedName faultCode, string reason, SdmxFault detail)
        {
            _writer.WriteStartElement("s", "Fault","http://schemas.xmlsoap.org/soap/envelope/");
            this._state = SoapWriteState.FaultStarted;
            var soapPrefix = _writer.LookupPrefix(faultCode.Namespace);
            _writer.WriteElementString("faultcode",  soapPrefix + ":" + faultCode.Name);
            _writer.WriteElementString("faultstring", reason);
            _writer.WriteStartElement("detail");
            this._state = SoapWriteState.FaultDetailStarted;
            _writer.WriteStartElement("e", "Error", detail.Ns);
            _writer.WriteElementString(nameof(SdmxFault.ErrorNumber), detail.ErrorNumber.ToString(CultureInfo.InvariantCulture));
            _writer.WriteElementString(nameof(SdmxFault.ErrorMessage), detail.ErrorMessage);
            _writer.WriteElementString(nameof(SdmxFault.ErrorSource), detail.ErrorSource);
            _writer.WriteEndElement(); // Error
        }

        public void CloseDocument()
        {
            // if there is an error return to avoid hiding the original error
            if (this._writer.WriteState == WriteState.Error)
            {
                return;
            }

            if (this._writer.WriteState == WriteState.Closed)
            {
                throw new InvalidOperationException("Writer already closed");
            }

            if (this._writer.WriteState == WriteState.Start)
            {
                // no write - we don't write
                return;
            }

            switch (_state)
            {
                case SoapWriteState.None:
                    return;
                case SoapWriteState.EnvelopeStarted:
                    _writer.WriteEndElement(); // Envelope
                    _writer.WriteEndDocument();
                    this._state = SoapWriteState.EnvelopeClosed;
                    _writer.Flush();
                    break;
                case SoapWriteState.BodyStarted:
                    _writer.WriteEndElement(); // Body
                    goto case SoapWriteState.EnvelopeStarted;
                case SoapWriteState.OperationStarted:
                    _writer.WriteEndElement(); // operation name
                    goto case SoapWriteState.BodyStarted;
                case SoapWriteState.QueryParameterStarted:
                    _writer.WriteEndElement(); // query name
                    goto case SoapWriteState.OperationStarted;
                case SoapWriteState.FaultStarted:
                    _writer.WriteEndElement(); // Fault. 
                    goto case SoapWriteState.BodyStarted;
                case SoapWriteState.FaultDetailStarted:
                    _writer.WriteEndElement(); // details
                    goto case SoapWriteState.FaultStarted;
                case SoapWriteState.EnvelopeClosed:
                    return;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
