﻿// -----------------------------------------------------------------------
// <copyright file="WebServiceEndpointExtension.cs" company="EUROSTAT">
//   Date Created : 2016-07-07
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Controllers.Extension
{
    using System;

    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sri.Ws.Controllers.Constants;

    /// <summary>
    /// The web service endpoint extension.
    /// </summary>
    public static class WebServiceEndpointExtension
    {
        /// <summary>
        /// Gets the SDMX extension.
        /// </summary>
        /// <param name="webServiceEndpoint">
        /// The web service endpoint.
        /// </param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// webServiceEndpoint;New value of WebServiceEndpoint not supported in WebServiceEndpointExtension
        /// </exception>
        /// <returns>
        /// The <see cref="SdmxExtension"/>.
        /// </returns>
        public static SdmxExtension GetSdmxExtension(this WebServiceEndpoint webServiceEndpoint)
        {
            switch (webServiceEndpoint)
            {
                case WebServiceEndpoint.StandardEndpoint:
                    return SdmxExtension.None;
                case WebServiceEndpoint.EstatEndpoint:
                    return SdmxExtension.EstatExtensions;
                default:
                    throw new ArgumentOutOfRangeException("webServiceEndpoint", webServiceEndpoint, "New value of WebServiceEndpoint not supported in WebServiceEndpointExtension");
            }
        }
    }
}