// -----------------------------------------------------------------------
// <copyright file="DataRequestController.cs" company="EUROSTAT">
//   Date Created : 2015-12-09
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Ws.Controllers.Controller
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Engine;
    using Estat.Sri.Ws.Controllers.Extension;
    using Estat.Sri.Ws.Controllers.Factory;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.Controllers.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    /// <summary>
    ///     The controller class responsible for Data Requests.
    /// </summary>
    public class DataRequestController : IDataRequestController
    {
        /// <summary>
        ///     The _data query visitor factory
        /// </summary>
        private readonly IDataQueryVisitorFactory _dataQueryVisitorFactory;

        /// <summary>
        /// The _data writer manager
        /// </summary>
        private readonly IDataWriterManager _dataWriterManager;

        /// <summary>
        /// The SDMX v2.0 Cross Sectional data writer manager
        /// </summary>
        private readonly ICrossDataWriterManager _crossDataWriterManager;

        /// <summary>
        ///     The retriever manager
        /// </summary>
        private readonly IRetrieverManager _retrieverManager;

        /// <summary>
        /// The _format resolver manager
        /// </summary>
        private readonly IFormatResolverManager _formatResolverManager;

        /// <summary>
        /// The range header handler
        /// </summary>
        private readonly IRangeHeaderHandler _rangeHeaderHandler;

        private readonly IHttpContextAccessor _contextAccessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataRequestController" /> class.
        /// </summary>
        /// <param name="dataQueryVisitorFactory">The data query visitor factory.</param>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="dataWriterManager">The data writer manager.</param>
        /// <param name="crossDataWriterManager">The SDMX v2.0 Cross Sectional data writer manager.</param>
        /// <param name="formatResolverManager">The format resolver manager.</param>
        /// <param name="rangeHeaderHandler"></param>
        /// <param name="contextAccessor"></param>
        public DataRequestController(IDataQueryVisitorFactory dataQueryVisitorFactory, IRetrieverManager retrieverManager, IDataWriterManager dataWriterManager, ICrossDataWriterManager crossDataWriterManager, IFormatResolverManager formatResolverManager, IRangeHeaderHandler rangeHeaderHandler,IHttpContextAccessor contextAccessor)
        {
            this._dataQueryVisitorFactory = dataQueryVisitorFactory;
            this._retrieverManager = retrieverManager;
            this._dataWriterManager = dataWriterManager;
            this._crossDataWriterManager = crossDataWriterManager;
            this._formatResolverManager = formatResolverManager;
            this._rangeHeaderHandler = rangeHeaderHandler;
            this._contextAccessor = contextAccessor;
        }

        /// <summary>
        /// Parses the data request for SDMX SOAP (WCF).
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="queryParameter"></param>
        /// <returns>The <see cref="StreamController{XmlWriter}" />.</returns>
        /// <exception cref="SdmxInternalServerException">Not initialized correctly</exception>
        /// <exception cref="ArgumentNullException"><paramref name="dataType"/> is <see langword="null" />.</exception>
        public IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, ISoapRequest<DataType> dataType, string webServiceNamespace, string queryParameter)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (dataType == null)
            {
                throw new ArgumentNullException("dataType");
            }

            if (webServiceNamespace == null)
            {
                throw new ArgumentNullException(nameof(webServiceNamespace));
            }

            SdmxSchema sdmxSchema = dataType.FormatType.SchemaVersion;
            return sdmxSchema.EnumType == SdmxSchemaEnumType.VersionTwoPointOne ? this.ParseRequest21(request, dataType) : this.ParseRequestV20(request, dataType, webServiceNamespace, queryParameter);
        }

        /// <summary>
        /// Parses the data request for SDMX REST.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="headers">The headers.</param>
        /// <returns>
        /// The <see cref="IStreamController{Stream}" />.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Impossible request. Requested CrossSectional
        /// for SDMX v2.1.</exception>
        public ResponseActionWithHeader ParseRequest(IRestDataQuery request, IHeaderDictionary headers)
        {
            if (headers == null)
            {
                throw new ArgumentNullException(nameof(headers));
            }

            var dataRequest = this._retrieverManager.ParseDataRequest(request);
            if (!string.IsNullOrWhiteSpace(headers["If-Modified-Since"]))
            {
                if (DateTime.Parse(headers["If-Modified-Since"], System.Globalization.CultureInfo.InvariantCulture) > HeaderScope.DataLastUpdate)
                {
                    throw new SdmxNotModifiedException();
                }
            }
            var webHeaderCollection = new WebHeaderCollection();
            foreach (var header in headers)
            {
                if (!header.Key.StartsWith(':'))
                {
                    webHeaderCollection.Add(header.Key, header.Value);
                }
            }

            var restDataResponse = this._formatResolverManager.GetDataFormat(webHeaderCollection, dataRequest.SdmxObjectRetrieval, dataRequest.DataQuery);

            Func<Stream, Queue<Action>, Task> function = this.GetFunction(dataRequest, DataWriterDelayBehavior.UntilFirstKey, restDataResponse);

            return new ResponseActionWithHeader(function, restDataResponse.ResponseContentHeader);
        }
        
        /// <summary>
        /// Gets the function.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="behavior">The behavior.</param>
        /// <param name="restDataResponse">The REST data response.</param>
        /// <returns>
        /// The <see cref="Action{Stream, Queue}" />.
        /// </returns>
        /// <exception cref="SdmxSemmanticException">Impossible request. Requested CrossSectional for SDMX v2.1.</exception>
        private Func<Stream, Queue<Action>, Task> GetFunction(DataRequest request, DataWriterDelayBehavior behavior, IRestResponse<IDataFormat> restDataResponse)
        {
            Func<Stream, Queue<Action>, Task> function;
            DataType dataType = restDataResponse.Format.SdmxDataFormat ?? DataType.GetFromEnum(DataEnumType.Other);
            if (dataType != null && dataType.EnumType == DataEnumType.CrossSectional20)
            {
                SdmxSchema sdmxSchema = dataType.SchemaVersion;
                if (sdmxSchema.EnumType != SdmxSchemaEnumType.VersionTwo)
                {
                    throw new SdmxSemmanticException("Impossible request. Requested CrossSectional for SDMX v2.1.");
                }

                var dataResponse = this._retrieverManager.GetDataResponseCross(request);
                this.VisitDataResponse(dataType, dataResponse);
                function = async(writer, queue) =>
                {
                    using (var delayedCrossWriterEngine = new DelayedCrossWriterEngine(queue, this._crossDataWriterManager.GetWriterEngine(restDataResponse.Format, writer), behavior))
                    {
                        this.AddRangeHeader(request, sdmxSchema);

                        await dataResponse.WriteResponseAsync(delayedCrossWriterEngine);
                    }
                };
            }
            else
            {
                var dataResponse = this._retrieverManager.GetDataResponseSimple(request);
                this.VisitDataResponse(dataType, dataResponse);

                function = async(writer, queue) =>
                {
                    using (var delayedDataWriterEngine = new DelayedDataWriterEngine(this._dataWriterManager.GetDataWriterEngine(restDataResponse.Format, writer), queue, behavior))
                    {
                        this.AddRangeHeader(request, dataType?.SchemaVersion);

                        // Write metadata availability information only for Json20
                        if (dataType == DataType.GetFromEnum(DataEnumType.Json20) && request.DataQuery.MetadataStructure != null)
                        {
                            request.DataQuery.IsMetadataAvailabilityNeeded = true;
                        }

                        await dataResponse.WriteResponseAsync(delayedDataWriterEngine);
                    }
                };
            }

            return function;
        }

        /// <summary>
        ///     Gets the function.
        /// </summary>
        /// <param name="soapRequest">The data format.</param>
        /// <param name="readableDataLocation">The readable data location.</param>
        /// <param name="behavior">The behavior.</param>
        /// <param name="soapOperation"></param>
        /// <param name="webServiceNs"></param>
        /// <param name="queryParameter"></param>
        /// <returns>The command that will retrieve data</returns>
        private Func<XmlWriter, Queue<Action>, Task> GetFunction(ISoapRequest<DataType> soapRequest, IReadableDataLocation readableDataLocation, DataWriterDelayBehavior behavior, SoapOperation soapOperation, string webServiceNs, string queryParameter)
        {
            Func<XmlWriter, Queue<Action>,Task> function;
            var dataType = soapRequest.FormatType;

            if (dataType.EnumType == DataEnumType.CrossSectional20)
            {
                var dataResponse = this._retrieverManager.GetDataResponseCross(readableDataLocation);
                this.VisitDataResponse(dataType, dataResponse);
                function = async(writer, queue) =>
                {
                    var soapWriter = InitSoap(soapOperation, webServiceNs, queryParameter, writer, queue);
                    IDataFormat sdmxDataFormat = this._formatResolverManager.GetDataFormat(soapRequest, writer);
                    using (var delayedCrossWriterEngine = new DelayedCrossWriterEngine(queue, this._crossDataWriterManager.GetWriterEngine(sdmxDataFormat, null), behavior, () => soapWriter.CloseDocument()))
                    {
                        await dataResponse.WriteResponseAsync(delayedCrossWriterEngine);
                    }
                };
            }
            else
            {
                var dataResponse = this._retrieverManager.GetDataResponseSimple(readableDataLocation);
                this.VisitDataResponse(dataType, dataResponse);

                function = async (writer, queue) =>
                {
                    var soapWriter = InitSoap(soapOperation, webServiceNs, queryParameter, writer, queue);
                    IDataFormat sdmxDataFormat = this._formatResolverManager.GetDataFormat(soapRequest, writer);

                    using (var delayedDataWriterEngine = new DelayedDataWriterEngine(this._dataWriterManager.GetDataWriterEngine(sdmxDataFormat, null), queue, behavior, () => soapWriter.CloseDocument()))
                    {
                        await dataResponse.WriteResponseAsync(delayedDataWriterEngine);
                    }
                };
            }

            return function;
        }

        /// <summary>
        /// Initialize the SOAP part of the data request
        /// </summary>
        /// <param name="soapOperation">The SOAP operation</param>
        /// <param name="webServiceNs">The web service namespace</param>
        /// <param name="queryParameter">The extra query parameter used in </param>
        /// <param name="writer"></param>
        /// <param name="queue"></param>
        /// <returns></returns>
        private static SoapWriter InitSoap(SoapOperation soapOperation, string webServiceNs, string queryParameter, XmlWriter writer, Queue<Action> queue)
        {
            SoapOperationResponse response = soapOperation.GetResponse();
            XmlQualifiedName qualifiedName = new XmlQualifiedName(response.ToString(), webServiceNs);
            var soapWriter = new SoapWriter(writer);
            queue.Enqueue(() =>
            {
                soapWriter.WriteStartEnvelope();
                soapWriter.WriteBodyContents(qualifiedName, queryParameter);
            });
            return soapWriter;
        }

        /// <summary>
        ///     Parses the data request for SDMX SOAP V21.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="soapRequest">The data format.</param>
        /// <returns>The <see cref="StreamController{XmlWriter}" />.</returns>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxInternalServerException">Not initialized correctly</exception>
        private IStreamController<XmlWriter> ParseRequest21(IReadableDataLocation request, ISoapRequest<DataType> soapRequest)
        {
            var formatType = soapRequest.FormatType;
            SoapOperation soapOperation = formatType.BaseDataFormat.GetSoapOperation(SdmxSchemaEnumType.VersionTwoPointOne);
            IDataResponse<IDataWriterEngine> dataResponse = this._retrieverManager.GetDataResponseComplex(request);

            this.VisitDataResponse(formatType, dataResponse);

            return new StreamController<XmlWriter>(async (writer, queue) =>
            {
                var soapWriter = InitSoap(soapOperation, SoapNamespaces.SdmxV21, null, writer, queue);
                IDataFormat sdmxDataFormat = this._formatResolverManager.GetDataFormat(soapRequest, writer);
                using (var delayedDataWriterEngine = new DelayedDataWriterEngine(this._dataWriterManager.GetDataWriterEngine(sdmxDataFormat, null), queue, DataWriterDelayBehavior.UntilFirstKey, () => soapWriter.CloseDocument()))
                {
                    await dataResponse.WriteResponseAsync(delayedDataWriterEngine);
                }
            });
        }

        /// <summary>
        ///     Parses the data request for SDMX SOAP V21.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="soapRequest">The data format.</param>
        /// <param name="webServiceNs"></param>
        /// <param name="queryParameter"></param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxInternalServerException">Not initialized correctly</exception>
        private IStreamController<XmlWriter> ParseRequestV20(IReadableDataLocation request, ISoapRequest<DataType> soapRequest, string webServiceNs, string queryParameter)
        {
            var dataType = soapRequest.FormatType;
            SoapOperation soapOperation = dataType.BaseDataFormat.GetSoapOperation(SdmxSchemaEnumType.VersionTwo);
            Func<XmlWriter, Queue<Action>, Task> function = this.GetFunction(soapRequest, request, DataWriterDelayBehavior.UntilFirstStartDataSet, soapOperation, webServiceNs, queryParameter);

            return new StreamController<XmlWriter>(function);
        }

        /// <summary>
        ///     Visits the data response.
        /// </summary>
        /// <typeparam name="TDataWriter">The type of the data writer</typeparam>
        /// <param name="dataFormat">The data format.</param>
        /// <param name="dataResponse">The data response.</param>
        private void VisitDataResponse<TDataWriter>(DataType dataFormat, IDataResponse<TDataWriter> dataResponse) where TDataWriter : IDisposable
        {
            var dataQueryVisitor = this._dataQueryVisitorFactory.GetDataQueryVisitor(dataFormat);
            dataResponse.Accept(dataQueryVisitor);
        }

        /// <summary>
        /// Adds the Range or X-Range header.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="sdmxSchema">The sdmx schema.</param>
        private void AddRangeHeader(DataRequest request, SdmxSchema sdmxSchema)
        {
            var rangeHeader = this._contextAccessor.HttpContext.Request.Headers["Range"];

            if (StringValues.IsNullOrEmpty(rangeHeader))
            {
                rangeHeader = this._contextAccessor.HttpContext.Request.Headers["X-Range"];
            }

            if (!StringValues.IsNullOrEmpty(rangeHeader))
            {
                this._rangeHeaderHandler.AddHeaderForData(rangeHeader, request, sdmxSchema);
            }
        }
    }
}