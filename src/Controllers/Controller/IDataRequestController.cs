// -----------------------------------------------------------------------
// <copyright file="IDataRequestController.cs" company="EUROSTAT">
//   Date Created : 2015-12-15
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Util;

namespace Estat.Sri.Ws.Controllers.Controller
{
    using System.Net;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Controllers.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    /// Interface for data requests
    /// </summary>
    public interface IDataRequestController
    {
        /// <summary>
        /// Parse the data request for SOAP 
        /// </summary>
        /// <param name="request">The SDMX part of the request </param>
        /// <param name="dataType">The SOAP metadata</param>
        /// <param name="webServiceNamespace">The namespace of the webservice </param>
        /// <param name="queryParameter">Th query parameter in case of a </param>
        /// <returns>The <see cref="StreamController{XmlWriter}" />.</returns>
        IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, ISoapRequest<DataType> dataType, string webServiceNamespace, string queryParameter);

        /// <summary>
        /// Parses the data request for SDMX REST.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="headers">
        /// The headers.
        /// </param>
        /// <returns>
        /// The <see cref="ResponseActionWithHeader"/>.
        /// </returns>
        ResponseActionWithHeader ParseRequest(IRestDataQuery request, IHeaderDictionary headers);
    }
}