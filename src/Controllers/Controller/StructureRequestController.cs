// -----------------------------------------------------------------------
// <copyright file="StructureRequestController.cs" company="EUROSTAT">
//   Date Created : 2015-12-10
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Linq;
using System.Threading;
using Estat.Sri.Ws.Controllers.Engine;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Util;

namespace Estat.Sri.Ws.Controllers.Controller
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Controllers.Constants;
    using Estat.Sri.Ws.Controllers.Extension;
    using Estat.Sri.Ws.Controllers.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;

    /// <summary>
    ///     The Controller responsible for structure requests.
    /// </summary>
    public class StructureRequestController : IStructureRequestController
    {
        /// <summary>
        ///     The retriever manager
        /// </summary>
        private readonly IRetrieverManager _retrieverManager;

        /// <summary>
        /// The _writer manager
        /// </summary>
        private readonly IStructureWriterManager _writerManager;

        /// <summary>
        /// The _format resolver manager
        /// </summary>
        private readonly IFormatResolverManager _formatResolverManager;

        /// <summary>
        /// The _translator factory
        /// </summary>
        private readonly ITranslatorFactory _translatorFactory;

        /// <summary>
        /// The range header handler
        /// </summary>
        private readonly IRangeHeaderHandler _rangeHeaderHandler;

        /// <summary>
        /// The sorted accept headers builder
        /// </summary>
        private readonly SortedAcceptHeadersBuilder _acceptHeaderBuilder = new SortedAcceptHeadersBuilder();

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureRequestController" /> class.
        /// </summary>
        /// <param name="retrieverManager">The retriever manager.</param>
        /// <param name="writerManager">The writer manager.</param>
        /// <param name="formatResolverManager">The format resolver manager.</param>
        /// <param name="translatorFactory">The translator factory.</param>
        /// <param name="rangeHeaderHandler"></param>
        public StructureRequestController(IRetrieverManager retrieverManager, IStructureWriterManager writerManager, IFormatResolverManager formatResolverManager, ITranslatorFactory translatorFactory,IRangeHeaderHandler rangeHeaderHandler)
        {
            this._retrieverManager = retrieverManager;
            this._writerManager = writerManager;
            this._formatResolverManager = formatResolverManager;
            this._translatorFactory = translatorFactory;
            this._rangeHeaderHandler = rangeHeaderHandler;
        }

        /// <inheritdoc />
        public IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, WebServiceEndpoint endpoint, string webServiceNamespace, string queryParameter)
        {
            var structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV2RegistryQueryResponseDocument);
            var sdmxSchema = structureOutputFormat.OutputVersion;
            var soapOperation = SoapOperation.QueryStructure;

            var sdmxObjects = this.GetStructureResponse(request, sdmxSchema);

            return this.GetStreamController(endpoint, structureOutputFormat, sdmxObjects, soapOperation, webServiceNamespace, queryParameter);
        }

        private ISdmxObjects GetStructureResponse(IReadableDataLocation request, SdmxSchema sdmxSchema)
        {
            return this._retrieverManager.ParseRequest(sdmxSchema, request); 
        }

        /// <summary>
        /// Handles the structure request.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="headers">The headers.</param>
        /// <returns>
        /// The <see cref="ResponseActionWithHeader" />.
        /// </returns>
        public ResponseActionWithHeader ParseRequest(IRestStructureQuery query, IHeaderDictionary headersFromRequest, HttpContext ctx)
        {
            // TODO why are parsing headers twice
            var headers = new WebHeaderCollection();
            foreach (var keyPairValue in headersFromRequest)
            {
                // TODO WebHeaderCollection doesn't support HTTP/2 HTTP headers :method, :path
                if (!keyPairValue.Key.StartsWith(':'))
                {
                    headers.Add(keyPairValue.Key, keyPairValue.Value);
                }
            }
            var response = this._formatResolverManager.GetStructureFormat(headers, query);

            var outputVersion = response.Format.SdmxOutputFormat != null ? response.Format.SdmxOutputFormat.OutputVersion : SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne);
            ISdmxObjects sdmxObjects;
            var webHeaderCollection = new WebHeaderCollection();
            foreach (var header in ctx.Request.Headers)
            {
                // TODO WebHeaderCollection doesn't support HTTP/2 HTTP headers :method, :path
                if (!header.Key.StartsWith(':'))
                { 
                    webHeaderCollection.Add(header.Key, header.Value); 
                }
            }
            var acceptHeader = ctx.Request.Headers["Accept"];
            var requestAccept = acceptHeader.FirstOrDefault() ?? "";

            var rawAcceptMatch = requestAccept
                .Split(',')
                .FirstOrDefault(x => x.Contains(response.ResponseContentHeader.MediaType.MediaType));

            var writeUrn = rawAcceptMatch?.Contains("urn=true") ?? false;

            ctx.RequestAborted.ThrowIfCancellationRequested();

            using (new HeaderScope(webHeaderCollection, writeUrn, ctx.RequestAborted))
            {
                sdmxObjects = this._retrieverManager.ParseRequest(outputVersion, query);

                HeaderScope.RequestAborted.ThrowIfCancellationRequested();

                var nrOfStructureRecords = HeaderScope.NumberOfStructureRecords;
                if (response.Format is IFormatWithTranslator formatTranslator)
                {
                    var requestedLanguages = _acceptHeaderBuilder.Build(headers).Languages;
                    formatTranslator.Translator = this._translatorFactory.GetTranslator(sdmxObjects, requestedLanguages);
                }

                Func<Stream, Queue<Action>,Task> function = async(stream, queue) =>
                {
                    var rangeHeader = ctx.Request.Headers["Range"];

                    if (StringValues.IsNullOrEmpty(rangeHeader))
                    {
                        rangeHeader = ctx.Request.Headers["X-Range"];
                    }

                    if (!StringValues.IsNullOrEmpty(rangeHeader))
                    {
                        this._rangeHeaderHandler.AddHeader(rangeHeader, nrOfStructureRecords);
                    }

                    queue.RunAll();
                    this._writerManager.WriteStructures(sdmxObjects, response.Format, stream);
                };

                return new ResponseActionWithHeader(function, response.ResponseContentHeader);
            }
        }

        public IStreamController<XmlWriter> ParseRequest(IReadableDataLocation request, SoapOperation soapOperation)
        {
            var structureOutputFormat = StructureOutputFormat.GetFromEnum(StructureOutputFormatEnumType.SdmxV21StructureDocument);
            var sdmxSchema = structureOutputFormat.OutputVersion;

            var sdmxObjects = this.GetStructureResponse(request, sdmxSchema);

            return this.GetStreamController(WebServiceEndpoint.StandardEndpoint, structureOutputFormat, sdmxObjects, soapOperation, SoapNamespaces.SdmxV21, null);
        }

        /// <summary>
        /// Gets the stream controller.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        /// <param name="structureOutputFormat">The structure output format.</param>
        /// <param name="sdmxObjects">The SDMX objects.</param>
        /// <param name="operation">The operation.</param>
        /// <returns>
        /// The <see cref="IStreamController{XmlWriter}" />.
        /// </returns>
        private IStreamController<XmlWriter> GetStreamController(WebServiceEndpoint endpoint, StructureOutputFormat structureOutputFormat, ISdmxObjects sdmxObjects, SoapOperation operation, string webServiceNameSpace, string queryParameter)
        {
            IStreamController<XmlWriter> streamController = new StreamController<XmlWriter>(async(writer, queue) =>
            {
                SoapWriter soapWriter = new SoapWriter(writer);
                IStructureFormat format = this._formatResolverManager.GetStructureFormat(new SoapStructureRequest(operation.ToString(), structureOutputFormat, endpoint == WebServiceEndpoint.EstatEndpoint ? SdmxExtension.EstatExtensions : SdmxExtension.None), writer);
                queue.RunAll();
                XmlQualifiedName operationQualifiedName = new XmlQualifiedName(operation.GetResponse().ToString(), webServiceNameSpace);
                soapWriter.WriteStartEnvelope();
                soapWriter.WriteBodyContents(operationQualifiedName, queryParameter);
                this._writerManager.WriteStructures(sdmxObjects, format, null);
                soapWriter.CloseDocument();
            });
            return streamController;
        }
    }
}