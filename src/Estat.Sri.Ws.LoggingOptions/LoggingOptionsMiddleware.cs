﻿using log4net;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Estat.Sri.Ws.LoggingOptions
{
    public class LoggingOptionsMiddleware
    {
        private readonly RequestDelegate _next;

        public LoggingOptionsMiddleware(RequestDelegate next)
        {
            this._next = next;
        }
        public async Task Invoke(HttpContext context)
        {
            var user = string.Empty;
            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && !string.IsNullOrWhiteSpace(Thread.CurrentPrincipal.Identity.Name))
            {
                user = Thread.CurrentPrincipal.Identity.Name;
            }
            
            if (context.User != null && context.User.Identity != null && !string.IsNullOrWhiteSpace(context.User.Identity.Name))
            {
                user = context.User.Identity.Name;
            }

            if(string.IsNullOrWhiteSpace(user))
            {
                user = "anonymous";
            }

            LogicalThreadContext.Properties["User"] = user;
            await this._next.Invoke(context);
        }
    }
}
