﻿// -----------------------------------------------------------------------
// <copyright file="PolicyInformationFromXml.cs" company="EUROSTAT">
//   Date Created : 2018-02-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Engine
{
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.Schema;

    using Estat.Sri.Ws.PolicyModule.Model;

    public class PolicyInformationFromXml
    {
        public Rules Parse(Stream xmlFile, string schemaPath)
        {
            var restRules = new List<RestResource>();
            var soapRules = new List<SoapEndpoint>();
            var xmlReaderSettings = new XmlReaderSettings() {IgnoreComments = true, IgnoreProcessingInstructions = true, IgnoreWhitespace = true};

            if (!string.IsNullOrWhiteSpace(schemaPath))
            {
                xmlReaderSettings.Schemas = GetSchema(schemaPath);
                xmlReaderSettings.ValidationType = ValidationType.Schema;
            }

            using (var reader = XmlReader.Create(xmlFile, xmlReaderSettings))
            {
                MutableEndpoint mutableEndpoint = new MutableEndpoint();
                string text = null;
                while (reader.Read())
                {
                    var nodeType = reader.NodeType;
                    switch (nodeType)
                    {
                        case XmlNodeType.Element:
                            {
                                var localName = reader.LocalName;
                                ParseStartElement(localName, mutableEndpoint, reader);
                            }
                            break;
                        case XmlNodeType.Text:
                            text = reader.Value;
                            break;
                        case XmlNodeType.CDATA:
                            text = reader.Value;
                            break;
                        case XmlNodeType.EndElement:
                            {
                                var localName = reader.LocalName;
                                switch (localName)
                                {
                                        case "script":
                                            mutableEndpoint.Expression = text;
                                            text = null;
                                            break;
                                    case "rest":
                                        {
                                            var restResource = mutableEndpoint.CreateRestResource();
                                            restRules.Add(restResource);
                                        }
                                        break;
                                    case "soap":
                                        {
                                            var soap = mutableEndpoint.CreateSoapEndpoint();
                                            soapRules.Add(soap);
                                        }
                                        break;
                                }
                            }

                            break;
                    }
                }
            }

            return new Rules(restRules, soapRules);
        }

        private XmlSchemaSet GetSchema(string path)
        {
            var xmlSchema = new XmlSchemaSet();
            using (XmlReader reader = XmlReader.Create(path))
            {
                xmlSchema.Add("http://ec.europa.eu/eurostat/sri/authorisation/1.0", reader);
            }

            return xmlSchema;
        }

        private static void ParseStartElement(string localName, MutableEndpoint mutableEndpoint, XmlReader reader)
        {
            switch (localName)
            {
                case "rest":
                    {
                        CommonInit(mutableEndpoint, reader);
                        mutableEndpoint.Method = reader.GetAttribute("method");
                    }
                    break;
                case "soap":
                    {
                        CommonInit(mutableEndpoint, reader);
                    }
                    break;
                case "and":
                    mutableEndpoint.IsAnd = true;
                    break;
                case "or":
                    mutableEndpoint.IsAnd = false;
                    break;
                case "permission":
                    mutableEndpoint.Add(reader.GetAttribute("id"));
                    break;
                case "declare":
                    {
                        var prefix = reader.GetAttribute("prefix");
                        var ns = reader.GetAttribute("namespace");
                        if (!string.IsNullOrWhiteSpace(prefix) && !string.IsNullOrWhiteSpace(ns))
                        {
                            mutableEndpoint.NamespaceResolver.AddNamespace(prefix, ns);
                        }
                    }
                    break;
            }
        }

        private static void CommonInit(MutableEndpoint mutableEndpoint, XmlReader reader)
        {
            mutableEndpoint.Init();
            mutableEndpoint.SetAllowAnonymous(reader.GetAttribute("allowAnonymous"));
            mutableEndpoint.Path = reader.GetAttribute("path");
        }
    }
}