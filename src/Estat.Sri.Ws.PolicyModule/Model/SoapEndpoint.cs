﻿// -----------------------------------------------------------------------
// <copyright file="SoapEndpoint.cs" company="EUROSTAT">
//   Date Created : 2018-02-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Model
{
    using System;
    using System.Collections.Generic;
    using System.Xml.XPath;

    public class SoapEndpoint : BaseEndpointType
    {
        /// <summary>
        /// To string
        /// </summary>
        private readonly string _toString;

        /// <summary>
        /// Initializes a new instance of the <see cref="SoapEndpoint"/> class.
        /// </summary>
        /// <param name="pathExpression">The path expression.</param>
        /// <param name="allowAnonymous">if set to <c>true</c> [allow anonymous].</param>
        /// <param name="andSet">The and set.</param>
        /// <param name="orSet">The or set.</param>
        /// <param name="xPathType">Type of the x path.</param>
        /// <exception cref="ArgumentNullException">xPathType</exception>
        public SoapEndpoint(string pathExpression, bool allowAnonymous, IEnumerable<string> andSet, IEnumerable<string> orSet, XPathType xPathType)
            : base(pathExpression, allowAnonymous, andSet, orSet)
        {
            if (xPathType == null)
            {
                throw new ArgumentNullException(nameof(xPathType));
            }

            Expression = xPathType.Expression;
            _toString = ", XPath Type:" + xPathType;
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>The expression.</value>
        public XPathExpression Expression { get; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return base.ToString() + _toString;
        }
    }
}