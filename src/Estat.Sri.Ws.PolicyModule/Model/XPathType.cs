﻿// -----------------------------------------------------------------------
// <copyright file="XPathType.cs" company="EUROSTAT">
//   Date Created : 2018-02-09
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.PolicyModule.Model
{
    using log4net;
    using System.Globalization;
    using System.Xml;
    using System.Xml.XPath;

    public class XPathType
    {
        /// <summary>
        /// The log
        /// </summary>
        private readonly static ILog _log = LogManager.GetLogger(typeof(XPathType));

        /// <summary>
        /// The expression
        /// </summary>
        private readonly XPathExpression _expression;

        private readonly string _toString;

        /// <summary>
        /// Initializes a new instance of the <see cref="XPathType"/> class.
        /// </summary>
        /// <param name="xmlNamespaceResolver">The XML namespace resolver.</param>
        /// <param name="expression">The expression.</param>
        public XPathType(IXmlNamespaceResolver xmlNamespaceResolver, string expression)
        {
            if (expression == null)
            {
                throw new System.ArgumentNullException(nameof(expression));
            }

            _toString = expression;

            try
            {
                this._expression = XPathExpression.Compile(expression.Trim(), xmlNamespaceResolver);
            }
            catch (XPathException e)
            {
                _log.ErrorFormat(CultureInfo.InvariantCulture, "Error parsing provided XPath Expression {0}. Replacing with 'false()'", expression);
                _log.Error(e);
                this._expression = XPathExpression.Compile("false()");
                _toString = "false() and old " + expression;
            }
        }

        /// <summary>
        /// Gets the expression.
        /// </summary>
        /// <value>
        /// The expression.
        /// </value>
        public XPathExpression Expression => this._expression;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString()
        {
            return _toString;
        }
    }
}