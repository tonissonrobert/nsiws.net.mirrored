﻿// -----------------------------------------------------------------------
// <copyright file="PolicyDecisionPointManager.cs" company="EUROSTAT">
//   Date Created : 2018-02-12
//   Copyright (c) 2009, 2018 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading;
using Microsoft.AspNetCore.Http;

namespace Estat.Sri.Ws.PolicyModule.Manager
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Principal;
    using System.Xml.XPath;
    using Estat.Sri.Ws.PolicyModule.Constant;
    using Estat.Sri.Ws.PolicyModule.Model;
    using log4net;

    public class PolicyDecisionPointManager
    {
        /// <summary>
        /// The Request size limit to store in memory or in a temporary file
        /// </summary>
        private const long InMemoryLimitBytes = 5 * 1024 * 1024;

        /// <summary>
        /// The log
        /// </summary>
        private readonly ILog _log = LogManager.GetLogger(typeof(PolicyDecisionPointManager));

        /// <summary>
        /// The rules
        /// </summary>
        private readonly Rules _rules;

        /// <summary>
        /// Initializes a new instance of the <see cref="PolicyDecisionPointManager"/> class.
        /// </summary>
        /// <param name="rules">The rules.</param>
        public PolicyDecisionPointManager(Rules rules)
        {
            this._rules = rules;
        }

        /// <summary>
        /// Determines whether this instance can access the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns><c>true</c> if this instance can access the specified context; otherwise, <c>false</c>.</returns>
        public AccessResponseType CanAccess(HttpContext context)
        {
            var path = context.Request.Path.Value;
            if (string.IsNullOrWhiteSpace(path))
            {
                path = "/";
            }
            else if (path[0] != '/')
            {
                path = "/" + path;
            }

            var method = context.Request.Method;
            var user = Thread.CurrentPrincipal;

            _log.Debug("Checking REST resources");
            foreach (var rulesRestResource in this._rules.RestResources)
            {
                if (method.Equals(rulesRestResource.Method) && rulesRestResource.PathExpression.IsMatch(path))
                {
                    _log.DebugFormat(CultureInfo.InvariantCulture, "Found REST match for method {0} and path {1}, with REST info {2}", method, path, rulesRestResource);
                    return Authorize(user, rulesRestResource);
                }
            }

            _log.Debug("Checking SOAP endpoints");
            // SOAP supports only POST
            if (method.Equals(HttpMethod.Post.Method))
            {
                string temp = null;
                XPathDocument document = null;
                try
                {
                    foreach (var soapEndpoint in this._rules.SoapEndpoints)
                    {
                        if (soapEndpoint.PathExpression.IsMatch(path))
                        {
                            if (document == null)
                            {

                                _log.Debug("Copying SOAP request body");
                                if (!context.Request.ContentLength.HasValue || context.Request.ContentLength > InMemoryLimitBytes)
                                {
                                    _log.DebugFormat(CultureInfo.InvariantCulture, "Either no Content-Length is given or Content-Length value {0} is greater than {1}", context.Request.ContentLength, InMemoryLimitBytes);
                                    temp = Path.GetTempFileName();
                                    _log.DebugFormat(CultureInfo.InvariantCulture, "Using temporary file to store SOAP request {0} ", temp);
                                    document = GetFileXPathDocument(context, temp);
                                }
                                else
                                {
                                    _log.DebugFormat(CultureInfo.InvariantCulture, "Content-Length value {0} is equal or less than {1}", context.Request.ContentLength, InMemoryLimitBytes);
                                    document = GetMemoryXPathDocument(context, (int)context.Request.ContentLength.Value);
                                }
                            }

                            var navigator = document.CreateNavigator();
                            var result = navigator.Evaluate(soapEndpoint.Expression);
                            if (result is bool && ((bool)result))
                            {
                                _log.DebugFormat(CultureInfo.InvariantCulture, "Found SOAP match for endpoint {0} and Endpoint info ({1})", path, soapEndpoint);
                                return Authorize(user, soapEndpoint);
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    this._log.Error(e.Message, e);
                    // we delete the temporary file only in case of an error
                    // because we use it as a DeleteOnClose stream on HttpContext.Request.Body
                    if (temp != null && File.Exists(temp))
                    {
                        File.Delete(temp);
                    }

                    throw;
                }
            }
            else if (method.Equals(HttpMethod.Get.Method))
            {
                // WSDL and SOAP API access
                foreach (var soapEndpoint in this._rules.SoapEndpoints)
                {
                    if (soapEndpoint.PathExpression.IsMatch(path))
                    {
                        _log.DebugFormat(CultureInfo.InvariantCulture, "Allowing {1} to SOAP endpoints for help pages or WSDL for path {0}", path, method);
                        // we allow it
                        return AccessResponseType.Success;
                    }
                }
            }

            _log.DebugFormat(CultureInfo.InvariantCulture, "No match found for path: {0} and HTTP method: {1}", path, method);

            return AccessResponseType.RuleNotFound;
        }

        /// <summary>
        /// Create <see cref="XPathDocument"/> from a temporary file from the SOAP request. Use it also on <c>HttpContext.Request.Body</c>
        /// </summary>
        /// <param name="context">The current <see cref="HttpContext"/></param>
        /// <param name="temp">The temporary file location</param>
        /// <returns>The <see cref="XPathDocument"/></returns>
        private static XPathDocument GetFileXPathDocument(HttpContext context, string temp)
        {
            using (var originalRequestBody = context.Request.Body)
            using (var fileStream = File.Create(temp))
            {
                originalRequestBody.CopyTo(fileStream);
            }

            var document = new XPathDocument(temp);
            // The Request body has a forward only stream, it other words it can be read only once
            // so we need to set it to a new stream in order for NSI WS to parse the request.
            context.Request.Body = new FileStream(temp, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.DeleteOnClose);
            return document;
        }

        /// <summary>
        /// Create <see cref="XPathDocument"/> from a memory stream from the SOAP request. Use it also on <c>HttpContext.Request.Body</c>
        /// </summary>
        /// <param name="context">The current <see cref="HttpContext"/></param>
        /// <param name="size">The size of the request in bytes</param>
        /// <returns>The <see cref="XPathDocument"/></returns>
        private static XPathDocument GetMemoryXPathDocument(HttpContext context, int size)
        {
            MemoryStream fileStream;
            using (var originalRequestBody = context.Request.Body)
            {
                fileStream = new MemoryStream(size);
                originalRequestBody.CopyTo(fileStream);
            }

            // rewind
            fileStream.Position = 0;

            // XPathDocument seems to dispose the Stream passed to it's constructor
            // so we save a copy of it's bytes
            var buffer = fileStream.ToArray();

            var document = new XPathDocument(fileStream);

            // The Request body has a forward only stream, it other words it can be read only once
            // so we need to set it to a new stream in order for NSI WS to parse the request.
            context.Request.Body = new MemoryStream(buffer);
            return document;
        }

        /// <summary>
        /// Authorizes the specified user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="rulesRestResource">The rules rest resource.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private static AccessResponseType Authorize(IPrincipal user, BaseEndpointType rulesRestResource)
        {
            if (user == null)
            {
                return AllowAnonymousUser(rulesRestResource);
            }
            else
            {
                // handle windows anonymous user
                WindowsIdentity identity = user.Identity as WindowsIdentity;
                if (identity != null && identity.IsAnonymous)
                {
                    return AllowAnonymousUser(rulesRestResource);
                }

                var andSet = rulesRestResource.AndSet;
                if (andSet.Count > 0)
                {
                    if (andSet.Count == 1 && andSet.Contains("Any"))
                    {
                        return AccessResponseType.Success;
                    }

                    return andSet.All(user.IsInRole) ? AccessResponseType.Success : AccessResponseType.InsufficientPermissions;
                }

                var orSet = rulesRestResource.OrSet;
                if (orSet.Count > 0)
                {
                    if (orSet.Count == 1 && orSet.Contains("Any"))
                    {
                        return AccessResponseType.Success;
                    }

                    return orSet.Any(user.IsInRole) ? AccessResponseType.Success : AccessResponseType.InsufficientPermissions;
                }

                return AccessResponseType.InsufficientPermissions;
            }
        }


        /// <summary>
        /// Check if an anonymous user is allowed by the specified <paramref name="rulesRestResource"/>.
        /// </summary>
        /// <param name="rulesRestResource">The rules rest resource.</param>
        /// <returns><c>true</c> if anonymous users are allowed, <c>false</c> otherwise.</returns>
        private static AccessResponseType AllowAnonymousUser(BaseEndpointType rulesRestResource)
        {
            if (rulesRestResource.AllowAnonymous)
            {
                return AccessResponseType.Success;
            }
            else
            {
                return AccessResponseType.AnonymousUserDenied;
            }
        }
    }
}