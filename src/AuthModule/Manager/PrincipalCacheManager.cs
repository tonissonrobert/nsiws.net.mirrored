﻿// -----------------------------------------------------------------------
// <copyright file="PrincipalCacheManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-10
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;

namespace Estat.Nsi.AuthModule.Manager
{
    using System;
    using System.Security.Principal;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    /// An Oauth2 client_credentials response builder
    /// </summary>
    public class PrincipalCacheManager
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMemoryCache _memoryCache;

        /// <summary>
        /// The bearer regex
        /// </summary>
        private readonly Regex _bearerRegex;

        /// <summary>
        /// Initializes a new instance of the <see cref="PrincipalCacheManager"/> class.
        /// </summary>
        public PrincipalCacheManager(IHttpContextAccessor httpContextAccessor, IMemoryCache memoryCache)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._memoryCache = memoryCache;
            this._bearerRegex = new Regex("^\\s*Bearer\\s+(?<b64>[a-zA-Z0-9_.~\\-\\+/]+={0,2})\\s*$");
        }

        /// <summary>
        /// Gets the principal from token.
        /// </summary>
        /// <param name="application">The application.</param>
        /// <returns>The access token in base 64.</returns>
        public string GetPrincipalFromToken()
        {
            var context = this._httpContextAccessor.HttpContext;
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var s = context.Request.Headers["Authorization"];
            if (!string.IsNullOrWhiteSpace(s))
            {
                var match = this._bearerRegex.Match(s);
                if (match.Success)
                {
                    return match.Result("${b64}");
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the principal from token.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <returns>The <see cref="IPrincipal"/>.</returns>
        public IPrincipal GetPrincipalFromToken(string accessToken)
        {
            if (accessToken == null)
            {
                throw new ArgumentNullException(nameof(accessToken));
            }

            this._memoryCache.TryGetValue(accessToken,out IPrincipal principal);
            return principal;
        }
    }
}