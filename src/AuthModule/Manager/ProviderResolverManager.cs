﻿// -----------------------------------------------------------------------
// <copyright file="ProviderResolverManager.cs" company="EUROSTAT">
//   Date Created : 2017-06-03
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Manager
{
    using Estat.Nsi.AuthModule.Engine;

    /// <summary>
    /// Resolve providers using the <see cref="IProviderManager{TProvider}"/> implementations and <see cref="ConfigManager"/> (from <c>Web.Config</c>)
    /// </summary>
    public class ProviderResolverManager
    {
        /// <summary>
        /// The configuration manager
        /// </summary>
        private readonly ConfigManager _configManager;

        /// <summary>
        /// The credentials manager
        /// </summary>
        private readonly IProviderManager<IUserCredentials> _credentialsManager;

        /// <summary>
        /// The authentication provider manager
        /// </summary>
        private readonly IProviderManager<IAuthenticationProvider> _authenticationProviderManager;

        /// <summary>
        /// The authorization provider manager
        /// </summary>
        private readonly IProviderManager<IAuthorizationProvider> _authorisationProviderManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProviderResolverManager"/> class.
        /// </summary>
        /// <param name="credentialsManager">The credentials manager.</param>
        /// <param name="authenticationProviderManager">The authentication provider manager.</param>
        /// <param name="authorisationProviderManager">The authorization provider manager.</param>
        public ProviderResolverManager(IProviderManager<IUserCredentials> credentialsManager, IProviderManager<IAuthenticationProvider> authenticationProviderManager, IProviderManager<IAuthorizationProvider> authorisationProviderManager)
        {
            this._credentialsManager = credentialsManager;
            this._authenticationProviderManager = authenticationProviderManager;
            this._authorisationProviderManager = authorisationProviderManager;
            this._configManager = ConfigManager.Instance;
        }

        /// <summary>
        /// Creates the user credentials.
        /// </summary>
        /// <returns>
        /// The <see cref="IUserCredentials"/>.
        /// </returns>
        public IUserCredentials CreateUserCredentials()
        {
            return this._credentialsManager.GetProvider(this._configManager.Config.UserCredentialsImplementation.ImplementationType);
        }

        /// <summary>
        /// Creates the authorization provider.
        /// </summary>
        /// <returns>
        /// The <see cref="IAuthorizationProvider"/>.
        /// </returns>
        public IAuthorizationProvider CreateAuthorizationProvider()
        {
            return this._authorisationProviderManager.GetProvider(this._configManager.Config.AuthorizationImplementation.ImplementationType);
        }

        /// <summary>
        /// Creates the authentication provider.
        /// </summary>
        /// <returns>
        /// The <see cref="IAuthenticationProvider"/>.
        /// </returns>
        public IAuthenticationProvider CreateAuthenticationProvider()
        {
            return this._authenticationProviderManager.GetProvider(this._configManager.Config.AuthenticationImplementation.ImplementationType);
        }
    }
}