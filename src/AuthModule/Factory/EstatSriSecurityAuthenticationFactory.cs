﻿// -----------------------------------------------------------------------
// <copyright file="EstatSriSecurityAuthenticationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;

namespace Estat.Nsi.AuthModule.Factory
{
    using Estat.Nsi.AuthModule.Engine;
    using Estat.Sri.Security.Engine;
    using Estat.Sri.Security.Model;

    /// <summary>
    /// Class AuthSchemaAuthenticationProviderFactory.
    /// </summary>
    public class EstatSriSecurityAuthenticationFactory : AbstractProviderFactory<EstatSriSecurityAuthenticationProvider, IAuthenticationProvider>
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly AuthDbSecurityStore _authDbSecurityStore;

        private readonly PrincipalRetriever _principalRetriever;

        public EstatSriSecurityAuthenticationFactory(IHttpContextAccessor contextAccessor)
        {
            this._contextAccessor = contextAccessor;
            _authDbSecurityStore = new AuthDbSecurityStore();
            _principalRetriever = new PrincipalRetriever();
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <returns>The provider implementation.</returns>
        protected override EstatSriSecurityAuthenticationProvider CreateInstance()
        {
            return new EstatSriSecurityAuthenticationProvider(this._authDbSecurityStore, this._principalRetriever, this._contextAccessor);
        }
    }
}