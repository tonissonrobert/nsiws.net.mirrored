﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreAuthenticationFactory.cs" company="EUROSTAT">
//   Date Created : 2017-06-07
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Factory
{
    using Estat.Nsi.AuthModule.Engine;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;

    /// <summary>
    /// The mapping store authentication factory.
    /// </summary>
    public class MappingStoreAuthenticationFactory : AbstractProviderFactory<MappingStoreAuthenticationProvider, IAuthenticationProvider>
    {
        /// <summary>
        /// The bytes converter
        /// </summary>
        private readonly BytesConverter _bytesConverter;
     
        /// <summary>
        /// The hash builder
        /// </summary>
        private readonly HashBuilder _hashBuilder;

        /// <summary>
        /// The store identifier repo
        /// </summary>
        private readonly IMappingStoreIdRepository _storeIdRepo;

        /// <summary>
        /// The configuration store manager
        /// </summary>
        private readonly IConfigurationStoreManager _configurationStoreManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreAuthenticationFactory" /> class.
        /// </summary>
        /// <param name="storeIdRepo">The store identifier repository.</param>
        /// <param name="configurationStoreManager">The configuration store manager.</param>
        public MappingStoreAuthenticationFactory(IMappingStoreIdRepository storeIdRepo, IConfigurationStoreManager configurationStoreManager)
        {
            this._bytesConverter = new BytesConverter();
          
            this._hashBuilder = new HashBuilder(this._bytesConverter);
            this._storeIdRepo = storeIdRepo;
            this._configurationStoreManager = configurationStoreManager;
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <returns>The provider implementation.</returns>
        protected override MappingStoreAuthenticationProvider CreateInstance()
        {
            return new MappingStoreAuthenticationProvider(this._bytesConverter, this._hashBuilder, this._storeIdRepo, this._configurationStoreManager);
        }
    }
}