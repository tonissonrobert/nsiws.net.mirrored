namespace Estat.Nsi.AuthModule.Engine
{
    using System;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Text;

    using Estat.Nsi.AuthModule.Model;

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="DbAuthenticationProviderBase" />
    public class DbAuthenticationProviderMD5 : DbAuthenticationProviderBase
    {

        /// <summary>
        /// Checks the password encoding
        /// </summary>
        /// <param name="user">
        /// The user.
        /// </param>
        /// <param name="convertDbValue">
        /// The convert database value.
        /// </param>
        /// <returns>
        /// True if the password matches
        /// </returns>
        protected override bool CheckPasswordEnc(IUser user, string convertDbValue)
        {
            return string.Equals(GetPasswordHash(user.Password), convertDbValue, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Get MD5 hash of the specified <paramref name="txtNewPassword"/>
        /// </summary>
        /// <param name="txtNewPassword">
        /// The text to get the MD5 hash
        /// </param>
        /// <returns>
        /// The MD5 hash of the specified <paramref name="txtNewPassword"/>
        /// </returns>
        private static string GetPasswordHash(string txtNewPassword)
        {
            // Create a byte array from source data.
            byte[] tmpSource = Encoding.UTF8.GetBytes(txtNewPassword);

            // Compute hash based on source data.
            using (var md5CryptoServiceProvider = new MD5CryptoServiceProvider())
            {
                byte[] tmpHash = md5CryptoServiceProvider.ComputeHash(tmpSource);
                var output = new StringBuilder(tmpHash.Length);
                for (int i = 0; i < tmpHash.Length; i++)
                {
                    output.Append(tmpHash[i].ToString("X2", CultureInfo.InvariantCulture));
                }

                return output.ToString();
            }
        }
    }
}