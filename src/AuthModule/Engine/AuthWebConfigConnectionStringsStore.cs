﻿// -----------------------------------------------------------------------
// <copyright file="WebConfigConnectionStringsStore.cs" company="EUROSTAT">
//   Date Created : 2017-06-13
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Engine
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;

    using Estat.Sri.Mapping.Api.Engine;

    /// <summary>
    /// Class WebConfigConnectionStringsStore.
    /// </summary>
    /// <seealso cref="Estat.Sri.Mapping.Api.Engine.IConfigurationStoreEngine{ConnectionStringSettings}" />
    internal class AuthWebConfigConnectionStringsStore : IConfigurationStoreEngine<ConnectionStringSettings>
    {
        /// <summary>
        /// Gets the settings.
        /// </summary>
        /// <returns>The <see cref="IEnumerable{ConnectionStringSettings}"/></returns>
        public IEnumerable<ConnectionStringSettings> GetSettings()
        {
            return ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>();
        }

        /// <summary>
        /// Saves the settings.
        /// </summary>
        /// <param name="save">The save.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void SaveSettings(ConnectionStringSettings save)
        {
        }

        /// <summary>
        /// Deletes the settings.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void DeleteSettings(string name)
        {
        }
    }
}