﻿// -----------------------------------------------------------------------
// <copyright file="DbAuthenticationProvider.cs" company="EUROSTAT">
//   Date Created : 2011-06-19
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Nsi.AuthModule.Engine
{
    using Estat.Nsi.AuthModule.Model;

    /// <summary>
    /// An implementation of the <see cref="IAuthenticationProvider"/> interface.
    /// This implementation uses a database and a configurable SQL query to authenticate the user.
    /// The database connection string and SQL query are configured in .config files.
    /// </summary>
    public class DbAuthenticationProvider : DbAuthenticationProviderBase
    {
        /// <summary>
        /// Checks the password encoding
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="convertDbValue">The convert database value.</param>
        /// <returns>True if the password matches</returns>
        protected override bool CheckPasswordEnc(IUser user, string convertDbValue)
        {
            return string.Equals(user.Password, convertDbValue);
        }

        public DbAuthenticationProvider()
        {
        }
    }
}