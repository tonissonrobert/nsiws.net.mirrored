﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreAuthenticationProvider.cs" company="EUROSTAT">
//   Date Created : 2017-06-06
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using Microsoft.AspNetCore.Http;

namespace Estat.Nsi.AuthModule.Engine
{
    using System.Configuration;
    using System.Linq;
    using System.Security.Principal;

    using Estat.Nsi.AuthModule.Model;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.MappingStoreRetrieval.Builder;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Manager;

    using Org.Sdmxsource.Sdmx.Api.Exception;
    using log4net;

    /// <summary>
    /// The mapping store authentication provider. This is depreciated
    /// </summary>
    public class MappingStoreAuthenticationProvider : IAuthenticationProvider
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(MappingStoreAuthenticationProvider));

        /// <summary>
        /// The bytes converter
        /// </summary>
        private readonly BytesConverter _bytesConverter;

        /// <summary>
        /// The hash builder
        /// </summary>
        private readonly HashBuilder _hashBuilder;

        /// <summary>
        /// The store identifier repo
        /// </summary>
        private readonly IMappingStoreIdRepository _storeIdRepo;

        /// <summary>
        /// The application configuration store
        /// </summary>
        private readonly IConfigurationStoreManager _appConfigStore;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreAuthenticationProvider" /> class.
        /// </summary>
        /// <param name="bytesConverter">The bytes converter.</param>
        /// <param name="hashBuilder">The hash builder.</param>
        /// <param name="storeIdRepo">The store identifier repo.</param>
        /// <param name="appConfigStore">The application configuration store.</param>
        public MappingStoreAuthenticationProvider(BytesConverter bytesConverter, HashBuilder hashBuilder, IMappingStoreIdRepository storeIdRepo, IConfigurationStoreManager appConfigStore)
        {
            this._bytesConverter = bytesConverter;
            this._hashBuilder = hashBuilder;
            this._storeIdRepo = storeIdRepo;
            this._appConfigStore = appConfigStore;
        }

        /// <summary>
        /// Authenticate the specified user
        /// </summary>
        /// <param name="user">
        /// The <see cref="IUser"/> instance containing the user information
        /// </param>
        /// <returns>
        /// If the user is authenticated <see cref="IPrincipal"/> else null
        /// </returns>
        public IPrincipal Authenticate(IUser user)
        {
            var mappingAssistantUserRetriever = this.CreateMappingAssistantUserRetriever();
            try
            {
                return mappingAssistantUserRetriever.GetPrincipal(user.UserName, user.Password);
            }
            catch (SdmxUnauthorisedException)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the anonymous user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns>An implementation specific anonymous <see cref="IPrincipal"/></returns>
        public IPrincipal GetAnonymous(string userName)
        {
            var mappingAssistantUserRetriever = this.CreateMappingAssistantUserRetriever();
            try
            {
                return mappingAssistantUserRetriever.GetPrincipal(userName, string.Empty);
            }
            catch (SdmxUnauthorisedException)
            {
                return null;
            }
        }

        /// <summary>
        /// Creates the mapping assistant user retriever.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>The <see cref="MappingAssistantUserRetriever"/></returns>
        /// <exception cref="SdmxServiceUnavailableException">Authentication store cannot be accessed</exception>
        private MappingAssistantUserRetriever CreateMappingAssistantUserRetriever()
        {
            var connectionStringName = this._storeIdRepo.GetConnectionName();

            var connectionStringSettings = this._appConfigStore.GetSettings<ConnectionStringSettings>().FirstOrDefault(x => x.Name == connectionStringName);

            if (connectionStringSettings == null)
            {
                _log.ErrorFormat("Couldn't find a connection string with name='{0}'", connectionStringName);
                throw new SdmxServiceUnavailableException("Authentication store cannot be accessed");
            }

            Database database = new Database(connectionStringSettings);
            var mappingAssistantUserRetriever = new MappingAssistantUserRetriever(this._hashBuilder, this._bytesConverter, database, connectionStringName);
            return mappingAssistantUserRetriever;
        }
    }
}