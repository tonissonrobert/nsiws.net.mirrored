﻿// -----------------------------------------------------------------------
// <copyright file="JsonResponseContentHeader.cs" company="EUROSTAT">
//   Date Created : 2016-08-02
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Json.Model
{
    using System;
    using System.Net.Mime;

    using Estat.Sdmxsource.Extension.Model;

    /// <summary>
    /// An implementation of <see cref="IResponseContentHeader"/> that will postpone the language retrieval until it is needed.
    /// </summary>
    /// <seealso cref="Estat.Sdmxsource.Extension.Model.IResponseContentHeader" />
    public class JsonResponseContentHeader : IResponseContentHeader
    {
        /// <summary>
        /// The language function
        /// </summary>
        private readonly Func<string> _languageFunc;

        /// <summary>
        /// The media type
        /// </summary>
        private readonly ContentType _mediaType;

        /// <summary>
        /// The language
        /// </summary>
        private string _language;

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonResponseContentHeader"/> class.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="languageFunc">The language function.</param>
        public JsonResponseContentHeader(ContentType mediaType, Func<string> languageFunc)
        {
            this._languageFunc = languageFunc;
            this._mediaType = mediaType;
        }

        /// <summary>
        /// Gets the <see cref="T:System.Net.Mime.ContentType" />.
        /// </summary>
        /// <value>
        /// The type of the media.
        /// </value>
        public ContentType MediaType
        {
            get
            {
                return this._mediaType;
            }
        }

        /// <summary>
        /// Gets the language. The value is expected to be valid for HTTP header Accept-Language
        /// </summary>
        /// <value>
        /// The language.
        /// </value>
        public string Language
        {
            get
            {
                if (this._language == null)
                {
                    this._language = this._languageFunc();
                }

                return this._language;
            }
        }
    }
}