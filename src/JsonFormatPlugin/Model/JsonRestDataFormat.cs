﻿// -----------------------------------------------------------------------
// <copyright file="JsonRestDataFormat.cs" company="EUROSTAT">
//   Date Created : 2016-07-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Json.Model
{
    using System.Text;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;

    /// <summary>
    /// The JSON rest data format.
    /// </summary>
    internal class JsonRestDataFormat : AbstractRestFormat, IDataFormat
    {
        public JsonRestDataFormat(Encoding encoding, SuperObjectRetriever retriever, DataType sdmxDataFormat = null, ISdmxObjectRetrievalManager retrievalManager = null) : base(encoding)
        {
            this.Retriever = retriever;
            this.SdmxDataFormat = sdmxDataFormat ?? DataType.GetFromEnum(DataEnumType.Json);
            this.RetrieverManager = retrievalManager;
        }

        /// <summary>
        /// Gets a string representation of the format, that can be used for auditing and debugging purposes.
        /// <p />
        /// This is expected to return a not null response.
        /// </summary>
        public string FormatAsString
        {
            get
            {
                return this.ToString(); // TODO Change me
            }
        }

        /// <summary>
        /// Gets the SDMX data format that this interface is describing.
        /// If this is not describing an SDMX message then this will return null and the implementation class will be expected
        /// to expose additional methods to understand the data
        /// </summary>
        public DataType SdmxDataFormat { get; private set; }

        public SuperObjectRetriever Retriever { get; private set; }

        public ISdmxObjectRetrievalManager RetrieverManager { get; private set; }
    }
}