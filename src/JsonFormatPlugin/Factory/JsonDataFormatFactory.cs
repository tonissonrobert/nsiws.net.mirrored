﻿// -----------------------------------------------------------------------
// <copyright file="JsonDataFormatFactory.cs" company="EUROSTAT">
//   Date Created : 2016-07-20
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Format.Json.Factory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mime;
    using System.Text;
    using System.Xml;

    using Estat.Sdmxsource.Extension.Engine;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Ws.Format.Json.Model;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Data;
    using Org.Sdmxsource.Sdmx.Structureparser.Builder.SuperObjects;

    /// <summary>
    /// The JSON data format factory.
    /// </summary>
    public class JsonDataFormatFactory : IDataWriterPluginContract
    {
        /// <summary>
        /// The _content list
        /// </summary>
        private readonly IList<string> _contentList = new[]
                                                          {
                                                              "application/vnd.sdmx.data+json;version=2",
                                                              "application/vnd.sdmx.data+json;version=2.0",
                                                              "application/vnd.sdmx.data+json;version=2.0.0",
                                                              "application/vnd.sdmx.data+json;version=1.0",
                                                              "application/vnd.sdmx.data+json;version=1.0.0-wd",
                                                              "text/json", 
                                                              "application/json"
                                                          };

        /// <summary>
        /// The _super objects builder
        /// </summary>
        private readonly SuperObjectsBuilder _superObjectsBuilder = new SuperObjectsBuilder();

        private readonly ITranslatorFactory _translatorFactory;

        public JsonDataFormatFactory(ITranslatorFactory translatorFactory)
        {
            this._translatorFactory = translatorFactory;
        }

        /// <summary>
        /// Gets the supported formats and header values.
        /// </summary>
        /// <param name="restDataRequest">The rest data request.</param>
        /// <returns>
        /// The list of supported formats - <see cref="T:Estat.Sdmxsource.Extension.Model.IRestResponse`1" />.
        /// </returns>
        public IEnumerable<IRestResponse<IDataFormat>> GetDataFormats(IRestDataRequest restDataRequest)
        {
            if (restDataRequest == null)
            {
                throw new ArgumentNullException("restDataRequest");
            }

            var charSet = restDataRequest.SortedAcceptHeaders.CharSets.Select(quality => quality.Value).FirstOrDefault()
                          ?? new UTF8Encoding(false);

            var superObjectRetriever = new Lazy<SuperObjectRetriever>(() => new SuperObjectRetriever(
                restDataRequest,
                this._superObjectsBuilder, this._translatorFactory));

            return
                this._contentList.Select(
                    s => BuildRestResponse(s, charSet, superObjectRetriever, restDataRequest.RetrievalManager)
                );
        }

        /// <inheritdoc cref="IDataWriterPluginContract.GetAvailableDataFormats"/>
        public IEnumerable<string> GetAvailableDataFormats()
        {
            return this._contentList;
        }

        /// <summary>
        /// Gets the data format.
        /// </summary>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="xmlWriter">The XML writer.</param>
        /// <returns>
        /// The <see cref="T:Org.Sdmxsource.Sdmx.Api.Model.Data.IDataFormat" />.
        /// </returns>
        /// <exception cref="System.NotImplementedException">Not supported</exception>
        public IDataFormat GetDataFormat(ISoapRequest<DataType> dataType, XmlWriter xmlWriter)
        {
            // This is used for SOAP. Only XML formats apply here normally 
            return null;
        }

        /// <summary>
        /// Builds the rest response.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="s">The s.</param>
        /// <param name="charSet">The character set.</param>
        /// <param name="retriever">The state.</param>
        /// <param name="retrievalManager">Retrieval manager</param>
        /// <returns>The <see cref="IRestResponse{IDataFormat}"/></returns>
        private static IRestResponse<IDataFormat> BuildRestResponse(string s, Encoding charSet, Lazy<SuperObjectRetriever> retriever, ISdmxObjectRetrievalManager retrievalManager)
        {
            var contentType = new ContentType(s) { CharSet = charSet.WebName };

            var jsonResponseContentHeader = new JsonResponseContentHeader(
                contentType,
                () => string.Join(",", retriever.Value.Translator.SelectedLanguages));

            Lazy<IDataFormat> format;


            if (contentType.Parameters != null && contentType.Parameters.ContainsKey("version") && (contentType.Parameters["version"] == "2.0.0" || contentType.Parameters["version"] == "2.0" || contentType.Parameters["version"] == "2"))
            {
                format = new Lazy<IDataFormat>(() => new JsonRestDataFormat(charSet, retriever.Value, DataType.GetFromEnum(DataEnumType.Json20), retrievalManager));
            }
            else if (contentType.Parameters != null && contentType.Parameters.ContainsKey("version") && (contentType.Parameters["version"] == "1.0.0-wd" || contentType.Parameters["version"] == "1.0"))
            {
                format = new Lazy<IDataFormat>(() => new JsonRestDataFormat(charSet, retriever.Value, DataType.GetFromEnum(DataEnumType.Json10)));
            }
            else
            {
                format = new Lazy<IDataFormat>( () => new JsonRestDataFormat(charSet, retriever.Value));
            }

            return new RestResponse<IDataFormat>(format, jsonResponseContentHeader);
        }
    }
}