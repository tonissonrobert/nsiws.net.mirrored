﻿// -----------------------------------------------------------------------
// <copyright file="DependencyRegisterManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-18
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Text;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model;

namespace Estat.Sri.Ws.Format.Json.Factory
{
    using System;
    using System.IO;
    using Estat.Sri.Ws.Format.Json.Model;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Engine;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Model.Format;
    using Org.Sdmxsource.Sdmx.Structureparser.Engine.Writing;


    internal class RestJsonStructureWriterFactory : IStructureWriterFactory
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="structureFormat"></param>
        /// <param name="streamWriter"></param>
        /// <returns></returns>
        public IStructureWriterEngine GetStructureWriterEngine(IStructureFormat structureFormat, Stream streamWriter)
        {
            if (structureFormat == null)
            {
                throw new ArgumentNullException("structureFormat");
            }

            if (structureFormat.SdmxOutputFormat != null && (structureFormat.SdmxOutputFormat.EnumType == StructureOutputFormatEnumType.Json || structureFormat.SdmxOutputFormat.EnumType == StructureOutputFormatEnumType.JsonV10))
            {
                var wsSdmxStructureJsonFormat = structureFormat as JsonRestStructureFormat;

                if (wsSdmxStructureJsonFormat != null)
                {
                    // translate the WS format to SdmxSource format
                    var sdmxStructureJsonFormat = new SdmxStructureJsonFormat(wsSdmxStructureJsonFormat.Translator, wsSdmxStructureJsonFormat.SdmxOutputFormat);

                    if (sdmxStructureJsonFormat.SdmxOutputFormat.EnumType == StructureOutputFormatEnumType.JsonV10)
                    {
                        return new StructureWriterEngineJsonV1(streamWriter, sdmxStructureJsonFormat);
                    }
                    else
                    {
                        return new StructureWriterEngineJson(streamWriter, sdmxStructureJsonFormat);
                    }
                }
            }
            return null;
        }
    }
}