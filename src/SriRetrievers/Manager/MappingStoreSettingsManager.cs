﻿// -----------------------------------------------------------------------
// <copyright file="MappingStoreSettingsManager.cs" company="EUROSTAT">
//   Date Created : 2009-10-01
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Retrievers.Manager
{
    using System;
    using System.Configuration;

    using Estat.Sri.Ws.Retrievers.Constant;

    using log4net;

    /// <summary>
    ///     This singleton class is used to read the configuration settings from the web.config file
    ///     and load them in corresponding objects
    /// </summary>
    public class MappingStoreSettingsManager
    {

        /// <summary>
        ///     Private field that stores the Log object
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        ///     Internal field used to store connection string settings
        /// </summary>
        private ConnectionStringSettings _mappingStoreConnectionSettings;

        /// <summary>
        ///     Holds the configured DDB oracle provider or null
        /// </summary>
        private string _oracleProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="MappingStoreSettingsManager"/> class.
        /// </summary>
        public MappingStoreSettingsManager()
        {
            // initialize the log
            this._log = LogManager.GetLogger(typeof(MappingStoreSettingsManager));

            // initialise MappingStoreConnectionSettings
            this.InitialiseMappingStoreConnectionSettings();

            // initialize the oracle data provider if any
            this.InitializeDDBProviders();
        }

        /// <summary>
        ///     Gets the connection string settings.
        /// </summary>
        public ConnectionStringSettings MappingStoreConnectionSettings
        {
            get
            {
                return this._mappingStoreConnectionSettings;
            }
        }

        /// <summary>
        ///     Gets the configured DDB Oracle Provider
        /// </summary>
        [Obsolete("Use DatabaseSettings")]
        public string OracleProvider
        {
            get
            {
                return this._oracleProvider;
            }
        }

        /// <summary>
        ///     This method initializes the connection string settings. It reads them from the web.config file.
        /// </summary>
        private void InitialiseMappingStoreConnectionSettings()
        {
            this._mappingStoreConnectionSettings = ConfigurationManager.ConnectionStrings[SettingsConstants.MappingStoreConnectionName];
            if (this._mappingStoreConnectionSettings == null)
            {
                this._log.ErrorFormat("No connection string with name {0} could not be found. Please add at the Web.config at <connectionStrings> section a connection string with name {0}.", SettingsConstants.MappingStoreConnectionName);
            }
        }

        /// <summary>
        ///     This method initializes the DDB default providers. Currently only for Oracle
        /// </summary>
        private void InitializeDDBProviders()
        {
            this._oracleProvider = ConfigurationManager.AppSettings[SettingsConstants.DefaultDdbOracleProvider];
        }
    }
}