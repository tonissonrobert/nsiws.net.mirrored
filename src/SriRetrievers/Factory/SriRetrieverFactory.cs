// -----------------------------------------------------------------------
// <copyright file="SriRetrieverFactory.cs" company="EUROSTAT">
//   Date Created : 2015-12-16
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Retrievers.Factory
{
    using Estat.Nsi.DataRetriever;
    using Estat.Nsi.DataRetriever.Model;
    using Estat.Nsi.StructureRetriever.Manager;
    using Estat.Nsi.StructureRetriever.Model;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Util;
    using Estat.Sri.CustomRequests.Model;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.MappingStoreRetrieval.Engine;
    using Estat.Sri.MappingStoreRetrieval.Factory;
    using Estat.Sri.MappingStoreRetrieval.Helper;
    using Estat.Sri.MappingStoreRetrieval.Manager;
    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Retrievers.Manager;
    using log4net;
    using Microsoft.Extensions.Configuration;
    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Registry;
    using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
    using Org.Sdmxsource.Util;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Common;
    using System.Linq;
    using System.Security.Principal;
    using AuthAdvancedStructureRetriever = Estat.Nsi.StructureRetriever.Manager.AuthAdvancedStructureRetriever;

    /// <summary>
    /// The SDMX RI implementation of <see cref="IRetrieverFactory"/>
    /// </summary>
    public class SriRetrieverFactory : IRetrieverFactory
    {
        /// <summary>
        /// The SDMX schema V21
        /// </summary>
        private static readonly SdmxSchema _sdmxSchemaV21 = SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne);

        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(SriRetrieverFactory));

        /// <summary>
        /// The default header retrieval manager
        /// </summary>
        private readonly IHeaderRetrievalManager _defaultHeaderRetrievalManager;

        /// <summary>
        /// The configuration manager
        /// </summary>
        private readonly IConnectionStringBuilderManager _mappingStoreConnectionStringBuilder;

        /// <summary>
        /// The mapping store connection settings
        /// </summary>
        private readonly ConnectionStringSettings _mappingStoreConnectionSettings;

        /// <summary>
        /// The configuration
        /// </summary>
        private readonly SettingsFromConfigurationManager _configuration;

        /// <summary>
        /// The connection builder
        /// </summary>
        private readonly IBuilder<DbConnection, DdbConnectionEntity> _connectionBuilder;

        /// <summary>
        /// The mapping manager
        /// </summary>
        private readonly IComponentMappingManager _componentMappingManager;

        /// <summary>
        /// The mapping validation manager
        /// </summary>
        private readonly IComponentMappingValidationManager _mappingValidationManager;

        private readonly IDataflowPrincipalManager _dataflowPrincipalManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SriRetrieverFactory" /> class.
        /// </summary>
        /// <param name="defaultHeaderRetrievalManager">The default header retrieval manager.</param>
        /// <param name="mappingStoreConnectionStringBuilder">The configuration manager.</param>
        /// <param name="connectionBuilder">The connection builder.</param>
        /// <param name="mappingManager">The mapping manager.</param>
        /// <param name="mappingValidationManager">The mapping validation manager.</param>
        /// <param name="dataflowPrincipalManager">Get the current principal</param>
        /// <param name="configuration">The current configuration</param>
        /// <exception cref="ArgumentNullException">
        /// defaultHeaderRetrievalManager
        /// or
        /// configManager
        /// or
        /// connectionBuilder
        /// or
        /// mappingManager
        /// or
        /// mappingValidationManager
        /// </exception>
        /// <exception cref="SdmxException">Could not establish a connection to the mapping store DB</exception>
        //// ========================================================================
        //// *WARNING* this constructor & class is used by 3rd party software. Do not modify any signatures unless it is really necessary
        //// In case of changes it needs to be mentioned in the CHANGELOG.md / Important Changes
        //// ========================================================================
        public SriRetrieverFactory(IHeaderRetrievalManager defaultHeaderRetrievalManager, IConnectionStringBuilderManager mappingStoreConnectionStringBuilder, IBuilder<DbConnection, DdbConnectionEntity> connectionBuilder, IComponentMappingManager mappingManager, IComponentMappingValidationManager mappingValidationManager,IDataflowPrincipalManager dataflowPrincipalManager, SettingsFromConfigurationManager configuration)
        {
            if (defaultHeaderRetrievalManager == null)
            {
                throw new ArgumentNullException(nameof(defaultHeaderRetrievalManager));
            }

            if (mappingStoreConnectionStringBuilder == null)
            {
                throw new ArgumentNullException(nameof(mappingStoreConnectionStringBuilder));
            }

            if (connectionBuilder == null)
            {
                throw new ArgumentNullException(nameof(connectionBuilder));
            }

            if (mappingManager == null)
            {
                throw new ArgumentNullException(nameof(mappingManager));
            }

            if (mappingValidationManager == null)
            {
                throw new ArgumentNullException(nameof(mappingValidationManager));
            }

            MappingStoreIoc.Register<RetrievalEngineContainerFactory>(this.Id);

            this._defaultHeaderRetrievalManager = defaultHeaderRetrievalManager;
            this._mappingStoreConnectionStringBuilder = mappingStoreConnectionStringBuilder;
            this._connectionBuilder = connectionBuilder;
            this._componentMappingManager = mappingManager;
            this._mappingValidationManager = mappingValidationManager;
            this._dataflowPrincipalManager = dataflowPrincipalManager;
            var settingsManager = new MappingStoreSettingsManager();
            this._mappingStoreConnectionSettings = settingsManager.MappingStoreConnectionSettings;
            this._configuration = configuration;
        }

        /// <summary>
        /// Gets the data number of records.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <returns></returns>
        /// <inheritdoc />
        public virtual int GetNumberOfObservations(IDataQuery dataQuery)
        {
            var availableDataManager = this.GetAvailableDataManager(_sdmxSchemaV21, this._dataflowPrincipalManager.GetCurrentPrincipal());
            return (int) availableDataManager.GetCount(new AvailableConstraintQuery(dataQuery),dataQuery.IncludeHistory);
        }

        /// <inheritdoc />
        public virtual IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal)
        {
            return this.CreateInstance(settings => new AvailableDataManager(this.CreateStructureRetrieverSettings(settings, sdmxVersion)), principal);
        }

        private StructureReferenceImpl BuildConstrainableStructureReference(IDataQuery dataQuery)
        {
            IStructureReference currentDataflowReference = dataQuery.Dataflow.AsReference;
            IContentConstraintMutableObject mutableConstraint = new ContentConstraintMutableCore { Id = currentDataflowReference.MaintainableId, AgencyId = currentDataflowReference.AgencyId, Version = currentDataflowReference.Version };
            mutableConstraint.AddName("en", "en");

            var cubeRegion = new CubeRegionMutableCore();
            mutableConstraint.IncludedCubeRegion = cubeRegion;
            foreach (var group in dataQuery.SelectionGroups.SelectMany(x => x.Selections))
            {
                IKeyValuesMutable requestedDimension = new KeyValuesMutableImpl();
                requestedDimension.Id = group.ComponentId;
                if (group.HasMultipleValues)
                {
                    foreach (var groupValue in group.Values)
                    {
                        requestedDimension.AddValue(groupValue);
                    }
                }
                else
                {
                    requestedDimension.AddValue(group.Value);
                }

                cubeRegion.KeyValues.Add(requestedDimension);
            }

            IContentConstraintObject constraint = new ContentConstraintObjectCore(mutableConstraint);
            var specialRequest = new ConstrainableStructureReference(currentDataflowReference, constraint);
            return specialRequest;
        }

        /// <summary>
        ///     Gets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public virtual string Id
        {
            get
            {
                return "MappingStoreRetrieversFactory";
            }
        }

        /// <summary>
        /// Gets the advanced data retrieval.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IAdvancedSdmxDataRetrievalWithWriter" />.</returns>
        public virtual IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            return this.GetDataRetrieverCore(_sdmxSchemaV21, principal);
        }

        /// <summary>
        /// Gets the advanced mutable structure search manager.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IAdvancedMutableStructureSearchManager" />.</returns>
        public virtual IAdvancedMutableStructureSearchManager GetAdvancedMutableStructureSearchManager(IPrincipal principal)
        {
            return this.CreateInstance(settings => new Nsi.StructureRetriever.Manager.AdvancedStructureRetriever(settings), principal);
        }

        /// <summary>
        /// Gets the authentication advanced mutable structure search.
        /// </summary>
        /// <param name="authorizedDataflows">The authorized dataflows.</param>
        /// <returns>The <see cref="IAuthAdvancedMutableStructureSearchManager" />.</returns>
        public virtual IAdvancedMutableStructureSearchManager GetAuthAdvancedMutableStructureSearch(IList<IMaintainableRefObject> authorizedDataflows)
        {
            return this.CreateInstance(settings => new AdvancedMutableStructureSearchManagerWithAuth(new AuthAdvancedStructureRetriever(settings), authorizedDataflows), null);
        }

        /// <summary>
        /// Gets the authentication mutable structure search.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="authorizedDataflows">The authorized dataflows.</param>
        /// <returns>The <see cref="IAuthMutableStructureSearchManager" />.</returns>
        public virtual IMutableStructureSearchManager GetAuthMutableStructureSearch(SdmxSchema sdmxSchema, IList<IMaintainableRefObject> authorizedDataflows)
        {
            return this.CreateInstance(settings => new MutableStructureSearchManagerWithAuth(new AuthStructureRetrieverAvailableData(this.CreateStructureRetrieverSettings(settings, sdmxSchema)), authorizedDataflows), null);
        }

        /// <summary>
        /// Gets the data retrieval.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithWriter" />.</returns>
        public virtual ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this.GetDataRetrieverCore(sdmxSchema, principal);
        }

        /// <summary>
        /// Gets the data retrieval with cross.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxDataRetrievalWithCrossWriter" />.</returns>
        public virtual ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this.GetDataRetrieverCore(sdmxSchema, principal);
        }

        /// <summary>
        /// Gets the mutable structure search.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="IMutableStructureSearchManager" />.</returns>
        public virtual IMutableStructureSearchManager GetMutableStructureSearch(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this.CreateInstance(settings => new StructureRetrieverAvailableData(this.CreateStructureRetrieverSettings(settings, sdmxSchema)), principal);
        }

        /// <summary>
        /// Gets the SDMX object retrieval.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ISdmxObjectRetrievalManager" />.</returns>
        public virtual ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal)
        {
            return this.CreateInstance(settings => new MutableWrapperSdmxObjectRetrievalManager(new MappingStoreRetrievalManager(settings), new StructureRetrieverAvailableData(this.CreateStructureRetrieverSettings(settings, _sdmxSchemaV21))), principal);
        }

        /// <summary>
        /// Gets the data retriever core.
        /// </summary>
        /// <param name="sdmxSchema">The SDMX schema.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="DataRetrieverCore"/></returns>
        private DataRetrieverCore GetDataRetrieverCore(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            return this.CreateInstance(settings => new DataRetrieverCore(this.CreateDataRetrieverSettings(settings, sdmxSchema)), principal);
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <typeparam name="TManager">The type of the manager.</typeparam>
        /// <param name="managerBuilder">The manager builder.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The <typeparamref name="TManager"/></returns>
        private TManager CreateInstance<TManager>(Func<ConnectionStringSettings, TManager> managerBuilder, IPrincipal principal) where TManager : class
        {
            var connectionStringSettings = this.GetConnectionStringSettings(principal);
            if (connectionStringSettings != null)
            {
                return managerBuilder(connectionStringSettings);
            }

            return null;
        }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>The <see cref="ConnectionStringSettings" /></returns>
        private ConnectionStringSettings GetConnectionStringSettings(IPrincipal principal)
        {
            ConnectionStringSettings connectionStringSettings = this._mappingStoreConnectionStringBuilder.Build(principal);
            if (connectionStringSettings == null)
            {
                _log.Debug("Could not find Connection strings from connection manager");
                return this._mappingStoreConnectionSettings;
            }

            _log.Debug("Using Connection strings from connection manager");
            return connectionStringSettings;
        }

        /// <summary>
        /// Gets the structure retriever settings.
        /// </summary>
        /// <param name="connectionStringSettings">The connection string settings.</param>
        /// <param name="sdmxVersion">The SDMX version.</param>
        /// <returns>
        /// The <see cref="StructureRetrieverSettings" />.
        /// </returns>
        private StructureRetrieverSettings CreateStructureRetrieverSettings(ConnectionStringSettings connectionStringSettings, SdmxSchemaEnumType sdmxVersion)
        {
            StructureRetrieverSettings settings = new StructureRetrieverSettings();
            settings.ConnectionBuilder = this._connectionBuilder;
            settings.MappingManager = this._componentMappingManager;
            settings.ConnectionStringSettings = connectionStringSettings;
            settings.SdmxSchemaVersion = sdmxVersion;
            settings.ComponentMappingValidationManager = this._mappingValidationManager;
            settings.StoreId = connectionStringSettings.Name;
            settings.SpecialRetrievalManager = new SpecialMutableObjectRetrievalManager(settings.ConnectionStringSettings);
            settings.AllowSdmx21CrossSectional = this._configuration.AllowSdmx21Cross;
            
            return settings;
        }

        /// <summary>
        /// Gets the data retriever settings.
        /// </summary>
        /// <param name="connectionStringSettings">The connection string settings.</param>
        /// <param name="sdmxVersion">The SDMX version.</param>
        /// <returns>
        /// The <see cref="DataRetrieverSettings" />.
        /// </returns>
        private DataRetrieverSettings CreateDataRetrieverSettings(ConnectionStringSettings connectionStringSettings, SdmxSchemaEnumType sdmxVersion)
        {
            DataRetrieverSettings settings = new DataRetrieverSettings();
            settings.ConnectionBuilder = this._connectionBuilder;
            settings.ComponentMappingManager = this._componentMappingManager;
            settings.ConnectionStringSettings = connectionStringSettings;
            settings.SdmxSchemaVersion = sdmxVersion;
            settings.ComponentMappingValidationManager = this._mappingValidationManager;
            settings.StoreId = connectionStringSettings.Name;
            settings.DefaultHeader = this._defaultHeaderRetrievalManager.Header;
            settings.HeaderRetrieverEngine = new HeaderRetrieverEngine(settings.ConnectionStringSettings);
            settings.OutputLocalDataAsAnnotation = OnTheFlyAnnotationScope.HasAnnotationType(OnTheFlyAnnotationType.LocalData);
            settings.Range = HeaderScope.Range;
            settings.MetadataLevel = HeaderScope.MetadataLevel;

            if (this._configuration.EnableReleaseManagement)
            {
                settings.IsPit = HeaderScope.IsPit;
            }

            settings.StructureUsage = SdmxStructureType.ParseClass(this._configuration.StructureUsage).EnumType;
            settings.DatasetAction = this._configuration.DatasetAction;
            return settings;
        }
    }
}
