﻿using System.Threading.Tasks;
using Estat.Sri.Ws.NSIWebServiceCore;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace NSIWebServiceCore.Controllers
{
    [Route("rest/")]
    [Route("rest/v{apiVersion:apiVersion}/")]
    [ApiController]
    [ApiVersion("1")]
    public class AvailableController : Controller
    {
        private readonly IAvailableResource _availableResource;

        public AvailableController(IAvailableResource availableResource)
        {
            this._availableResource = availableResource;
        }

        /// <summary>
        /// Get information about data availability
        /// </summary>
        /// <param name="flowRef">The <strong>statistical domain</strong> (aka dataflow) of the data to be returned.</param>
        /// <param name="key">The (possibly partial) <strong>key identifying the data to be returned</strong>.</param>
        /// <param name="providerRef">The <strong>provider of the data</strong> to be retrieved.</param>
        /// <param name="componentIds">The id of the Dimension for which to obtain availability information about. Use all to indicate that data availability should be provided for all dimensions.</param>
        [HttpGet("availableconstraint/{flowRef}/{key=ALL}/{providerRef=ALL}/{componentIds=ALL}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Data availability queries" })]
        [ApiParameter("flowRef", ParameterLocation.Path, ApiParameter.FLOW)]
        [ApiParameter("key", ParameterLocation.Path, ApiParameter.KEY)]
        [ApiParameter("providerRef", ParameterLocation.Path, ApiParameter.PROVIDER)]
        [ApiParameter("mode", ParameterLocation.Query, ApiParameter.MODE_V1)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.AC_REFERENCES_V1)]
        [ApiParameter("startPeriod", ParameterLocation.Query, ApiParameter.START_PERIOD)]
        [ApiParameter("endPeriod", ParameterLocation.Query, ApiParameter.END_PERIOD)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time",
            Description = "<p>The last time the query was performed by the client.</p><p>The response should include the latest version of what has " +
                          "changed in the database since that point in time (i.e. additions, revisions or deletions since the last time the query was performed).</p>")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING,
            Description = "<p>Specifies whether the response should be compressed and how.</p><p><code>identity</code> " +
                          "(the default) indicates that no compression will be performed.</p>")]
        [ApiParameter("accept-language", ParameterLocation.Header, Description = "Specifies the client's preferred language.")]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time",
            Description = "Instructs to return the content matching the query only if it has changed since the supplied timestamp.")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V1)]
        public async Task GetData(string flowRef, string key, string providerRef, string componentIds)
        {
            await this._availableResource.GetDataAsync(flowRef, key, providerRef, componentIds);
        }

        /// <summary>
        /// Get information about data availability
        /// </summary>
        /// <remarks>
        /// Same applicability as the GET method.
        /// The POST method is used to include the 'key' argument in the body of query in order to workaround URL size limitations.
        ///
        /// Use and set the Content-Type HTTP header to <code>application/x-www-form-urlencoded</code>
        /// </remarks>
        /// <param name="flowRef">The <strong>statistical domain</strong> (aka dataflow) of the data to be returned.</param>
        /// <param name="key">The (possibly partial) <strong>key identifying the data to be returned</strong>.</param>
        /// <param name="providerRef">The <strong>provider of the data</strong> to be retrieved.</param>
        /// <param name="componentIds">The id of the Dimension for which to obtain availability information about. Use all to indicate that data availability should be provided for all dimensions.</param>
        [HttpPost("availableconstraint/{flowRef}/body/{providerRef=ALL}/{componentIds=ALL}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Data availability queries" })]
        [ApiParameter("flowRef", ParameterLocation.Path, ApiParameter.FLOW)]
        [ApiParameter("key", ParameterLocation.Path, ApiParameter.KEY)]
        [ApiParameter("providerRef", ParameterLocation.Path, ApiParameter.PROVIDER)]
        [ApiParameter("mode", ParameterLocation.Query, ApiParameter.MODE_V1)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.AC_REFERENCES_V1)]
        [ApiParameter("startPeriod", ParameterLocation.Query, ApiParameter.START_PERIOD)]
        [ApiParameter("endPeriod", ParameterLocation.Query, ApiParameter.END_PERIOD)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time",
            Description = "<p>The last time the query was performed by the client.</p><p>The response should include the latest version of what has " +
                          "changed in the database since that point in time (i.e. additions, revisions or deletions since the last time the query was performed).</p>")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING,
            Description = "<p>Specifies whether the response should be compressed and how.</p><p><code>identity</code> " +
                          "(the default) indicates that no compression will be performed.</p>")]
        [ApiParameter("accept-language", ParameterLocation.Header, Description = "Specifies the client's preferred language.")]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time",
            Description = "Instructs to return the content matching the query only if it has changed since the supplied timestamp.")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V1)]
        public async Task PostAvailableConstraintData(string flowRef, string providerRef, [FromForm] string key,string componentIds)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                key = "ALL";
            }

            await this._availableResource.GetDataAsync(flowRef, key, providerRef, componentIds);
        }
    }
}