﻿using System.Threading.Tasks;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;
// ReSharper disable RouteTemplates.ActionRoutePrefixCanBeExtractedToControllerRoute

// ReSharper disable once CheckNamespace
namespace NSIWebServiceCore.Controllers
{
    /// <summary>
    /// The available controller for SDMX REST 2.0
    /// </summary>
    [Route("rest/v{apiVersion:apiVersion}")]
    [ApiController]
    [ApiVersion("2")]
    public class AvailabilityController : Controller
    {
        private readonly IAvailableResource _availableResource;

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailabilityController"/>.
        /// </summary>
        /// <param name="availableResource">taken from DIC</param>
        public AvailabilityController(IAvailableResource availableResource)
        {
            this._availableResource = availableResource;
        }

        /// <summary>
        ///     Data availability queries
        /// </summary>
        /// <remarks>
        ///     See which data would match a query, without actually retrieving these data.
        ///
        ///     This can be used, for example, to build a data query form that enables users of the UI to create a data query by selecting dimension values. 
        ///     For example the user is able to click 'Reporting Country' and then select the codes 'United Kingdom', 'Greece', and 'Switzerland'.
        ///
        ///     The query returns a <code>Constraint</code>, i.e. structural metadata, and is therefore similar to the other structural metadata queries but 
        ///     the query itself is more akin to a data query.
        /// </remarks>
        /// <param name="context"></param>
        /// <param name="agencyId">
        ///     <p>The maintainer(s) of the artefacts.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available maintainers.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The artefact ID(s).</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version(s) of the artefact.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        ///     <p><code>+</code> can be used to retrieve the latest stable version.</p>
        ///     <p><code>~</code> can be used to retrieve the latest version, regardless of its status (stable, draft, etc.).</p>
        /// </param>
        /// <param name="key">
        ///     <p>The combination of dimension values identifying series or slices of the cube (for example <code>D.USD.EUR.SP00.A</code>).</p>
        ///     <p>Multiple values are possible. Wildcards are supported using <code>*</code> (<code>D.*.EUR.SP00.A</code>).</p>
        /// </param>
        /// <param name="componentId">
        ///     <p>The ID of the dimension for which to obtain availability information.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available dimensions.</p>
        /// </param>
        [HttpGet("availability/{context=*}/{agencyId=*}/{resourceId=*}/{version=*}/{key=*}/{componentId=*}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Data availability queries" })]
        [ApiParameter("context", ParameterLocation.Path, ApiParameter.CONTEXT, Required = true)]
        [ApiParameter("c", ParameterLocation.Query, ApiParameter.C)]
        [ApiParameter("mode", ParameterLocation.Query, ApiParameter.MODE_V2)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.AC_REFERENCES_V2)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time", 
            Description = "The last time the query was performed by the client.")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING)]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        // Uncomment the following line to include response content types from the specifications.  
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V2)]
        public async Task GetData(string context, string agencyId, string resourceId, string version, string key, string componentId)
        {
            await this._availableResource.GetDataV2Async(context, agencyId, resourceId, version, key, componentId);
        }
        
        /// <summary>
        ///     Data availability queries
        /// </summary>
        /// <remarks>
        ///     Same applicability as the GET method.
        ///     The POST method is used to include the 'key' argument in the body of query in order to workaround URL size limitations.
        ///
        ///     Use and set the Content-Type HTTP header to <code>application/x-www-form-urlencoded</code>
        /// </remarks>
        /// <param name="context"></param>
        /// <param name="agencyId">
        ///     <p>The maintainer(s) of the artefacts.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available maintainers.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The artefact ID(s).</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version(s) of the artefact.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        ///     <p><code>+</code> can be used to retrieve the latest stable version.</p>
        ///     <p><code>~</code> can be used to retrieve the latest version, regardless of its status (stable, draft, etc.).</p>
        /// </param>
        /// <param name="key">
        ///     <p>The combination of dimension values identifying series or slices of the cube (for example <code>D.USD.EUR.SP00.A</code>).</p>
        ///     <p>Multiple values are possible. Wildcards are supported using <code>*</code> (<code>D.*.EUR.SP00.A</code>).</p>
        /// </param>
        /// <param name="componentId">
        ///     <p>The ID of the dimension for which to obtain availability information.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available dimensions.</p>
        /// </param>
        [HttpPost("availability/{context=*}/{agencyId=*}/{resourceId=*}/{version=*}/body/{componentId=*}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Data availability queries" })]
        [ApiParameter("context", ParameterLocation.Path, ApiParameter.CONTEXT, Required = true)]
        [ApiParameter("c", ParameterLocation.Query, ApiParameter.C)]
        [ApiParameter("mode", ParameterLocation.Query, ApiParameter.MODE_V2)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.AC_REFERENCES_V2)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time", 
            Description = "The last time the query was performed by the client.")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING)]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        // Uncomment the following line to include response content types from the specifications.  
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V2)]
        public async Task PostAvailableConstraintData(string context, string agencyId, string resourceId, string version, [FromForm] string key, string componentId)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                key = "*";
            }

            await this._availableResource.GetDataV2Async(context, agencyId, resourceId, version, key, componentId);
        }
    }
}