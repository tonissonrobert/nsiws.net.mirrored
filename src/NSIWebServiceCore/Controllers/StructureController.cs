using System.IO;
using System.Threading.Tasks;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace Estat.Sri.Ws.NSIWebServiceCore.Controllers
{
    [Route("rest/",Order = 2)]
    [Route("rest/v{apiVersion:apiVersion}/")]
    [ApiController]
    [ApiVersion("1")]
    public class StructureController : Controller
    {
        private readonly IStructureResource _structureResource;

        private readonly IStructureServiceUriHelper _structureServiceUriHelper;

        public StructureController(IStructureResource structureResource, IStructureServiceUriHelper structureServiceUriHelper)
        {
            this._structureResource = structureResource;
            this._structureServiceUriHelper = structureServiceUriHelper;
        }

        /// <summary>
        ///     Item Scheme queries
        /// </summary>
        /// <remarks>
        ///     Item queries extend structure queries by allowing to retrieve items in item schemes such as particular codes in a codelist.
        /// </remarks>
        /// <param name="structure">
        ///     <p>The type of item scheme (e.g. codelist, agencyscheme, etc.)</p>
        /// </param>
        /// <param name="agencyId">
        ///     <p>The agency maintaining the artefact to be returned.</p>
        ///     <p>It is possible to set more than one agency, using <code>+</code> as separator (e.g. BIS+ECB).</p>
        ///     <p>The keyword <code>all</code> can be used to indicate that artefacts maintained by any maintenance agency should be returned.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The id of the artefact to be returned.</p>
        ///     <p>It is possible to set more than one id, using <code>+</code> as separator (e.g. CL_FREQ+CL_CONF_STATUS).</p>
        ///     <p>The keyword <code>all</code> can be used to indicate that any artefact of the specified resource type, {agencyID} and {version} should be returned.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version of the artefact to be returned.</p>
        ///     <p>It is possible to set more than one version, using <code>+</code> as separator (e.g. 1.0+2.1).</p>
        ///     <p>The keyword <code>all</code> can be used to return all versions of the matching resource.</p>
        ///     <p>The keyword <code>latest</code> can be used to return the latest production version of the matching resource.</p>
        /// </param>
        /// <param name="itemId">
        ///     <p>The id of the item to be returned.</p>
        ///     <p>It is possible to set more than one id, using <code>+</code> as separator (e.g. A+Q+M).</p>
        ///     <p>The keyword <code>all</code> can be used to return all items of the matching resource.</p>
        /// </param>
        [HttpGet("{structure}/{agencyId=ALL}/{resourceId=ALL}/{version=LATEST}/{itemId}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Structure queries" })]
        [ApiParameter("structure", ParameterLocation.Path, ApiParameter.ITEM_SCHEME_TYPE, Required = true)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.REFERENCES_V1)]
        [ApiParameter("detail", ParameterLocation.Query, ApiParameter.STRUCT_DETAIL_V1)]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING,
            Description = "<p>Specifies whether the response should be compressed and how.</p><p><code>identity</code> " +
                          "(the default) indicates that no compression will be performed.</p>")]
        [ApiParameter("accept-language", ParameterLocation.Header, Description = "Specifies the client's preferred language.")]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time",
            Description = "Instructs to return the content matching the query only if it has changed since the supplied timestamp.")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V1)]
        public async Task GetStructureWithFilter(string structure, string agencyId, string resourceId, string version,string itemId)
        {
            var request = this.HttpContext.Request;

            this._structureServiceUriHelper.SetBaseUri(request.Scheme, request.Host.ToString(), request.PathBase.Add("/rest/").ToString());
            this._structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.StructureUri);

            await this._structureResource.GetStructureAsync(structure, agencyId, resourceId, version, itemId);
        }

        /// <summary>
        /// Get data structures
        /// </summary>
        /// <param name="structure">
        ///     <p>The type of the structure (e.g. dataflow, datastructure, etc.)</p>
        /// </param>
        /// <param name="agencyId">
        ///     <p>The agency maintaining the artefact to be returned.</p>
        ///     <p>It is possible to set more than one agency, using <code>+</code> as separator (e.g. BIS+ECB).</p>
        ///     <p>The keyword <code>all</code> can be used to indicate that artefacts maintained by any maintenance agency should be returned.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The id of the artefact to be returned.</p>
        ///     <p>It is possible to set more than one id, using <code>+</code> as separator (e.g. CL_FREQ+CL_CONF_STATUS).</p>
        ///     <p>The keyword <code>all</code> can be used to indicate that any artefact of the specified resource type, {agencyID} and {version} should be returned.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version of the artefact to be returned.</p>
        ///     <p>It is possible to set more than one version, using <code>+</code> as separator (e.g. 1.0+2.1).</p>
        ///     <p>The keyword <code>all</code> can be used to return all versions of the matching resource.</p>
        ///     <p>The keyword <code>latest</code> can be used to return the latest production version of the matching resource.</p>
        /// </param>
        [HttpGet("{structure}/{agencyId=ALL}/{resourceId=ALL}/{version=LATEST}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Structure queries" })]
        [ApiParameter("structure", ParameterLocation.Path, ApiParameter.STRUCTURE_TYPE, Required = true)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.REFERENCES_V1)]
        [ApiParameter("detail", ParameterLocation.Query, ApiParameter.STRUCT_DETAIL_V1)]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING,
            Description = "<p>Specifies whether the response should be compressed and how.</p><p><code>identity</code> " +
                          "(the default) indicates that no compression will be performed.</p>")]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V1)]
        public async Task GetStructure(string structure, string agencyId, string resourceId, string version)
        {
            var request = this.HttpContext.Request;

            this._structureServiceUriHelper.SetBaseUri(request.Scheme, request.Host.ToString(), request.PathBase.Add("/rest/").ToString());
            this._structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.StructureUri);

            await this._structureResource.GetStructureAsync(structure, agencyId, resourceId, version, string.Empty);
        }

        /// <summary>
        /// Append the specified structure stream.
        /// </summary>
        /// <returns>The <see cref="Message" />.</returns>
        [HttpPost("structure/")]
        [SwaggerOperation(Tags = new [] { "Structure actions" })]
        [MapToApiVersion("1")]
        public void Store()
        {
            this._structureResource.Store(this.HttpContext.Request.Body);
        }

        /// <summary>
        /// Replaces the specified structure stream.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        [HttpPut("{structure}/{agencyId}/{resourceId}/{version}/")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Structure actions" })]
        public void Replace(string structure, string agencyId, string resourceId, string version)
        {
            this._structureResource.Replace(structure, agencyId, resourceId, version, this.HttpContext.Request.Body);
        }

        /// <summary>
        /// Deletes the specified structure.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        
        [HttpDelete("{structure}/{agencyId}/{resourceId}/{version}/")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Structure actions" })]
        public void Delete(string structure, string agencyId, string resourceId, string version)
        {
            this._structureResource.Delete(structure, agencyId, resourceId, version);
        }
    }
}