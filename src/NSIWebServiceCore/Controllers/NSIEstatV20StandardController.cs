﻿// -----------------------------------------------------------------------
// <copyright file="NSIEstatV20ExtendedController.cs" company="EUROSTAT">
//   Date Created : 2019-01-30
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Threading.Tasks;
using Estat.Sri.Ws.Controllers.Constants;
using Estat.Sri.Ws.Soap;
using Estat.Sri.Ws.Wsdl;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Estat.Sri.Ws.NSIWebServiceCore.Controllers
{
    [Route("NSIStdV20Service")]
    public class NSIEstatV20StandardController : Controller
    {
        private readonly IHttpContextAccessor _contextAccessor;

        private INSIStdV20Service _nsiStdV20Service;
        private readonly IStaticWsdlService _wsdlService;

        public NSIEstatV20StandardController(IHttpContextAccessor contextAccessor,  INSIStdV20Service nsiStdV20Service, IStaticWsdlService wsdlService)
        {
            this._contextAccessor = contextAccessor;
            this._nsiStdV20Service = nsiStdV20Service;
            this._wsdlService = wsdlService;
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        public async Task PostSoapMessage()
        {
            await SoapHelper .InvokeSoapService(SoapNamespaces.SdmxV20JavaStd, this._nsiStdV20Service, this._contextAccessor.HttpContext);
        }

        [HttpGet]
        [QueryStringConstraint("wsdl")]
        public Stream GetWsdl()
        {
            var serviceName = this._contextAccessor.HttpContext.Request.Path.ToString().TrimStart('/');
            return this._wsdlService.GetWsdl(serviceName);
        }
    }
}