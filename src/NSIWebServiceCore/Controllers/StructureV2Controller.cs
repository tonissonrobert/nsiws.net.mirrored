using System.Threading.Tasks;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;
// ReSharper disable RouteTemplates.ActionRoutePrefixCanBeExtractedToControllerRoute

namespace Estat.Sri.Ws.NSIWebServiceCore.Controllers
{
    /// <summary>
    /// The endpoints for the structure queries fro the SDMX REST 2.0.
    /// </summary>
    [Route("rest/v{apiVersion:apiVersion}")]
    [ApiController]
    [ApiVersion("2")]
    public class StructureV2Controller : Controller
    {
        private readonly IStructureResource _structureResource;

        private readonly IStructureServiceUriHelper _structureServiceUriHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="StructureV2Controller"/>
        /// </summary>
        /// <param name="structureResource">taken from DIC</param>
        /// <param name="structureServiceUriHelper">taken from DIC</param>
        public StructureV2Controller(IStructureResource structureResource, IStructureServiceUriHelper structureServiceUriHelper)
        {
            this._structureResource = structureResource;
            this._structureServiceUriHelper = structureServiceUriHelper;
        }

        /// <summary>
        ///     Item Scheme queries
        /// </summary>
        /// <remarks>
        ///     Item queries extend structure queries by allowing to retrieve items in item schemes such as particular codes in a codelist.
        /// </remarks>
        /// <param name="structure"></param>
        /// <param name="agencyId">
        ///     <p>The maintainer(s) of the artefacts.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available maintainers.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The artefact ID(s).</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version(s) of the artefact.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        ///     <p><code>+</code> can be used to retrieve the latest stable version.</p>
        ///     <p><code>~</code> can be used to retrieve the latest version, regardless of its status (stable, draft, etc.).</p>
        /// </param>
        /// <param name="itemId">
        ///     <p>The id of the item to be returned.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all items.</p>
        /// </param>
        [HttpGet("structure/{structure}/{agencyId=*}/{resourceId=*}/{version=~}/{itemId}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Structure queries" })]
        [ApiParameter("structure", ParameterLocation.Path, ApiParameter.ITEM_SCHEME_TYPE, Required = true)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.REFERENCES_V2)]
        [ApiParameter("detail", ParameterLocation.Query, ApiParameter.STRUCT_DETAIL_V2)]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING)]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        // Uncomment the following line to include response content types from the specifications. 
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V2)]
        public async Task GetStructureWithFilter(string structure, string agencyId, string resourceId, string version, string itemId)
        {
            var request = this.HttpContext.Request;

            this._structureServiceUriHelper.SetBaseUri(request.Scheme, request.Host.ToString(), request.PathBase.Add("/rest/").ToString());
            this._structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.StructureUri);

            await this._structureResource.GetStructuresAsync(structure, agencyId, resourceId, version, itemId);
        }

        /// <summary>
        ///     Structure queries
        /// </summary>
        /// <remarks>
        ///     Structure queries allow <strong>retrieving structural metadata</strong>.
        ///
        ///     Structure queries in SDMX allow you to retrieve structural metadata at various levels of granularity,
        ///     from all structural metadata available in the source to a single code from a particular version of a particular codelist maintained by a particular agency.
        /// </remarks>
        /// <param name="structure"></param>
        /// <param name="agencyId">
        ///     <p>The maintainer(s) of the artefacts.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available maintainers.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The artefact ID(s).</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version(s) of the artefact.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        ///     <p><code>+</code> can be used to retrieve the latest stable version.</p>
        ///     <p><code>~</code> can be used to retrieve the latest version, regardless of its status (stable, draft, etc.).</p>
        /// </param>
        [HttpGet("structure/{structure}/{agencyId=*}/{resourceId=*}/{version=~}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Structure queries" })]
        [ApiParameter("structure", ParameterLocation.Path, ApiParameter.STRUCTURE_TYPE, Required = true)]
        [ApiParameter("references", ParameterLocation.Query, ApiParameter.REFERENCES_V2)]
        [ApiParameter("detail", ParameterLocation.Query, ApiParameter.STRUCT_DETAIL_V2)]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING)]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200_STRUCT, ApiConstants.SupportedApiVersions.V2)]
        public async Task GetStructure(string structure, string agencyId, string resourceId, string version)
        {
            var request = this.HttpContext.Request;

            this._structureServiceUriHelper.SetBaseUri(request.Scheme, request.Host.ToString(), request.PathBase.Add("/rest/").ToString());
            this._structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.StructureUri);

            await this._structureResource.GetStructuresAsync(structure, agencyId, resourceId, version, string.Empty);
        }

        /// <summary>
        /// Append the specified structure stream.
        /// </summary>
        /// <returns>The <see cref="Message" />.</returns>
        [HttpPost("structure/")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Structure actions" })]
        public void Store()
        {
            this._structureResource.Store(this.HttpContext.Request.Body);
        }

        /// <summary>
        /// Replaces the specified structure stream.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        [HttpPut("structure/{structure}/{agencyId}/{resourceId}/{version}/")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Structure actions" })]
        public void Replace(string structure, string agencyId, string resourceId, string version)
        {
            this._structureResource.Replace(structure, agencyId, resourceId, version, this.HttpContext.Request.Body);
        }

        /// <summary>
        /// Deletes the specified structure.
        /// </summary>
        /// <param name="structure">The structure.</param>
        /// <param name="agencyId">The agency identifier.</param>
        /// <param name="resourceId">The resource identifier.</param>
        /// <param name="version">The version.</param>
        [HttpDelete("structure/{structure}/{agencyId}/{resourceId}/{version}/")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Structure actions" })]
        public void Delete(string structure, string agencyId, string resourceId, string version)
        {
            this._structureResource.Delete(structure, agencyId, resourceId, version);
        }
    }
}