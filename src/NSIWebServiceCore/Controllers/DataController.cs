﻿using System.IO;
using System.Text;
using System.Threading.Tasks;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;

namespace NSIWebServiceCore.Controllers
{
    //route to support backward compatibility
    [Route("rest/data/")]
    //route for new versioned controller
    [Route("rest/v{apiVersion:apiVersion}/data/")] 
    [ApiController]
    //attributes to mark the supported versions by the controller
    [ApiVersion("1")]
    public class DataController : Controller
    {   
        private readonly IDataResource _dataResource;

        public DataController(IDataResource dataResource)
        {
            this._dataResource = dataResource;
        }

        /// <summary>
        /// Get data
        /// </summary>
        /// <param name="flowRef">The <strong>statistical domain</strong> (aka dataflow) of the data to be returned.</param>
        /// <param name="key">The (possibly partial) <strong>key identifying the data to be returned</strong>.</param>
        /// <param name="providerRef">The <strong>provider of the data</strong> to be retrieved.</param>
        [HttpGet("{flowRef}/{key=ALL}/{providerRef=ALL}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Data queries" })]
        [ApiParameter("flowRef", ParameterLocation.Path, ApiParameter.FLOW)]
        [ApiParameter("key", ParameterLocation.Path, ApiParameter.KEY)]
        [ApiParameter("providerRef", ParameterLocation.Path, ApiParameter.PROVIDER)]
        [ApiParameter("startPeriod", ParameterLocation.Query, ApiParameter.START_PERIOD)]
        [ApiParameter("endPeriod", ParameterLocation.Query, ApiParameter.END_PERIOD)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time",
            Description = "<p>The last time the query was performed by the client.</p><p>The response should include the latest version of what has " +
                          "changed in the database since that point in time (i.e. additions, revisions or deletions since the last time the query was performed).</p>")]
        [ApiParameter("firstNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned starting from the oldest one.")]
        [ApiParameter("lastNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned starting from the most recent one.")]
        [ApiParameter("dimensionAtObservation", ParameterLocation.Query, ApiParameter.DIMENSION_AT_OBSERVATION)]
        [ApiParameter("detail", ParameterLocation.Query, ApiParameter.DETAIL)]
        [ApiParameter("includeHistory", ParameterLocation.Query, DataType = "boolean", DefaultValue = "false",
            Description = "<p>Retrieve <strong>previous versions of the data</strong>.</p><p>When <code>true</code>, the response will contain " +
                          "up to two datasets per dissemination, one containing new or updated values and one containing the deleted data (if any).</p>")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING, 
            Description = "<p>Specifies whether the response should be compressed and how.</p><p><code>identity</code> " +
                          "(the default) indicates that no compression will be performed.</p>")]
        [ApiParameter("accept-language", ParameterLocation.Header, Description = "Specifies the client's preferred language.")]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time",
            Description = "Instructs to return the content matching the query only if it has changed since the supplied timestamp.")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200, ApiConstants.SupportedApiVersions.V1)]
        public async Task GetGenericData(string flowRef, string key, string providerRef)
        {
            await this._dataResource.GetGenericDataAsync(flowRef, key, providerRef);
        }

        /// <summary>
        /// Get data
        /// </summary>
        /// <remarks>
        /// Same applicability as the GET method.
        /// The POST method is used to include the 'key' argument in the body of query in order to workaround URL size limitations.
        ///
        /// Use and set the Content-Type HTTP header to <code>application/x-www-form-urlencoded</code>
        /// </remarks>
        /// <param name="flowRef">The <strong>statistical domain</strong> (aka dataflow) of the data to be returned.</param>
        /// <param name="key">The (possibly partial) <strong>key identifying the data to be returned</strong>.</param>
        /// <param name="providerRef">The <strong>provider of the data</strong> to be retrieved.</param>
        [HttpPost("{flowRef}/body/{providerRef=ALL}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("1")]
        [SwaggerOperation(Tags = new [] { "Data queries" })]
        [ApiParameter("flowRef", ParameterLocation.Path, ApiParameter.FLOW)]
        [ApiParameter("key", ParameterLocation.Path, ApiParameter.KEY)]
        [ApiParameter("providerRef", ParameterLocation.Path, ApiParameter.PROVIDER)]
        [ApiParameter("startPeriod", ParameterLocation.Query, ApiParameter.START_PERIOD)]
        [ApiParameter("endPeriod", ParameterLocation.Query, ApiParameter.END_PERIOD)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time",
            Description = "<p>The last time the query was performed by the client.</p><p>The response should include the latest version of what has " +
                          "changed in the database since that point in time (i.e. additions, revisions or deletions since the last time the query was performed).</p>")]
        [ApiParameter("firstNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned starting from the oldest one.")]
        [ApiParameter("lastNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned starting from the most recent one.")]
        [ApiParameter("dimensionAtObservation", ParameterLocation.Query, ApiParameter.DIMENSION_AT_OBSERVATION)]
        [ApiParameter("detail", ParameterLocation.Query, ApiParameter.DETAIL)]
        [ApiParameter("includeHistory", ParameterLocation.Query, DataType = "boolean", DefaultValue = "false",
            Description = "<p>Retrieve <strong>previous versions of the data</strong>.</p><p>When <code>true</code>, the response will contain " +
                          "up to two datasets per dissemination, one containing new or updated values and one containing the deleted data (if any).</p>")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING, 
            Description = "<p>Specifies whether the response should be compressed and how.</p><p><code>identity</code> " +
                          "(the default) indicates that no compression will be performed.</p>")]
        [ApiParameter("accept-language", ParameterLocation.Header, Description = "Specifies the client's preferred language.")]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time",
            Description = "Instructs to return the content matching the query only if it has changed since the supplied timestamp.")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200, ApiConstants.SupportedApiVersions.V1)]
        public async Task PostGenericData(string flowRef, string providerRef, [FromForm] string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                key = "ALL";
            }

            await this._dataResource.GetGenericDataAsync(flowRef, key, providerRef);
        }
    }
}
