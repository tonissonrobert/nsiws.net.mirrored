﻿// -----------------------------------------------------------------------
// <copyright file="NSIEstatV20ExtendedController.cs" company="EUROSTAT">
//   Date Created : 2019-01-30
//   Copyright (c) 2009, 2019 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.IO;
using System.Threading.Tasks;
using DryIoc;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
using Estat.Sri.Ws.Controllers.Constants;
using Estat.Sri.Ws.Soap;
using Estat.Sri.Ws.Wsdl;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Estat.Sri.Ws.NSIWebServiceCore.Controllers
{
    [Route("SdmxService")]
    public class SdmxServiceController : Controller
    {
        private readonly IHttpContextAccessor _contextAccessor;

        private readonly INSIStdV21Service _soapService;

        private readonly IStaticWsdlService _wsdlService;

        private readonly IStructureServiceUriHelper _structureServiceUriHelper;


        public SdmxServiceController(IHttpContextAccessor contextAccessor, INSIStdV21Service soapService, IStaticWsdlService wsdlService, IStructureServiceUriHelper structureServiceUriHelper)
        {
            this._contextAccessor = contextAccessor;
            this._soapService = soapService;
            this._wsdlService = wsdlService;
            this._structureServiceUriHelper = structureServiceUriHelper;
        }

        [Microsoft.AspNetCore.Mvc.HttpPost]
        public async Task PostSoapMessage()
        {
            var request = this.HttpContext.Request;

            this._structureServiceUriHelper.SetBaseUri(request.Scheme, request.Host.ToString(), request.Path.ToString());
            this._structureServiceUriHelper.SetStructureServiceUriType(StructureServiceUriType.ServiceUri);

            await SoapHelper.InvokeSoapService(SoapNamespaces.SdmxV21, this._soapService, this._contextAccessor.HttpContext);
        }


        [HttpGet]
        [QueryStringConstraint("wsdl")]
        public Stream GetWsdl()
        {
            var serviceName = this._contextAccessor.HttpContext.Request.Path.ToString().TrimStart('/');
            return this._wsdlService.GetWsdl(serviceName);
        }
    }
}