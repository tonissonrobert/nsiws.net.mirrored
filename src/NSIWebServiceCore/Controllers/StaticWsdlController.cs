﻿using System.IO;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Wsdl;
using Microsoft.AspNetCore.Mvc;

namespace NSIWebServiceCore.Controllers
{
    [Route("wsdl")]
    public class StaticWsdlController : Controller
    {
        private readonly IStaticWsdlService _service;

        public StaticWsdlController(IStaticWsdlService service)
        {
            this._service = service;
        }

        [HttpGet("{name}")]
        public Stream GetWadl(string name)
        {
            return this._service.GetWsdl(name);
        }
    }
}