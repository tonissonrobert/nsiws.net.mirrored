using System.Threading.Tasks;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.Rest.Utils.ApiDocumentation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;
// ReSharper disable RouteTemplates.ActionRoutePrefixCanBeExtractedToControllerRoute

// ReSharper disable once CheckNamespace
namespace NSIWebServiceCore.Controllers
{
    /// <summary>
    /// The endpoints for the data queries fro the SDMX REST 2.0.
    /// </summary>
    [Route("rest/v{apiVersion:apiVersion}/data/")]
    [ApiController()]
    [ApiVersion("2")]
    public class DataV2Controller : Controller
    {   
        private readonly IDataResource _dataResource;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataV2Controller"/>.
        /// </summary>
        /// <param name="dataResource">taken from DIC</param>
        public DataV2Controller(IDataResource dataResource)
        {
            this._dataResource = dataResource;
        }

        /// <summary>
        ///     Data queries
        /// </summary>
        /// <remarks>
        ///     <p>Data queries allow <strong>retrieving statistical data</strong>.</p> 
        ///     <p>Entire datasets can be retrieved or individual observations, or anything in between, using filters on dimensions (including time), attributes and/or measures.</p> 
        ///     <p>All data matching a query can be retrieved or only the data that has changed since the last time the same query was performed.</p> 
        ///     <p>Using the <em>includeHistory</em> parameter, it is also possible to retrieve previous versions of the data.</p>
        ///     <p>Last but not least, the data retrieved can be packaged in different ways (as time series, cross-sections or as a table), in a variety of formats (JSON, XML, CSV, etc.).</p>
        /// </remarks>
        /// <param name="context"></param>
        /// <param name="agencyId">
        ///     <p>The maintainer(s) of the artefacts.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available maintainers.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The artefact ID(s).</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version(s) of the artefact.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        ///     <p><code>+</code> can be used to retrieve the latest stable version.</p>
        ///     <p><code>~</code> can be used to retrieve the latest version, regardless of its status (stable, draft, etc.).</p>
        /// </param>
        /// <param name="key">
        ///     <p>The combination of dimension values identifying series or slices of the cube (for example <code>D.USD.EUR.SP00.A</code>).</p>
        ///     <p>Multiple values are possible. Wildcards are supported using <code>*</code> (<code>D.*.EUR.SP00.A</code>).</p>
        /// </param>
        [HttpGet("{context=*}/{agencyId=*}/{resourceId=*}/{version=*}/{key=*}/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Data queries" })]
        [ApiParameter("context", ParameterLocation.Path, ApiParameter.CONTEXT, Required = true)]
        [ApiParameter("c", ParameterLocation.Query, ApiParameter.C)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, DataType = "data-time string", Description = "The last time the query was performed by the client.")]
        [ApiParameter("firstNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned for each of the matching series, starting from the first observation.")]
        [ApiParameter("lastNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned for each of the matching series, counting back from the most recent observation.")]
        [ApiParameter("dimensionAtObservation", ParameterLocation.Query, DataType = "string",
            Description = "The ID of the dimension to be attached at the observation level.")]
        [ApiParameter("attributes", ParameterLocation.Query, ApiParameter.ATTRIBUTES)]
        [ApiParameter("measures", ParameterLocation.Query, ApiParameter.MEASURES)]
        [ApiParameter("includeHistory", ParameterLocation.Query, DataType = "boolean", DefaultValue = "false",
            Description = "This attribute allows retrieving previous versions of the data, as they were disseminated in the past (<em>history</em> or <em>timeline</em> functionality).")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING)]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        [ApiParameter("x-level", ParameterLocation.Header, ApiParameter.X_LEVEL)]
        // Uncomment the following line to include response content types from the specifications.  
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200, ApiConstants.SupportedApiVersions.V2)]
        public async Task GetGenericData(string context, string agencyId, string resourceId, string version, string key)
        {
            await this._dataResource.GetGenericDataV2Async(context, agencyId, resourceId, version, key);
        }

        /// <summary>
        ///     Data queries
        /// </summary>
        /// <remarks>
        ///     Same applicability as the GET method.
        ///     The POST method is used to include the 'key' argument in the body of query in order to workaround URL size limitations.
        ///
        ///     Use and set the Content-Type HTTP header to <code>application/x-www-form-urlencoded</code>
        /// </remarks>
        /// <param name="context"></param>
        /// <param name="agencyId">
        ///     <p>The maintainer(s) of the artefacts.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available maintainers.</p>
        /// </param>
        /// <param name="resourceId">
        ///     <p>The artefact ID(s).</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        /// </param>
        /// <param name="version">
        ///     <p>The version(s) of the artefact.</p>
        ///     <p>Multiple values are possible (separated by commas) and <code>*</code> can be used as shortcut to select all available artefacts.</p>
        ///     <p><code>+</code> can be used to retrieve the latest stable version.</p>
        ///     <p><code>~</code> can be used to retrieve the latest version, regardless of its status (stable, draft, etc.).</p>
        /// </param>
        /// <param name="key">
        ///     <p>The combination of dimension values identifying series or slices of the cube (for example <code>D.USD.EUR.SP00.A</code>).</p>
        ///     <p>Multiple values are possible. Wildcards are supported using <code>*</code> (<code>D.*.EUR.SP00.A</code>).</p>
        /// </param>
        [HttpPost("{context=*}/{agencyId=*}/{resourceId=*}/{version=*}/body/")]
        [ResponseCache(CacheProfileName = "CustomCacheProfile")]
        [MapToApiVersion("2")]
        [SwaggerOperation(Tags = new [] { "Data queries" })]
        [ApiParameter("context", ParameterLocation.Path, ApiParameter.CONTEXT, Required = true)]
        [ApiParameter("c", ParameterLocation.Query, ApiParameter.C)]
        [ApiParameter("updatedAfter", ParameterLocation.Query, Format = "date-time", Description = "The last time the query was performed by the client.")]
        [ApiParameter("firstNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned for each of the matching series, starting from the first observation.")]
        [ApiParameter("lastNObservations", ParameterLocation.Query, DataType = "integer",
            Description = "The maximum number of observations to be returned for each of the matching series, counting back from the most recent observation.")]
        [ApiParameter("dimensionAtObservation", ParameterLocation.Query, DataType = "string",
            Description = "The ID of the dimension to be attached at the observation level.")]
        [ApiParameter("attributes", ParameterLocation.Query, ApiParameter.ATTRIBUTES)]
        [ApiParameter("measures", ParameterLocation.Query, ApiParameter.MEASURES)]
        [ApiParameter("includeHistory", ParameterLocation.Query, DataType = "boolean", DefaultValue = "false",
            Description = "This attribute allows retrieving previous versions of the data, as they were disseminated in the past (<em>history</em> or <em>timeline</em> functionality).")]
        [ApiParameter("accept-encoding", ParameterLocation.Header, ApiParameter.ACCEPT_ENCODING)]
        [ApiParameter("accept-language", ParameterLocation.Header)]
        [ApiParameter("if-modified-since", ParameterLocation.Header, Format = "date-time")]
        // Uncomment the following line to include response content types from the specifications.
        // [ProducesApiResponseType(ApiConstants.SuccessResponseTypes.CODE_200, ApiConstants.SupportedApiVersions.V2)]
        public async Task PostGenericData(string context, string agencyId, string resourceId, string version, [FromForm] string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                key = "*";
            }

            await this._dataResource.GetGenericDataV2Async(context, agencyId, resourceId, version, key);
        }
    }
}
