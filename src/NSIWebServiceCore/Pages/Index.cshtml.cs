﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Threading.Tasks;
using DryIoc;
using Estat.Nsi.DataDisseminationWS.WebServices;
using Estat.Sri.Ws.Dependency.Manager;
using Estat.Sri.Ws.NSIWebServiceCore;
using Estat.Sri.Ws.Wsdl;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ServiceInfo = Estat.Sri.Ws.Wsdl.ServiceInfo;

namespace NSIWebServiceCore.Pages
{
    public class IndexModel : PageModel
    {
        public void OnGet()
        {

        }

        public static IPAddress TryGetExternal(IPHostEntry host)
        {
            if (host == null)
            {
                throw new ArgumentNullException("host");
            }

            IPAddress ret = null;
            IPAddress ipv6 = null;
            for (int i = 0; i < host.AddressList.Length; i++)
            {
                IPAddress address = host.AddressList[i];
                bool isIpv4 = address.AddressFamily.Equals(AddressFamily.InterNetwork);

                if (isIpv4)
                {
                    if (ret == null)
                    {
                        ret = address;
                    }
                    else
                    {
                        if (!IPAddress.IsLoopback(address) && !IsPrivate(address))
                        {
                            return address;
                        }

                        if (IPAddress.IsLoopback(ret) && IsPrivate(address))
                        {
                            ret = address;
                        }
                    }
                }
                else
                {
                    if (ipv6 == null || (!IPAddress.IsLoopback(address) && !IsPrivate(address)))
                    {
                        ipv6 = address;
                    }
                    else if (IPAddress.IsLoopback(ipv6))
                    {
                        ipv6 = address;
                    }
                }
            }

            return ret ?? ipv6;
        }

        public static bool IsPrivate(IPAddress address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }

            if (address.IsIPv6SiteLocal || address.IsIPv6LinkLocal)
            {
                return true;
            }

            if (!address.AddressFamily.Equals(AddressFamily.InterNetwork))
            {
                return false;
            }

            bool isPrivate = false;
            byte[] addressBytes = address.GetAddressBytes();
            switch (addressBytes[0])
            {
                case 10:
                    isPrivate = true;
                    break;
                case 192:
                    isPrivate = addressBytes[1] == 168;
                    break;
                case 172:
                    isPrivate = ((addressBytes[1] & 240) ^ 16) == 0;
                    break;
                case 169: // link-local address RFC 5735, RFC 3927
                    isPrivate = addressBytes[1] == 254;
                    break;
            }

            return isPrivate;
        }

        public static string Version => string.Format(CultureInfo.InvariantCulture, Messages.app_version, Assembly.GetExecutingAssembly().GetName().Version);

        public static List<ServiceInfo> WebServices
        {
            get
            {
                var wsdlRegistry = DependencyRegisterManager.Container.Resolve<WsdlRegistry>();
                return wsdlRegistry.GetAll();
            }
        }
    }
}
