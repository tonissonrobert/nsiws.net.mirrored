﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Mapping.Api.Constant;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Model;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Estat.Sri.Ws.NSIWebServiceCore.HealthCheck
{
    public class DbHealthCheck : IHealthCheck
    {
        private IMappingStoreEngine _mappingStoreEngine;
        private DatabaseIdentificationOptions options;

        public DbHealthCheck(IMappingStoreEngine mappingStoreEngine, IConnectionStringBuilderManager connectionStringManager)
        {
            this._mappingStoreEngine = mappingStoreEngine;
            var connectionString = connectionStringManager?.Build();
            this.options = connectionString != null
                ? new DatabaseIdentificationOptions() { StoreId = connectionString.Name}
                : null;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var isHealthy = this.options!=null
                            && _mappingStoreEngine != null
                            && _mappingStoreEngine.TestConnection(options).Status == StatusType.Success;

            return isHealthy
               ? HealthCheckResult.Healthy(data: this.Details())
               : HealthCheckResult.Unhealthy();
        }

        private Dictionary<string, object> Details()
        {
            var currentVersion = _mappingStoreEngine.RetrieveVersion(options);
            var lastVersion = _mappingStoreEngine.GetAvailableVersions(options).LastOrDefault();
            var isLatest = lastVersion == null || lastVersion == currentVersion;

            return new Dictionary<string, object>()
            {
                {"storeId", this.options.StoreId},
                {"version", currentVersion.ToString(2)},
                {"isLatest", isLatest},
            };
        }
    }
}
