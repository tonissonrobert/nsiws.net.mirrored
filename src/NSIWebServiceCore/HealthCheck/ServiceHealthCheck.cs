﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Estat.Sdmxsource.Extension.Factory;
using Estat.Sri.Ws.Controllers.Manager;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;

namespace Estat.Sri.Ws.NSIWebServiceCore.HealthCheck
{
    public class ServiceHealthCheck : IHealthCheck
    {
        private readonly IRetrieverFactory _retrieverFactory;
        private readonly KestrelServerOptions _kestrelServerOptions;

        public ServiceHealthCheck(IRetrieverFactory retrieverFactory, IOptionsMonitor<KestrelServerOptions> kestrelServerOptions)
        {
            this._retrieverFactory = retrieverFactory;
            this._kestrelServerOptions = kestrelServerOptions.CurrentValue;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var isHealthy = this._retrieverFactory != null;

            return isHealthy
                ? HealthCheckResult.Healthy(data: this.WebInfo())
                : HealthCheckResult.Unhealthy();
        }

        private Dictionary<string, object> WebInfo()
        {
            return new Dictionary<string, object>()
            {
                {"version", this.GetInformationalVersion(Assembly.GetExecutingAssembly())},
                {"retriever", this._retrieverFactory.Id},
                {"retriever-version", this.GetInformationalVersion(this._retrieverFactory.GetType().Assembly)},
                {"middleware", SettingsManager.MiddlewareImplementation },
                {"maxRequestBodySize", this._kestrelServerOptions.Limits.MaxRequestBodySize}
            };
        }

        private string GetInformationalVersion(Assembly assembly)
        {
            return FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;
        }
    }
}
