﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Estat.Sri.Ws.NSIWebServiceCore.HealthCheck
{
    public class MemoryHealthCheck : IHealthCheck
    {
        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
        {
            var allocated = decimal.Divide(GC.GetTotalMemory(false), 1048576);

            var data = new Dictionary<string, object>()
            {
                { "allocatedMb", Math.Round(allocated, 4) },
                { "gen0Collections", GC.CollectionCount(0) },
                { "gen1Collections", GC.CollectionCount(1) },
                { "gen2Collections", GC.CollectionCount(2) },
            };

            return HealthCheckResult.Healthy(data: data);
        }
    }
}
