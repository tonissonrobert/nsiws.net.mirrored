﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Estat.Sri.Ws.CorsModule.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace Estat.Sri.Ws.CorsModule
{
    public class CorsHttpModule
    {
        private readonly RequestDelegate _next;
        private readonly IEnumerable<ICorsItem> _corsItems;

        public CorsHttpModule(RequestDelegate next)
        {
            this._next = next;
            var settings = (CorsConfigSection)System.Configuration.ConfigurationManager.GetSection("corsSettings");

            if (settings == null)
                return;

            this._corsItems = settings.CorsCollection.Cast<ICorsItem>();
        }

        public async Task Invoke(HttpContext context)
        {
            if(!this.IsOptionsRequest(context))
                await this._next.Invoke(context);
        }

        private bool IsOptionsRequest(HttpContext context)
        {
            var request = context.Request;
            var origin = (string)request.Headers["Origin"];
            var requestMethod = request.Headers["Access-Control-Request-Method"];
            var requestHeaders = request.Headers["Access-Control-Request-Headers"];

            if (string.IsNullOrEmpty(origin))
                return false;

            context.Response.OnStarting(() =>
            {
                var cors = this._corsItems.FirstOrDefault(x => x.Domain == "*" || origin.EndsWith(x.Domain, StringComparison.InvariantCultureIgnoreCase));

                if (cors != null)
                {
                    context.Response.Headers.Add("Access-Control-Allow-Origin", origin);

                    if (!string.IsNullOrEmpty(requestMethod))
                    {
                        context.Response.Headers.Add("Access-Control-Allow-Methods", cors.AllowedMethods == "*" ? requestMethod : (StringValues)cors.AllowedMethods);
                    }

                    if (!string.IsNullOrEmpty(requestHeaders))
                    {
                        context.Response.Headers.Add("Access-Control-Allow-Headers", cors.AllowedHeaders == "*" ? requestHeaders : (StringValues)cors.AllowedHeaders);
                    }

                    if (!string.IsNullOrEmpty(cors.ExposedHeaders))
                    {
                        context.Response.Headers.Add("Access-Control-Expose-Headers", cors.ExposedHeaders);
                    }

                    if (cors.AllowCredentials)
                    {
                        context.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
                    }
                }

                context.Response.Headers.Append("Vary", "Origin");

                return Task.CompletedTask;
            });

            // if OPTIONS request end chain here and return response.
            return HttpMethods.IsOptions(request.Method);
        }
    }
}
