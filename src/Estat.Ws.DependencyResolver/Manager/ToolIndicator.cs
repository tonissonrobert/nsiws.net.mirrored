// -----------------------------------------------------------------------
// <copyright file="ToolIndicator.cs" company="EUROSTAT">
//   Date Created : 2016-08-18
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Dependency.Manager
{
    using System.Diagnostics;
    using System.Web;

    using Org.Sdmxsource.Sdmx.Structureparser.Model;

    /// <summary>
    /// The NSI WS <see cref="IToolIndicator"/> implementation
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Justification = "It is injected.")]
    internal class ToolIndicator : IToolIndicator
    {

        /// <summary>
        /// Gets the comment.
        /// </summary>
        /// <returns>
        /// The comment.
        /// </returns>
        public string GetComment()
        {
            var location = System.Reflection.Assembly.GetEntryAssembly().Location;
            return "NSI Web Service v" + FileVersionInfo.GetVersionInfo(location).FileVersion;
        }
    }
}