﻿// -----------------------------------------------------------------------
// <copyright file="PluginManager.cs" company="EUROSTAT">
//   Date Created : 2016-01-13
//   Copyright (c) 2009, 2016 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Dependency.Manager
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security;

    using DryIoc;

    using log4net;

    /// <summary>
    /// The Plugin Manager.
    /// </summary>
    public class PluginManager
    {
        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(PluginManager));

        /// <summary>
        /// The _container
        /// </summary>
        private readonly Container _container;

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManager"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public PluginManager(Container container)
        {
            this._container = container;
        }

        /// <summary>
        /// Runs the <paramref name="action"/> for all assemblies in the DLL in the specified <paramref name="pluginDirectory"/> and currently loaded assemblies.
        /// </summary>
        /// <param name="pluginDirectory">The plug in directory.</param>
        /// <param name="action">The registration action to execute with the assemblies.</param>
        /// <exception cref="DirectoryNotFoundException">The path is invalid, such as being on an unmapped drive. </exception>
        /// <exception cref="Exception">A delegate callback throws an exception.</exception>
        /// <exception cref="FileLoadException">A file that was found could not be loaded. </exception>
        /// <exception cref="SecurityException">The caller does not have the required permission. </exception>
        /// <exception cref="FileNotFoundException">The  path parameter is an empty string ("") or does not exist. </exception>
        /// <exception cref="BadImageFormatException">Found assembly that is not a valid assembly. -or-Version 2.0 or later of the common language runtime is currently loaded the DL was compiled with a later version.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="pluginDirectory"/> is <see langword="null" />.</exception>
        public void RegisterAll(DirectoryInfo pluginDirectory, Action<PluginManager, IEnumerable<Assembly>> action)
        {
            if (pluginDirectory == null)
            {
                throw new ArgumentNullException("pluginDirectory");
            }

            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            var pluginAssemblies = from file in pluginDirectory.GetFiles() where file.Extension.ToLowerInvariant() == ".dll" select Assembly.LoadFile(file.FullName);
            action(this, pluginAssemblies);
        }

        /// <summary>
        /// Registers all.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the plug in. The interface.
        /// </typeparam>
        /// <param name="pluginAssemblies">
        /// The plug in assemblies.
        /// </param>
        public void RegisterAll<T>(IList<Assembly> pluginAssemblies)
        {
            Type typeOfT = typeof(T);
            var pluginTypes = from assembly in pluginAssemblies from type in assembly.GetExportedTypes() where typeOfT.IsAssignableFrom(type) && !type.IsAbstract && !type.IsGenericTypeDefinition select type;
            foreach (var pluginType in pluginTypes)
            {
                this._container.Register(typeOfT, pluginType, Reuse.Singleton, made: FactoryMethod.ConstructorWithResolvableArguments);
            }
        }

        /// <summary>
        /// Registers all 
        /// </summary>
        /// <typeparam name="T">The type of the plug in. The interface.</typeparam>
        /// <param name="pluginAssemblies">The plug in assemblies.</param>
        /// <param name="name">The name.</param>
        public void RegisterAll<T>(IList<Assembly> pluginAssemblies, string name)
        {
            Type typeOfT = typeof(T);
            var pluginTypes = from assembly in pluginAssemblies from type in assembly.GetExportedTypes() where typeOfT.IsAssignableFrom(type) && !type.IsAbstract && !type.IsGenericTypeDefinition && type.Name.Equals(name) select type;
            foreach (var pluginType in pluginTypes)
            {
                this._container.Register(typeOfT, pluginType, Reuse.Singleton, made: FactoryMethod.ConstructorWithResolvableArguments);
            }
        }

        /// <summary>
        /// Registers all.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the plug in. The interface.
        /// </typeparam>
        /// <param name="pluginAssemblies">
        /// The plug in assemblies.
        /// </param>
        public void RegisterSingle<T>(IList<Assembly> pluginAssemblies)
        {
            Type typeOfT = typeof(T);
            var pluginTypes = (from assembly in pluginAssemblies from type in assembly.GetExportedTypes() where typeOfT.IsAssignableFrom(type) && !type.IsAbstract && !type.IsGenericTypeDefinition select type).ToArray();
            if (pluginTypes.Length == 0)
            {
                return;
            }

            this._container.Register(typeOfT, pluginTypes[0], Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            if (_log.IsWarnEnabled)
            {
                foreach (var pluginType in pluginTypes.Skip(1))
                {
                    _log.WarnFormat(CultureInfo.InvariantCulture, "Already registered an implementation for {1}. Unable to register '{0}' for interface '{1}'", pluginType, typeOfT.Name);
                }
            }
        }

        /// <summary>
        /// Registers a single instance with the given <paramref name="name" />
        /// </summary>
        /// <typeparam name="T">The type of the plug in. The interface.</typeparam>
        /// <param name="pluginAssemblies">The plug in assemblies.</param>
        /// <param name="name">The name.</param>
        public void RegisterSingle<T>(IList<Assembly> pluginAssemblies, string name)
        {
            Type typeOfT = typeof(T);
            var pluginTypes = (from assembly in pluginAssemblies from type in assembly.GetExportedTypes() where typeOfT.IsAssignableFrom(type) && !type.IsAbstract && !type.IsGenericTypeDefinition && type.Name.Equals(name) select type).ToArray();
            if (pluginTypes.Length == 0)
            {
                return;
            }

            this._container.Register(typeOfT, pluginTypes[0], Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            if (_log.IsWarnEnabled)
            {
                foreach (var pluginType in pluginTypes.Skip(1))
                {
                    _log.WarnFormat(CultureInfo.InvariantCulture, "Already registered an implementation for {1}. Unable to register '{0}' for interface '{1}'", pluginType, typeOfT.Name);
                }
            }
        }

        /// <summary>
        /// Registers all.
        /// </summary>
        /// <param name="pluginAssemblies">The plug in assemblies.</param>
        /// <param name="pluginTypes">The plugin types.</param>
        /// <exception cref="TargetInvocationException">A static initializer is invoked and throws an exception.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="pluginTypes"/> is <see langword="null" />.</exception>
        public void RegisterAllInterfaces(IEnumerable<Assembly> pluginAssemblies, ISet<Type> pluginTypes)
        {
            if (pluginTypes == null)
            {
                throw new ArgumentNullException("pluginTypes");
            }

            var implementingClassTypes = from assembly in pluginAssemblies from type in assembly.GetExportedTypes() where type.IsPublic && !type.IsAbstract && !type.IsStatic() && type.GetInterfaces().Length > 0 select type;
            foreach (var implementationType in implementingClassTypes)
            {
                foreach (var serviceType in implementationType.GetInterfaces().Where(type => type != typeof(IDisposable)))
                {
                    if (pluginTypes.Contains(serviceType))
                    {
                        this._container.Register(serviceType, implementationType, Reuse.Singleton);
                    }
                    else
                    {
                        this._container.Register(serviceType, implementationType, Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
                    }
                }
            }
        }

        /// <summary>
        /// Registers the singleton.
        /// </summary>
        /// <typeparam name="TInterface">The type of the interface.</typeparam>
        /// <typeparam name="TImplementation">The type of the implementation.</typeparam>
        public void RegisterSingleton<TInterface, TImplementation>() where TImplementation : TInterface
        {
            this._container.Register<TInterface, TImplementation>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
        }

        /// <summary>
        /// Registers the singleton.
        /// </summary>
        /// <typeparam name="TImplementation">The type of the implementation.</typeparam>
        public void RegisterSingleton<TImplementation>()
        {
            this._container.Register<TImplementation>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
        }
    }
}