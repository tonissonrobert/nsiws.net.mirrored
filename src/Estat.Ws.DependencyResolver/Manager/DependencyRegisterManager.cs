// -----------------------------------------------------------------------
// <copyright file="DependencyRegisterManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-18
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Xml;
using Estat.Nsi.AuthModule;
using Estat.Nsi.AuthModule.Factory;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.Sdmx.MappingStore.Retrieve.Helper;
using Estat.Sri.Ws.Configuration;
using Estat.Sri.Ws.LoginService;
using Estat.Sri.Ws.LoginService.Manager;
using Estat.Sri.Ws.Rest;
using Estat.Sri.Ws.SubmitStructure;
using Estat.Sri.Ws.Wsdl;


namespace Estat.Sri.Ws.Dependency.Manager
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Security.Cryptography;

    using DryIoc;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Constant;
    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sdmxsource.Extension.Model;
    using Estat.Sri.Mapping.Api.Factory;
    using Estat.Sri.Mapping.Api.Manager;
    using Estat.Sri.Mapping.Api.Model;
    using Estat.Sri.Ws.Controllers.Builder;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Factory;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.Dependency.Factory;
   // using Estat.Sri.Ws.LoginService;
   // using Estat.Sri.Ws.LoginService.Manager;
 //   using Estat.Sri.Ws.Rest;
    using Estat.Sri.Ws.Soap;
  //  using Estat.Sri.Ws.Wsdl;


    using log4net;

    using Org.Sdmxsource.Sdmx.Api.Builder;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Factory;
    using Org.Sdmxsource.Sdmx.Api.Manager.Output;
    using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
    using Org.Sdmxsource.Sdmx.Api.Manager.Persist;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager;
    using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
    using Org.Sdmxsource.Sdmx.Structureparser.Model;
    using Org.Sdmxsource.Util.Io;
  //  using Estat.Sri.Ws.SubmitStructure;

    /// <summary>
    /// This class registers the PLUGIN DLL and the default SDMX RI implementations of various interfaces.
    /// </summary>
    public static class DependencyRegisterManager
    {
        private static IContainer container;

        /// <summary>
        /// The log
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(DependencyRegisterManager));

        /// <summary>
        /// The _PLUGIN directory
        /// </summary>
        private static string _pluginDirectory;

        public static Container Container => (Container) container;

        public static void RegisterPlugins(string binPath)
        {
            if (binPath == null)
            {
                throw new ArgumentNullException("binPath");
            }

            var configuredPluginPath = SettingsManager.PluginFolder ?? "Plugins";

            _pluginDirectory = Path.IsPathRooted(configuredPluginPath) ? configuredPluginPath : Path.Combine(binPath, configuredPluginPath);

            try
            {
                SetPluginImplementations();
            }
            catch (ReflectionTypeLoadException e)
            {
                _log.Error(e);
                if (e.LoaderExceptions != null)
                {
                    foreach (var loaderException in e.LoaderExceptions)
                    {
                        _log.Error(loaderException);
                    }
                }
            }

            }

        /// <summary>
        /// Sets the default implementations.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public static void SetDefaultImplementations(IContainer container)
        {
            DependencyRegisterManager.container = container;

            // TODO auto-register at least from Controllers and NSI WS
            container.Register<IRetrieverManager, RetrieverManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IStructureWriterManager, StructureWriterManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);
            container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);
            MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);
            
            container.Register<IRestStructureSubmitController, RestStructureSubmitController>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);

            container.Register<IDataflowAuthorizationManager, DataflowAuthorizationManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IConnectionStringBuilderManager, ConnectionStringBuilderManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IStoreIdBuilderManager, StoreIdBuilderManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            container.Register<IFormatResolverManager, FormatResolverManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IBuilder<ISortedAcceptHeaders, WebHeaderCollection>, SortedAcceptHeadersBuilder>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IBuilder<IMaintainableObject, IStructureReference>, StubSdmxObjectBuilder>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            container.Register<IDataWriterManager, DataWriterManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<ICrossDataWriterManager, CrossDataWriterManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IDataRequestController, DataRequestController>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IDataRequestController, AuthorizationDataRequestDecorator>(Reuse.Singleton, setup: Setup.Decorator);
            container.Register<IStructureRequestController, StructureRequestController>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);


            ////TODO check this hack and ask why the SDMXSchema is a parameter
            //container.Register(typeof(SdmxSchema), made: Made.Of(() => SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne)));
            //container.Register(typeof(XmlWriter), made: Made.Of(() => XmlWriter.Create("hack.xml")));
            //container.Register(typeof(ConnectionStringSettings));
            //container.Register<IStructureWriterFactory, SriStructureWriterFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            // SDMX v2.1
            container.Register<INSIStdV21Service, NSIStdV21Service>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<INSIStdV21Service, NSIStdV21ServiceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // SDMX v2.1 Registry (DRAFT)
            container.Register<INSIRegistryV21Service, NSIRegistryV21Service>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<INSIRegistryV21Service, NSIRegistryV21ServiceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // SDMX v2.0 ESTAT Extensions
            container.Register<INSIEstatV20Service, NsiEstatV20Service>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<INSIEstatV20Service, NSIEstatV20ServiceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // SDMX v2.0 original
            container.Register<INSIStdV20Service, NsiStdV20Service>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<INSIStdV20Service, NSIStdV20ServiceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // WADL & WSDL services
            container.Register<IWadlProvider, WadlProvider>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IStaticWsdlService, StaticWsdlService>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            // REST Data
            container.Register<IDataResource, DataResource>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            if (SettingsManager.OnTheFlyAnnotation != OnTheFlyAnnotationType.None)
            {
                container.Register<IDataResource, DataResourceOnTheFlyAnnotations>(Reuse.Singleton, setup: Setup.Decorator);
            }

            container.Register<IDataResource, DataResourceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // REST Structure
            container.Register<IStructureResource, StructureResource>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IStructureResource, StructureResourceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // REST available Data
            container.Register<IAvailableResource, AvailableResource>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IAvailableResource, AvailableResourceErrorHandler>(Reuse.Singleton, setup: Setup.Decorator);

            // Login service
            container.Register<ILoginResource, LoginResource>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<TokenManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<RNGCryptoServiceProvider>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: Made.Of(() => new RNGCryptoServiceProvider()));

            // Submit manager
            container.Register<IStructureSubmitter, StructureSubmitter>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IStructureSubmitter, AuthorizationStructureSubmitterDecorator>(Reuse.Singleton, setup: Setup.Decorator);
            container.Register<IStructureSubmitter, StructureSubmitterObserverDecorator>(Reuse.Singleton, setup: Setup.Decorator);

            container.Register<IResponseWriterManager, ResponseWriterManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IResponseWriterFactory, ErrorMessage21WriterFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
            container.Register<IResponseWriterFactory, SubmitStructureResponseWriterFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
            container.Register<SubmitStructureController>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            // Other things
            container.Register<IMessageFaultSoapBuilderFactory, MessageFaultSoapBuilderFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IDataQueryVisitorFactory, DataQueryVisitorFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IDataflowPrincipalManager, DataflowPrincipalManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IToolIndicator, ToolIndicator>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IReadableDataLocationFactory, ReadableDataLocationFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<WsdlRegistry>(Reuse.Singleton);
            container.RegisterMany(new[] { typeof(IConfigurationStoreManager).Assembly }, type => !typeof(IEntity).IsAssignableFrom(type), Reuse.Singleton);
            container.Register<IConfigurationStoreFactory, SriWebConfigFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
            container.Register<IRangeHeaderHandler, RangeHeaderHandler>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            //container.Unregister<IEntityAuthorizationManager>();

            container.Register<IStructureServiceUriHelper, AsyncLocalStructureServiceUriHelper>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            MappingStoreIoc.Container.Register<IStructureServiceUriHelper, AsyncLocalStructureServiceUriHelper>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Replace);
            
            MappingStoreIoc.Container.RegisterMany(
                new[] { typeof(IEntityRetrieverManager).Assembly },
                type => !typeof(IEntity).IsAssignableFrom(type),
                reuse: Reuse.Singleton,
                made: FactoryMethod.ConstructorWithResolvableArguments,
                setup: Setup.With(allowDisposableTransient: true),
                ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);

            container.RegisterMany(new[] { typeof(NsiAuthModule).Assembly, typeof(IConfigurationStoreManager).Assembly }, type =>
            !typeof(IEntity).IsAssignableFrom(type) && type != typeof(SingleRequestScope), reuse: Reuse.Singleton);
            container.Register<IConfigurationStoreFactory, AuthWebConfigFactory>();
            container.Register<ISdmxAuthorizationScopeFactory, SdmxAuthorizationScopeFactory>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep);
            container.Register<IStoreIdBuilder, DefaultsStoreIdBuilder>(reuse: Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.AppendNotKeyed);
            container.Register<SettingsFromConfigurationManager>();
        }
        
        /// <summary>
        /// Filters the types.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>True if this type should be registered </returns>
        private static bool FilterTypes(Type type)
        {
            if (typeof(IBuilder<DbConnection, DdbConnectionEntity>).IsAssignableFrom(type))
            {
                return false;
            }
            if (!typeof(IHeaderRetrievalManager).IsAssignableFrom(type))
            {
                return true;
            }

            if (typeof(IHeaderRetrievalManager) == type)
            {
                return true;
            }

            return type.Name.Equals(SettingsManager.DefaultHeaderRetriever);
        }

        /// <summary>
        /// Sets the plugin implementations.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public static void SetPluginImplementations()
        {
            var directoryInfo = new DirectoryInfo(_pluginDirectory);
            if (!directoryInfo.Exists)
            {
                throw new SdmxInternalServerException("Plugin folder doesn't exist:" + directoryInfo.FullName);
            }

            var fileInfos = directoryInfo.GetFiles().Where(file => string.Equals(file.Extension, ".dll", StringComparison.OrdinalIgnoreCase));
            var pluginAssemblies = fileInfos.Select(file => Assembly.LoadFrom(file.FullName));
            //Assembly.LoadFrom("Estat.Sri.Mapping.MappingStore.dll");
            container.RegisterMany(pluginAssemblies, FilterTypes, Reuse.Singleton);

            MappingStoreIoc.Container.RegisterMany(pluginAssemblies,
                type => !typeof(IEntity).IsAssignableFrom(type),
                reuse: Reuse.Singleton,
                made: FactoryMethod.ConstructorWithResolvableArguments,
                setup: Setup.With(allowDisposableTransient: true),
                ifAlreadyRegistered: IfAlreadyRegistered.AppendNewImplementation);
            
            var ddbConnectionBuilderDll = pluginAssemblies.Where(x => x.FullName.Contains(DbConnectionBuilderDll));
            container.RegisterMany(ddbConnectionBuilderDll,type => typeof(IBuilder<DbConnection, DdbConnectionEntity>).IsAssignableFrom(type),Reuse.Singleton);
            
        }

        private static string DbConnectionBuilderDll => ConfigurationManager.AppSettings["dbConnectionBuilderDll"];
    }
}