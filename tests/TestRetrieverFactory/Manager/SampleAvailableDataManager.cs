// -----------------------------------------------------------------------
// <copyright file="SampleAvailableDataManager.cs" company="EUROSTAT">
//   Date Created : 2022-05-10
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Test.Manager
{
    using System;
    using Estat.Sdmxsource.Extension.Manager;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    internal class SampleAvailableDataManager : IAvailableDataManager
    {
        private readonly ISdmxObjects _sdmxObjects;

        public SampleAvailableDataManager(ISdmxObjects sdmxObjects)
        {
            _sdmxObjects = sdmxObjects;
        }

        public long GetCount(IAvailableConstraintQuery query, bool includeHistory)
        {
            throw new NotImplementedException();
        }

        public IMutableObjects Retrieve(IAvailableConstraintQuery query, IMutableObjects objects = null, bool multipleReferences = false)
        {
            objects ??= new MutableObjectsImpl();
            if (_sdmxObjects.Dataflows.Contains(query.Dataflow))
            {
                objects.AddDataflow(query.Dataflow.MutableInstance);
            }
            if (_sdmxObjects.DataStructures.Contains(query.DataStructure))
            {
                objects.AddDataStructure(query.DataStructure.MutableInstance);
            }
            return objects;
        }
    }
}
