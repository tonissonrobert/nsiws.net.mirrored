﻿// -----------------------------------------------------------------------
// <copyright file="SampleStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Manager
{
    using System;
    using System.Collections.Generic;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// Sample implementation of <see cref="IMutableStructureSearchManager"/>.
    /// </summary>
    /// <remarks>It uses a <see cref="ISdmxObjectRetrievalManager"/> for demonstration proposes, it is not recommended to use such solution in production
    /// for performance reasons.</remarks>
    public class SampleStructureSearchManager : IMutableStructureSearchManager
    {
        /// <summary>
        /// The _SDMX object retrieval
        /// </summary>
        private readonly ISdmxObjectRetrievalManager _sdmxObjectRetrieval;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleStructureSearchManager"/> class.
        /// </summary>
        /// <param name="sdmxObjectRetrieval">The SDMX object retrieval.</param>
        public SampleStructureSearchManager(ISdmxObjectRetrievalManager sdmxObjectRetrieval)
        {
            this._sdmxObjectRetrieval = sdmxObjectRetrieval;
        }

        /// <summary>
        /// Gets the latest.
        /// </summary>
        /// <param name="maintainableObject">The maintainable object.</param>
        /// <returns>The <see cref="IMaintainableMutableObject"/>.</returns>
        public IMaintainableMutableObject GetLatest(IMaintainableMutableObject maintainableObject)
        {
            var immutable = this._sdmxObjectRetrieval.GetMaintainableObject(maintainableObject.ImmutableInstance.AsReference, false, true);
            if (immutable != null)
            {
                return immutable.MutableInstance;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the maintainables.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <returns>The <see cref="IMutableObjects"/>.</returns>
        public IMutableObjects GetMaintainables(IRestStructureQuery structureQuery)
        {
            var sdmxObjects = this._sdmxObjectRetrieval.GetMaintainables(structureQuery);
            if (sdmxObjects != null)
            {
                return sdmxObjects.MutableObjects;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the structures.
        /// </summary>
        /// <param name="queries">The queries.</param>
        /// <param name="resolveReferences">if set to <c>true</c> [resolve references].</param>
        /// <param name="returnStub">if set to <c>true</c> [return stub].</param>
        /// <returns>The <see cref="IMutableObjects"/>.</returns>
        public IMutableObjects RetrieveStructures(IList<IStructureReference> queries, bool resolveReferences, bool returnStub)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            foreach (var structureReference in queries)
            {
                var objects = this._sdmxObjectRetrieval.GetSdmxObjects(structureReference, resolveReferences ? ResolveCrossReferences.ResolveExcludeAgencies : ResolveCrossReferences.DoNotResolve);
                if (returnStub)
                {
                    foreach (var maintainableObject in objects.GetAllMaintainables())
                    {
                        sdmxObjects.AddIdentifiable(maintainableObject.GetStub(new Uri("http://change.me"), false));
                    }
                }
                else
                {
                    sdmxObjects.Merge(objects);
                }
            }

            return sdmxObjects.MutableObjects;
        }
    }
}