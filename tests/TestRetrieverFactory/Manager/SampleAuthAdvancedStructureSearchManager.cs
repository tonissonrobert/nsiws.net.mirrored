﻿// -----------------------------------------------------------------------
// <copyright file="SampleAuthAdvancedStructureSearchManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Manager
{
    using System.Collections.Generic;
    using System.Linq;

    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Ws.Test.Extension;

    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference.Complex;
    using Org.Sdmxsource.Sdmx.Util.Objects.Container;

    /// <summary>
    /// A sample implementation of the <see cref="IAuthAdvancedMutableStructureSearchManager"/> interface.
    /// </summary>
    /// <remarks>Only the subset compatible with REST is supported.</remarks>
    public class SampleAuthAdvancedStructureSearchManager : IAuthAdvancedMutableStructureSearchManager
    {
        /// <summary>
        /// The _mutable structure search manager
        /// </summary>
        private readonly IAdvancedMutableStructureSearchManager _mutableStructureSearchManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="SampleAuthAdvancedStructureSearchManager"/> class.
        /// </summary>
        /// <param name="mutableStructureSearchManager">The mutable structure search manager.</param>
        public SampleAuthAdvancedStructureSearchManager(IAdvancedMutableStructureSearchManager mutableStructureSearchManager)
        {
            this._mutableStructureSearchManager = mutableStructureSearchManager;
        }

        /// <summary>
        /// Gets the maintainables.
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        /// <param name="allowedDataflows">The allowed dataflows.</param>
        /// <returns>The <see cref="IMutableObjects"/>.</returns>
        public IMutableObjects GetMaintainables(IComplexStructureQuery structureQuery, IList<IMaintainableRefObject> allowedDataflows)
        {
            var mutableObjects = this._mutableStructureSearchManager.GetMaintainables(structureQuery);
            if (allowedDataflows != null)
            {
                return new MutableObjectsImpl(mutableObjects.AllMaintainables.Where(o => allowedDataflows.Any(refObject => refObject.IsMatch(o))));
            }

            return mutableObjects;
        }
    }
}