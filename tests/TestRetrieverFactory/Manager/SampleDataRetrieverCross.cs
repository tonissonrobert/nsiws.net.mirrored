﻿// -----------------------------------------------------------------------
// <copyright file="SampleDataRetrieverCross.cs" company="EUROSTAT">
//   Date Created : 2015-12-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Threading.Tasks;

namespace Estat.Sri.Ws.Test.Manager
{
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
    using Org.Sdmxsource.Sdmx.DataParser.Engine;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    /// <summary>
    /// A sample implementation of <see cref="ISdmxDataRetrievalWithCrossWriter"/>. It will return data for any dataflow with a SDMX v2.0 XS DSD. 
    /// </summary>
    public class SampleDataRetrieverCross : ISdmxDataRetrievalWithCrossWriter
    {
        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Cross Sectional data requested for non-cross sectional dataflow  + dataQuery.Dataflow.Id</exception>
        public void GetData(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            var crossDsd = dataQuery.DataStructure as ICrossSectionalDataStructureObject;
            if (crossDsd == null)
            {
                throw new SdmxSemmanticException("Cross Sectional data requested for non-cross sectional dataflow " + dataQuery.Dataflow.Id);
            }
            dataWriter.StartDataset(null, crossDsd, null);
            dataWriter.WriteAttributeValue("TAB_NUM", "AL");
            dataWriter.WriteAttributeValue("REV_NUM", "1");
            dataWriter.StartXSGroup();
            dataWriter.WriteXSGroupKeyValue("TIME_PERIOD", "2001"); // must use the component id which is fixed for TimeDimension to TIME_PERIOD
            dataWriter.WriteXSGroupKeyValue("FREQ", "A");
            dataWriter.WriteXSGroupKeyValue("COUNTRY", "A");
            dataWriter.WriteAttributeValue("TIME_FORMAT", "P1Y");
            dataWriter.StartSection();
            dataWriter.WriteAttributeValue("UNIT_MULT", "9");
            dataWriter.WriteAttributeValue("DECI", "0");
            dataWriter.WriteAttributeValue("UNIT", "AUD");

            // write observations
            dataWriter.StartXSObservation("ADJT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("DEATHST", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("DEATHUN1", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("DIV", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("EMIGT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("IMMIT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("LBIRTHOUT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("LBIRTHST", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("LEXPNSIT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("MAR", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("NETMT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("PJAN1T", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("PJANT", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
            dataWriter.StartXSObservation("TFRNSI", "1");
            dataWriter.WriteXSObservationKeyValue("SEX", "T");
            dataWriter.WriteAttributeValue("OBS_STATUS", "P");
        }

        /// <summary>
        /// Gets the data async.
        /// </summary>
        /// <param name="dataQuery">The data query.</param>
        /// <param name="dataWriter">The data writer.</param>
        /// <exception cref="Org.Sdmxsource.Sdmx.Api.Exception.SdmxSemmanticException">Cross Sectional data requested for non-cross sectional dataflow  + dataQuery.Dataflow.Id</exception>
        public async Task GetDataAsync(IDataQuery dataQuery, ICrossSectionalWriterEngine dataWriter)
        {
            this.GetData(dataQuery,dataWriter);
        }
    }
}