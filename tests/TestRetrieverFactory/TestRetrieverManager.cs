// -----------------------------------------------------------------------
// <copyright file="TestRetrieverManager.cs" company="EUROSTAT">
//   Date Created : 2015-12-28
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace Estat.Sri.Ws.Test
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Text;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;
    using Estat.Sri.Ws.Configuration;
    using Estat.Sri.Ws.Controllers.Controller;
    using Estat.Sri.Ws.Controllers.Factory;
    using Estat.Sri.Ws.Controllers.Manager;
    using Estat.Sri.Ws.Controllers.Model;
    using Estat.Sri.Ws.Test.Constant;
    using Estat.Sri.Ws.Test.Factory;
    using Microsoft.Extensions.Configuration;
    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Exception;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects;
    using Org.Sdmxsource.Sdmx.Api.Model.Query;
    using Org.Sdmxsource.Sdmx.Api.Util;
    using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
    using Org.Sdmxsource.Util.Io;

    /// <summary>
    /// Test unit for <see cref="RetrieverManager"/>
    /// </summary>
    [TestFixture]
    public class TestRetrieverManager
    {
        private DataflowAuthorizationManager _dataflowAuthorizationManager;

        private readonly RetrieverManager _retrieverManager;

        public TestRetrieverManager()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("Properties.json");
            IConfiguration config = builder.Build();

            this._dataflowAuthorizationManager = new DataflowAuthorizationManager(new IDataflowAuthorizationFactory[] { new SampleDataflowAuthorizationFactory(), });
            this._retrieverManager = new RetrieverManager(
                new IRetrieverFactory[] { new RetrieverFactoryAlwaysNoResults(), new TestWsRetrieverFactory() },
                new DataflowPrincipalManager(null),
                this._dataflowAuthorizationManager,
                new RangeHeaderHandler(null),
                new SdmxAuthorizationScopeFactory(new DataflowPrincipalManager(null), null),
                new SettingsFromConfigurationManager(config));
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.GetDataResponseSimple(Org.Sdmxsource.Sdmx.Api.Util.IReadableDataLocation)" />
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        [TestCase(Dataflows.TimeSeries)]
        //[TestCase("DOES_NOT_EXIST", ExpectedException = typeof(SdmxNoResultsException))]
        public void TestGetDataResponseSimple(string dataflow)
        {
            var retrieverManager = new RetrieverManager(new IRetrieverFactory[] { new RetrieverFactoryAlwaysNoResults(), new TestWsRetrieverFactory()}, new DataflowPrincipalManager(null), this._dataflowAuthorizationManager, null, null,null);

            var dataResponseSimple = retrieverManager.GetDataResponseSimple(CreateDataQueryV2(dataflow));
            Assert.NotNull(dataResponseSimple);
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.GetDataResponseSimple(Org.Sdmxsource.Sdmx.Api.Util.IReadableDataLocation)" />
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        [TestCase(Dataflows.CrossV20Only)]
       // [TestCase("DOES_NOT_EXIST", ExpectedException = typeof(SdmxNoResultsException))]
        public void TestGetDataResponseCross(string dataflow)
        {
            var dataResponseSimple = this._retrieverManager.GetDataResponseCross(CreateDataQueryV2(dataflow));
            Assert.NotNull(dataResponseSimple);
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.GetDataResponseSimple(Sdmxsource.Extension.Model.DataRequest)" />
        /// </summary>
        /// <param name="dataQuery">The rest query.</param>
        [TestCaseSource(nameof(DataQueries))]
        public void TestGetDataResponseSimpleRest(IRestDataQuery dataQuery)
        {
            var dataRequest = this._retrieverManager.ParseDataRequest(dataQuery);
            var dataResponseSimple = this._retrieverManager.GetDataResponseSimple(dataRequest);
            Assert.NotNull(dataResponseSimple);
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.ParseRequest(SdmxSchema, Org.Sdmxsource.Sdmx.Api.Model.Query.IRestAvailableConstraintQuery)/>
        /// </summary>
        /// <param name="availableQuery">The rest query.</param>
        [TestCaseSource(nameof(AvailableQueries))]
        public void TestGetAvailableConstraintResponseSimpleRest(IRestAvailableConstraintQuery availableQuery)
        {
            var objects = this._retrieverManager.ParseRequest(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne), availableQuery);
            Assert.NotNull(objects);
            Assert.IsNotEmpty(objects.GetAllMaintainables());
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.ParseRequest(SdmxSchema, IRestStructureQuery)" />
        /// </summary>
        /// <param name="structureQuery">The structure query.</param>
        [TestCaseSource(nameof(StructureQueries))]
        public void TestGetStructureResponseSimpleRest(IRestStructureQuery structureQuery)
        {
            var objects = this._retrieverManager.ParseRequest(SdmxSchema.GetFromEnum(SdmxSchemaEnumType.VersionTwoPointOne),
                structureQuery);
            Assert.NotNull(objects);
            Assert.IsNotEmpty(objects.GetAllMaintainables());
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.GetDataResponseSimple(Org.Sdmxsource.Sdmx.Api.Util.IReadableDataLocation)" />
        /// </summary>
        /// <param name="queryFile">The query file.</param>
        [TestCase("StructureQueryV20.xml")]
        //[TestCase("StructureQueryV20_no_result.xml", ExpectedException = typeof(SdmxNoResultsException))]
        public void TestGetStructureV20(string queryFile)
        {
            ISdmxObjects objects;
            using (var readableDataLocation = new FileReadableDataLocation(queryFile))
            {
                objects = this._retrieverManager.ParseRequest(SdmxSchemaEnumType.VersionTwo, readableDataLocation);
            }
            Assert.NotNull(objects);
            Assert.IsNotEmpty(objects.GetAllMaintainables());
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.GetDataResponseSimple(Org.Sdmxsource.Sdmx.Api.Util.IReadableDataLocation)" />
        /// </summary>
        /// <param name="queryFile">The query file.</param>
        [TestCase("StructureQueryV21.xml")]
        // [TestCase("StructureQueryV21_no_result.xml", ExpectedException = typeof(SdmxNoResultsException))]
        public void TestGetStructureV21(string queryFile)
        {
            ISdmxObjects objects;
            using (var readableDataLocation = new FileReadableDataLocation(queryFile))
            {
                objects = this._retrieverManager.ParseRequest(SdmxSchemaEnumType.VersionTwoPointOne, readableDataLocation);
            }
            Assert.NotNull(objects);
            Assert.IsNotEmpty(objects.GetAllMaintainables());
        }

        /// <summary>
        /// Test unit for <see cref="RetrieverManager.GetDataResponseSimple(Org.Sdmxsource.Sdmx.Api.Util.IReadableDataLocation)" />
        /// </summary>
        /// <param name="dataflow">The dataflow.</param>
        [TestCase(Dataflows.TimeSeries)]
        //[TestCase("DOES_NOT_EXIST", ExpectedException = typeof(SdmxNoResultsException))]
        public void TestGetDataResponseV21(string dataflow)
        {

            var dataResponseSimple = this._retrieverManager.GetDataResponseComplex(CreateDataQueryV21(dataflow));
            Assert.NotNull(dataResponseSimple);
        }

        /// <summary>
        /// Creates the data query for SDMX v2.0
        /// </summary>
        /// <param name="dataflowId">The dataflow identifier.</param>
        /// <returns>The <see cref="IReadableDataLocation"/>.</returns>
        private static IReadableDataLocation CreateDataQueryV2(string dataflowId)
        {
            var dataQueryTemplate = File.ReadAllText("DataQueryV2_template.xml");
            var dataQueryText = string.Format(CultureInfo.InvariantCulture, dataQueryTemplate, dataflowId);
            return new MemoryReadableLocation(Encoding.UTF8.GetBytes(dataQueryText));
        }

        /// <summary>
        /// Creates the data query for SDMX v2.1
        /// </summary>
        /// <param name="dataflowId">The dataflow identifier.</param>
        /// <returns>The <see cref="IReadableDataLocation"/>.</returns>
        private static IReadableDataLocation CreateDataQueryV21(string dataflowId)
        {
            var dataQueryTemplate = File.ReadAllText("DataQueryV21_template.xml");
            var dataQueryText = string.Format(CultureInfo.InvariantCulture, dataQueryTemplate, dataflowId);
            return new MemoryReadableLocation(Encoding.UTF8.GetBytes(dataQueryText));
        }

        private static IEnumerable<IRestDataQuery> DataQueries()
        {
            yield return new RESTDataQueryCore(string.Format("data/{0}", Dataflows.TimeSeries));
            yield return new RESTDataQueryCoreV2(string.Format("data/dataflow/*/{0}/*", Dataflows.TimeSeries).Split("/"), new Dictionary<string, string>());
        }

        private static IEnumerable<IRestAvailableConstraintQuery> AvailableQueries()
        {
            yield return new RestAvailableConstraintQuery(string.Format("availableconstraint/{0}/ALL", Dataflows.TimeSeries), new Dictionary<string, string>());
            yield return new RestAvailabilityQuery(string.Format("availability/dataflow/*/{0}/*", Dataflows.TimeSeries), new Dictionary<string, string>());
        }

        private static IEnumerable<IRestStructureQuery> StructureQueries()
        {
            yield return new RESTStructureQueryCore("dataflow/ALL");
            yield return new RESTStructureQueryCoreV2("dataflow/*/*/~");
        }
    }
}
