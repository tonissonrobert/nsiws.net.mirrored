﻿// -----------------------------------------------------------------------
// <copyright file="MaintainableRefExtension.cs" company="EUROSTAT">
//   Date Created : 2015-12-22
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace Estat.Sri.Ws.Test.Extension
{
    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

    /// <summary>
    /// Extensions for <see cref="MaintainableRefExtension"/>
    /// </summary>
    public static class MaintainableRefExtension
    {
        /// <summary>
        ///     Determines whether the specified maintainable reference matches the <paramref name="maintainable" />.
        /// </summary>
        /// <param name="maintainableReference">The maintainable reference.</param>
        /// <param name="maintainable">The maintainable.</param>
        /// <returns><c>true</c> if the specified maintainable reference is match; otherwise, <c>false</c>.</returns>
        public static bool IsMatch(this IMaintainableRefObject maintainableReference, IMaintainableMutableObject maintainable)
        {
            return maintainable.StructureType.EnumType != SdmxStructureEnumType.Dataflow
                   || (maintainable.Id.Equals(maintainableReference.MaintainableId) && (!maintainableReference.HasAgencyId() || maintainable.AgencyId.Equals(maintainableReference.AgencyId))
                       && (!maintainableReference.HasVersion() || maintainable.Version.Equals(maintainableReference.Version)));
        }
    }
}