﻿// -----------------------------------------------------------------------
// <copyright file="RetrieverFactoryTest1.cs" company="EUROSTAT">
//   Date Created : 2015-12-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace ControllerTests.Factory
{
    using System.Collections.Generic;
    using System.Security.Principal;

    using Estat.Sdmxsource.Extension.Factory;
    using Estat.Sdmxsource.Extension.Manager;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Data;
    using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval.Mutable;
    using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.DataParser.Manager;

    internal class RetrieverFactoryTest1 : IRetrieverFactory
    {
        private readonly string _id;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public RetrieverFactoryTest1(string id)
        {
            this._id = id;
        }

        public ISdmxDataRetrievalWithWriter GetDataRetrieval(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public ISdmxDataRetrievalWithCrossWriter GetDataRetrievalWithCross(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public IAdvancedSdmxDataRetrievalWithWriter GetAdvancedDataRetrieval(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public IAdvancedMutableStructureSearchManager GetAdvancedMutableStructureSearchManager(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public IAdvancedMutableStructureSearchManager GetAuthAdvancedMutableStructureSearch(IList<IMaintainableRefObject> authorizedDataflows)
        {
            throw new System.NotImplementedException();
        }

        public IMutableStructureSearchManager GetAuthMutableStructureSearch(SdmxSchema sdmxSchema, IList<IMaintainableRefObject> authorizedDataflows)
        {
            throw new System.NotImplementedException();
        }

        public IMutableStructureSearchManager GetMutableStructureSearch(SdmxSchema sdmxSchema, IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public ISdmxObjectRetrievalManager GetSdmxObjectRetrieval(IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public int GetNumberOfObservations(IDataQuery dataQuery)
        {
            throw new System.NotImplementedException();
        }

        public IAvailableDataManager GetAvailableDataManager(SdmxSchema sdmxVersion, IPrincipal principal)
        {
            throw new System.NotImplementedException();
        }

        public string Id
        {
            get
            {
                return this._id;
            }
        }
    }
}