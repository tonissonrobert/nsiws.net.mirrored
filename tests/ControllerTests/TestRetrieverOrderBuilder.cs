﻿// -----------------------------------------------------------------------
// <copyright file="TestRetrieverOrderBuilder.cs" company="EUROSTAT">
//   Date Created : 2015-12-23
//   Copyright (c) 2009, 2015 by the European Commission, represented by Eurostat.   All rights reserved.
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// https://joinup.ec.europa.eu/software/page/eupl 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
namespace ControllerTests
{
    using ControllerTests.Factory;

    using Estat.Sdmxsource.Extension.Builder;
    using Estat.Sdmxsource.Extension.Factory;

    using NUnit.Framework;

    /// <summary>
    ///     Test unit for <see cref="RetrieverOrderBuilder" />
    /// </summary>
    [TestFixture]
    public class TestRetrieverOrderBuilder
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuild()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new[] { "Test2", "Test1" });
            var retrieverFactories = retrieverOrderBuilder.Build(new IRetrieverFactory[] { new RetrieverFactoryTest1("Test2"), new RetrieverFactoryTest1("Test1") });
            Assert.AreEqual(2, retrieverFactories.Count);
            Assert.AreEqual("Test2", retrieverFactories[0].Id);
            Assert.AreEqual("Test1", retrieverFactories[1].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuild2()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new[] { "Test2", "Test1" });
            var retrieverFactories = retrieverOrderBuilder.Build(new IRetrieverFactory[] { new RetrieverFactoryTest1("Test1"), new RetrieverFactoryTest1("Test2") });
            Assert.AreEqual(2, retrieverFactories.Count);
            Assert.AreEqual("Test2", retrieverFactories[0].Id);
            Assert.AreEqual("Test1", retrieverFactories[1].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildDefaultIsLast()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new[] { "Test2", "MappingStoreRetrieversFactory", "Test1" });
            var retrieverFactories =
                retrieverOrderBuilder.Build(
                    new IRetrieverFactory[] { new RetrieverFactoryTest1("Test2"), new RetrieverFactoryTest1("Test1"), new RetrieverFactoryTest1("MappingStoreRetrieversFactory") });
            Assert.AreEqual(3, retrieverFactories.Count);
            Assert.AreEqual("MappingStoreRetrieversFactory", retrieverFactories[1].Id);
            Assert.AreEqual("Test1", retrieverFactories[2].Id);
            Assert.AreEqual("Test2", retrieverFactories[0].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildDefaultNotConfigured()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new[] { "Test2", "Test1" });
            var retrieverFactories =
                retrieverOrderBuilder.Build(
                    new IRetrieverFactory[] { new RetrieverFactoryTest1("Test2"), new RetrieverFactoryTest1("Test1"), new RetrieverFactoryTest1("MappingStoreRetrieversFactory") });
            Assert.AreEqual(3, retrieverFactories.Count);
            Assert.AreEqual("MappingStoreRetrieversFactory", retrieverFactories[2].Id);
            Assert.AreEqual("Test1", retrieverFactories[1].Id);
            Assert.AreEqual("Test2", retrieverFactories[0].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildEmpty()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new string[0]);
            var retrieverFactories = retrieverOrderBuilder.Build(new IRetrieverFactory[] { new RetrieverFactoryTest1("Test2"), new RetrieverFactoryTest1("Test1") });
            Assert.AreEqual(2, retrieverFactories.Count);
            Assert.AreEqual("Test1", retrieverFactories[0].Id);
            Assert.AreEqual("Test2", retrieverFactories[1].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildEmptyWithDefault()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new string[0]);
            var retrieverFactories =
                retrieverOrderBuilder.Build(
                    new IRetrieverFactory[] { new RetrieverFactoryTest1("Test2"), new RetrieverFactoryTest1("Test1"), new RetrieverFactoryTest1("MappingStoreRetrieversFactory") });
            Assert.AreEqual(3, retrieverFactories.Count);
            Assert.AreEqual("MappingStoreRetrieversFactory", retrieverFactories[0].Id);
            Assert.AreEqual("Test1", retrieverFactories[1].Id);
            Assert.AreEqual("Test2", retrieverFactories[2].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildFromConfigAllConfigured()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder();
            var retrieverFactories =
                retrieverOrderBuilder.Build(
                    new IRetrieverFactory[] { new RetrieverFactoryTest1("A"), new RetrieverFactoryTest1("B"), new RetrieverFactoryTest1("C"), new RetrieverFactoryTest1("Test3") });
            Assert.AreEqual(4, retrieverFactories.Count);
            Assert.AreEqual("Test3", retrieverFactories[0].Id);
            Assert.AreEqual("B", retrieverFactories[1].Id);
            Assert.AreEqual("A", retrieverFactories[2].Id);
            Assert.AreEqual("C", retrieverFactories[3].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildFromConfigOneNotConfigured()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder();
            var retrieverFactories = retrieverOrderBuilder.Build(new IRetrieverFactory[] { new RetrieverFactoryTest1("A"), new RetrieverFactoryTest1("B"), new RetrieverFactoryTest1("C") });
            Assert.AreEqual(3, retrieverFactories.Count);
            Assert.AreEqual("B", retrieverFactories[0].Id);
            Assert.AreEqual("A", retrieverFactories[1].Id);
            Assert.AreEqual("C", retrieverFactories[2].Id);
        }

        /// <summary>
        ///     Test unit for <see cref="RetrieverOrderBuilder.Build" />
        /// </summary>
        [Test]
        public void TestBuildOneMissing()
        {
            RetrieverOrderBuilder retrieverOrderBuilder = new RetrieverOrderBuilder(new[] { "Test2" });
            var retrieverFactories = retrieverOrderBuilder.Build(new IRetrieverFactory[] { new RetrieverFactoryTest1("Test1"), new RetrieverFactoryTest1("Test2") });
            Assert.AreEqual(2, retrieverFactories.Count);
            Assert.AreEqual("Test2", retrieverFactories[0].Id);
            Assert.AreEqual("Test1", retrieverFactories[1].Id);
        }

        #endregion
    }
}