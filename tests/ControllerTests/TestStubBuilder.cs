﻿// -----------------------------------------------------------------------
// <copyright file="TestStubBuilder.cs" company="EUROSTAT">
//   Date Created : 2017-03-17
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

namespace ControllerTests
{
    using Estat.Sri.Ws.Controllers.Builder;

    using NUnit.Framework;

    using Org.Sdmxsource.Sdmx.Api.Constants;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
    using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
    using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

    public class TestStubBuilder
    {
        private readonly StubSdmxObjectBuilder _builder;

        public TestStubBuilder()
        {
            _builder = new StubSdmxObjectBuilder();
        }

        [TestCase("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ESTAT:STS(2.0)")]
        [TestCase("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=IMF:BOP(1.0)")]
        [TestCase("urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=ESTAT:ESTAT_DATAFLOWS_SCHEME(1.1)")]
        [TestCase("urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=TFFS:CRED_EXT_DEBT(1.0)")]
        [TestCase("urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=TFFS.ABC:EXTERNAL_DEBT(1.0)")]
        [TestCase("urn:sdmx:org.sdmx.infomodel.codelist.Codelist=IT1:CL_ADJUSTMENT(1.0)")]
        [TestCase("urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=IT1:CROSS_DOMAIN(1.6)")]
        public void TestBuild(string urn)
        {
            IStructureReference reference = new StructureReferenceImpl(urn);
            var maintainableObject = _builder.Build(reference);
            Assert.That(maintainableObject, Is.Not.Null);
            Assert.That(maintainableObject.Id, Is.EqualTo(reference.MaintainableId));
            Assert.That(maintainableObject.AgencyId, Is.EqualTo(reference.AgencyId));
            Assert.That(maintainableObject.Version, Is.EqualTo(reference.Version));
            Assert.That(maintainableObject.StructureType, Is.EqualTo(reference.MaintainableStructureEnumType));
        }
    }
}