# Synopsis

A SDMX compliant REST web service, which allows retrieval data and structural metadata and submission of structural metadata
By default it uses a Mapping Assistant Database, which is a store/database that contains mapping configuration between local data from a dissemination database to SDMX

## Requirements

For running under IIS:

1. NET 6.0
1. ASP.NET Hosting Core 6.0
1. IIS 7 or later

For running under Kestrel:

1. NET 6.0

### Databases

The list of [Supported databases and their versions](doc/SupportedDatabases.md).

## Changelog

The changes are recorded in [CHANGELOG](CHANGELOG.md)

## Installation

Please see the [INSTALL](doc/INSTALL.md) for the details installation instructions and IIS configuration.
For very short instructions, see below:

### Short instructions for deploying to IIS

Copy the contents of the app folder into a folder configured as a IIS application and uses a `No managed code` application pool.
For more information creating a Web application on IIS please visit IIS documentation. For [IIS7](https://technet.microsoft.com/en-us/library/cc772042(v=ws.10).aspx)

For common issues and workarounds related to IIS and NSIWS can be found in [IIS Common issues and workarounds](doc/IIS_WORKAROUNDS.md)

## Configuration

### Common SDMX RI Web Service/Application settings

Please see the [CONFIGURATION](doc/CONFIGURATION.md) for SDMX RI Web Service settings

### Connection strings

Connection Strings may be configured in several locations.
Please see [CONNECTION_STRING](doc/CONNECTION_STRING.md)

### Logging

Log settings can be configured in `config/log4net.config`. See [CONFIGURATION](doc/CONFIGURATION.md) for more details.

### Database Drivers

Oracle, MySQL and SQL Server drivers are included by default.

### Authentication/Authorization

For more information please see [AUTH](doc/AUTH.md)

## Uninstallation procedure

Please see the [UNINSTALL](doc/UNINSTALL.md) for more information

## Mapping Store Database initialization/upgrade

Please use the command line tool under the Tool folder to list, initialize or upgrade a Mapping Store DB
[Estat.Sri.Mapping.Tool.exe](doc/Tool.md)

Note this tool has separate configuration. Its configuration is in
`Estat.Sri.Mapping.Tool.exe.config`

For more information please see [Tool](doc/Tool.md)

## Plugins

All plugin `dll` should be copied in the 'bin\Plugins' folder of the web service.If plugin A depends on plugin B both `dll` should be copied to the 'bin\Plugins'.
Other `dll` dependencies of the plugins should be copied to the 'bin' folder.

For details are included in [PLUGINS](doc/PLUGINS.md)

## Submit Structural metadata

NSI WS supports submitting structural metadata via SOAP, using the SDMX v2.1 Registry Interface and via REST.
To submit Structural metadata, it must be enabled. For more information see [Configuration](doc/CONFIGURATION.md) and [Authentication/Authorization](doc/AUTH.md)

Please also see [Submitting Structural metadata](doc/SUBMIT_STRUCTURE.md) for more information.
