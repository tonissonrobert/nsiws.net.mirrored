#This script will start SQL Server, attach databases and run sql scripts

initdbsql=false
initdbdata=false

#Check if there are data to be attached
echo "******** Sql Server entrypoint"
if [ ! -d /docker-entrypoint/initdb/data -o -z "$(ls -A /docker-entrypoint/initdb/data)" ]; then
   echo "******** No mdf to attach in data directory"
else
   echo "******** Found mdf files in data directory:"
   ls /docker-entrypoint/initdb/data/
   initdbdata=true
fi

#Check if there are sql scripts to run
if [ ! -d  /docker-entrypoint/initdb/sql -o -z "$(ls -A /docker-entrypoint/initdb/sql)" ]; then
   echo "******** No initialization scripts to run in initdb directory"
else
   echo "******** Found initialization scripts in initdb directory:"
   ls /docker-entrypoint/initdb/sql/
   initdbsql=true
fi

#Run server and attach data/run sql scripts if needed
if [ "$initdbdata" = false ] && [ "$initdbsql" = false ]; then
	echo "******** No initialization to be done. Starting sql server"
	/opt/mssql/bin/sqlservr
elif [ "$initdbdata" = true ] && [ "$initdbsql" = false ]; then
	echo "******** Starting sql server to attach data"
    /opt/mssql/bin/sqlservr & /docker-entrypoint/attach-data.sh
	echo "******** Sql Server attach-data completed"
	wait
elif [ "$initdbdata" = false ] && [ "$initdbsql" = true ]; then
	echo "******** Starting sql server to run sql initialization scripts"
    /opt/mssql/bin/sqlservr & /docker-entrypoint/run-initdb.sh
    echo "******** Sql Server initialization scripts completed"
	wait
elif [ "$initdbdata" = true ] && [ "$initdbsql" = true ]; then
	echo "******** Starting sql server to attach data and run initdb initialization scripts"
    /opt/mssql/bin/sqlservr & /docker-entrypoint/attach-data.sh
	echo "******** Sql Server attach-data completed"
	echo "******** Will run sql initialization scripts"
    /docker-entrypoint/run-initdb.sh
	echo "******** Sql Server initialization scripts completed"
    wait
fi
