--CREATE EMPTY DATABASE MASTORE
USE MASTER
GO
CREATE DATABASE MASTORE;
GO

--SET MASTORE AS DEFAULT LOGIN DB
ALTER LOGIN [$(userplaceholder)] WITH DEFAULT_DATABASE = [MASTORE]
GO

--CREATE MASTORE USER
USE MASTORE
CREATE USER [$(userplaceholder)] FOR LOGIN [$(userplaceholder)];
ALTER ROLE [db_datareader] ADD MEMBER [$(userplaceholder)];
ALTER ROLE [db_datawriter] ADD MEMBER [$(userplaceholder)];
ALTER ROLE [db_ddladmin] ADD MEMBER [$(userplaceholder)];
ALTER ROLE [db_owner] ADD MEMBER [$(userplaceholder)];
GO