#wait for the SQL Server to come up
echo "**** will wait to make sure sql server has started before attaching data"
sleep 30s

for filenamefull in /docker-entrypoint/initdb/data/*.mdf; do
	#this for loop will also return the folder name itself which we skip
	 [ -e "$filenamefull" ] || continue

	#get only filename without full path and without extension
	filename=$(basename -- "$filenamefull")
	extension="${filename##*.}"
	filename="${filename%.*}"
	echo "**** will attach mdf: $filename"
	export DBNAME="$filename"
	mv "$filenamefull" /var/opt/mssql/data/
  # log file names my have .LDF or .ldf extension and their names can be anything. The actual name is stored inside the mdf ..
#	mv /docker-entrypoint/initdb/data/"$filename"_log.LDF /var/opt/mssql/data/
	/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -d master -i "attach-data.sql"
	echo "**** attached mdf: $filename"
done
